package wala.dataflow;

import java.util.Arrays;

import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.UnaryOperator;


public class BitVectorCallPropagation extends UnaryOperator<BitVectorVariable> 
{
	private static final boolean DEBUG = false;
	
	private String callerMethodSig;
	private String calleeMethodSig;
	
	private int[] callerActualParams;
	private int[] calleeFormalParams;
	

	private BitVectorCallPropagation() {}
	
	public BitVectorCallPropagation(String callerSig, String calleeSig, int[] actParams, int[] formParams) 
	{
		this.callerMethodSig = callerSig;
		this.calleeMethodSig = calleeSig;
		this.callerActualParams = actParams;
		this.calleeFormalParams = formParams;
	}
	
	@Override
	public String toString() 
	{
		return "CALLPROPAGATION " + Arrays.toString(callerActualParams) + " " + Arrays.toString(calleeFormalParams);
	}
  
	@Override
	public int hashCode() 
	{
		return 9905 + Arrays.hashCode(callerActualParams) + Arrays.hashCode(calleeFormalParams);
	}

	@Override
	public boolean equals(Object o) 
	{
		if (o instanceof BitVectorCallPropagation)
		{
			BitVectorCallPropagation other = (BitVectorCallPropagation) o;
			if ( ! Arrays.equals(this.callerActualParams, other.callerActualParams) ) return false;
			if ( ! Arrays.equals(this.calleeFormalParams, other.calleeFormalParams) ) return false;
			return true;
		}
		
		return false;
	}

	
	@Override
	public byte evaluate(BitVectorVariable lhs, BitVectorVariable rhs) throws IllegalArgumentException 
	{
		if (lhs == null) throw new IllegalArgumentException("null lhs");
		if (rhs == null) throw new IllegalArgumentException("rhs == null");
	
		if (DEBUG)
		{
			System.out.println("[DEBUG BitVectorCallPropagation.evaluate] caller = '" + callerMethodSig + "', callee = '" + calleeMethodSig + "'");
			System.out.println("[DEBUG BitVectorCallPropagation.evaluate] before: lhs.size = " + lhs.getValue().size() + ", rhs.size = " + rhs.getValue().size() + ", actual = " + Arrays.toString(callerActualParams) + ", formal = " + Arrays.toString(calleeFormalParams));
		}
		
		// we must copy 'rhs' (value that flows into the edge)
		BitVectorVariable P = new BitVectorVariable();
		P.copyState(rhs);
		
		// propagate bits for actual parameters to formal parameters
		for (int j = 0; j < callerActualParams.length; j++)
		{
			if ((calleeFormalParams[j] != -1) && (callerActualParams[j] != -1))
			{
				if (rhs.get(callerActualParams[j])) P.set(calleeFormalParams[j]);
				else P.clear(calleeFormalParams[j]);
			}
		}
		
		if (DEBUG)
		{
			System.out.println("[DEBUG BitVectorCallPropagation.evaluate] after: lhs.size = " + P.getValue().size() + ", rhs.size = " + rhs.getValue().size());
		}
		
		if (!lhs.sameValue(P)) 
		{
			lhs.copyState(P);
			return CHANGED;
		} 
		else 
		{
			return NOT_CHANGED;
		}
	}
}
