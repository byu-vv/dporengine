package wala.dataflow;

import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.FixedPointConstants;
import com.ibm.wala.dataflow.graph.AbstractMeetOperator;
import com.ibm.wala.util.intset.IntSet;
import com.ibm.wala.util.intset.IntIterator;


public class BitVectorIntersection extends AbstractMeetOperator<BitVectorVariable> implements FixedPointConstants 
{
	private static final boolean DEBUG = false;

	private final static BitVectorIntersection SINGLETON = new BitVectorIntersection();

	public static BitVectorIntersection instance() 
	{
		return SINGLETON;
	}

	private BitVectorIntersection() 
	{
	}

	@Override
	public String toString() 
	{
		return "INTERSECTION";
	}

  
	@Override
	public int hashCode() 
	{
		return 9904;
	}

	@Override
	public boolean equals(Object o) 
	{
		return (o instanceof BitVectorIntersection);
	}

	
	@Override
	public byte evaluate(BitVectorVariable lhs, BitVectorVariable[] rhs) throws IllegalArgumentException 
	{
		if (lhs == null) throw new IllegalArgumentException("null lhs");
		if (rhs == null) throw new IllegalArgumentException("rhs == null");
	
		if (DEBUG)
		{
			System.out.println("[DEBUG BitVectorIntersection.evaluate] lhs = " + lhs);
			for (int i = 0; i < rhs.length; i++) System.out.println("\t rhs["+(i+1)+"] = " + rhs[i]);
		}
		
		BitVectorVariable I = new BitVectorVariable();
		I.copyState(lhs);
		
		IntSet lhsSet = lhs.getValue();		
		if (lhsSet != null)
		{
			IntIterator lhsIt = lhsSet.intIterator();
			while (lhsIt.hasNext())
			{
				int vn = lhsIt.next();
				
				boolean someUnset = false;
			
				for (int i = 0; i < rhs.length; i++) 
				{
					if ( ! rhs[i].get(vn) ) someUnset = true;
				}
				
				if (someUnset) I.clear(vn);
			}
		}
		
		if (DEBUG)
		{
			System.out.println("[DEBUG BitVectorIntersection.evaluate] I = " + I);
		}
		
		if (!lhs.sameValue(I)) 
		{
			lhs.copyState(I);
			return CHANGED;
		} 
		else 
		{
			return NOT_CHANGED;
		}
	}
}