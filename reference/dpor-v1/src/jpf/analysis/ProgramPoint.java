package jpf.analysis;


public class ProgramPoint
{
	private String methodSig;
	private int insnPos;
	
	public static final ProgramPoint INVALID = new ProgramPoint("<none>", -1);
	
	
	public ProgramPoint(String methodSig, int insnPos)
	{
		this.methodSig = methodSig;
		this.insnPos = insnPos;
	}
	
	public String getMethodSig()
	{
		return this.methodSig;
	}
	
	public int getInsnPos()
	{
		return this.insnPos;
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof ProgramPoint) ) return false;
		
		ProgramPoint pp = (ProgramPoint) obj;
		
		if ( ! this.methodSig.equals(pp.methodSig) ) return false;
		if (this.insnPos != pp.insnPos) return false;
		
		return true;
	}
	
	public int hashCode()
	{
		int hash = 0;
		
		hash = hash * 31 + this.methodSig.hashCode();
		hash = hash * 31 + this.insnPos;
		
		return hash;
	}
	
	public String toString()
	{
		StringBuffer strbuf = new StringBuffer();

		strbuf.append(methodSig);
		strbuf.append(":");
		strbuf.append(insnPos);
		
		return strbuf.toString();
	}
	
}
