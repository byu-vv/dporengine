package jpf.analysis;


public class FieldName
{
	// class name or allocation site
	public String objectName;
	
	public String fieldName;
	
	public String methodSig;
	public int localVarNo;
	
	public FieldName(String objName, String fldName)
	{
		this.objectName = objName;
		
		this.fieldName = fldName;
		
		this.methodSig = "";
		this.localVarNo = 0;
	}
	
	public FieldName(String objName, String fldName, String mthSig, int locVarNo)
	{
		this.objectName = objName;
		
		this.fieldName = fldName;
		
		this.methodSig = mthSig;
		this.localVarNo = locVarNo;
	}
	
	public boolean equalsObjField(FieldName fn)
	{
		if (fn == null) return false;
		
		if ( ! this.objectName.equals(fn.objectName) ) return false;
		if ( ! this.fieldName.equals(fn.fieldName) ) return false;
		
		return true;
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof FieldName) ) return false;
		
		FieldName fn = (FieldName) obj;
		
		if ( ! this.objectName.equals(fn.objectName) ) return false;
		if ( ! this.fieldName.equals(fn.fieldName) ) return false;
		if ( ! this.methodSig.equals(fn.methodSig) ) return false;
		if (this.localVarNo != fn.localVarNo) return false;
		
		return true;
	}
	
	public int hashCode()
	{
		int hash = 0;
		
		hash = hash * 31 + 1 + this.objectName.hashCode();
		hash = hash * 31 + 1 + this.fieldName.hashCode();
		hash = hash * 31 + 1 + this.methodSig.hashCode();
		hash = hash * 31 + this.localVarNo;
		
		return hash;
	}
	
	public String toString()
	{
		StringBuffer strbuf = new StringBuffer();

		strbuf.append(methodSig);
		strbuf.append(":");
		strbuf.append(localVarNo);
		strbuf.append(":");
		strbuf.append(objectName);
		strbuf.append(".");
		strbuf.append(fieldName);

		return strbuf.toString();
	}	
}
