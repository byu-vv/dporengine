package jpf.analysis;

import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;

import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.impl.Everywhere;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ipa.cfg.ExplodedInterproceduralCFG;
import com.ibm.wala.ipa.cfg.BasicBlockInContext;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.IField;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.analysis.typeInference.TypeInference;


public class ImmutableFieldsDetector
{
	private static final boolean INFO = false;
	private static final boolean DEBUG = false;

	private static Set<FieldName> mutableFields;
		
	
	/**
	 * This method performs stage 3 of the immutable fields analysis.
	 */
	public static void findImmutableFields(CallGraph graph, AnalysisScope scope, IClassHierarchy cha) throws Exception
	{
		AnalysisCache cache = new AnalysisCache();
		
		mutableFields = new HashSet<FieldName>();
		
		// analyze each method in each class
		for (Iterator<IClass> clsIt = cha.iterator(); clsIt.hasNext(); )
		{
			IClass cls = clsIt.next();
			
			String className = WALAUtils.getClassName(cls);
			
			// ignore library classes (JPF does not make thread choices at field accesses in their methods)
			if (className.startsWith("java.") || className.startsWith("javax.")) continue;
			
			Collection<IMethod> methods = cls.getDeclaredMethods();
			
			for (IMethod mth : methods)
			{
				// we skip native methods and abstract methods
				if (mth.isNative() || mth.isAbstract()) continue;
				
				String methodSig = WALAUtils.getCorrectMethodSignature(mth.getReference().getSignature());
		
				IR mthIR = cache.getIRFactory().makeIR(mth, Everywhere.EVERYWHERE, SSAOptions.defaultOptions());

				TypeInference typesInfer = TypeInference.make(mthIR, false);
				
				// for each field write look into results of escape analysis (stage 2) to see whether the target object already escaped to the heap (at the previous code location)

				SSAInstruction[] instructions = mthIR.getInstructions();
				for (int i = 0; i < instructions.length; i++) 
				{
					SSAInstruction instr = instructions[i];
					
					if (instr == null) continue;
					
					if (instr instanceof SSAPutInstruction)
					{
						SSAPutInstruction putInsn = (SSAPutInstruction) instr;
		
						String fieldName = putInsn.getDeclaredField().getName().toUnicodeString();
						
						String fieldClsName = WALAUtils.getFieldsClassName(putInsn.getDeclaredField(), cha);

						int bcPos = WALAUtils.getInsnBytecodeIndex(mth, i-1);
			
						ProgramPoint pp = new ProgramPoint(methodSig, bcPos);
						
						int objVarNum = putInsn.getRef(); 
						
						// not used anymore
						/*
						String objTypeName = null;
						if (objVarNum != -1)
						{
							TypeReference objTypeRef = typesInfer.getType(objVarNum).getTypeReference();
							if (objTypeRef != null) objTypeName = WALAUtils.getTypeName(objTypeRef);
						}
						*/
						
						if (DEBUG)
						{
							System.out.println("[DEBUG ImmutableFieldsDetector.findImmutableFields] field write '" + fieldClsName + "." + fieldName + "': program point = '" + pp + "'");
						}								
						
						// get local variables that may be already escaped at the given code location
						Set<LocalVarID> escapedVars = EscapingVariablesManager.getEscapedVariablesForProgramPoint(pp);
						
						// check for the target variable of the field write access whether the variable itself escaped already
						// we do not have to consider aliasing here
						
						LocalVarID escVarID = null;
						
						if (escapedVars != null)
						{
							for (LocalVarID lvid : escapedVars)
							{
								// we do not have to check the method signature here
								if (objVarNum == lvid.localVarNo) escVarID = lvid;
								
								// not used anymore
								/*
								if ((lvid.typeName != null) && (objTypeName != null) && methodSig.equals(lvid.methodSig))
								{
									if (objTypeName.equals(lvid.typeName)) escVarID = lvid;
									
									// two variables are may-aliased if their types (classes) are on one path in the class hierarchy (if one type is a subtype of the other)
									
									if (WALAUtils.isSubtypeClass(cha, objTypeName, lvid.typeName)) escVarID = lvid;
								}
								*/
								
								if (escVarID != null) break;
							}
						}
						
						if (DEBUG)
						{
							if (escVarID != null) System.out.println("\t escaped because of this local variable: " + escVarID.methodSig + ":" + escVarID.localVarNo + " (" + escVarID.typeName + ")"); 
						}
						
						// important cases:
							// not escaped and field not present in the set -> immutable field
							// not escaped and present in the set -> already known mutable field
							// escaped -> possibly new mutable field
					
						if (escVarID != null) 
						{						
							FieldName tgtField = new FieldName(fieldClsName, fieldName);
							
							mutableFields.add(tgtField);
						}
					}					
				}
			}
		}
		
		if (INFO)
		{
			System.out.println("");
			System.out.println("IMMUTABLE FIELDS");
			System.out.println("================");
		
			// check all fields in each class
			for (Iterator<IClass> clsIt = cha.iterator(); clsIt.hasNext(); )
			{
				IClass cls = clsIt.next();
				
				String className = WALAUtils.getClassName(cls);
				
				if (className.startsWith("java.")) continue;
			
				Collection<IField> fields = cls.getAllFields();
			
				for (IField fld : fields)
				{
					String fieldName = fld.getName().toUnicodeString();
				
					FieldName fn = new FieldName(className, fieldName);
					
					if ( ! mutableFields.contains(fn) ) System.out.println(fn.toString());
				}
			}
		}
		
		if (DEBUG)
		{
			System.out.println("");
			System.out.println("MUTABLE FIELDS");
			System.out.println("==============");
		
			for (FieldName fn : mutableFields) 
			{
				// filter core libraries
				if (fn.objectName.startsWith("java.")) continue;
				if (fn.objectName.startsWith("com.ibm.wala.")) continue;
				
				System.out.println(fn.toString());
			}
		}
	}
	
	public static boolean isImmutableField(String className, String fieldName)
	{
		FieldName fn = new FieldName(className, fieldName);
		
		return ( ! mutableFields.contains(fn) );
	}
}
