package jpf.analysis;

import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;

import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.impl.Everywhere;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ipa.cfg.ExplodedInterproceduralCFG;
import com.ibm.wala.ipa.cfg.BasicBlockInContext;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.IField;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAMonitorInstruction;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.analysis.typeInference.TypeInference;


public class SharedMonitorsDetector
{
	private static final boolean INFO = false;
	private static final boolean DEBUG = false;

	// map from program points to boolean values where "true" means that the target object of the monitor instruction at the program point is never visible from multiple threads
	// each program point stored as a key represents one monitor enter bytecode instruction
	// no value defined for some program point corresponding to monitor enter means that we do not know (e.g., for library methods)
	private static Map<ProgramPoint,Boolean> progpoint2monlocal;

	
	public static void identifyLocalMonitors(CallGraph graph, AnalysisScope scope, IClassHierarchy cha) throws Exception
	{
		AnalysisCache cache = new AnalysisCache();
		
		progpoint2monlocal = new HashMap<ProgramPoint,Boolean>();
		
		// analyze each method in each class
		for (Iterator<IClass> clsIt = cha.iterator(); clsIt.hasNext(); )
		{
			IClass cls = clsIt.next();
			
			String className = WALAUtils.getClassName(cls);
			
			// ignore library classes
			if (className.startsWith("java.") || className.startsWith("javax.")) continue;
			
			Collection<IMethod> methods = cls.getDeclaredMethods();
			
			for (IMethod mth : methods)
			{
				// we skip native methods and abstract methods
				if (mth.isNative() || mth.isAbstract()) continue;
				
				String methodSig = WALAUtils.getCorrectMethodSignature(mth.getReference().getSignature());
		
				IR mthIR = cache.getIRFactory().makeIR(mth, Everywhere.EVERYWHERE, SSAOptions.defaultOptions());
				
				TypeInference typesInfer = TypeInference.make(mthIR, false);

				// for each monitor enter look into results of escape analysis to see whether the target object already escaped to the heap

				SSAInstruction[] instructions = mthIR.getInstructions();
				for (int i = 0; i < instructions.length; i++) 
				{
					SSAInstruction instr = instructions[i];
					
					if (instr == null) continue;
					
					if (instr instanceof SSAMonitorInstruction)
					{
						SSAMonitorInstruction monInsn = (SSAMonitorInstruction) instr;
		
						if (monInsn.isMonitorEnter())
						{						
							int bcPos = WALAUtils.getInsnBytecodeIndex(mth, i);
			
							ProgramPoint pp = new ProgramPoint(methodSig, bcPos);
							
							String objTypeName = null;
							if (monInsn.getRef() != -1)
							{
								TypeReference objTypeRef = typesInfer.getType(monInsn.getRef()).getTypeReference();
								if (objTypeRef != null) objTypeName = WALAUtils.getTypeName(objTypeRef);
							}
						
							// get local variables that may be already escaped at the given code location
							Set<LocalVarID> escapedVars = EscapingVariablesManager.getEscapedVariablesForProgramPoint(pp);

							boolean localTargetObj = true;
							
							if (DEBUG) System.out.println("[DEBUG SharedMonitorsDetector.identifyLocalMonitors] program point = '" + pp + "', target object = " + monInsn.getRef());
							
							// check whether the target object of monitor enter instruction already escaped
							
							if (escapedVars == null)
							{
								// we do not know in this case
								localTargetObj = false;
							}
							else
							{
								for (LocalVarID lvid : escapedVars)
								{
									if (DEBUG) System.out.println("\t\t escaped variable: " + lvid);
									
									if (methodSig.equals(lvid.methodSig))
									{
										if (monInsn.getRef() == lvid.localVarNo) localTargetObj = false;
										if (objTypeName.equals(lvid.typeName)) localTargetObj = false;
									}
								}
							}
							
							if (DEBUG) System.out.println("\t local target = " + localTargetObj);
							
							progpoint2monlocal.put(pp, localTargetObj);
						}
					}				
				}
			}
		}
		
		if (INFO)
		{
			System.out.println("");
			System.out.println("MONITOR ENTER ON LOCAL TARGETS");
			System.out.println("==============================");
		
			for (Map.Entry<ProgramPoint,Boolean> me : progpoint2monlocal.entrySet())
			{
				ProgramPoint pp = me.getKey();
				Boolean localTarget = me.getValue();
			
				if (localTarget.booleanValue()) System.out.println(pp.toString());
			}
		}
	}
	
	public static boolean isMonitorEnterOnLocalTarget(ProgramPoint pp)
	{
		// no information available
		if (progpoint2monlocal == null) return false;
		
		Boolean localTarget = progpoint2monlocal.get(pp);
		
		if (localTarget == null) return false;
		
		return localTarget.booleanValue();
	}
}
