package jpf.analysis;

import java.util.Iterator;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Date;

import java.io.File;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;

import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CallGraphBuilder;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.callgraph.impl.DefaultContextSelector;
import com.ibm.wala.ipa.callgraph.propagation.HeapModel;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.callgraph.propagation.PointerKey;
import com.ibm.wala.ipa.callgraph.propagation.InstanceFieldKey;
import com.ibm.wala.ipa.callgraph.propagation.StaticFieldKey;
import com.ibm.wala.ipa.callgraph.propagation.LocalPointerKey;
import com.ibm.wala.ipa.callgraph.propagation.NormalAllocationInNode;
import com.ibm.wala.ipa.callgraph.propagation.ConstantKey;
import com.ibm.wala.ipa.callgraph.propagation.StringConstantCharArray;
import com.ibm.wala.ipa.callgraph.propagation.SSAPropagationCallGraphBuilder;
import com.ibm.wala.ipa.callgraph.propagation.cfa.OneLevelSiteContextSelector;
import com.ibm.wala.ipa.callgraph.propagation.cfa.nCFAContextSelector;
import com.ibm.wala.ipa.callgraph.propagation.cfa.ContainerContextSelector;
import com.ibm.wala.ipa.callgraph.propagation.cfa.ZeroXInstanceKeys;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAFieldAccessInstruction; 
import com.ibm.wala.classLoader.CallSiteReference;
import com.ibm.wala.classLoader.IField;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.ArrayClass;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.config.AnalysisScopeReader;
import com.ibm.wala.util.intset.OrdinalSet;
import com.ibm.wala.demandpa.util.MemoryAccessMap;
import com.ibm.wala.demandpa.util.PABasedMemoryAccessMap;
import com.ibm.wala.demandpa.alg.DemandRefinementPointsTo;
import com.ibm.wala.demandpa.alg.ContextSensitiveStateMachine;
import com.ibm.wala.demandpa.alg.statemachine.DummyStateMachine;
import com.ibm.wala.demandpa.alg.statemachine.StateMachineFactory;
import com.ibm.wala.demandpa.alg.refinepolicy.TunedRefinementPolicy;
import com.ibm.wala.demandpa.flowgraph.IFlowLabel;


/**
 * This class can execute analyses ahead of time (in constructor) or on-the-fly (in various handler methods).
 */
public class AnalysisExecutor extends ListenerAdapter
{
	private static final boolean DEBUG = false;
	
	
	public AnalysisExecutor(Config cfg, JPF jpf)
	{
		Date startTime = new Date();

		String mainClassName = cfg.getString("target");
		if (mainClassName == null)
		{
			String[] freeArgs = cfg.getFreeArgs();
			if (freeArgs != null) mainClassName = freeArgs[0];	
		}

		// possible values: directory name (path)
		String targetClassPath = cfg.getString("analysis.target.dir", "");

		// possible values: file name (path)
		// exclusion file determines whether the analysis is whole program (including all used libraries) or limited only to application classes
		String walaExclusionFilePath = cfg.getString("analysis.exclusion.file", "");
		
		// possible values: zerocfa
		String callGraphAnalysisType = cfg.getString("analysis.callgraph.type", "zerocfa");
		
		// possible values: sensitive, callee, insensitive
		String fieldAccessAnalysisType = cfg.getString("analysis.fieldaccess.type", "sensitive");
		
		// possible values: none, andersen, demanddrv
		String pointerAnalysisType = cfg.getString("analysis.pointer.type", "none");
		
		// possible values: full, vars
		String pointerResultsUsage = cfg.getString("analysis.pointer.results.usage", "full");
		
		// possible values: none, callone, calltwo, object, demand
		String pointerContextType = cfg.getString("analysis.pointer.context.type", "none");

		// possible values: drop, keep
		String pointerContextUsage = cfg.getString("analysis.pointer.context.usage", "drop");
		
		String escapeAnalysisUsage = cfg.getString("analysis.escape.usage", "false");
		
		String immutableFieldsUsage = cfg.getString("analysis.immutable.usage", "false");
		
		
		String cfgDotFileName = cfg.getString("analysis.controlflowgraph.filename", "");
		
		try
		{
			AnalysisScope scope = createAnalysisScope(targetClassPath, walaExclusionFilePath);
			
			if (DEBUG)
			{
				System.out.println("ANALYSIS SCOPE");
				System.out.println("==============");

				System.out.println(scope.toString());
			}

			IClassHierarchy cha = makeClassHierarchy(scope);
			
			
			// build call graph and compute pointer analysis
			
			Iterable<Entrypoint> entryPoints = Util.makeMainEntrypoints(scope, cha, WALAUtils.getWalaClassName(mainClassName));	
			
			AnalysisOptions options = new AnalysisOptions(scope, entryPoints);
			options.setHandleStaticInit(true);
			
			AnalysisCache cache = new AnalysisCache();			
			
			SSAPropagationCallGraphBuilder builder = null;
			
			if (callGraphAnalysisType.equals("zerocfa")) builder = Util.makeZeroCFABuilder(options, cache, cha, scope);
			
			if (pointerAnalysisType.equals("andersen")) 
			{
				if (pointerContextType.equals("callone")) 
				{
					AllocationSiteManager.useCallerSite = true;
					builder = Util.makeVanillaZeroOneCFABuilder(options, cache, cha, scope, new OneLevelSiteContextSelector(new DefaultContextSelector(options, cha)), null);
				}
				else if (pointerContextType.equals("calltwo")) 
				{
					AllocationSiteManager.useCallString = true;
					builder = Util.makeVanillaZeroOneCFABuilder(options, cache, cha, scope, new nCFAContextSelector(2, new DefaultContextSelector(options, cha)), null);
				}
				else if (pointerContextType.equals("object")) 
				{
					AllocationSiteManager.useContainerObject = true;
					builder = Util.makeVanillaZeroOneContainerCFABuilder(options, cache, cha, scope);
				}
				else
				{
					// context-insensitive pointer analysis
					builder = Util.makeVanillaZeroOneCFABuilder(options, cache, cha, scope);
				}

				AllocationSiteManager.keepContext = pointerContextUsage.equals("keep");
			}
			
			if (pointerAnalysisType.equals("demanddrv")) builder = Util.makeVanillaZeroOneCFABuilder(options, cache, cha, scope);
			
			CallGraph graph = builder.makeCallGraph(options, null);
			
			storeCallGraphNodes(graph);

			if (DEBUG) printCallGraph(graph, 5);
			
			// pointer analysis is not enabled by default
			if ( ! pointerAnalysisType.equals("") )
			{
				FieldAccessManager.usePointerAnalysis = true;

				PointerAnalysis pa = builder.getPointerAnalysis();
				
				if (pointerAnalysisType.equals("andersen"))
				{
					collectAllocationSites(pa, cha);

					if (DEBUG) printPointsToInformation(pa);
				}
				else if (pointerAnalysisType.equals("demanddrv"))
				{
					MemoryAccessMap mam = new PABasedMemoryAccessMap(graph, pa);
					StateMachineFactory<IFlowLabel> smf = null;
					if (pointerContextType.equals("demand")) smf = new ContextSensitiveStateMachine.Factory();
					else smf = new DummyStateMachine.Factory<IFlowLabel>();					
					AllocationSiteManager.demandPointsTo = DemandRefinementPointsTo.makeWithDefaultFlowGraph(graph, builder, mam, cha, options, smf);
					AllocationSiteManager.demandPointsTo.setRefinementPolicyFactory(new TunedRefinementPolicy.Factory(cha));
					FieldAccessManager.useDemandDrivenPA = true;


					// create map from field names to list of pointer keys (local variables)
					// the list will have several elements of the form 'l:C.f' for each C.f

					HeapModel hm = AllocationSiteManager.demandPointsTo.getHeapModel(); 

					Iterator<CGNode> cgnIt = graph.iterator();
					while (cgnIt.hasNext())
					{
						CGNode cgn = cgnIt.next();

						IR methodIR = cgn.getIR();
						if (methodIR == null) continue;
   
						String methodSig = WALAUtils.getCorrectMethodSignature(cgn.getMethod().getSignature());
				
						SSAInstruction[] instructions = methodIR.getInstructions();
						for (int i = 0; i < instructions.length; i++) 
						{
							SSAInstruction instr = instructions[i];
							if (instr instanceof SSAFieldAccessInstruction) 
							{
								SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) instr;
						
								try
								{
									String fieldName = faInsn.getDeclaredField().getName().toUnicodeString();
									String className = WALAUtils.getFieldsClassName(faInsn.getDeclaredField(), cha);
									
									FieldName fn = new FieldName(className, fieldName);
									
									if ( ! faInsn.isStatic() )
									{
										fn.methodSig = methodSig;
										fn.localVarNo = faInsn.getRef(); // local variable pointing to the object instance
									}

									List<PointerKey> pkList = AllocationSiteManager.field2locvar.get(fn);
									if (pkList == null)
									{
										pkList = new ArrayList<PointerKey>();
										AllocationSiteManager.field2locvar.put(fn, pkList);
									}
									
									Iterator<PointerKey> pkIt = hm.iteratePointerKeys();		
									while (pkIt.hasNext())
									{
										PointerKey pk = pkIt.next();
		
										if (pk instanceof LocalPointerKey)
										{	
											CGNode pkMthNode = ((LocalPointerKey) pk).getNode();
											int pkLocalVarNo = ((LocalPointerKey) pk).getValueNumber();

											String pkMthSig = WALAUtils.getCorrectMethodSignature(pkMthNode.getMethod().getSignature());

											if ((fn.localVarNo == pkLocalVarNo) && methodSig.equals(pkMthSig)) pkList.add(pk);
										}
									}
								}
								catch (Exception ex) { ex.printStackTrace(); }						
							}
						}						
					}
				}
				
				if (escapeAnalysisUsage.equals("true"))
				{
					computeThreadEscapeAnalysis(cha, graph, pa);
					
					FieldAccessManager.useEscapeAnalysis = true;
				}
			}
						
			if (immutableFieldsUsage.equals("true")) 
			{
				FieldAccessManager.useImmutableFields = true;
				
				EscapingVariablesManager.generateMethodSummaries(graph, scope, cha);
				EscapingVariablesManager.collectEscapedVariables(graph, scope, cha);
				
				ImmutableFieldsDetector.findImmutableFields(graph, scope, cha);
				
				SharedMonitorsDetector.identifyLocalMonitors(graph, scope, cha);
			}

			// compute field access analysis using call graph and points-to information			
			
			if (pointerResultsUsage.equals("vars")) FieldAccessManager.useVariableNamesOnly = true;
			
			if (fieldAccessAnalysisType.equals("sensitive")) FieldAccessManager.analyzeWholeProgUsingCallStack(graph, scope, cha, cfgDotFileName);
			else if (fieldAccessAnalysisType.equals("callee")) FieldAccessManager.analyzeWholeProgStandalone(graph, scope, cha, cfgDotFileName);
			else if (fieldAccessAnalysisType.equals("insensitive")) FieldAccessManager.analyzeWholeProgInOnePhase(graph, scope, cha, cfgDotFileName);			
			
			if (DEBUG) FieldAccessManager.printFieldAccessSets(false);
		}
		catch (Exception ex)
		{
			System.out.println("[ERROR] " + ex.getMessage());
			ex.printStackTrace();
		}
		
		Date finishTime = new Date();
		
		long usedMemoryInMB = (Runtime.getRuntime().totalMemory() >> 20);
  		
		System.out.println("[ANALYSIS] time = " + printTimeDiff(startTime, finishTime) + " s, memory = " + usedMemoryInMB + " MB");
	}
	

	private static AnalysisScope createAnalysisScope(String targetClassPath, String exclusionFilePath) throws Exception
	{
		AnalysisScope scope = null;
		
		scope = AnalysisScopeReader.readJavaScope("wala-jpf.txt", new File(exclusionFilePath), AnalysisExecutor.class.getClassLoader());

		AnalysisScopeReader.processScopeDefLine(scope, AnalysisExecutor.class.getClassLoader(), "Application,Java,binaryDir,"+targetClassPath);

		return scope;
	}

	private static IClassHierarchy makeClassHierarchy(AnalysisScope scope) throws Exception
	{	
		IClassHierarchy cha = ClassHierarchy.make(scope);
		return cha;
	}
	
	private static void printCallGraph(CallGraph graph, int maxLevel)
	{
		System.out.println("CALL GRAPH");
		System.out.println("==========");

		CGNode entryNode = graph.getFakeRootNode(); 
		printCallGraphNode(graph, entryNode, "method: ", maxLevel, 0);

		System.out.println("");
	}

	private static void printCallGraphNode(CallGraph graph, CGNode node, String prefix, int maxLevel, int curLevel)
	{
		if (curLevel > maxLevel) return;
		
		System.out.println(prefix + WALAUtils.getCorrectMethodSignature(node.getMethod().getSignature()));
		
		Iterator<CallSiteReference> callSitesIt = node.iterateCallSites();
		while (callSitesIt.hasNext())
		{
			CallSiteReference callSite = callSitesIt.next();
			
			Set<CGNode> targetNodes = graph.getPossibleTargets(node, callSite);
			
			for (CGNode tgtNode : targetNodes) printCallGraphNode(graph, tgtNode, "\t"+prefix, maxLevel, curLevel + 1);
		}
	}

	private static void storeCallGraphNodes(CallGraph graph)
	{
		Iterator<CGNode> cgnIt = graph.iterator();
		
		while (cgnIt.hasNext())
		{
			CGNode cgn = cgnIt.next();
			
			if (DEBUG) System.out.println("[DEBUG AnalysisExecutor.storeCallGraphNodes] methodSig = " + WALAUtils.getCorrectMethodSignature(cgn.getMethod().getSignature()));
			
			WALAUtils.storeMethodNode(cgn);
		}
	}

	private static void collectAllocationSites(PointerAnalysis pa, IClassHierarchy cha) throws Exception
	{
		// we cannot iterate directly because concurrent modification exception is thrown in that case
		List<InstanceKey> instKeys = new ArrayList<InstanceKey>();
		instKeys.addAll(pa.getInstanceKeys());
		
		for (InstanceKey ik : instKeys)
		{
			if (ik instanceof NormalAllocationInNode)
			{
				NormalAllocationInNode allocKey = (NormalAllocationInNode) ik;
				
				String allocSite = WALAUtils.getProperAllocSite(allocKey);
				
				Iterator<Object> ofIt = pa.getHeapGraph().getSuccNodes(ik);

				while (ofIt.hasNext())
				{
					Object pk = ofIt.next();
					
					IField field = null;
					
					if (pk instanceof InstanceFieldKey) field = ((InstanceFieldKey) pk).getField();
					if (pk instanceof StaticFieldKey) field = ((StaticFieldKey) pk).getField();
					
					String className = WALAUtils.getFieldsClassName(field, cha);
			
					// other kind of field (pointer key)
					if (className == null) continue;
					
					if (DEBUG) System.out.println("[DEBUG AnalysisExecutor.collectAllocationSites] className = " + className + ", allocSite = " + allocSite);
					
					AllocationSiteManager.addAllocSiteForClass(className, allocSite);
				}
			}
		}
		
		for (PointerKey pk : pa.getPointerKeys())
		{
			if (pk instanceof LocalPointerKey)
			{	
				CGNode mthNode = ((LocalPointerKey) pk).getNode();
				int localVarNo = ((LocalPointerKey) pk).getValueNumber();

				String mthSignature = WALAUtils.getCorrectMethodSignature(mthNode.getMethod().getSignature());
				
				FieldName fn = new FieldName("", "", mthSignature, localVarNo);
				
				if (DEBUG) System.out.println("[DEBUG AnalysisExecutor.collectAllocationSites] mthSignature = " + mthSignature + ", localVarNo = " + localVarNo);
				
				Iterator<Object> asIt = pa.getHeapGraph().getSuccNodes(pk);

				while (asIt.hasNext())
				{
					Object ik = asIt.next();
					
					if (ik instanceof NormalAllocationInNode)
					{
						NormalAllocationInNode allocKey = (NormalAllocationInNode) ik;
							
						String allocSite = WALAUtils.getProperAllocSite(allocKey);
						
						if (DEBUG) System.out.println("[DEBUG AnalysisExecutor.collectAllocationSites] allocSite = " + allocSite);
		
						AllocationSiteManager.addAllocSiteForLocalVar(fn, allocSite);
					}
				}
			}		
		}		
	}
	
	private static void printPointsToInformation(PointerAnalysis pa) throws Exception
	{
		System.out.println("ALLOCATION SITES");
		System.out.println("================");
		
		// for each allocation site, print list of field names
		
		for (InstanceKey ik : pa.getInstanceKeys())
		{
			if (ik instanceof NormalAllocationInNode)
			{
				NormalAllocationInNode allocKey = (NormalAllocationInNode) ik;
				
				String allocSite = WALAUtils.getProperAllocSite(allocKey);
						
				System.out.println(allocSite);
		
				Iterator<Object> ofIt = pa.getHeapGraph().getSuccNodes(ik);

				while (ofIt.hasNext())
				{
					Object pk = ofIt.next();
					
					IField field = null;
					
					if (pk instanceof InstanceFieldKey) field = ((InstanceFieldKey) pk).getField();
					if (pk instanceof StaticFieldKey) field = ((StaticFieldKey) pk).getField();
					
					String fieldName = WALAUtils.getFieldName(field);
			
					// other kind of field (pointer key)
					if (fieldName == null) continue;
					
					System.out.println("\t" + fieldName);
				}
			}
		}

		System.out.println("");
		
		
		System.out.println("POINTS-TO INFORMATION");
		System.out.println("=====================");
		
		// for each field, print objects that it can point to
		
		Iterable<PointerKey> pointers = pa.getPointerKeys();
		
		for (PointerKey pk : pointers)
		{
			IField field = null;
			
			if (pk instanceof InstanceFieldKey) field = ((InstanceFieldKey) pk).getField();
			if (pk instanceof StaticFieldKey) field = ((StaticFieldKey) pk).getField();
			
			String fieldName = WALAUtils.getFieldName(field);

			// other kind of field (pointer key)
			if (fieldName == null) continue;
			
			OrdinalSet<InstanceKey> targetObjects = pa.getPointsToSet(pk);
			

			System.out.println(fieldName);
			
			for (InstanceKey ik : targetObjects)
			{
				if (ik instanceof NormalAllocationInNode)
				{
					NormalAllocationInNode allocKey = (NormalAllocationInNode) ik;
					
					String allocSite = WALAUtils.getProperAllocSite(allocKey);
						
					System.out.println("\t" + allocSite);
				}
				else if ((ik instanceof ConstantKey) || (ik instanceof StringConstantCharArray))
				{
					System.out.println("\t constant object or string");
				}					
			}
		}
		
		System.out.println("");
	}
	
	/**
	 * This method is based on the SimpleThreadEscapeAnalysis example from WALA.
	 */
	private static void computeThreadEscapeAnalysis(IClassHierarchy cha, CallGraph cg, PointerAnalysis pa)
	{
		HeapModel heapModel = pa.getHeapModel();
		
		Set<PointerKey> roots = new HashSet<PointerKey>();

		// collect all static fields
		for (IClass cls : cha) 
		{
			Collection<IField> staticFields = cls.getDeclaredStaticFields();
			for (IField sf : staticFields) 
			{
				if (sf.getFieldTypeReference().isReferenceType()) roots.add(heapModel.getPointerKeyForStaticField(sf));
			}
		}
	
		// collect "this" objects for all thread constructors 
		Collection<IClass> threadClasses = cha.computeSubClasses(TypeReference.JavaLangThread);
		for (IClass thCls : threadClasses) 
		{
			for (IMethod mth : thCls.getDeclaredMethods()) 
			{
				if (mth.isInit()) 
				{
					Set<CGNode> cgNodes = cg.getNodes(mth.getReference());
					
					for (CGNode node : cgNodes)
					{
						roots.add(heapModel.getPointerKeyForLocal(node, 1));
					}
				}
			}
		}
	
		
		// identify all objects transitively reachable from escaping roots		
		
		Set<InstanceKey> escapingObjects = new HashSet<InstanceKey>();
	
		// pass 1: get abstract objects (instance keys) for escaping roots
		for (PointerKey rtpk : roots) 
		{
			OrdinalSet<InstanceKey> targetObjects = pa.getPointsToSet(rtpk);
			for (InstanceKey obj : targetObjects) escapingObjects.add(obj);
		}
	
		// passes 2+: get fields of escaping objects (keys), and add pointed-to keys
		Set<InstanceKey> newKeys = new HashSet<InstanceKey>();
		do 
		{
			newKeys.clear();
			
			for (InstanceKey eoKey : escapingObjects) 
			{
				IClass eoType = eoKey.getConcreteType();
				
				if ( ! eoType.isReferenceType() ) continue;
				
				if (eoType.isArrayClass()) 
				{
					if ( ((ArrayClass) eoType).getElementClass() != null )  
					{
						PointerKey elemKey = heapModel.getPointerKeyForArrayContents(eoKey);
						OrdinalSet<InstanceKey> elemObjects = pa.getPointsToSet(elemKey);
						
						for (InstanceKey elObj : elemObjects) 
						{
							if ( ! escapingObjects.contains(elObj) ) newKeys.add(elObj);
						}
					}
				}
				else 
				{
					Collection<IField> objFields = eoType.getAllInstanceFields();
					
					for (IField of : objFields) 
					{
						if (of.getFieldTypeReference().isReferenceType()) 
						{
							PointerKey fk = heapModel.getPointerKeyForInstanceField(eoKey, of);
							OrdinalSet<InstanceKey> fieldObjects = pa.getPointsToSet(fk);
							
							for (InstanceKey fieldObj : fieldObjects) 
							{
								if ( ! escapingObjects.contains(fieldObj) ) newKeys.add(fieldObj);
							}
						}
					}
				}
			}
			escapingObjects.addAll(newKeys);
		} 
		while (!newKeys.isEmpty());
	
		
		// process results
		for (InstanceKey eoKey : escapingObjects)
		{
			if (eoKey instanceof NormalAllocationInNode)
			{
				NormalAllocationInNode allocKey = (NormalAllocationInNode) eoKey;
					
				String allocSite = WALAUtils.getProperAllocSite(allocKey);
									
				AllocationSiteManager.addThreadSharedObject(allocSite);
			}
		}
	}
	
    private static String printTimeDiff(Date start, Date finish)
    {
    	long startMS = start.getTime();
    	long finishMS = finish.getTime();
    	
    	long diffMS = finishMS - startMS;
    	
    	long diffSeconds = (diffMS / 1000);
    	
    	return String.valueOf(diffSeconds);
    }

}
