package jpf.analysis;


public class LocalVarID
{
	public String methodSig;
	
	public int localVarNo;
	
	public String typeName;
	
	
	public LocalVarID(String mthSig, int varNo)
	{
		this.methodSig = mthSig;
		this.localVarNo = varNo;
		this.typeName = null;
	}
	
	public LocalVarID(String mthSig, int varNo, String tn)
	{
		this.methodSig = mthSig;
		this.localVarNo = varNo;
		this.typeName = tn;
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof LocalVarID) ) return false;
		
		LocalVarID other = (LocalVarID) obj;
		
		if ( ! this.methodSig.equals(other.methodSig) ) return false;
		if (this.localVarNo != other.localVarNo) return false;
		
		// we do not consider type here because it may not be set
		
		return true;
	}
	
	public int hashCode()
	{
		int hash = 0;
		
		hash = hash * 31 + 1 + this.methodSig.hashCode();
		hash = hash * 31 + this.localVarNo;
		
		// we do not consider type here because it may not be set
		
		return hash;
	}
	
	public String toString()
	{
		StringBuffer strbuf = new StringBuffer();

		strbuf.append(methodSig);
		strbuf.append(":");
		strbuf.append(localVarNo);
		
		if (typeName != null)
		{
			strbuf.append(":");
			strbuf.append(typeName);
		}

		return strbuf.toString();
	}	
}
