package jpf.analysis;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.SystemState;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.DefaultSchedulerFactory;
import gov.nasa.jpf.vm.choice.ThreadChoiceFromSet;


public class FieldAnalysisSchedulerFactory extends DefaultSchedulerFactory 
{
	public FieldAnalysisSchedulerFactory(Config config, VM vm, SystemState ss)
	{
		super(config, vm, ss);
	}

	@Override
	public ChoiceGenerator<ThreadInfo> createMonitorEnterCG(ElementInfo ei, ThreadInfo ti) 
	{
		if (ti.isBlocked()) 
		{
			if (ss.isAtomic()) ss.setBlockedInAtomicSection();
			
			return new ThreadChoiceFromSet(MONITOR_ENTER, getRunnables(ti), true);
		}
		else 
		{
			if (ss.isAtomic()) return null;
			
			// class object is always visible (it means static field or a static method)
			if ( ! ei.getClassInfo().getName().equals("java.lang.Class") )
			{
				ProgramPoint pp = JPFUtils.getProgramPointForThreadPC(ti);
	
				// target object is always local
				if (SharedMonitorsDetector.isMonitorEnterOnLocalTarget(pp)) return null;
			}
				
			return getSyncCG(MONITOR_ENTER, ei, ti);
		}
	}
}
