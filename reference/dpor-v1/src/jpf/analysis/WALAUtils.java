package jpf.analysis;

import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IField;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.util.strings.Atom;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.types.TypeName;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.types.FieldReference;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.Context;
import com.ibm.wala.ipa.callgraph.ContextKey;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.NormalAllocationInNode;
import com.ibm.wala.ipa.callgraph.propagation.cfa.CallerSiteContext;
import com.ibm.wala.ipa.callgraph.propagation.cfa.CallStringContext;
import com.ibm.wala.ipa.callgraph.propagation.cfa.CallStringContextSelector;
import com.ibm.wala.ipa.callgraph.propagation.cfa.CallString;
import com.ibm.wala.ipa.callgraph.propagation.ReceiverInstanceContext;
import com.ibm.wala.analysis.typeInference.TypeInference;
import com.ibm.wala.analysis.typeInference.TypeAbstraction;


public class WALAUtils
{
	public static Map<String, CGNode> mthsig2cgnode;
	public static Map<String, Integer> mthinsn2locvarno;
	
	public static Map<String, IClass> clsname2object;
	
	public static Map<TypeReference, String> typeref2name;
	
	public static Map<IClass, String> clsobj2name;
	

	static
	{
		mthsig2cgnode = new HashMap<String, CGNode>();
		mthinsn2locvarno = new HashMap<String, Integer>();
		
		clsname2object = new HashMap<String, IClass>();
		
		typeref2name = new HashMap<TypeReference, String>();
		
		clsobj2name = new HashMap<IClass, String>();
	}
	
	
	public static String getWalaClassName(String className)
	{
		return "L"+className.replace('.', '/');	
	}
	
	public static String getPackageName(String fullClassName)
    {
		String packageName = "";
		int k = fullClassName.lastIndexOf('.');
		if (k != -1) packageName = fullClassName.substring(0, k);
		return packageName;
	}

	public static String getClassName(TypeReference typeRef) throws Exception
	{
		return getTypeName(typeRef);
	}
	
	public static String getClassName(IClass cls) throws Exception
	{
		if (cls == null) return null;
		
		if (clsobj2name.containsKey(cls)) return clsobj2name.get(cls);
		
		StringBuffer cnStrBuf = new StringBuffer();

		cnStrBuf.append(cls.getName().getPackage().toUnicodeString().replace('/', '.'));
		cnStrBuf.append(".");
		cnStrBuf.append(cls.getName().getClassName().toUnicodeString());
			
		String cnStr = cnStrBuf.toString();
		
		clsobj2name.put(cls, cnStr);
					
		return cnStr;
	}
	
	public static String getFieldName(IField field) throws Exception
	{
		if (field == null) return null;
		
		TypeName classType = field.getDeclaringClass().getName();
		
		StringBuffer fnStrBuf = new StringBuffer();

		fnStrBuf.append(classType.getPackage().toUnicodeString().replace('/', '.'));
		fnStrBuf.append(".");
		fnStrBuf.append(classType.getClassName().toUnicodeString());
		fnStrBuf.append(".");
		fnStrBuf.append(field.getName().toUnicodeString());
		
		return fnStrBuf.toString();
	}
	
	public static String getFieldsClassName(FieldReference tgtField, IClassHierarchy cha) throws Exception
	{
		if (tgtField == null) return null;
		
		// input class name: loaded from bytecode instruction (field access)
		String inClassName = getClassName(tgtField.getDeclaringClass());

		IClass tgtCls = null;
	
		// find the class which really declares the given field

		IClass cls = findClass(cha, inClassName);

		// unknown class (e.g., due to exclusions)
		if (cls == null)
		{
			// we use the class name loaded from the bytecode instruction as the fallback result	
			return inClassName;	
		}
		
		while (cls != null)
		{
			for (IField fld : cls.getDeclaredStaticFields()) 
			{
				if (fld.getName().equals(tgtField.getName()))
				{
					tgtCls = cls;
					break;
				}
			}
			
			if (tgtCls != null) break;
			
			for (IField fld : cls.getDeclaredInstanceFields()) 
			{
				if (fld.getName().equals(tgtField.getName()))
				{
					tgtCls = cls;
					break;
				}
			}
			
			if (tgtCls != null) break;
			
			cls = cls.getSuperclass();
		}
		
		// unknown field name (this may happen, for example, when the field is defined and used only in the built-in model of a native method)
		if (tgtCls == null)
		{
			// we use the class name loaded from the bytecode instruction as the fallback result
			return inClassName;			
		}
		
		return getClassName(tgtCls);		
	}
	
	public static String getFieldsClassName(IField field, IClassHierarchy cha) throws Exception
	{		
		if (field == null) return null;
		
		return getFieldsClassName(field.getReference(), cha);		
	}
	
	/**
	 * Returns null for non-reference types.
	 */
	public static String getFieldTypeClassName(IField field) throws Exception
	{
		if (field == null) return null;
		
		TypeReference fieldTypeRef = field.getFieldTypeReference();
		if ( ! fieldTypeRef.isReferenceType() ) return null;
		
		TypeName fieldTypeClass = fieldTypeRef.getName();
		
		StringBuffer cnStrBuf = new StringBuffer();
		
		if (fieldTypeClass.getPackage() != null) cnStrBuf.append(fieldTypeClass.getPackage().toUnicodeString().replace('/', '.')).append(".");
		
		String className = fieldTypeClass.getClassName().toUnicodeString();
		
		cnStrBuf.append(className);
		
		if (fieldTypeRef.isArrayType()) cnStrBuf.append("[]");

		return cnStrBuf.toString();
	}
	
	/**
	 * Returns null for primitive types.
	 */
	public static String getTypeName(TypeReference typeRef)
	{
		if (typeRef == null) return null;
		
		if ( ! typeRef.isReferenceType() ) return null;
		
		if (typeref2name.containsKey(typeRef)) 
		{
			return typeref2name.get(typeRef);
		}
		
		try
		{
			TypeName typeClass = typeRef.getName();
			
			StringBuffer typeStrBuf = new StringBuffer();
			
			if (typeClass.getPackage() != null) typeStrBuf.append(typeClass.getPackage().toUnicodeString().replace('/', '.')).append(".");
			
			typeStrBuf.append(typeClass.getClassName().toUnicodeString());
			
			if (typeRef.isArrayType()) typeStrBuf.append("[]");
			
			String typeStr = typeStrBuf.toString();
			
			typeref2name.put(typeRef, typeStr);
	
			return typeStr;
		}
		catch (Exception ex) { ex.printStackTrace(); }
		
		return null;
	}		
	
	public static String getFullMethodNameFromCGNode(CGNode node) throws Exception
	{	
		IClass cls = node.getMethod().getDeclaringClass();
		
		String className = WALAUtils.getClassName(cls);
		
		String methodName = node.getMethod().getReference().getName().toUnicodeString();
				
		StringBuffer mnStrBuf = new StringBuffer();

		mnStrBuf.append(className).append(".").append(methodName);

		return mnStrBuf.toString();
	}
	
	public static String getClassNameFromCGNode(CGNode node) throws Exception
	{	
		IClass cls = node.getMethod().getDeclaringClass();
		
		String className = WALAUtils.getClassName(cls);
		
		return className;
	}
	
	public static String getMethodNameFromCGNode(CGNode node) throws Exception
	{	
		String methodName = node.getMethod().getReference().getName().toUnicodeString();
				
		return methodName;
	}
	
	public static boolean isMethodConstructor(CGNode node) throws Exception
	{	
		return node.getMethod().isInit();
	}
	
	public static boolean isThreadClassName(IClassHierarchy cha, String clsName) throws Exception
	{
		IClass cls = WALAUtils.findClass(cha, clsName);
		
		if (clsName.equals("java.lang.Thread")) return true;
		if (clsName.equals("java.lang.Runnable")) return true;
		
		if (cls != null)
		{
			IClass clsSuper = cls.getSuperclass();
			while (clsSuper != null)
			{
				String clsSuperName = WALAUtils.getClassName(clsSuper);						
				if (clsSuperName.equals("java.lang.Thread")) return true;
				if (clsSuperName.equals("java.lang.Runnable")) return true;
				clsSuper = clsSuper.getSuperclass();
			}
		}
		
		return false;
	}
	
	public static IClass findClass(IClassHierarchy cha, String className) throws Exception
	{
		if (className.endsWith("[]")) return null;
		
		if (clsname2object.containsKey(className)) 
		{
			return clsname2object.get(className);
		}
		
		Iterator<IClass> clsIt = cha.iterator();
		while (clsIt.hasNext())
		{
			IClass cls = clsIt.next();
			
			if (className.equals(getClassName(cls)))
			{
				clsname2object.put(className, cls);
				return cls;
			}
		}
		
		return null;
	}
	
	public static boolean isTypeAlias(IClassHierarchy cha, String typeName1, String typeName2) throws Exception
	{
		if (typeName1.equals(typeName2)) return true;
		
		// two variables are may-aliased if their types (classes) are on one path in the class hierarchy (if one type is a subtype of the other)
		// for two variables of interface types, we also check if they have a common implementing class
			
		IClass typeCls1 = WALAUtils.findClass(cha, typeName1);
		IClass typeCls2 = WALAUtils.findClass(cha, typeName2);

		if ((typeCls1 != null) && (typeCls2 != null))
		{
			if (cha.isSubclassOf(typeCls1, typeCls2)) return true;
			if (cha.isSubclassOf(typeCls2, typeCls1)) return true;
				
			if (typeCls1.isInterface() && typeCls2.isInterface())
			{
				Set<IClass> implClasses1 = cha.getImplementors(typeCls1.getReference());
				Set<IClass> implClasses2 = cha.getImplementors(typeCls2.getReference());
				
				for (IClass implCls1 : implClasses1)
				{
					String implClsName1 = getClassName(implCls1);
					
					for (IClass implCls2 : implClasses2)
					{
						String implClsName2 = getClassName(implCls2);
						
						if (implClsName1.equals(implClsName2)) return true;
					}
				}
			}
			else
			{				
				if (cha.implementsInterface(typeCls1, typeCls2)) return true;
				if (cha.implementsInterface(typeCls2, typeCls1)) return true;
			}
		}
		
		return false;
	}
	
	public static boolean existsTypeAlias(IClassHierarchy cha, TypeInference mthTypes, int tgtValNum, Set<Integer> otherVals)
	{	
		TypeReference tgtValTypeRef = mthTypes.getType(tgtValNum).getTypeReference();							
		if ((tgtValTypeRef != null) && tgtValTypeRef.isReferenceType())
		{
			String tgtValTypeName = WALAUtils.getTypeName(tgtValTypeRef);
			
			for (Integer otherValNum : otherVals)
			{
				TypeReference otherValTypeRef = mthTypes.getType(otherValNum.intValue()).getTypeReference();							
				if ((otherValTypeRef != null) && otherValTypeRef.isReferenceType())
				{
					String otherValTypeName = WALAUtils.getTypeName(otherValTypeRef);
					
					try
					{
						// we use type-based aliasing
						if (WALAUtils.isTypeAlias(cha, tgtValTypeName, otherValTypeName)) return true;
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
						return false;
					}
				}
			}
		}
		
		return false;
	}

	public static boolean isTypeAlias(IClassHierarchy cha, TypeInference mthTypes, int tgtValNum, int otherValNum)
	{	
		TypeReference tgtValTypeRef = mthTypes.getType(tgtValNum).getTypeReference();							
		if ((tgtValTypeRef != null) && tgtValTypeRef.isReferenceType())
		{
			String tgtValTypeName = WALAUtils.getTypeName(tgtValTypeRef);
			
			TypeReference otherValTypeRef = mthTypes.getType(otherValNum).getTypeReference();							
			if ((otherValTypeRef != null) && otherValTypeRef.isReferenceType())
			{
				String otherValTypeName = WALAUtils.getTypeName(otherValTypeRef);
					
				try
				{
					// we use type-based aliasing
					if (WALAUtils.isTypeAlias(cha, tgtValTypeName, otherValTypeName)) return true;
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
					return false;
				}
			}
		}
		
		return false;
	}	

	public static String getAllocationSite(NormalAllocationInNode allocKey)
	{
		String methodSig = getCorrectMethodSignature(allocKey.getNode().getMethod().getSignature());
				
		int insnPos = allocKey.getSite().getProgramCounter();
				
		StringBuffer asStrBuf = new StringBuffer();

		asStrBuf.append(methodSig).append(":").append(insnPos);
		
		return asStrBuf.toString();
	}
	
	public static String getAllocationSiteWithCallerSite(NormalAllocationInNode allocKey)
	{
		CallerSiteContext csCtx = (CallerSiteContext) allocKey.getNode().getContext();
		
		StringBuffer ascsStrBuf = new StringBuffer();

		ascsStrBuf.append(getCorrectMethodSignature(csCtx.getCaller().getMethod().getSignature())).append(":").append(csCtx.getCallSite().getProgramCounter());
		
		String methodSig = getCorrectMethodSignature(allocKey.getNode().getMethod().getSignature());
				
		int insnPos = allocKey.getSite().getProgramCounter();
		
		ascsStrBuf.insert(0, "[").append("] ").append(methodSig).append(":").append(insnPos);
		
		return ascsStrBuf.toString();
	}
	
	public static String getAllocationSiteWithCallString(NormalAllocationInNode allocKey)
	{
		CallStringContext csCtx = (CallStringContext) allocKey.getNode().getContext();
		
		CallString cs = (CallString) csCtx.get(CallStringContextSelector.CALL_STRING);
		
		StringBuffer ascsStrBuf = new StringBuffer("");

		for (int i = 0; i < cs.getCallSiteRefs().length; i++) 
		{
			if (i > 0) ascsStrBuf.append("@");
			ascsStrBuf.append(getCorrectMethodSignature((cs.getMethods()[i]).getSignature())).append(":").append((cs.getCallSiteRefs()[i]).getProgramCounter());
		}
		
		String methodSig = getCorrectMethodSignature(allocKey.getNode().getMethod().getSignature());
				
		int insnPos = allocKey.getSite().getProgramCounter();
				
		ascsStrBuf.insert(0, "[").append("]").append(methodSig).append(":").append(insnPos);
		
		return ascsStrBuf.toString();
	}

	public static String getAllocationSiteWithContainerObject(NormalAllocationInNode allocKey)
	{
		StringBuffer ascoStrBuf = new StringBuffer();
		
		Context ctx = allocKey.getNode().getContext();
		if (ctx instanceof CallerSiteContext)
		{
			CallerSiteContext csCtx = (CallerSiteContext) ctx;
			
			ascoStrBuf.append(getCorrectMethodSignature(csCtx.getCaller().getMethod().getSignature())).append(":").append(csCtx.getCallSite().getProgramCounter());
		}
		else if (ctx instanceof ReceiverInstanceContext)
		{
			ReceiverInstanceContext riCtx = (ReceiverInstanceContext) ctx;
			
			InstanceKey ik = (InstanceKey) riCtx.get(ContextKey.RECEIVER);
			
			if (ik instanceof NormalAllocationInNode) ascoStrBuf.append(getAllocationSiteWithContainerObject((NormalAllocationInNode) ik));
			else ascoStrBuf.append(ik.toString());
		}
		
		String methodSig = getCorrectMethodSignature(allocKey.getNode().getMethod().getSignature());
				
		int insnPos = allocKey.getSite().getProgramCounter();
		
		// wrap existing context
		if (ascoStrBuf.length() > 0) ascoStrBuf.insert(0, "[").append("]");

		ascoStrBuf.append(methodSig).append(":").append(insnPos);
		
		return ascoStrBuf.toString();
	}

	public static String getProperAllocSite(NormalAllocationInNode allocKey)
	{
		String allocSite = "";

		if (AllocationSiteManager.useCallerSite && AllocationSiteManager.keepContext) allocSite = WALAUtils.getAllocationSiteWithCallerSite(allocKey);
		else if (AllocationSiteManager.useCallString && AllocationSiteManager.keepContext) allocSite = WALAUtils.getAllocationSiteWithCallString(allocKey);
		else if (AllocationSiteManager.useContainerObject && AllocationSiteManager.keepContext) allocSite = WALAUtils.getAllocationSiteWithContainerObject(allocKey);
		else allocSite = WALAUtils.getAllocationSite(allocKey);

		return allocSite;
	}
	
	public static void storeMethodNode(CGNode cgn)
	{
		mthsig2cgnode.put(getCorrectMethodSignature(cgn.getMethod().getSignature()), cgn);
	}
	
	public static CGNode getNodeForMethod(String methodSig)
	{
		return mthsig2cgnode.get(methodSig);
	}
	
	public static CGNode getNodeForMethodName(String methodName)
	{
		Set<String> mthSigs = mthsig2cgnode.keySet();
		
		for (String mthSig : mthSigs)
		{
			if (mthSig.startsWith(methodName)) return mthsig2cgnode.get(mthSig);
		}
		
		return null;
	}

	public static void storeLocalVarNumberForMethodInsn(String methodSig, int insnPos, int localVarNo)
	{
		mthinsn2locvarno.put(methodSig + ":" + insnPos, localVarNo);
	}

	public static int getLocalVarNumberForMethodInsn(String methodSig, int insnPos)
	{
		Integer locVarNoObj = mthinsn2locvarno.get(methodSig + ":" + insnPos);
		if (locVarNoObj == null) return -1;

		return locVarNoObj.intValue();
	}
	
	public static String getCorrectMethodSignature(String walaMethodSig)
	{
		return walaMethodSig;
	}
	
	public static Set<Integer> getLocalVariablesRefType(IR mthIR, TypeInference mthTypes)
	{
		Set<Integer> valNums = new HashSet<Integer>();
		
		// method parameters
				
		int[] methodParams = mthIR.getParameterValueNumbers();
		for (int i = 0; i < methodParams.length; i++) 
		{
			TypeReference valTypeRef = mthTypes.getType(methodParams[i]).getTypeReference();
			if ((valTypeRef != null) && valTypeRef.isReferenceType()) valNums.add(methodParams[i]);
		}

		// local variables 
				
		SSAInstruction[] instructions = mthIR.getInstructions();
		for (int i = 0; i < instructions.length; i++) 
		{
			SSAInstruction instr = instructions[i];					
			if (instr == null) continue;
					
			for (int j = 0; j < instr.getNumberOfDefs(); j++) 
			{
				TypeReference valTypeRef = null;
				
				TypeAbstraction valTypeAbs = mthTypes.getType(instr.getDef(j));
				if (valTypeAbs != null) valTypeRef = valTypeAbs.getTypeReference();
				
				if ((valTypeRef != null) && valTypeRef.isReferenceType()) valNums.add(instr.getDef(j));
			}
		}
		
		return valNums;
	}
	
	public static int getInsnBytecodeIndex(CGNode mthNode, int ssaInsnIndex)
	{
		return getInsnBytecodeIndex(mthNode.getMethod(), ssaInsnIndex);	
	}
	
	public static int getInsnBytecodeIndex(IMethod mth, int ssaInsnIndex)
	{
		try
		{
			return ((IBytecodeMethod) mth).getBytecodeIndex(ssaInsnIndex);
		}
		catch (Exception ex)
		{
			return -1;
		}
	}
}
