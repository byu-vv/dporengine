package jpf.analysis;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;

import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.KernelState;
import gov.nasa.jpf.vm.ThreadList;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.jvm.bytecode.INVOKEINTERFACE;

import com.ibm.wala.classLoader.IField;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.propagation.HeapModel;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.PointerKey;
import com.ibm.wala.ipa.callgraph.propagation.InstanceFieldKey;
import com.ibm.wala.ipa.callgraph.propagation.StaticFieldKey;
import com.ibm.wala.ipa.callgraph.propagation.LocalPointerKey;
import com.ibm.wala.ipa.callgraph.propagation.NormalAllocationInNode;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAFieldAccessInstruction;


public class JPFUtils
{
	private static final boolean INFO = false;
	private static final boolean DEBUG = false;
	
	private static final boolean TRACK = false;
	
	//public static final String JPF_VM_ALLOC_SITE = "JPF_VM";

	
	public static String getMethodSignature(MethodInfo mi)
	{
		return mi.getBaseName() + mi.getSignature();
	}

	public static String getAllocSite(MethodInfo curMI, int insnPos)
	{
		String methodSig = JPFUtils.getMethodSignature(curMI);

		StringBuffer strbuf = new StringBuffer();

		strbuf.append(methodSig);
		strbuf.append(":");
		strbuf.append(insnPos);

		return strbuf.toString();
	}

	public static List<ProgramPoint> getProgramPointsForThreadStack(ThreadInfo th)
	{
		List<ProgramPoint> progpoints = new ArrayList<ProgramPoint>();
		
		// top stack frame is the first
		boolean isTop = true;

		for (StackFrame sf : th)
		{								
			Instruction curInsn = sf.getPC();

			String methodSig = getMethodSignature(curInsn.getMethodInfo());

			// for stack frames other than top we need the next instruction after the currently processed method call
			if ( ! isTop )
			{
				// returns "null" if we are at the end (last instruction) of the given method
				curInsn = curInsn.getNext();
			}
			isTop = false;
		
			int insnPos = -1;			
			if (curInsn != null) insnPos = curInsn.getPosition();
			
			// ignore calls of methods without body
			if (curInsn instanceof INVOKEINTERFACE) continue;
			
			// ignore library methods in Thread and ThreadGroup except thread start
			if (isLibraryEntityForThread(methodSig) && ( ! isThreadStartMethod(methodSig) ) ) continue;
			
			// ignore synthetic methods like "[run]"
			if (sf.isSynthetic())
			{
				if (sf.getMethodInfo().getName().equals("[run]")) continue;
				if (sf.getMethodInfo().getName().equals("[main]")) continue;				
			}
			
			// static analysis provides results only for the position -1
			if (sf.isNative()) insnPos = -1;
			
			ProgramPoint pp = new ProgramPoint(methodSig, insnPos);
			
			progpoints.add(pp);
		}
		
		return progpoints;
	}
	
	public static ProgramPoint getProgramPointForThreadPC(ThreadInfo th)
	{
		if (th.getTopFrame() == null) return ProgramPoint.INVALID;

		for (StackFrame sf : th)
		{								
			Instruction curInsn = sf.getPC();
			
			// thread not yet started or already terminated
			if (curInsn == null) return ProgramPoint.INVALID;
			
			String methodSig = getMethodSignature(curInsn.getMethodInfo());
				
			int insnPos = curInsn.getPosition();

			// ignore calls of methods without body
			if (curInsn instanceof INVOKEINTERFACE) continue;
			
			// skip library methods in Thread and ThreadGroup except thread start
			if (isLibraryEntityForThread(methodSig) && ( ! isThreadStartMethod(methodSig) ) ) continue;
			
			if (sf.isSynthetic()) 
			{
				if ( ! sf.isNative() )
				{
					// ignore other synthetic methods
					return ProgramPoint.INVALID;
				}
			}
			
			// static field access analysis provides results only for the position -1
			if (sf.isNative()) insnPos = -1;
			
			ProgramPoint pp = new ProgramPoint(methodSig, insnPos);
			
			return pp;
		}
		
		// we have not found any usable program point (not in library methods, etc)
		return ProgramPoint.INVALID;
	}

	public static List<String> getReceiverObjectStringForThreadStack(ThreadInfo th)
	{
		List<String> objString = new ArrayList<String>();

		for (StackFrame sf : th)
		{
			// ignore synthetic methods like "[run]"
			if (sf.isSynthetic()) continue;
			
			String receiverObjAllocSite = null;

			MethodInfo mi = sf.getMethodInfo();

			if (mi == null) continue;

			if (mi.isStatic()) 
			{
				if (mi.getClassInfo() != null) receiverObjAllocSite = mi.getClassInfo().getName();
			}
			else
			{
				int receiverObjRef = sf.getThis();
			
				receiverObjAllocSite = AllocationSiteManager.getAllocSiteForObject(receiverObjRef);
			}

			if (DEBUG) System.out.println("[DEBUG JPFUtils.getReceiverObjectStringForThreadStack] mi.name = " + mi.getBaseName() + ", receiverObjAllocSite = " + receiverObjAllocSite);

			if (receiverObjAllocSite == null) continue; // we don't know, so we skip (this happens to "main" and some JVM-internal calls)

			objString.add(receiverObjAllocSite);
		}

		return objString;
	}
	
	public static boolean isMethodOnThreadStack(ThreadInfo th, String methodSig)
	{
		for (StackFrame sf : th)
		{
			Instruction curInsn = sf.getPC();
			
			String curMethodSig = getMethodSignature(curInsn.getMethodInfo());
			
			if (curMethodSig.equals(methodSig)) return true;
		}
		
		return false;
	}
	
	public static boolean existsFutureRead(ThreadInfo curTh, String curMthSig, int curInsnPos, String className, String fieldName, boolean isStatic, int objRef)
	{
		if (INFO) System.out.println("[INFO JPFUtils.existsFutureRead] className = " + className + ", fieldName = " + fieldName + ", static = " + isStatic + ", objRef = " + objRef); 
		if (INFO) System.out.println("[INFO JPFUtils.existsFutureRead] curMthSig = " + curMthSig + ", curInsnPos = " + curInsnPos);

		
		// there cannot be a future read access to any field of a thread-local object by any other thread than the current one
		if (FieldAccessManager.useEscapeAnalysis)
		{
			String objAllocSite = "";
			
			if ( ! isStatic )
			{
				if (AllocationSiteManager.useContainerObject && AllocationSiteManager.keepContext) objAllocSite = AllocationSiteManager.getCtxAllocSiteForObject(objRef);
				else objAllocSite = AllocationSiteManager.getAllocSiteForObject(objRef);
			}
			else
			{
				objAllocSite = className;	
			}
			
			if (DEBUG) System.out.println("[DEBUG JPFUtils.existsFutureRead] objAllocSite = " + objAllocSite + ", thread local object = " + AllocationSiteManager.isThreadLocalObject(objAllocSite));
			
			if (AllocationSiteManager.isThreadLocalObject(objAllocSite)) 
			{
				if (INFO) System.out.println("[INFO JPFUtils.existsFutureRead] target object " + objAllocSite + " is thread local (escape analysis) -> no thread choice needed");
				
				return false;
			}
		}
	
		// here we assume knowledge of immutable fields
		if (FieldAccessManager.useImmutableFields)
		{
			boolean immutFieldDetected = false;

			immutFieldDetected = isImmutableField(className, fieldName);

			if (immutFieldDetected) 
			{
				if (DEBUG) System.out.println("[DEBUG JPFUtils.existsFutureRead] return false, className = " + className + ", fieldName = " + fieldName);
				
				if (INFO) System.out.println("[INFO JPFUtils.existsFutureRead] detected immutable field " + className + "." + fieldName + " -> no thread choice needed");
					
				return false;
			}
		}


		boolean existsRead = false;
		
		ThreadInfo[] tl = curTh.getVM().getThreadList().getThreads();
		for (int i = 0; i < tl.length; i++)
		{
			boolean existsReadByThread = false;
			
			ThreadInfo th = tl[i];

			// skip current thread
			if (th.getId() == curTh.getId()) continue;
			
			Set<FieldName> threadFieldReads = null;
		
			if (FieldAccessManager.useThreadCallStacks)
			{
				List<ProgramPoint> threadPPs = JPFUtils.getProgramPointsForThreadStack(th);
				
				// only synthetic methods on thread stack
				if (threadPPs.isEmpty()) continue;

				/*
				if (DEBUG)
				{
					for (ProgramPoint pp : threadPPs) System.out.println("[DEBUG JPFUtils.existsFutureRead] program point = " + pp.getMethodName() + ":" + pp.getInsnPos());					
				}
				*/
				
				threadFieldReads = FieldAccessManager.getMergedFieldReadsForProgramPoints(threadPPs);
			}
			else
			{
				ProgramPoint threadPP = JPFUtils.getProgramPointForThreadPC(th);

				//if (DEBUG) System.out.println("[DEBUG JPFUtils.existsFutureRead] program point = " + threadPP.getMethodName() + ":" + threadPP.getInsnPos());					
				
				threadFieldReads = FieldAccessManager.getFieldReadsForProgramPoint(threadPP, true);
			}
			
			/*
			if (DEBUG)
			{
				if (threadFieldReads != null)
				{
					for (String fieldReadStr : threadFieldReads) System.out.println("[DEBUG JPFUtils.existsFutureRead] field read: " + fieldReadStr);
				}
			}
			*/
			
			if (threadFieldReads == null) 
			{
				existsReadByThread = true;
								
				if (DEBUG) System.out.println("[DEBUG JPFUtils.existsFutureRead] thread = " + th.getId() + ", threadFieldReads = null, existsRead = true");
			}
			else 
			{
				Set<FieldName> fieldIDs = getFieldIDs(className, fieldName, isStatic, objRef, curMthSig, curInsnPos);
				
				for (FieldName fieldID : fieldIDs)
				{
					for (FieldName fn : threadFieldReads)
					{
						if (fn.equalsObjField(fieldID)) existsReadByThread = true;
					}
					
					if (DEBUG) System.out.println("[DEBUG JPFUtils.existsFutureRead] thread = " + th.getId() + ", fieldID = " + fieldID.toString() + ", existsRead = " + existsReadByThread);
				}
			}
			
			
			if (existsReadByThread && (threadFieldReads != null) && FieldAccessManager.useDemandDrivenPA)
			{
				existsReadByThread = existsAccessByDemandDrivenPA(th, curMthSig, curInsnPos, className, fieldName, isStatic, objRef, threadFieldReads);
			}
			
			
			if (existsReadByThread) existsRead = true;
		}
		
		if (INFO) 
		{
			if ( ! existsRead ) System.out.println("[INFO JPFUtils.existsFutureRead] no read field access by any other thread exists -> no thread choice needed");
		}
		
		if (TRACK)
		{
			if (existsRead) System.out.println("[TRACK] field name = " + className + "." + fieldName + ", object index = " + objRef + ", static = " + isStatic + ", code location = " + curMthSig + ":" + curInsnPos);
		}			
				
		return existsRead;
	}
	
	public static boolean existsFutureWrite(ThreadInfo curTh, String curMthSig, int curInsnPos, String className, String fieldName, boolean isStatic, int objRef, boolean insideNormalInstanceMethod)
	{
		if (INFO) System.out.println("[INFO JPFUtils.existsFutureWrite] className = " + className + ", fieldName = " + fieldName + ", static = " + isStatic + ", objRef = " + objRef);
		if (INFO) System.out.println("[INFO JPFUtils.existsFutureWrite] curMthSig = " + curMthSig + ", curInsnPos = " + curInsnPos);

		
		// there cannot be a future write access to any field of a thread-local object by any other thread than the current one
		if (FieldAccessManager.useEscapeAnalysis)
		{
			String objAllocSite = "";
				
			if ( ! isStatic )
			{
				if (AllocationSiteManager.useContainerObject && AllocationSiteManager.keepContext) objAllocSite = AllocationSiteManager.getCtxAllocSiteForObject(objRef);
				else objAllocSite = AllocationSiteManager.getAllocSiteForObject(objRef);
			}
			else
			{
				objAllocSite = className;
			}
				
			if (DEBUG) System.out.println("[DEBUG JPFUtils.existsFutureWrite] objAllocSite = " + objAllocSite + ", thread local object = " + AllocationSiteManager.isThreadLocalObject(objAllocSite));
				
			if (AllocationSiteManager.isThreadLocalObject(objAllocSite)) 
			{
				if (INFO) System.out.println("[INFO JPFUtils.existsFutureWrite] target object " + objAllocSite + " is thread local (escape analysis) -> no thread choice needed");
				
				return false;
			}
			
			if (insideNormalInstanceMethod)
			{
				// we are not processing object's constructor
				
				boolean thisEscapes = false;
				Boolean teObj = FieldAccessManager.clsname2thisobjescapes.get(className);
				if ( (teObj != null) && teObj.booleanValue() ) thisEscapes = true;
				
				boolean fieldWrittenInit = true;
				Boolean fwiObj = FieldAccessManager.fieldname2writteninit.get(className + "." + fieldName);
				if ( (fwiObj != null) && ( ! fwiObj.booleanValue() ) ) fieldWrittenInit = false;
				
				// the field is written-to only on "this object" in the object's constructor and "this" object does not escape before the constructor finishes -> there cannot be future write access if we are inside an instance method
				if (fieldWrittenInit && ( ! thisEscapes ) ) 
				{
					if (INFO) System.out.println("[INFO JPFUtils.existsFutureWrite] detected immutable field (automatic) -> no thread choice needed");
					
					return false;
				}
			}
		}
		
		// here we assume knowledge of immutable fields
		if (FieldAccessManager.useImmutableFields)
		{
			boolean immutFieldDetected = false;

			immutFieldDetected = isImmutableField(className, fieldName);

			if (immutFieldDetected) 
			{
				if (DEBUG) System.out.println("[DEBUG JPFUtils.existsFutureWrite] return false, className = " + className + ", fieldName = " + fieldName);
				
				if (INFO) System.out.println("[INFO JPFUtils.existsFutureWrite] detected immutable field " + className + "." + fieldName + " -> no thread choice needed");
					
				return false;
			}
		}

		
		boolean existsWrite = false;
		
		ThreadInfo[] tl = curTh.getVM().getThreadList().getThreads();
		for (int i = 0; i < tl.length; i++)
		{
			boolean existsWriteByThread = false;
			
			ThreadInfo th = tl[i];

			// skip current thread
			if (th.getId() == curTh.getId()) continue;
			
			Set<FieldName> threadFieldWrites = null;
		
			if (FieldAccessManager.useThreadCallStacks)
			{
				List<ProgramPoint> threadPPs = JPFUtils.getProgramPointsForThreadStack(th);
				
				// only synthetic methods on thread stack
				if (threadPPs.isEmpty()) continue;
				
				threadFieldWrites = FieldAccessManager.getMergedFieldWritesForProgramPoints(threadPPs);
			}
			else
			{
				ProgramPoint threadPP = JPFUtils.getProgramPointForThreadPC(th);				
				threadFieldWrites = FieldAccessManager.getFieldWritesForProgramPoint(threadPP, true);			
			}
			
			if (threadFieldWrites == null) 
			{
				existsWriteByThread = true;
				
				if (DEBUG) System.out.println("[DEBUG JPFUtils.existsFutureWrite] thread = " + th.getId() + ", threadFieldWrites = null, existsWrite = true");
			}
			else 
			{
				Set<FieldName> fieldIDs = getFieldIDs(className, fieldName, isStatic, objRef, curMthSig, curInsnPos);
				
				for (FieldName fieldID : fieldIDs)
				{
					for (FieldName fn : threadFieldWrites)
					{
						if (fn.equalsObjField(fieldID)) existsWriteByThread = true;
					}
					
					if (DEBUG) System.out.println("[DEBUG JPFUtils.existsFutureWrite] thread = " + th.getId() + ", fieldID = " + fieldID.toString() + ", existsWrite = " + existsWriteByThread);
				}
			}
			
			
			if (existsWriteByThread && (threadFieldWrites != null) && FieldAccessManager.useDemandDrivenPA)
			{
				existsWriteByThread = existsAccessByDemandDrivenPA(th, curMthSig, curInsnPos, className, fieldName, isStatic, objRef, threadFieldWrites);
			}
			
			
			if (existsWriteByThread) existsWrite = true;
		}
		
		if (INFO) 
		{
			if ( ! existsWrite ) System.out.println("[INFO JPFUtils.existsFutureWrite] no write field access by any other thread exists -> no thread choice needed");
		}
		
		if (TRACK)
		{
			if (existsWrite) System.out.println("[TRACK] field name = " + className + "." + fieldName + ", static = " + isStatic + ", object index = " + objRef + ", code location = " + curMthSig + ":" + curInsnPos);
		}
		
		return existsWrite;
	}
	
	private static Set<FieldName> getFieldIDs(String className, String fieldName, boolean isStatic, int objRef, String methodSig, int curInsnPos)
	{
		Set<FieldName> fieldIDs = new HashSet<FieldName>();
		
		FieldName fullFN = new FieldName(className, fieldName);
		
		if (FieldAccessManager.usePointerAnalysis && ( ! FieldAccessManager.useDemandDrivenPA ) )
		{
			if (AllocationSiteManager.useContainerObject && AllocationSiteManager.keepContext) 
			{
				if ( ! isStatic )
				{
					String objAllocSite = AllocationSiteManager.getCtxAllocSiteForObject(objRef);
				
					if (DEBUG) System.out.println("[DEBUG JPFUtils.getFieldIDs] objRef = " + objRef + ", objAllocSite = " + objAllocSite);
	
					if (objAllocSite != null) fieldIDs.add(new FieldName(objAllocSite, fieldName));
				}
				else
				{
					fieldIDs.add(fullFN);
				}
			}
			else if (FieldAccessManager.useVariableNamesOnly)
			{
				int objLocalVarNo = -1;

				CGNode methodNode = WALAUtils.getNodeForMethod(methodSig);
		
				if (methodNode != null)
				{
					int ssaInsnIndex = -1;
					for (int i = 0; i < methodNode.getIR().getInstructions().length; i++)
					{
						if (WALAUtils.getInsnBytecodeIndex(methodNode, i) == curInsnPos) 
						{
							ssaInsnIndex = i;
							break;
						}
					}

					if (DEBUG) System.out.println("[DEBUG JPFUtils.getFieldIDs] curInsnPos = " + curInsnPos + ", ssaInsnIndex = " + ssaInsnIndex);

					if (ssaInsnIndex != -1)
					{
						SSAInstruction ssaInsn = methodNode.getIR().getInstructions()[ssaInsnIndex];

						if (DEBUG) System.out.println("[DEBUG JPFUtils.getFieldIDs] ssaInsn instanceof FieldAccess  = " + (ssaInsn instanceof SSAFieldAccessInstruction));

						if (ssaInsn instanceof SSAFieldAccessInstruction) objLocalVarNo = ((SSAFieldAccessInstruction) ssaInsn).getRef();
					}
				}

				/*
				if (objLocalVarNo == -1)
				{
					objLocalVarNo = WALAUtils.getLocalVarNumberForMethodInsn(methodSig, curInsnPos);
				}
				*/

				if (DEBUG) System.out.println("[DEBUG JPFUtils.getFieldIDs] methodSig = " + methodSig + ", curInsnPos = " + curInsnPos + ", objLocalVarNo = " + objLocalVarNo);
				
				Set<String> allocSites = AllocationSiteManager.getAllocSitesForLocalVar(new FieldName("", "", methodSig, objLocalVarNo));
								
				// for static variables
				fieldIDs.add(fullFN);
				
				if (allocSites != null)
				{
					for (String site : allocSites) fieldIDs.add(new FieldName(site, fieldName));
				}
			}
			else
			{
				String objAllocSite = null;
				
				if ( ! isStatic ) objAllocSite = AllocationSiteManager.getAllocSiteForObject(objRef);
				
				//Set<String> allocSites = AllocationSiteManager.getAllocSitesForClass(className);
				
				if (objAllocSite == null) 
				{
					fieldIDs.add(fullFN);
				}
				else
				{
					fieldIDs.add(new FieldName(objAllocSite, fieldName));
					
					//for (String site : allocSites) fieldIDs.add(new FieldName(site, fieldName));
				}
			}
			
			if (FieldAccessManager.nativeFieldReads.contains(fullFN) || FieldAccessManager.nativeFieldWrites.contains(fullFN)) fieldIDs.add(fullFN); 
		}
		else
		{
			fieldIDs.add(fullFN);
		}
		
		return fieldIDs;
	}

	private static boolean existsAccessByDemandDrivenPA(ThreadInfo curTh, String curMethodSig, int curInsnPos, String className, String fieldName, boolean isStatic, int objRef, Set<FieldName> threadFieldAccesses)
	{
		// find the variable number of the local variable 'u' pointing to the object 'o' by comparing bytecode instruction offsets (get variable number from corresponding SSA instruction)

		CGNode curMethodNode = WALAUtils.getNodeForMethod(curMethodSig);

		// we do not know for sure so we must give a safe answer (i.e., that there is a possible field access)
		if (curMethodNode == null)
		{
			if (DEBUG) System.out.println("[DEBUG JPFUtils.existsAccessByDemandDrivenPA] curMethodSig = " + curMethodSig + ", curMethodNode = null");

			return true;
		}

		int objLocalVarNo = -1;
		

		int ssaInsnIndex = -1;
		for (int i = 0; i < curMethodNode.getIR().getInstructions().length; i++)
		{
			if (WALAUtils.getInsnBytecodeIndex(curMethodNode, i) == curInsnPos) 
			{
				ssaInsnIndex = i;
				break;
			}
		}

		if (DEBUG) System.out.println("[DEBUG JPFUtils.getFieldIDs] curInsnPos = " + curInsnPos + ", ssaInsnIndex = " + ssaInsnIndex);

		if (ssaInsnIndex != -1)
		{
			SSAInstruction ssaInsn = curMethodNode.getIR().getInstructions()[ssaInsnIndex];
			
			if (DEBUG) System.out.println("[DEBUG JPFUtils.existsAccessByDemandDrivenPA] ssaInsn instanceof FieldAccess  = " + (ssaInsn instanceof SSAFieldAccessInstruction));
			
			if (ssaInsn instanceof SSAFieldAccessInstruction) objLocalVarNo = ((SSAFieldAccessInstruction) ssaInsn).getRef();
		}
		
		String objAllocSite = null;
		
		if ( ! isStatic )
		{
			objAllocSite = AllocationSiteManager.getAllocSiteForObject(objRef);
		}
		else
		{
			objAllocSite = className;
		}
					
		if (DEBUG) System.out.println("[DEBUG JPFUtils.existsAccessByDemandDrivenPA] objLocalVarNo = " + objLocalVarNo + ", objAllocSite = " + objAllocSite);
		
		// we do not know for sure so we must give a safe answer (i.e., that there is a possible field access)
		if ((objAllocSite == null) || (objLocalVarNo == -1)) return true;

		
		HeapModel hm = AllocationSiteManager.demandPointsTo.getHeapModel();

		
		// find pointer key associated with the local variable pointing to target object
		
		PointerKey objLocalVarPK = hm.getPointerKeyForLocal(curMethodNode, objLocalVarNo);
		
		if (objLocalVarPK == null)
		{
			if (DEBUG) System.out.println("[DEBUG JPFUtils.existsAccessByDemandDrivenPA] objLocalVarPK = null");
			
			return true;
		}
		
		Collection<InstanceKey> objLocalVarAllocSites = retrievePointsToSetOnDemand(objLocalVarPK);
		
		// we do not know for sure so we must give a safe answer (i.e., that there is a possible field access)
		if (objLocalVarAllocSites == null)
		{
			if (DEBUG) System.out.println("[DEBUG JPFUtils.existsAccessByDemandDrivenPA] budget exceeded by points-to query or some error occurred for 'objLocalVarPK'");
			return true;
		}

		// for each local variable 'l' pointing to objects of the class 'C' such that 'C.f' is accessed through it (i.e., field names analysis results contain 'l:C.f'), check whether the intersection of points-to set for 'l' with points-to set for 'u' (variable pointing to 'o') is not empty
		// if this holds for some local variable 'l', then the analysis claims that there is a possible future read access to 'o.f'

		for (FieldName taFN : threadFieldAccesses)
		{
			List<PointerKey> pkList = AllocationSiteManager.field2locvar.get(taFN);
			
			if (INFO)
			{
				if ((pkList == null) || (pkList.isEmpty())) System.out.println("[INFO JPFUtils.existsAccessByDemandDrivenPA] empty list of local variables for given field");
			}
			
			if (pkList == null) continue;

			for (PointerKey pk : pkList)
			{
				if ( ! (pk instanceof LocalPointerKey) ) continue;

				Collection<InstanceKey> pkAllocSites = retrievePointsToSetOnDemand(pk);
		
				// we do not know for sure so we must give a safe answer (i.e., that there is a possible field access)
				if (pkAllocSites == null)
				{
					if (DEBUG) System.out.println("[DEBUG JPFUtils.existsAccessByDemandDrivenPA] budget exceeded by points-to query or some error occurred for 'pk'");
					return true;
				}
				
				for (InstanceKey ik : objLocalVarAllocSites)
				{
					// possible field access exists
					if ((ik instanceof NormalAllocationInNode) && pkAllocSites.contains(ik)) return true;
				}	
			}
		}
		
		if (INFO)
		{
			System.out.println("[INFO JPFUtils.existsAccessByDemandDrivenPA] empty intersection of points-to sets for all local variables for given field");
		}
		
		return false;
	}

	private static Collection<InstanceKey> retrievePointsToSetOnDemand(PointerKey pk)
	{
		// compute the points-to set only if it is not already stored in the cache
		
		Collection<InstanceKey> allocSites = AllocationSiteManager.pointstoCache.get(pk);

		if (allocSites == null)
		{	
			try
			{
				allocSites = AllocationSiteManager.demandPointsTo.getPointsTo(pk);
			}
			catch (Exception ex)
			{
				return null;
			}

			AllocationSiteManager.pointstoCache.put(pk, allocSites);
		}

		return allocSites;
	}

	private static boolean isImmutableField(String className, String fieldName)
	{
		// there is no future write access to any of these fields by a different thread than the one that created the encapsulating objects after any read access
		
		return ImmutableFieldsDetector.isImmutableField(className, fieldName);

		
		// manually defined
		/*
		if (className.endsWith("JBButil"))
		{
			if (fieldName.equals("warehouse_random_stream")) return true;
		}
		if (className.endsWith("TransactionManager"))
		{
			if (fieldName.equals("warehouseId")) return true;
			if (fieldName.equals("company")) return true;
			if (fieldName.equals("transactionInstance")) return true;
		}
		if (className.endsWith("Order"))
		{
			if (fieldName.equals("orderlineList")) return true;
			if (fieldName.equals("company")) return true;
			if (fieldName.equals("orderId")) return true;
			if (fieldName.equals("districtId")) return true;
			if (fieldName.equals("warehouseId")) return true;
			if (fieldName.equals("customerPtr")) return true;
		}
		if (className.endsWith("Orderline"))
		{
			if (fieldName.equals("stockPtr")) return true;
			if (fieldName.equals("stockQuantity")) return true;
			if (fieldName.equals("BrandGeneric")) return true;
			if (fieldName.equals("itemPrice")) return true;
			if (fieldName.equals("itemName")) return true;
			if (fieldName.equals("supplyWarehouseId")) return true;
		}
		if (className.endsWith("Company"))
		{
			if (fieldName.equals("MaxItems")) return true;
			if (fieldName.equals("MaxDistrictsPerWarehouse")) return true;
			if (fieldName.equals("MaxCustomersPerDistrict")) return true;
			if (fieldName.equals("InitialOrders")) return true;
			if (fieldName.equals("InitialNewOrders")) return true;
		}
		if (className.endsWith("Warehouse"))
		{
			if (fieldName.equals("districts")) return true;
			if (fieldName.equals("distCount")) return true;
			if (fieldName.equals("itemTable")) return true;
			if (fieldName.equals("stockTable")) return true;
			if (fieldName.equals("historyTable")) return true;
		}
		if (className.endsWith("Customer"))
		{
			if (fieldName.equals("customerId")) return true;
			if (fieldName.equals("discount")) return true;
			if (fieldName.equals("firstName")) return true;
			if (fieldName.equals("lastName")) return true;
			if (fieldName.equals("credit1")) return true;
			if (fieldName.equals("districtId")) return true;
			if (fieldName.equals("warehouseId")) return true;
			if (fieldName.equals("address")) return true;
			if (fieldName.equals("since")) return true;
			if (fieldName.equals("phone")) return true;
		}
		if (className.endsWith("Item"))
		{
			if (fieldName.equals("name")) return true;
			if (fieldName.equals("price")) return true;
			if (fieldName.equals("brandInformation")) return true;
		}
		if (className.endsWith("Stock"))
		{
			if (fieldName.equals("district_text")) return true;
			if (fieldName.equals("data")) return true;
		}
		if (className.endsWith("History"))
		{
			if (fieldName.equals("customerId")) return true;
			if (fieldName.equals("date")) return true;
			if (fieldName.equals("amount")) return true;
		}
		if (className.endsWith("NewOrderTransaction"))
		{
			if (fieldName.equals("warehouseTaxRate")) return true;
			if (fieldName.equals("districtTaxRate")) return true;
			if (fieldName.equals("number_of_orderlines")) return true;
		}
		if (className.endsWith("OrderStatusTransaction"))
		{
			if (fieldName.equals("customerPtr")) return true;
			if (fieldName.equals("currentOrderPtr")) return true;
			if (fieldName.equals("districtId")) return true;
			if (fieldName.equals("cust_last_name")) return true;
			if (fieldName.equals("customerBalance")) return true;
			if (fieldName.equals("customerFirstName")) return true;
			if (fieldName.equals("customerMiddleName")) return true;
			if (fieldName.equals("customerLastName")) return true;
			if (fieldName.equals("orderLineCount")) return true;
			if (fieldName.equals("warehousePtr")) return true;
		}
		if (className.endsWith("PaymentTransaction"))
		{
			if (fieldName.equals("districtId")) return true;
			if (fieldName.equals("districtPtr")) return true;
			if (fieldName.equals("use_customerId")) return true;
			if (fieldName.equals("customerDistrictId")) return true;
			if (fieldName.equals("customerWarehouseId")) return true;
		}
		if (className.endsWith("CustomerReportTransaction"))
		{
			if (fieldName.equals("customerPtr")) return true;
			if (fieldName.equals("districtId")) return true;
			if (fieldName.equals("customerDistrictId")) return true;
			if (fieldName.equals("customerWarehouseId")) return true;
			if (fieldName.equals("cust_last_name")) return true;
			if (fieldName.equals("use_customerId")) return true;
			if (fieldName.equals("districtPtr")) return true;
			if (fieldName.equals("company")) return true;
			if (fieldName.equals("warehousePtr")) return true;
			if (fieldName.equals("warehouseId")) return true;
			if (fieldName.equals("lastPaymentsQuantity")) return true;
			if (fieldName.equals("paymentDates")) return true;
			if (fieldName.equals("paymentAmount")) return true;
			if (fieldName.equals("orderDates")) return true;
			if (fieldName.equals("orderAmount")) return true;
		}
		if (className.endsWith("StockLevelTransaction"))
		{
			if (fieldName.equals("orderTable")) return true;
		}
		*/
	}

	public static boolean isLibraryMethod(String entityName)
	{
		if (entityName.startsWith("java.")) return true;
		if (entityName.startsWith("javax.")) return true;
		if (entityName.startsWith("sun.")) return true;
		if (entityName.startsWith("com.ibm.wala.")) return true;
		
		return false;
	}

	public static boolean isLibraryEntityForThread(String entityName)
	{
		if (entityName.startsWith("java.lang.Thread.")) return true;
		if (entityName.startsWith("java.lang.ThreadGroup.")) return true;
	
		return false;
	}

	public static boolean isThreadStartMethod(String entityName)
	{
		if (entityName.startsWith("java.lang.Thread.start()")) return true;
		
		return false;
	}
	
}
