package jpf.analysis;

import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;

import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.impl.Everywhere;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ipa.cfg.ExplodedInterproceduralCFG;
import com.ibm.wala.ipa.cfg.BasicBlockInContext;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAFieldAccessInstruction;
import com.ibm.wala.ssa.SSAGetInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.ssa.SSAReturnInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SSAArrayStoreInstruction;
import com.ibm.wala.ssa.analysis.ExplodedControlFlowGraph;
import com.ibm.wala.ssa.analysis.IExplodedBasicBlock;
import com.ibm.wala.util.intset.BitVector;
import com.ibm.wala.util.intset.OrdinalSetMapping;
import com.ibm.wala.util.intset.MutableMapping;
import com.ibm.wala.util.intset.IntSet; 
import com.ibm.wala.util.intset.IntIterator;
import com.ibm.wala.util.graph.impl.GraphInverter;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.UnaryOperator;
import com.ibm.wala.dataflow.graph.BitVectorSolver;
import com.ibm.wala.dataflow.graph.BitVectorFramework;
import com.ibm.wala.dataflow.graph.AbstractMeetOperator;
import com.ibm.wala.dataflow.graph.BitVectorKillGen;
import com.ibm.wala.dataflow.graph.BitVectorUnion;
import com.ibm.wala.dataflow.graph.BitVectorIdentity;
import com.ibm.wala.examples.analysis.dataflow.BitVectorKillAll;
import com.ibm.wala.dataflow.graph.ITransferFunctionProvider;


public class FieldAccessManager
{
	private static final boolean INFO = false;
	private static final boolean DEBUG = false;
	
	
	// map from program point (method signature and instruction position) to the set of field read accesses (getfield, getstatic)
	private static Map<ProgramPoint, Set<FieldName>> progpoint2fieldreads; 
	
	// map from program point (method signature and instruction position) to the set of field write accesses (putfield, putstatic)
	private static Map<ProgramPoint, Set<FieldName>> progpoint2fieldwrites;
	
	// the set of all fields read in the program
	private static Set<FieldName> wholeprogFieldsRead;
	
	// the set of all fields written-to in the program
	private static Set<FieldName> wholeprogFieldsWritten;
	
	// fields stored in these sets are always considered as read/written
	public static Set<FieldName> nativeFieldReads;
	public static Set<FieldName> nativeFieldWrites;

	// map from class name to a flag saying whether "this" object may escape from any constructor before it finishes (default false)
	public static Map<String, Boolean> clsname2thisobjescapes;
	
	// map from field name to a flag saying whether it is written-to only for "this" object and in its constructor (default true)
	public static Map<String, Boolean> fieldname2writteninit;
	
	// true -> thread call stacks must be used for getting the correct result 
	public static boolean useThreadCallStacks;

	// true -> propagate only variable names (instead of full points-to sets)
	public static boolean useVariableNamesOnly;
	
	public static boolean usePointerAnalysis; 
	public static boolean useDemandDrivenPA;
	
	public static boolean useEscapeAnalysis;
	
	public static boolean useImmutableFields;
	

	static
	{
		progpoint2fieldreads = new LinkedHashMap<ProgramPoint, Set<FieldName>>();
		progpoint2fieldwrites = new LinkedHashMap<ProgramPoint, Set<FieldName>>();
		
		wholeprogFieldsRead = new HashSet<FieldName>();
		wholeprogFieldsWritten = new HashSet<FieldName>();
		
		nativeFieldReads = new HashSet<FieldName>();
		nativeFieldWrites = new HashSet<FieldName>();
		
		nativeFieldReads.add(new FieldName("java.io.File", "filename"));
		nativeFieldReads.add(new FieldName("java.io.FileDescriptor", "fd"));
		nativeFieldReads.add(new FieldName("java.io.FileDescriptor", "off"));
		nativeFieldReads.add(new FieldName("java.io.FileDescriptor", "mode"));
		nativeFieldReads.add(new FieldName("java.io.FileDescriptor", "fileName"));
		nativeFieldReads.add(new FieldName("java.io.FileDescriptor", "state"));
		nativeFieldReads.add(new FieldName("java.io.OutputStreamWriter", "value"));
		nativeFieldReads.add(new FieldName("java.io.RandomAccessFile", "filename"));
		nativeFieldReads.add(new FieldName("java.io.RandomAccessFile", "data_root"));
		nativeFieldReads.add(new FieldName("java.io.RandomAccessFile", "currentPosition"));
		nativeFieldReads.add(new FieldName("java.io.RandomAccessFile", "currentLength"));
		nativeFieldReads.add(new FieldName("java.io.RandomAccessFile", "CHUNK_SIZE"));
		nativeFieldReads.add(new FieldName("java.io.RandomAccessFile$DataRepresentation", "data"));
		nativeFieldReads.add(new FieldName("java.io.RandomAccessFile$DataRepresentation", "next"));
		nativeFieldReads.add(new FieldName("java.io.RandomAccessFile$DataRepresentation", "chunk_index"));
		nativeFieldReads.add(new FieldName("java.lang.String", "value"));
		nativeFieldReads.add(new FieldName("java.lang.String", "hash"));
		nativeFieldReads.add(new FieldName("java.lang.StringBuffer", "value"));
		nativeFieldReads.add(new FieldName("java.lang.StringBuffer", "count"));
		nativeFieldReads.add(new FieldName("java.lang.StringBuffer", "shared"));
		nativeFieldReads.add(new FieldName("java.lang.StringBuilder", "value"));
		nativeFieldReads.add(new FieldName("java.lang.StringBuilder", "count"));		
		nativeFieldReads.add(new FieldName("java.lang.Thread", "name"));
		nativeFieldReads.add(new FieldName("java.lang.Throwable", "snapshot"));
		nativeFieldReads.add(new FieldName("java.lang.Throwable", "detailMessage"));
		nativeFieldReads.add(new FieldName("java.util.Date", "fastTime"));
		nativeFieldReads.add(new FieldName("java.util.Random", "seed"));
		
		nativeFieldWrites.add(new FieldName("java.io.File", "filename"));
		nativeFieldWrites.add(new FieldName("java.io.FileDescriptor", "off"));
		nativeFieldWrites.add(new FieldName("java.io.RandomAccessFile", "currentPosition"));
		nativeFieldWrites.add(new FieldName("java.io.RandomAccessFile", "currentLength"));
		nativeFieldWrites.add(new FieldName("java.io.RandomAccessFile", "data_root"));
		nativeFieldWrites.add(new FieldName("java.io.RandomAccessFile$DataRepresentation", "data"));
		nativeFieldWrites.add(new FieldName("java.io.RandomAccessFile$DataRepresentation", "next"));
		nativeFieldWrites.add(new FieldName("java.io.RandomAccessFile$DataRepresentation", "chunk_index"));
		nativeFieldWrites.add(new FieldName("java.lang.Integer", "value"));
		nativeFieldWrites.add(new FieldName("java.lang.Long", "value"));
		nativeFieldWrites.add(new FieldName("java.lang.Float", "value"));
		nativeFieldWrites.add(new FieldName("java.lang.Double", "value"));
		nativeFieldWrites.add(new FieldName("java.lang.Boolean", "value"));
		nativeFieldWrites.add(new FieldName("java.lang.Character", "value"));
		nativeFieldWrites.add(new FieldName("java.lang.Byte", "value"));
		nativeFieldWrites.add(new FieldName("java.lang.Short", "value"));		
		nativeFieldWrites.add(new FieldName("java.lang.String", "hash"));
		nativeFieldWrites.add(new FieldName("java.lang.StringBuffer", "value"));
		nativeFieldWrites.add(new FieldName("java.lang.StringBuffer", "count"));
		nativeFieldWrites.add(new FieldName("java.lang.StringBuffer", "shared"));
		nativeFieldWrites.add(new FieldName("java.lang.StringBuilder", "value"));
		nativeFieldWrites.add(new FieldName("java.lang.StringBuilder", "count"));
		nativeFieldWrites.add(new FieldName("java.lang.Throwable", "snapshot"));
		nativeFieldWrites.add(new FieldName("java.lang.reflect.Field", "regIdx"));
		nativeFieldWrites.add(new FieldName("java.util.Calendar", "firstDayOfWeek"));
		nativeFieldWrites.add(new FieldName("java.util.Calendar", "minimalDaysInFirstWeek"));
		nativeFieldWrites.add(new FieldName("java.util.Random", "seed"));
	
		clsname2thisobjescapes = new HashMap<String, Boolean>();
		fieldname2writteninit = new HashMap<String, Boolean>();
	
		useThreadCallStacks = false;
		useVariableNamesOnly = false;
		usePointerAnalysis = false;
		useDemandDrivenPA = false;		
		useEscapeAnalysis = false;
		useImmutableFields = false;
	}
	

	/**
	 * This method performs inter-procedural field access analysis of the whole program, whose results must be used together with the knowledge of call stack in JPF.
	 */
	public static void analyzeWholeProgUsingCallStack(CallGraph graph, AnalysisScope scope, IClassHierarchy cha, String dotFileName) throws Exception
	{
		AnalysisCache cache = new AnalysisCache();
		
		ExplodedInterproceduralCFG eicfg = ExplodedInterproceduralCFG.make(graph);

		// create the backwards-oriented control-flow graph of the method
		Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG = GraphInverter.invert(eicfg);

		/*
		if (DEBUG)
		{
			if ((dotFileName != null) && (dotFileName.length() > 0)) DotUtil.writeDotFile(bwICFG, null, "bwICFG", dotFileName);
		}
		*/
		
		
		// perform the analysis of field reads
		
		ProgFieldAccessesBR pfaReads = new ProgFieldAccessesBR(bwICFG, cha, true, false, true);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverReads = pfaReads.analyze();

		// collect analysis results: create sets of field accesses for all program points (instructions) 
		// the set for a program point will contain all field names for which there is '1' in the bit vector associated with the basic block (cfg node)
		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();

			for (int i = ebb.getFirstInstructionIndex(); i <= ebb.getLastInstructionIndex(); i++)
			{
				int insnPos = WALAUtils.getInsnBytecodeIndex(bb.getNode(), i);
			
				String fullMethodSig = WALAUtils.getCorrectMethodSignature(bb.getNode().getMethod().getSignature());
			
				ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);
			
				Set<FieldName> fieldAccesses = new HashSet<FieldName>();
			
				fieldAccesses.addAll(nativeFieldReads);
			
				IntSet out = solverReads.getOut(bb).getValue();
				if (out != null)
				{
					for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
					{
						int fieldNum = outIt.next();
					
						FieldName fn = pfaReads.getFieldNameForNum(fieldNum);
					
						if (useVariableNamesOnly)
						{
							Set<String> allocSites = AllocationSiteManager.getAllocSitesForLocalVar(fn);
						
							if (allocSites == null)
							{
								// for static variables
								fieldAccesses.add(fn);
							}
							else
							{
								for (String site : allocSites) fieldAccesses.add(new FieldName(site, fn.fieldName));
							}
						}
						else
						{
							fieldAccesses.add(fn);
						}
					}	
				}

				progpoint2fieldreads.put(pp, fieldAccesses);
				wholeprogFieldsRead.addAll(fieldAccesses);
			}
		}

				
		// perform the analysis of field writes
		
		ProgFieldAccessesBR pfaWrites = new ProgFieldAccessesBR(bwICFG, cha, false, true, true);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverWrites = pfaWrites.analyze();

		// collect analysis results: create sets of field accesses for all program points (instructions) 
		// the set for a program point will contain all field names for which there is '1' in the bit vector associated with the basic block (cfg node)
		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();
			
			for (int i = ebb.getFirstInstructionIndex(); i <= ebb.getLastInstructionIndex(); i++)
			{
				int insnPos = WALAUtils.getInsnBytecodeIndex(bb.getNode(), i);
			
				String fullMethodSig = WALAUtils.getCorrectMethodSignature(bb.getNode().getMethod().getSignature());
			
				ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);
			
				Set<FieldName> fieldAccesses = new HashSet<FieldName>();
			
				fieldAccesses.addAll(nativeFieldWrites);
				
				IntSet out = solverWrites.getOut(bb).getValue();
				if (out != null)
				{
					for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
					{
						int fieldNum = outIt.next();
					
						FieldName fn = pfaWrites.getFieldNameForNum(fieldNum);
					
						if (useVariableNamesOnly)
						{
							Set<String> allocSites = AllocationSiteManager.getAllocSitesForLocalVar(fn);
						
							if (allocSites == null)
							{
								// for static variables
								fieldAccesses.add(fn);
							}
							else
							{
								for (String site : allocSites) fieldAccesses.add(new FieldName(site, fn.fieldName));
							}
						}
						else
						{
							fieldAccesses.add(fn);
						}
					}	
				}
				
				progpoint2fieldwrites.put(pp, fieldAccesses);
				wholeprogFieldsWritten.addAll(fieldAccesses);
			}
		}
		
		useThreadCallStacks = true;
	}

	public static void analyzeWholeProgStandalone(CallGraph graph, AnalysisScope scope, IClassHierarchy cha, String dotFileName) throws Exception
	{
		AnalysisCache cache = new AnalysisCache();
		
		ExplodedInterproceduralCFG eicfg = ExplodedInterproceduralCFG.make(graph);

		// create the backwards-oriented control-flow graph of the method
		Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG = GraphInverter.invert(eicfg);

		/*
		if (DEBUG)
		{
			if ((dotFileName != null) && (dotFileName.length() > 0)) DotUtil.writeDotFile(bwICFG, null, "bwICFG", dotFileName);
		}
		*/
		
		
		// perform the analysis of field reads
		
		// first step: getting fields accessed before method return
		
		ProgFieldAccessesBR pfaReadsBR = new ProgFieldAccessesBR(bwICFG, cha, true, false, true);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverReadsBR = pfaReadsBR.analyze();

		Map<String, Set<FieldName>> mthsig2frbr = new HashMap<String, Set<FieldName>>();
		
		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();

			for (int i = ebb.getFirstInstructionIndex(); i <= ebb.getLastInstructionIndex(); i++)
			{
				//int insnPos = WALAUtils.getInsnBytecodeIndex(bb.getNode(), i);
			
				String fullMethodSig = WALAUtils.getCorrectMethodSignature(bb.getNode().getMethod().getSignature());
			
				//ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);

				Set<FieldName> fieldAccesses = mthsig2frbr.get(fullMethodSig);
				if (fieldAccesses == null)
				{
					fieldAccesses = new HashSet<FieldName>();
					mthsig2frbr.put(fullMethodSig, fieldAccesses);
				}
		
				IntSet out = solverReadsBR.getOut(bb).getValue();
				if (out != null)
				{
					for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
					{
						int fieldNum = outIt.next();
					
						FieldName fn = pfaReadsBR.getFieldNameForNum(fieldNum);
					
						fieldAccesses.add(fn);
					}	
				}
				
				//pp2frbr.put(pp, fieldAccesses);
			}
		}
		
		// second step: getting fields accessed in the rest of thread's lifetime
		
		ProgFieldAccessesRT pfaReadsRT = new ProgFieldAccessesRT(bwICFG, cha, true, false, mthsig2frbr);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverReadsRT = pfaReadsRT.analyze();

		// collect analysis results: create sets of field accesses for all program points (instructions) 
		// the set for a program point will contain all field names for which there is '1' in the bit vector associated with the basic block (cfg node)
		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();

			for (int i = ebb.getFirstInstructionIndex(); i <= ebb.getLastInstructionIndex(); i++)
			{
				int insnPos = WALAUtils.getInsnBytecodeIndex(bb.getNode(), i);
			
				String fullMethodSig = WALAUtils.getCorrectMethodSignature(bb.getNode().getMethod().getSignature());
			
				ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);

				Set<FieldName> fieldAccesses = new HashSet<FieldName>();
			
				fieldAccesses.addAll(nativeFieldReads);
			
				IntSet out = solverReadsRT.getOut(bb).getValue();
				if (out != null)
				{
					for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
					{
						int fieldNum = outIt.next();
					
						FieldName fn = pfaReadsRT.getFieldNameForNum(fieldNum);
					
						if (useVariableNamesOnly)
						{
							Set<String> allocSites = AllocationSiteManager.getAllocSitesForLocalVar(fn);
						
							if (allocSites == null)
							{
								// for static variables
								fieldAccesses.add(fn);
							}
							else
							{
								for (String site : allocSites) fieldAccesses.add(new FieldName(site, fn.fieldName));
							}
						}
						else
						{
							fieldAccesses.add(fn);
						}
					}	
				}
				
				progpoint2fieldreads.put(pp, fieldAccesses);
				wholeprogFieldsRead.addAll(fieldAccesses);
			}
		}

		
						
		// perform the analysis of field writes
				
		// first step: getting fields accessed before method return
		
		ProgFieldAccessesBR pfaWritesBR = new ProgFieldAccessesBR(bwICFG, cha, false, true, true);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverWritesBR = pfaWritesBR.analyze();

		Map<String, Set<FieldName>> mthsig2fwbr = new HashMap<String, Set<FieldName>>();
		
		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();
		
			for (int i = ebb.getFirstInstructionIndex(); i <= ebb.getLastInstructionIndex(); i++)
			{
				//int insnPos = WALAUtils.getInsnBytecodeIndex(bb.getNode(), i);
			
				String fullMethodSig = WALAUtils.getCorrectMethodSignature(bb.getNode().getMethod().getSignature());
			
				//ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);

				Set<FieldName> fieldAccesses = mthsig2fwbr.get(fullMethodSig);
				if (fieldAccesses == null)
				{
					fieldAccesses = new HashSet<FieldName>();
					mthsig2fwbr.put(fullMethodSig, fieldAccesses);
				}

				IntSet out = solverWritesBR.getOut(bb).getValue();
				if (out != null)
				{
					for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
					{
						int fieldNum = outIt.next();
					
						FieldName fn = pfaWritesBR.getFieldNameForNum(fieldNum);
					
						fieldAccesses.add(fn);
					}	
				}
				
				//pp2fwbr.put(pp, fieldAccesses);
			}
		}
		
		// second step: getting fields accessed in the rest of thread's lifetime
		
		ProgFieldAccessesRT pfaWritesRT = new ProgFieldAccessesRT(bwICFG, cha, false, true, mthsig2fwbr);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverWritesRT = pfaWritesRT.analyze();

		// collect analysis results: create sets of field accesses for all program points (instructions) 
		// the set for a program point will contain all field names for which there is '1' in the bit vector associated with the basic block (cfg node)
		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();

			for (int i = ebb.getFirstInstructionIndex(); i <= ebb.getLastInstructionIndex(); i++)
			{
				int insnPos = WALAUtils.getInsnBytecodeIndex(bb.getNode(), i);
			
				String fullMethodSig = WALAUtils.getCorrectMethodSignature(bb.getNode().getMethod().getSignature());
		
				ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);
			
				Set<FieldName> fieldAccesses = new HashSet<FieldName>();
			
				fieldAccesses.addAll(nativeFieldWrites);
			
				IntSet out = solverWritesRT.getOut(bb).getValue();
				if (out != null)
				{
					for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
					{
						int fieldNum = outIt.next();
					
						FieldName fn = pfaWritesRT.getFieldNameForNum(fieldNum);
					
						if (useVariableNamesOnly)
						{
							Set<String> allocSites = AllocationSiteManager.getAllocSitesForLocalVar(fn);
						
							if (allocSites == null)
							{
								// for static variables
								fieldAccesses.add(fn);
							}
							else
							{
								for (String site : allocSites) fieldAccesses.add(new FieldName(site, fn.fieldName));
							}
						}
						else
						{
							fieldAccesses.add(fn);
						}
					}	
				}
				
				progpoint2fieldwrites.put(pp, fieldAccesses);
				wholeprogFieldsWritten.addAll(fieldAccesses);
			}
		}
		
		useThreadCallStacks = false;
	}
	
	public static void analyzeWholeProgInOnePhase(CallGraph graph, AnalysisScope scope, IClassHierarchy cha, String dotFileName) throws Exception
	{
		AnalysisCache cache = new AnalysisCache();
		
		ExplodedInterproceduralCFG eicfg = ExplodedInterproceduralCFG.make(graph);

		// create the backwards-oriented control-flow graph of the method
		Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG = GraphInverter.invert(eicfg);

		/*
		if (DEBUG)
		{
			if ((dotFileName != null) && (dotFileName.length() > 0)) DotUtil.writeDotFile(bwICFG, null, "bwICFG", dotFileName);
		}
		*/
		
		
		// perform the analysis of field reads
		
		ProgFieldAccessesOP pfaReads = new ProgFieldAccessesOP(bwICFG, cha, true, false);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverReads = pfaReads.analyze();

		// collect analysis results: create sets of field accesses for all program points (instructions) 
		// the set for a program point will contain all field names for which there is '1' in the bit vector associated with the basic block (cfg node)
		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();

			for (int i = ebb.getFirstInstructionIndex(); i <= ebb.getLastInstructionIndex(); i++)
			{
				int insnPos = WALAUtils.getInsnBytecodeIndex(bb.getNode(), i);
			
				String fullMethodSig = WALAUtils.getCorrectMethodSignature(bb.getNode().getMethod().getSignature());
			
				ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);
			
				Set<FieldName> fieldAccesses = new HashSet<FieldName>();
			
				fieldAccesses.addAll(nativeFieldReads);
			
				IntSet out = solverReads.getOut(bb).getValue();
				if (out != null)
				{
					for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
					{
						int fieldNum = outIt.next();
					
						FieldName fn = pfaReads.getFieldNameForNum(fieldNum);
					
						if (useVariableNamesOnly)
						{
							Set<String> allocSites = AllocationSiteManager.getAllocSitesForLocalVar(fn);
						
							if (allocSites == null)
							{
								// for static variables
								fieldAccesses.add(fn);
							}
							else
							{
								for (String site : allocSites) fieldAccesses.add(new FieldName(site, fn.fieldName));
							}
						}
						else
						{
							fieldAccesses.add(fn);
						}
					}	
				}

				progpoint2fieldreads.put(pp, fieldAccesses);
				wholeprogFieldsRead.addAll(fieldAccesses);
			}
		}

				
		// perform the analysis of field writes
		
		ProgFieldAccessesOP pfaWrites = new ProgFieldAccessesOP(bwICFG, cha, false, true);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverWrites = pfaWrites.analyze();

		// collect analysis results: create sets of field accesses for all program points (instructions) 
		// the set for a program point will contain all field names for which there is '1' in the bit vector associated with the basic block (cfg node)
		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();
			
			for (int i = ebb.getFirstInstructionIndex(); i <= ebb.getLastInstructionIndex(); i++)
			{
				int insnPos = WALAUtils.getInsnBytecodeIndex(bb.getNode(), i);
			
				String fullMethodSig = WALAUtils.getCorrectMethodSignature(bb.getNode().getMethod().getSignature());
			
				ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);
			
				Set<FieldName> fieldAccesses = new HashSet<FieldName>();
			
				fieldAccesses.addAll(nativeFieldWrites);
				
				IntSet out = solverWrites.getOut(bb).getValue();
				if (out != null)
				{
					for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
					{
						int fieldNum = outIt.next();
					
						FieldName fn = pfaWrites.getFieldNameForNum(fieldNum);
					
						if (useVariableNamesOnly)
						{
							Set<String> allocSites = AllocationSiteManager.getAllocSitesForLocalVar(fn);
						
							if (allocSites == null)
							{
								// for static variables
								fieldAccesses.add(fn);
							}
							else
							{
								for (String site : allocSites) fieldAccesses.add(new FieldName(site, fn.fieldName));
							}
						}
						else
						{
							fieldAccesses.add(fn);
						}
					}	
				}
				
				progpoint2fieldwrites.put(pp, fieldAccesses);
				wholeprogFieldsWritten.addAll(fieldAccesses);
			}
		}
		
		useThreadCallStacks = false;
	}


	/**
	 * This method performs intra-procedural field access analysis.
	 */
	public static void analyzeEachMethod(String mainClassName, AnalysisScope scope, IClassHierarchy cha) throws Exception
	{
		AnalysisCache cache = new AnalysisCache();
		
		// analyze each method in each class
		for (Iterator<IClass> clsIt = cha.iterator(); clsIt.hasNext(); )
		{
			IClass cls = clsIt.next();
			
			String className = WALAUtils.getClassName(cls);
			
			if (DEBUG) System.out.println("[DEBUG FieldAccessManager.analyzeEachMethod] processing class: " + className);
			
			Collection<IMethod> methods = cls.getDeclaredMethods();
			
			for (IMethod mth : methods)
			{
				// we skip native methods and abstract methods
				if (mth.isNative() || mth.isAbstract()) continue;
				
				String fullMethodSig = WALAUtils.getCorrectMethodSignature(mth.getReference().getSignature());
		
				if (DEBUG) System.out.println("[DEBUG FieldAccessManager.analyzeEachMethod] processing method: " + fullMethodSig);
				
				IR ir = cache.getIRFactory().makeIR(mth, Everywhere.EVERYWHERE, SSAOptions.defaultOptions());

				// create the backwards-oriented control-flow graph of the method
				ExplodedControlFlowGraph ecfg = ExplodedControlFlowGraph.make(ir);
				Graph<IExplodedBasicBlock> bwCFG = GraphInverter.invert(ecfg);
				
				
				// perform the analysis of field reads
				
				MethodFieldAccesses mfaReads = new MethodFieldAccesses(null, bwCFG, ir, cha, true, false);
				BitVectorSolver<IExplodedBasicBlock> solverReads = mfaReads.analyze();

				// collect analysis results: create sets of field accesses for all program points (instructions) 
				// the set for a program point will contain all field names for which there is '1' in the bit vector associated with the basic block (cfg node)
				for (IExplodedBasicBlock ebb : bwCFG) 
				{
					for (int i = ebb.getFirstInstructionIndex(); i <= ebb.getLastInstructionIndex(); i++)
					{		
						int insnPos = WALAUtils.getInsnBytecodeIndex(mth, i);
					
						ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);
					
						Set<FieldName> fieldAccesses = new HashSet<FieldName>();
						
						fieldAccesses.addAll(nativeFieldReads);
					
						IntSet out = solverReads.getOut(ebb).getValue();
						if (out != null)
						{
							for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
							{
								int fieldNum = outIt.next();
							
								FieldName fn = mfaReads.getFieldNameForNum(fieldNum);
							
								if (useVariableNamesOnly)
								{
									Set<String> allocSites = AllocationSiteManager.getAllocSitesForLocalVar(fn);
									
									if (allocSites == null)
									{
										// for static variables
										fieldAccesses.add(fn);
									}
									else
									{
										for (String site : allocSites) fieldAccesses.add(new FieldName(site, fn.fieldName));
									}
								}
								else
								{
									fieldAccesses.add(fn);
								}
							}	
						}
						
						progpoint2fieldreads.put(pp, fieldAccesses);
						wholeprogFieldsRead.addAll(fieldAccesses);
					}
				}
				
				
				// perform the analysis of field writes
				
				MethodFieldAccesses mfaWrites = new MethodFieldAccesses(null, bwCFG, ir, cha, false, true);
				BitVectorSolver<IExplodedBasicBlock> solverWrites = mfaWrites.analyze();

				// collect analysis results: create sets of field accesses for all program points (instructions) 
				// the set for a program point will contain all field names for which there is '1' in the bit vector associated with the basic block (cfg node)
				for (IExplodedBasicBlock ebb : bwCFG) 
				{
					for (int i = ebb.getFirstInstructionIndex(); i <= ebb.getLastInstructionIndex(); i++)
					{		
						int insnPos = WALAUtils.getInsnBytecodeIndex(mth, i);
					
						ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);
					
						Set<FieldName> fieldAccesses = new HashSet<FieldName>();
						
						fieldAccesses.addAll(nativeFieldWrites);
					
						IntSet out = solverWrites.getOut(ebb).getValue();
						if (out != null)
						{
							for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
							{
								int fieldNum = outIt.next();
							
								FieldName fn = mfaWrites.getFieldNameForNum(fieldNum);
							
								if (useVariableNamesOnly)
								{
									Set<String> allocSites = AllocationSiteManager.getAllocSitesForLocalVar(fn);
								
									if (allocSites == null)
									{
										// for static variables
										fieldAccesses.add(fn);
									}
									else
									{
										for (String site : allocSites) fieldAccesses.add(new FieldName(site, fn.fieldName));
									}
								}
								else
								{
									fieldAccesses.add(fn);
								}
							}	
						}
						
						progpoint2fieldwrites.put(pp, fieldAccesses);
						wholeprogFieldsWritten.addAll(fieldAccesses);
					}
				}
			}
		}
	}
	
	public static void printFieldAccessSets(boolean firstInsnOnly)
	{
		System.out.println("FIELD ACCESSES");
		System.out.println("==============");
		
		// the key set is same for both reads and writes
		Set<ProgramPoint> progPoints = progpoint2fieldreads.keySet();
		
		for (ProgramPoint pp : progPoints)
		{
			if (firstInsnOnly && (pp.getInsnPos() != 0)) continue;
			
			Set<FieldName> fieldReads = progpoint2fieldreads.get(pp);
			Set<FieldName> fieldWrites = progpoint2fieldwrites.get(pp);			

			Set<FieldName> fieldAccesses = new HashSet<FieldName>();
			fieldAccesses.addAll(fieldReads);
			fieldAccesses.addAll(fieldWrites);
			
			System.out.println(pp.getMethodSig() + ":" + pp.getInsnPos());
			
			for (FieldName fa : fieldAccesses)
			{
				if (fieldReads.contains(fa) && fieldWrites.contains(fa)) System.out.println("\t both:" + fa.toString());
				else if (fieldReads.contains(fa)) System.out.println("\t read:" + fa.toString());
				else if (fieldWrites.contains(fa)) System.out.println("\t write:" + fa.toString());
			}
		}
	}

	public static Set<FieldName> getMergedFieldReadsForProgramPoints(List<ProgramPoint> progPoints)
	{
		Set<FieldName> fieldReads = new HashSet<FieldName>();

		for (int i = 0; i < progPoints.size(); i++)
		{
			ProgramPoint pp = progPoints.get(i);

			// ignore synthetic methods
			if (pp == ProgramPoint.INVALID) continue;
				
			// we want to print warning only for the last program point which is the actual field access (all other are method calls)
			Set<FieldName> ppFieldReads = getFieldReadsForProgramPoint(pp, i == (progPoints.size() - 1));
			
			// conservative solution: program point was not analyzed, so we return a safe value
			if (ppFieldReads == null) return null;
			
			fieldReads.addAll(ppFieldReads);
		}
		
		return fieldReads;		
	}
	
	public static Set<FieldName> getFieldReadsForProgramPoint(ProgramPoint pp, boolean printWarning)
	{
		Set<FieldName> ppFieldReads = progpoint2fieldreads.get(pp);
					
		if (ppFieldReads != null) return ppFieldReads;
		
		if (pp != ProgramPoint.INVALID)
		{
			// there are some analysis results for the program point, but no field read accesses
			if (progpoint2fieldwrites.containsKey(pp)) return new HashSet<FieldName>();

			// print "missing analysis results warning" only if there are no data for the program point
			if ( ! pp.getMethodSig().startsWith("[VM]") )
			{
				if (INFO && printWarning) System.out.println("[WARNING] missing analysis results for program point \"" + pp.getMethodSig() + ":" + pp.getInsnPos() + "\"");
			}
		}
		
		return null; // safe value (we don't know)
	}
	
	public static Set<FieldName> getMergedFieldWritesForProgramPoints(List<ProgramPoint> progPoints)
	{
		Set<FieldName> fieldWrites = new HashSet<FieldName>();

		for (int i = 0; i < progPoints.size(); i++)
		{
			ProgramPoint pp = progPoints.get(i);
			
			// ignore synthetic methods
			if (pp == ProgramPoint.INVALID) continue;

			// we want to print warning only for the last program point which is the actual field access (all other are method calls)
			Set<FieldName> ppFieldWrites = getFieldWritesForProgramPoint(pp, i == (progPoints.size() - 1));
			
			// conservative solution: program point was not analyzed, so we return a safe value
			if (ppFieldWrites == null) return null;
			
			fieldWrites.addAll(ppFieldWrites);
		}
		
		return fieldWrites;		
	}
	
	public static Set<FieldName> getFieldWritesForProgramPoint(ProgramPoint pp, boolean printWarning)
	{
		Set<FieldName> ppFieldWrites = progpoint2fieldwrites.get(pp);
					
		if (ppFieldWrites != null) return ppFieldWrites;

		if (pp != ProgramPoint.INVALID)
		{
			// there are some analysis results for the program point, but no field write accesses
			if (progpoint2fieldreads.containsKey(pp)) return new HashSet<FieldName>();

			// print "missing analysis results warning" only if there are no data for the program point
			if ( ! pp.getMethodSig().startsWith("[VM]") )
			{
				if (INFO && printWarning) System.out.println("[WARNING] missing analysis results for program point \"" + pp.getMethodSig() + ":" + pp.getInsnPos() + "\"");
			}
		}
		
		return null; // safe value (we don't know)
	}


	static class FieldAccessesBase
	{
		protected IClassHierarchy cha;
		
		protected boolean considerReads;
		protected boolean considerWrites;
			
		// mapping between field names and integer numbers (used in bitvectors)
		protected OrdinalSetMapping<FieldName> fieldsNumbering;

		protected FieldAccessesBase(IClassHierarchy cha, boolean reads, boolean writes)
		{
			this.cha = cha;
			
			this.considerReads = reads;
			this.considerWrites = writes;
		}

		public Set<FieldName> getFieldNamesFromInsn(CGNode mthNode, SSAFieldAccessInstruction faInsn, int ssaInsnIndex)
		{
			Set<FieldName> fieldNames = new HashSet<FieldName>();
			
			try
			{
				String fieldName = faInsn.getDeclaredField().getName().toUnicodeString();

				String className = WALAUtils.getFieldsClassName(faInsn.getDeclaredField(), cha);
				
				String methodSig = WALAUtils.getCorrectMethodSignature(mthNode.getMethod().getSignature());
				
				if (DEBUG) System.out.println("[DEBUG FieldAccessManager.getFieldNamesFromInsn] className = " + className + ", fieldName = " + fieldName + ", mthSignature = " + methodSig + ", ssaInsnIndex = " + ssaInsnIndex + ", localVarNo = " + faInsn.getRef());
				
				if (usePointerAnalysis && ( ! useDemandDrivenPA ))
				{
					if (useVariableNamesOnly)
					{						
						if (faInsn.isStatic()) fieldNames.add(new FieldName(className, fieldName)); // for static variables
						else fieldNames.add(new FieldName("", fieldName, methodSig, faInsn.getRef())); // for instance fields
						
						//WALAUtils.storeLocalVarNumberForMethodInsn(methodSig, insnPos, faInsn.getRef());
					}
					else
					{						
						Set<String> allocSites = AllocationSiteManager.getAllocSitesForLocalVar(methodSig, faInsn.getRef());
						//Set<String> allocSites = AllocationSiteManager.getAllocSitesForClass(className);
												
						if (allocSites == null)
						{
							fieldNames.add(new FieldName(className, fieldName));
						}
						else
						{
							for (String site : allocSites) fieldNames.add(new FieldName(site, fieldName));
						}
					}
				}
				else if (usePointerAnalysis && useDemandDrivenPA)
				{
					FieldName fn = new FieldName(className, fieldName);
					
					if ( ! faInsn.isStatic() )
					{
						fn.methodSig = methodSig;
						fn.localVarNo = faInsn.getRef(); // local variable pointing to the object instance
					}
					
					fieldNames.add(fn);
				}
				else
				{
					fieldNames.add(new FieldName(className, fieldName));
				}
			}
			catch (Exception ex) 
			{
				ex.printStackTrace(); 
			}
			
			return fieldNames;
		}
		
		public FieldName getFieldNameForNum(int fieldNum)
		{
			return fieldsNumbering.getMappedObject(fieldNum);
		}
	}

	
	static class ProgFieldAccessesBR extends FieldAccessesBase 
	{
		private Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG;
		
		private boolean withCallStackUsage;
		

		public ProgFieldAccessesBR(Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG, IClassHierarchy cha, boolean reads, boolean writes, boolean withCallStackUsage)
		{
			super(cha, reads, writes);
			
			this.bwICFG = bwICFG;
			
			this.withCallStackUsage = withCallStackUsage;
			
			this.fieldsNumbering = createFieldAccessInstrMapping();
		}
		
		private OrdinalSetMapping<FieldName> createFieldAccessInstrMapping()
		{
			if (DEBUG)
			{
				// print IR for each method in given program
				
				Set<String> printedMethods = new HashSet<String>();
				
				for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
				{
					CGNode node = bb.getNode();
					
					String methodSig = WALAUtils.getCorrectMethodSignature(node.getMethod().getSignature());
					
					if (printedMethods.contains(methodSig)) continue;
					
					IR methodIR = node.getIR();
					
					if (methodIR == null) continue;
					
					System.out.println("[DEBUG ProgFieldAccessesBR.createFieldAccessInstrMapping] method signature = " + methodSig);
	   
					SSAInstruction[] instructions = methodIR.getInstructions();
					for (int i = 0; i < instructions.length; i++)
					{
						if (instructions[i] == null) System.out.println("[DEBUG ProgFieldAccessesBR.createFieldAccessInstrMapping] \t instruction " + i + ": null");
						else System.out.println("[DEBUG ProgFieldAccessesBR.createFieldAccessInstrMapping] \t instruction " + i + ": " + instructions[i].toString());
					}
					
					printedMethods.add(methodSig);
				}
			}

			
			MutableMapping<FieldName> fieldNums = new MutableMapping<FieldName>(new FieldName[1]);
		
			for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
			{
				CGNode node = bb.getNode();
				
				IR methodIR = node.getIR();
				
				if (methodIR == null) continue;
   
				SSAInstruction[] instructions = methodIR.getInstructions();
				for (int i = 0; i < instructions.length; i++) 
				{
					SSAInstruction instr = instructions[i];
					if (instr instanceof SSAFieldAccessInstruction) 
					{
						SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) instr;
						
						try
						{
							Set<FieldName> fieldNames = getFieldNamesFromInsn(node, faInsn, i);
							for (FieldName fn : fieldNames) fieldNums.add(fn);
						}
						catch (Exception ex) { ex.printStackTrace(); }						
					}
				}
			}
			
			return fieldNums;
		}
		
		class TransferFunctionsPFABR implements ITransferFunctionProvider<BasicBlockInContext<IExplodedBasicBlock>, BitVectorVariable> 
		{
			public AbstractMeetOperator<BitVectorVariable> getMeetOperator() 
			{
				return BitVectorUnion.instance();
			}

			public UnaryOperator<BitVectorVariable> getEdgeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> src, BasicBlockInContext<IExplodedBasicBlock> dst) 
			{
				// we do not propagate through exit-to-return edges in the variant with call stack usage
				if (withCallStackUsage && dst.getDelegate().isExitBlock()) 
				{
					if (DEBUG) System.out.println("[DEBUG FieldAccessManager.TransferFunctionsPFA] processing backwards exit-to-return edge from " + src + " to " + dst);
					
					return BitVectorKillAll.instance();
				}
				else
				{
					return BitVectorIdentity.instance();
				}
			}
			
	
			public UnaryOperator<BitVectorVariable> getNodeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> bb) 
			{
				IExplodedBasicBlock ebb = bb.getDelegate();
				
				SSAInstruction instr = ebb.getInstruction();

				int instrIndex = bb.getFirstInstructionIndex();
				
				/*
				if (DEBUG)
				{
					try
					{
						int insnPos = ebb.getFirstInstructionIndex();
						String fullMethodName = getFullMethodNameFromCGNode(bb.getNode());
						System.out.println("[DEBUG FieldAccessManager.TransferFunctionsPFABR] processing node for " + fullMethodName + ":" + insnPos);
					}
					catch (Exception ex) {}
				}
				*/
				
				if (instr instanceof SSAFieldAccessInstruction) 
				{
					SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) instr;
					
					if ((considerReads && (faInsn instanceof SSAGetInstruction)) || (considerWrites && (faInsn instanceof SSAPutInstruction)))
					{
						// this must be empty -> we do not need to kill anything
						BitVector kill = new BitVector();

						BitVector gen = new BitVector();

						Set<FieldName> fieldNames = getFieldNamesFromInsn(bb.getNode(), faInsn, instrIndex);							

						try
						{
							for (FieldName fn : fieldNames)
							{
								if (DEBUG) 
								{
									System.out.print("[DEBUG FieldAccessManager.TransferFunctionsPFABR] ");
									System.out.print("detected");
									if (faInsn instanceof SSAGetInstruction) System.out.print(" read ");
									if (faInsn instanceof SSAPutInstruction) System.out.print(" write ");
									String methodSig = WALAUtils.getCorrectMethodSignature(bb.getNode().getMethod().getSignature());
									System.out.println("access to field \"" + fn.toString() + "\" at " + methodSig + ":" + instrIndex);
								}
									
								int fieldNum = fieldsNumbering.getMappedIndex(fn);
								
								gen.set(fieldNum);
							}							
						
							if (useEscapeAnalysis)
							{								
								if (faInsn instanceof SSAPutInstruction)
								{
									if (WALAUtils.isMethodConstructor(bb.getNode()))
									{
										// check stores of "this" to some field of existing objects in the constructor
									
										SSAPutInstruction putInsn = (SSAPutInstruction) faInsn;
										
										String mthClassName = WALAUtils.getClassNameFromCGNode(bb.getNode());
										
										if (faInsn.getDeclaredField().getFieldType().isReferenceType() && (putInsn.getVal() == 1)) clsname2thisobjescapes.put(mthClassName, true);
									}
									
									// check writes to the field only in constructor and on "this" object
									
									String tgtClassName = WALAUtils.getFieldsClassName(faInsn.getDeclaredField(), cha);
									
									for (FieldName fn : fieldNames)
									{
										if ( ( ! WALAUtils.isMethodConstructor(bb.getNode()) ) || ( faInsn.getRef() != 1 ) ) fieldname2writteninit.put(tgtClassName + "." + fn.fieldName, false);
									}
								}								
							}
						}
						catch (Exception ex) { ex.printStackTrace(); }
						
						return new BitVectorKillGen(kill, gen);
					}
					else 
					{
						// identity function for all other cases
						return BitVectorIdentity.instance();
					}
				}
				else if (useEscapeAnalysis && (instr instanceof SSAArrayStoreInstruction))
				{
					// check stores of "this" to some field of existing objects in the constructor

					SSAArrayStoreInstruction asInsn = (SSAArrayStoreInstruction) instr;
					
					try
					{
						if (WALAUtils.isMethodConstructor(bb.getNode()))
						{
							String mthClassName = WALAUtils.getClassNameFromCGNode(bb.getNode());
							
							if (asInsn.getElementType().isReferenceType() && (asInsn.getValue() == 1)) clsname2thisobjescapes.put(mthClassName, true);
						}
					}
					catch (Exception ex) { ex.printStackTrace(); }
					
					// we do not change bit vectors here
					return BitVectorIdentity.instance();
				}
				else if (useEscapeAnalysis && (instr instanceof SSAInvokeInstruction))
				{
					// check method calls on "this" (with "this" as receiver) or calls where "this" is given as parameter

					SSAInvokeInstruction invokeInsn = (SSAInvokeInstruction) instr;
					
					try
					{
						if (WALAUtils.isMethodConstructor(bb.getNode()))
						{
							String mthClassName = WALAUtils.getClassNameFromCGNode(bb.getNode());
						
							if (( ! invokeInsn.isStatic() ) && (invokeInsn.getReceiver() == 1)) clsname2thisobjescapes.put(mthClassName, true);
							
							for (int i = 0; i < invokeInsn.getNumberOfUses(); i++)
							{
								try
								{
									if (invokeInsn.getUse(i) == 1) clsname2thisobjescapes.put(mthClassName, true);
								}
								catch (ArrayIndexOutOfBoundsException ex2) {}
							}
						}
					}
					catch (Exception ex) { ex.printStackTrace(); }
					
					// we do not change bit vectors here
					return BitVectorIdentity.instance();
				}				
				else
				{
					// identity function for all other instructions
					return BitVectorIdentity.instance();
				}
			}
			
			public boolean hasEdgeTransferFunctions() 
			{
				return true;
			}
			
			public boolean hasNodeTransferFunctions() 
			{
				return true;
			}
		}
	
		public BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> analyze() 
		{
			BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, FieldName> framework = new BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, FieldName>(bwICFG, new TransferFunctionsPFABR(), fieldsNumbering);
			
			BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solver = new BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>>(framework);

			try
			{
				solver.solve(null);
			}
			catch (Exception e) {}
			
			return solver;
		}
	}

	
	static class ProgFieldAccessesRT extends FieldAccessesBase 
	{
		private Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG;
		
		private Map<String, Set<FieldName>> mthsig2fabr;
		

		public ProgFieldAccessesRT(Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG, IClassHierarchy cha, boolean reads, boolean writes, Map<String, Set<FieldName>> mthsig2fabr)
		{
			super(cha, reads, writes);
			
			this.bwICFG = bwICFG;
			
			this.mthsig2fabr = mthsig2fabr;
			
			this.fieldsNumbering = createFieldAccessInstrMapping();
		}
		
		private OrdinalSetMapping<FieldName> createFieldAccessInstrMapping()
		{
			MutableMapping<FieldName> fieldNums = new MutableMapping<FieldName>(new FieldName[1]);
		
			for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
			{
				CGNode node = bb.getNode();
				
				IR methodIR = node.getIR();
				
				if (methodIR == null) continue;
   
				SSAInstruction[] instructions = methodIR.getInstructions();
				for (int i = 0; i < instructions.length; i++) 
				{
					SSAInstruction instr = instructions[i];
					if (instr instanceof SSAFieldAccessInstruction) 
					{
						SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) instr;
						
						try
						{
							Set<FieldName> fieldNames = getFieldNamesFromInsn(node, faInsn, i);
							for (FieldName fn : fieldNames) fieldNums.add(fn);
						}
						catch (Exception ex) { ex.printStackTrace(); }						
					}
				}
			}
			
			return fieldNums;
		}
		
		class TransferFunctionsPFART implements ITransferFunctionProvider<BasicBlockInContext<IExplodedBasicBlock>, BitVectorVariable> 
		{
			public AbstractMeetOperator<BitVectorVariable> getMeetOperator() 
			{
				return BitVectorUnion.instance();
			}

			public UnaryOperator<BitVectorVariable> getEdgeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> src, BasicBlockInContext<IExplodedBasicBlock> dst) 
			{
				if (dst.getDelegate().isExitBlock()) 
				{
					// return-to-exit edges propagate identical set

					return BitVectorIdentity.instance();
				}
				else if (src.getDelegate().isEntryBlock())
				{
					// call-to-entry edges propagate only results from the first phase

					// we need to kill everything except results from first phase

					BitVector kill = new BitVector(fieldsNumbering.getSize()+1);
					kill.setAll();
					kill.clear(0); 
					
					BitVector gen = new BitVector();

					try
					{
						IExplodedBasicBlock ebb = src.getDelegate();
	
						//int insnPos = WALAUtils.getInsnBytecodeIndex(src.getNode(), ebb.getFirstInstructionIndex());
						
						String fullMethodSig = WALAUtils.getCorrectMethodSignature(src.getNode().getMethod().getSignature());
						
						//ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);
	
						Set<FieldName> phaseOneResult = mthsig2fabr.get(fullMethodSig);					
						if (phaseOneResult == null) 
						{
							if (DEBUG)
							{
								System.out.print("[DEBUG FieldAccessManager.TransferFunctionsPFART] ");
								System.out.println("null phase one result for method \"" + fullMethodSig + "\""); // + ":" + pp.getInsnPos() + "\"");
							}
				
							phaseOneResult = new HashSet<FieldName>();
						}

						if (DEBUG)
						{
							if (phaseOneResult.isEmpty())
							{
								System.out.print("[DEBUG FieldAccessManager.TransferFunctionsPFART] ");
								System.out.println("empty phase one result for method \"" + fullMethodSig + "\""); // + ":" + pp.getInsnPos() + "\"");
							}
						}
						
						for (FieldName fn : phaseOneResult)
						{
							if (DEBUG) 
							{
								System.out.print("[DEBUG FieldAccessManager.TransferFunctionsPFART] ");
								System.out.println("propagating access to field \"" + fn.toString() + "\" from phase one");
							}
								
							int fieldNum = fieldsNumbering.getMappedIndex(fn);
							
							gen.set(fieldNum);
							kill.clear(fieldNum);
						}
					}
					catch (Exception ex) { ex.printStackTrace(); }
					
					return new BitVectorKillGen(kill, gen);
				}
				else
				{
					return BitVectorIdentity.instance();
				}
			}
			
	
			public UnaryOperator<BitVectorVariable> getNodeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> bb) 
			{
				IExplodedBasicBlock ebb = bb.getDelegate();
				
				SSAInstruction instr = ebb.getInstruction();

				int instrIndex = bb.getFirstInstructionIndex();
				
				if (instr instanceof SSAFieldAccessInstruction) 
				{
					SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) instr;
					
					if ((considerReads && (faInsn instanceof SSAGetInstruction)) || (considerWrites && (faInsn instanceof SSAPutInstruction)))
					{
						// this must be empty -> we do not need to kill anything
						BitVector kill = new BitVector();

						BitVector gen = new BitVector();

						try
						{
							Set<FieldName> fieldNames = getFieldNamesFromInsn(bb.getNode(), faInsn, instrIndex);
							
							for (FieldName fn : fieldNames)
							{
								if (DEBUG) 
								{
									System.out.print("[DEBUG FieldAccessManager.TransferFunctionsPFART] ");
									System.out.print("detected");
									if (faInsn instanceof SSAGetInstruction) System.out.print(" read ");
									if (faInsn instanceof SSAPutInstruction) System.out.print(" write ");
									String methodSig = WALAUtils.getCorrectMethodSignature(bb.getNode().getMethod().getSignature());
									System.out.println("access to field \"" + fn.toString() + "\"  at " + methodSig + ":" + instrIndex);
								}
									
								int fieldNum = fieldsNumbering.getMappedIndex(fn);
								
								gen.set(fieldNum);
							}							
						}
						catch (Exception ex) { ex.printStackTrace(); }
						
						return new BitVectorKillGen(kill, gen);
					}
					else 
					{
						// identity function for all other cases
						return BitVectorIdentity.instance();
					}
				}
				else
				{
					// identity function for all other instructions
					return BitVectorIdentity.instance();
				}
			}
			
			public boolean hasEdgeTransferFunctions() 
			{
				return true;
			}
			
			public boolean hasNodeTransferFunctions() 
			{
				return true;
			}
		}
	
		public BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> analyze() 
		{
			BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, FieldName> framework = new BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, FieldName>(bwICFG, new TransferFunctionsPFART(), fieldsNumbering);
			
			BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solver = new BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>>(framework);

			try
			{
				solver.solve(null);
			}
			catch (Exception e) {}
			
			return solver;
		}
	}
	
	
	static class ProgFieldAccessesOP extends FieldAccessesBase 
	{
		private Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG;
		

		public ProgFieldAccessesOP(Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG, IClassHierarchy cha, boolean reads, boolean writes)
		{
			super(cha, reads, writes);
			
			this.bwICFG = bwICFG;
			
			this.fieldsNumbering = createFieldAccessInstrMapping();
		}
		
		private OrdinalSetMapping<FieldName> createFieldAccessInstrMapping()
		{
			MutableMapping<FieldName> fieldNums = new MutableMapping<FieldName>(new FieldName[1]);
		
			for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
			{
				CGNode node = bb.getNode();
				
				IR methodIR = node.getIR();
				
				if (methodIR == null) continue;
   
				SSAInstruction[] instructions = methodIR.getInstructions();
				for (int i = 0; i < instructions.length; i++) 
				{
					SSAInstruction instr = instructions[i];
					if (instr instanceof SSAFieldAccessInstruction) 
					{
						SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) instr;
						
						try
						{
							Set<FieldName> fieldNames = getFieldNamesFromInsn(node, faInsn, i);
							for (FieldName fn : fieldNames) fieldNums.add(fn);
						}
						catch (Exception ex) { ex.printStackTrace(); }						
					}
				}
			}
			
			return fieldNums;
		}
		
		class TransferFunctionsPFAOP implements ITransferFunctionProvider<BasicBlockInContext<IExplodedBasicBlock>, BitVectorVariable> 
		{
			public AbstractMeetOperator<BitVectorVariable> getMeetOperator() 
			{
				return BitVectorUnion.instance();
			}

			public UnaryOperator<BitVectorVariable> getEdgeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> src, BasicBlockInContext<IExplodedBasicBlock> dst) 
			{
				return BitVectorIdentity.instance();
			}
			
	
			public UnaryOperator<BitVectorVariable> getNodeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> bb) 
			{
				IExplodedBasicBlock ebb = bb.getDelegate();
				
				SSAInstruction instr = ebb.getInstruction();

				int instrIndex = bb.getFirstInstructionIndex();
				
				if (instr instanceof SSAFieldAccessInstruction) 
				{
					SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) instr;
					
					if ((considerReads && (faInsn instanceof SSAGetInstruction)) || (considerWrites && (faInsn instanceof SSAPutInstruction)))
					{
						// this must be empty -> we do not need to kill anything
						BitVector kill = new BitVector();

						BitVector gen = new BitVector();

						Set<FieldName> fieldNames = getFieldNamesFromInsn(bb.getNode(), faInsn, instrIndex);							

						try
						{
							for (FieldName fn : fieldNames)
							{
								int fieldNum = fieldsNumbering.getMappedIndex(fn);
								
								gen.set(fieldNum);
							}							
						
							if (useEscapeAnalysis)
							{								
								if (faInsn instanceof SSAPutInstruction)
								{
									if (WALAUtils.isMethodConstructor(bb.getNode()))
									{
										// check stores of "this" to some field of existing objects in the constructor
									
										SSAPutInstruction putInsn = (SSAPutInstruction) faInsn;
										
										String mthClassName = WALAUtils.getClassNameFromCGNode(bb.getNode());
										
										if (faInsn.getDeclaredField().getFieldType().isReferenceType() && (putInsn.getVal() == 1)) clsname2thisobjescapes.put(mthClassName, true);
									}
									
									// check writes to the field only in constructor and on "this" object
									
									String tgtClassName = WALAUtils.getFieldsClassName(faInsn.getDeclaredField(), cha);
									
									for (FieldName fn : fieldNames)
									{
										if ( ( ! WALAUtils.isMethodConstructor(bb.getNode()) ) || ( faInsn.getRef() != 1 ) ) fieldname2writteninit.put(tgtClassName + "." + fn.fieldName, false);
									}
								}								
							}
						}
						catch (Exception ex) { ex.printStackTrace(); }
						
						return new BitVectorKillGen(kill, gen);
					}
					else 
					{
						// identity function for all other cases
						return BitVectorIdentity.instance();
					}
				}
				else if (useEscapeAnalysis && (instr instanceof SSAArrayStoreInstruction))
				{
					// check stores of "this" to some field of existing objects in the constructor

					SSAArrayStoreInstruction asInsn = (SSAArrayStoreInstruction) instr;
					
					try
					{
						if (WALAUtils.isMethodConstructor(bb.getNode()))
						{
							String mthClassName = WALAUtils.getClassNameFromCGNode(bb.getNode());
							
							if (asInsn.getElementType().isReferenceType() && (asInsn.getValue() == 1)) clsname2thisobjescapes.put(mthClassName, true);
						}
					}
					catch (Exception ex) { ex.printStackTrace(); }
					
					// we do not change bit vectors here
					return BitVectorIdentity.instance();
				}
				else if (useEscapeAnalysis && (instr instanceof SSAInvokeInstruction))
				{
					// check method calls on "this" (with "this" as receiver) or calls where "this" is given as parameter

					SSAInvokeInstruction invokeInsn = (SSAInvokeInstruction) instr;
					
					try
					{
						if (WALAUtils.isMethodConstructor(bb.getNode()))
						{
							String mthClassName = WALAUtils.getClassNameFromCGNode(bb.getNode());
						
							if (( ! invokeInsn.isStatic() ) && (invokeInsn.getReceiver() == 1)) clsname2thisobjescapes.put(mthClassName, true);
							
							for (int i = 0; i < invokeInsn.getNumberOfUses(); i++)
							{
								try
								{
									if (invokeInsn.getUse(i) == 1) clsname2thisobjescapes.put(mthClassName, true);
								}
								catch (ArrayIndexOutOfBoundsException ex2) {}
							}
						}
					}
					catch (Exception ex) { ex.printStackTrace(); }
					
					// we do not change bit vectors here
					return BitVectorIdentity.instance();
				}				
				else
				{
					// identity function for all other instructions
					return BitVectorIdentity.instance();
				}
			}
			
			public boolean hasEdgeTransferFunctions() 
			{
				return true;
			}
			
			public boolean hasNodeTransferFunctions() 
			{
				return true;
			}
		}
	
		public BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> analyze() 
		{
			BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, FieldName> framework = new BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, FieldName>(bwICFG, new TransferFunctionsPFAOP(), fieldsNumbering);
			
			BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solver = new BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>>(framework);

			try
			{
				solver.solve(null);
			}
			catch (Exception e) {}
			
			return solver;
		}
	}

	
	static class MethodFieldAccesses extends FieldAccessesBase
	{
		private CGNode cgNode;
		
		private Graph<IExplodedBasicBlock> bwCFG;
		
		private IR methodIR;
		
		
		public MethodFieldAccesses(CGNode cgn,Graph<IExplodedBasicBlock> bwCFG, IR methodIR, IClassHierarchy cha, boolean reads, boolean writes)
		{
			super(cha, reads, writes);
			
			this.cgNode = cgn;
			this.bwCFG = bwCFG;
			this.methodIR = methodIR;
			
			this.fieldsNumbering = createFieldAccessInstrMapping();
		}
		
		private OrdinalSetMapping<FieldName> createFieldAccessInstrMapping()
		{
			MutableMapping<FieldName> fieldNums = new MutableMapping<FieldName>(new FieldName[1]);
			
			SSAInstruction[] instructions = methodIR.getInstructions();
			for (int i = 0; i < instructions.length; i++) 
			{
				SSAInstruction instr = instructions[i];
				if (instr instanceof SSAFieldAccessInstruction) 
				{
					SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) instr;
					
					try
					{
						Set<FieldName> fieldNames = getFieldNamesFromInsn(cgNode, faInsn, i);
						for (FieldName fn : fieldNames) fieldNums.add(fn);
					}
					catch (Exception ex) { ex.printStackTrace(); }
				}
			}
			
			return fieldNums;
		}
		
		class TransferFunctionsMFA implements ITransferFunctionProvider<IExplodedBasicBlock, BitVectorVariable> 
		{
			public UnaryOperator<BitVectorVariable> getEdgeTransferFunction(IExplodedBasicBlock src, IExplodedBasicBlock dst) 
			{
				return BitVectorIdentity.instance();
			}
			
			public AbstractMeetOperator<BitVectorVariable> getMeetOperator() 
			{
				return BitVectorUnion.instance();
			}
	
			public UnaryOperator<BitVectorVariable> getNodeTransferFunction(IExplodedBasicBlock ebb) 
			{
				SSAInstruction instr = ebb.getInstruction();
				
				int instrIndex = ebb.getFirstInstructionIndex();
				
				if (instr instanceof SSAFieldAccessInstruction) 
				{
					SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) instr;
					
					if ((considerReads && (faInsn instanceof SSAGetInstruction)) || (considerWrites && (faInsn instanceof SSAPutInstruction)))
					{
						// this must be empty -> we do not need to kill anything
						BitVector kill = new BitVector();
						
						BitVector gen = new BitVector();

						try
						{
							Set<FieldName> fieldNames = getFieldNamesFromInsn(cgNode, faInsn, instrIndex);
							
							for (FieldName fn : fieldNames)
							{
								int fieldNum = fieldsNumbering.getMappedIndex(fn);
								
								gen.set(fieldNum);
							}								
						}
						catch (Exception ex) { ex.printStackTrace(); } 
						
						return new BitVectorKillGen(kill, gen);
					}
					else 
					{
						// identity function for all other cases
						return BitVectorIdentity.instance();
					}
				}
				else
				{
					// identity function for all other instructions
					return BitVectorIdentity.instance();
				}
			}
			
			public boolean hasEdgeTransferFunctions() 
			{
				return false;
			}
			
			public boolean hasNodeTransferFunctions() 
			{
				return true;
			}
		}
	
		public BitVectorSolver<IExplodedBasicBlock> analyze() 
		{
			BitVectorFramework<IExplodedBasicBlock, FieldName> framework = new BitVectorFramework<IExplodedBasicBlock, FieldName>(bwCFG, new TransferFunctionsMFA(), fieldsNumbering);
			
			BitVectorSolver<IExplodedBasicBlock> solver = new BitVectorSolver<IExplodedBasicBlock>(framework);
			
			try
			{
				solver.solve(null);
			}
			catch (Exception e) {}
			
			return solver;
		}
	}
}
