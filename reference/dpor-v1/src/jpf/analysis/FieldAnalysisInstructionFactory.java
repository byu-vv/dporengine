package jpf.analysis;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPFException; 
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.jvm.bytecode.InstructionFactory;


public class FieldAnalysisInstructionFactory extends InstructionFactory
{
	public FieldAnalysisInstructionFactory(Config conf)
	{
	}

	public Instruction getfield(String fieldName, String clsName, String fieldDescriptor)
	{
		return new jpf.analysis.bytecode.GETFIELD(fieldName, clsName, fieldDescriptor);
	}

	public Instruction getstatic(String fieldName, String clsName, String fieldDescriptor)
	{
		return new jpf.analysis.bytecode.GETSTATIC(fieldName, clsName, fieldDescriptor);
	}
	
	public Instruction putfield(String fieldName, String clsName, String fieldDescriptor)
	{
		return new jpf.analysis.bytecode.PUTFIELD(fieldName, clsName, fieldDescriptor);
	}

	public Instruction putstatic(String fieldName, String clsName, String fieldDescriptor)
	{
		return new jpf.analysis.bytecode.PUTSTATIC(fieldName, clsName, fieldDescriptor);
	}
}
