package jpf.analysis.bytecode;

import java.util.List;
import java.util.Set;

import gov.nasa.jpf.JPFException;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.StaticElementInfo;
import gov.nasa.jpf.vm.SystemState;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ThreadList;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.LoadOnJPFRequired;

import jpf.analysis.JPFUtils;
import jpf.analysis.FieldAccessManager;
import jpf.analysis.ProgramPoint;


public class GETSTATIC extends gov.nasa.jpf.jvm.bytecode.GETSTATIC
{
	public GETSTATIC(String fieldName, String clsDescriptor, String fieldDescriptor)
	{
		super(fieldName, clsDescriptor, fieldDescriptor);
	}

	public Instruction execute(ThreadInfo ti) 
	{
		ClassInfo ciField;
		FieldInfo fieldInfo;
		
		try 
		{
			fieldInfo = getFieldInfo();
		} 
		catch (LoadOnJPFRequired lre) 
		{
			return ti.getPC();
		}
		
		if (fieldInfo == null) return ti.createAndThrowException("java.lang.NoSuchFieldError", (className + '.' + fname));
		
		// this can be actually different (can be a base)
		ciField = fieldInfo.getClassInfo();

		// note - this returns the next insn in the topmost clinit that just got pushed		
		if (!mi.isClinit(ciField) && ciField.pushRequiredClinits(ti)) return ti.getPC();
		
		ElementInfo ei = ciField.getStaticElementInfo();
		
		if (ei == null) throw new JPFException("attempt to access field: " + fname + " of uninitialized class: " + ciField.getName());
		
		if (isNewPorFieldBoundary(ti)) 
		{
			String className = ciField.getName();
			String fieldName = fieldInfo.getName();
			
			MethodInfo mi = getMethodInfo();
			
			String curMethodSig = JPFUtils.getMethodSignature(mi);
			
			boolean normalInstanceMethod = ( ! mi.isCtor() ) && ( ! mi.isStatic() ) && ( ! mi.isInternalMethod() );
			
			if (JPFUtils.existsFutureWrite(ti, curMethodSig, getPosition(), className, fieldName, true, -1, normalInstanceMethod))
			{
				if (createAndSetSharedFieldAccessCG(ei, ti)) return this;
			}
		}

		Object attr = ei.getFieldAttr(fieldInfo);
		StackFrame frame = ti.getModifiableTopFrame();
		
		if (size == 1) 
		{
			int ival = ei.get1SlotField(fieldInfo);
			lastValue = ival;
			
			if (fieldInfo.isReference()) frame.pushRef(ival);
			else frame.push(ival);
			
			if (attr != null) frame.setOperandAttr(attr);
		} 
		else 
		{
			long lval = ei.get2SlotField(fieldInfo);
			lastValue = lval;
			
			frame.pushLong(lval);
			
			if (attr != null) frame.setLongOperandAttr(attr);
		}

		return getNext(ti);
	} 
}
