package jpf.analysis.bytecode;

import java.util.List;
import java.util.Set;

import gov.nasa.jpf.JPFException;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.SystemState;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ThreadList;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.LoadOnJPFRequired;

import jpf.analysis.JPFUtils;
import jpf.analysis.FieldAccessManager;
import jpf.analysis.ProgramPoint;


public class PUTSTATIC extends gov.nasa.jpf.jvm.bytecode.PUTSTATIC
{
	public PUTSTATIC(String fieldName, String clsDescriptor, String fieldDescriptor)
	{
		super(fieldName, clsDescriptor, fieldDescriptor);
	}

	public Instruction execute(ThreadInfo ti) 
	{
		if (!ti.isFirstStepInsn()) // top half
		{
			FieldInfo fieldInfo;
    
			try 
			{
				fieldInfo = getFieldInfo();
			} 
			catch (LoadOnJPFRequired lre) 
			{
				return ti.getPC();
			}
      
			if (fieldInfo == null) return ti.createAndThrowException("java.lang.NoSuchFieldError", (className + '.' + fname));
			
			ClassInfo ciField = fi.getClassInfo();

			// note - this returns the next insn in the topmost clinit that just got pushed			
			if (!mi.isClinit(ciField) && ciField.pushRequiredClinits(ti)) return ti.getPC();
			
			ElementInfo ei = ciField.getStaticElementInfo();
			
			if (isNewPorFieldBoundary(ti)) 
			{
				String className = getFieldInfo().getClassInfo().getName();
				String fieldName = getFieldInfo().getName();
				
				String curMethodSig = JPFUtils.getMethodSignature(getMethodInfo());			
	
				if (JPFUtils.existsFutureRead(ti, curMethodSig, getPosition(), className, fieldName, true, -1))
				{
					if (createAndSetSharedFieldAccessCG(ei, ti)) return this;
				}
			}
			
			return put(ti, ti.getTopFrame(), ei);
		} 
		else // re-execution
		{
			// no need to redo the exception checks, we already had them in the top half
			ClassInfo ciField = fi.getClassInfo();
			ElementInfo ei = ciField.getStaticElementInfo();
			
			return put(ti, ti.getTopFrame(), ei);
		}
	}
}
