package jpf.analysis;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.Collections;
import java.util.Stack;
import java.util.Collection;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.search.Search;

import com.ibm.wala.ipa.callgraph.propagation.PointerKey;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.demandpa.alg.DemandRefinementPointsTo;


public class AllocationSiteManager extends ListenerAdapter
{
	private static final boolean DEBUG = false;

		
	// map from class name to allocation sites for objects of the class
	private static Map<String, Set<String>> clsname2allocsites;

	// map from object index to context-insensitive allocation site
	private static Map<Integer, String> objid2allocsite;
	
	// map from local variable name to allocation sites for objects of the class
	private static Map<FieldName, Set<String>> locvar2allocsites;
	
	// map from object index to allocation site with context
	private static Map<Integer, String> objid2ctxallocsite;

	// set of objects (allocation sites) that are shared between threads (that escape from creating thread)	
	private static Set<String> escapedAllocSites;

	// map from object index to the index of a thread that created the object
	private static Map<Integer, Integer> objid2allocthid;
	
	// precomputed information for demand-driven pointer analysis
	public static DemandRefinementPointsTo demandPointsTo;

	// map from field names to local variable identifiers
	public static Map<FieldName, List<PointerKey>> field2locvar;

	// cache for points-to sets (used with demand-driven pointer analysis)
	public static Map<PointerKey, Collection<InstanceKey>> pointstoCache;

	
	public static boolean useCallerSite;
	public static boolean useCallString;
	public static boolean useContainerObject;

	public static boolean keepContext;

	private Stack<Map<Integer,String>> currentPathAS;
	private Stack<Map<Integer,String>> currentPathCTX;
	private Stack<Map<Integer,Integer>> currentPathOT;
	private boolean changedAllocData;
	
	
	static
	{
		clsname2allocsites = new HashMap<String, Set<String>>();
		objid2allocsite = new HashMap<Integer, String>();		
		locvar2allocsites = new HashMap<FieldName, Set<String>>();
		objid2ctxallocsite = new HashMap<Integer, String>();
		escapedAllocSites = new HashSet<String>();
		objid2allocthid = new HashMap<Integer, Integer>();
		
		field2locvar = new HashMap<FieldName, List<PointerKey>>();

		pointstoCache = new HashMap<PointerKey, Collection<InstanceKey>>();

		useCallerSite = false;
		useCallString = false;
		useContainerObject = false;

		keepContext = false;
	}
	
	
	public AllocationSiteManager(Config cfg, JPF jpf)
	{
		currentPathAS = new Stack<Map<Integer,String>>();
		currentPathCTX = new Stack<Map<Integer,String>>();
		currentPathOT = new Stack<Map<Integer,Integer>>();
		changedAllocData = false;
	}
	
	public void objectCreated(VM vm, ThreadInfo curTh, ElementInfo objEI)
	{
		if (curTh == null) return;

		Instruction instr = curTh.getPC();
		if (instr == null) return;		

		int objRef = objEI.getObjectRef();
	
		String objAllocSite = JPFUtils.getAllocSite(instr.getMethodInfo(), instr.getPosition());
		
		if (DEBUG) System.out.println("[DEBUG AllocationSiteManager.objectCreated] objRef = " + objRef + ", objAllocSite = " + objAllocSite);
		
		objid2allocsite.put(objRef, objAllocSite);

		if (useContainerObject && keepContext)
		{
			String objAllocCtx = "";

			List<String> objString = JPFUtils.getReceiverObjectStringForThreadStack(curTh);
			
			if (objString == null) objString = Collections.EMPTY_LIST;

			int lastCollPos = -1;

			for (int i = objString.size() - 1; i >= 0; i--)
			{
				String ctxElem = objString.get(i);

				if (involvesCollections(ctxElem)) 
				{
					lastCollPos = i;
					break;
				}
			}

			if (involvesCollections(objAllocSite)) 
			{
				// allocation site involves collections -> use one level of context
				if (objString.size() > 0) objAllocCtx = "[" + objString.get(objString.size() - 1) + "]";
				else objAllocCtx = "";
			}
			else if (lastCollPos != -1)
			{
				// object string involves collections and allocation site does not involve collections -> use one level of context prior to first occurrence

				int startIndex = lastCollPos - 1;
				if (startIndex < 0) startIndex = 0;

				StringBuffer ctxStrBuf = new StringBuffer();
				for (int i = startIndex; i < objString.size(); i++)
				{
					if (ctxStrBuf.length() > 0) ctxStrBuf.insert(0, "[").append("]");
					ctxStrBuf.append(objString.get(i));
				}
				ctxStrBuf.insert(0, "[");
				ctxStrBuf.append("]");

				objAllocCtx = ctxStrBuf.toString();
			}
			else // (( lastCollPos == -1 ) && ( ! involvesCollections(objAllocSite) )) 
			{
				// no collections anywhere
				objAllocCtx = "";
			}

		
			if (DEBUG) System.out.println("[DEBUG AllocationSiteManager.objectCreated] objRef = " + objRef + ", objAllocCtx = " + objAllocCtx + ", objAllocSite = " + objAllocSite);
			
			objid2ctxallocsite.put(objRef, objAllocCtx + objAllocSite);
		}
		
		if (FieldAccessManager.useEscapeAnalysis)
		{
			objid2allocthid.put(objRef, curTh.getId());
		}
		
		changedAllocData = true;
	}
	
	public void searchStarted(Search search)
	{
		Map<Integer,String> asCopy = new HashMap<Integer,String>();
		asCopy.putAll(objid2allocsite);
		currentPathAS.push(asCopy);
		
		Map<Integer,String> ctxCopy = new HashMap<Integer,String>();
		ctxCopy.putAll(objid2ctxallocsite);
		currentPathCTX.push(ctxCopy);
		
		Map<Integer,Integer> otCopy = new HashMap<Integer,Integer>();
		otCopy.putAll(objid2allocthid);
		currentPathOT.push(otCopy);
		
		changedAllocData = false;
	}
	
	public void stateAdvanced(Search search)
	{
		Map<Integer,String> asCopy = null;
		if (changedAllocData) 
		{
			asCopy = new HashMap<Integer,String>();
			asCopy.putAll(objid2allocsite);
		}
		else
		{
			asCopy = currentPathAS.peek();
		}
		currentPathAS.push(asCopy);
		
		Map<Integer,String> ctxCopy = null;
		if (changedAllocData)
		{
			ctxCopy = new HashMap<Integer,String>();
			ctxCopy.putAll(objid2ctxallocsite);
		}
		else
		{
			ctxCopy = currentPathCTX.peek();
		}
		currentPathCTX.push(ctxCopy);
		
		Map<Integer,Integer> otCopy = null;
		if (changedAllocData)
		{
			otCopy = new HashMap<Integer,Integer>();
			otCopy.putAll(objid2allocthid);
		}
		else
		{
			otCopy = currentPathOT.peek();
		}
		currentPathOT.push(otCopy);
		
		changedAllocData = false;
	}
	
	public void stateBacktracked(Search search) 
	{
		Map<Integer,String> curAS = currentPathAS.pop();
		Map<Integer,String> prevAS = currentPathAS.peek();
		if (curAS != prevAS)
		{
			objid2allocsite = new HashMap<Integer,String>();
			objid2allocsite.putAll(prevAS);
		}
		
		Map<Integer,String> curCTX = currentPathCTX.pop();
		Map<Integer,String> prevCTX = currentPathCTX.peek();
		if (curCTX != prevCTX)
		{
			objid2ctxallocsite = new HashMap<Integer,String>();
			objid2ctxallocsite.putAll(prevCTX);
		}
		
		Map<Integer,Integer> curOT = currentPathOT.pop();
		Map<Integer,Integer> prevOT = currentPathOT.peek();
		if (curOT != prevOT)
		{
			objid2allocthid = new HashMap<Integer,Integer>();
			objid2allocthid.putAll(prevOT);
		}
		
		changedAllocData = false;
	}

	
	public static void addAllocSiteForClass(String className, String allocSite)
	{
		Set<String> sites = clsname2allocsites.get(className);
		if (sites == null)
		{
			sites = new HashSet<String>();
			clsname2allocsites.put(className, sites);
		}
		
		sites.add(allocSite);
	}
	
	public static Set<String> getAllocSitesForClass(String className)
	{
		return clsname2allocsites.get(className);
	}
	
	public static String getAllocSiteForObject(int objRef)
	{
		String allocSite = objid2allocsite.get(objRef);

		//if (allocSite == null) allocSite = JPFUtils.JPF_VM_ALLOC_SITE;

		return allocSite;
	}

	public static String getCtxAllocSiteForObject(int objRef)
	{
		return objid2ctxallocsite.get(objRef);
	}
	
	public static void addAllocSiteForLocalVar(FieldName lv, String allocSite)
	{
		Set<String> sites = locvar2allocsites.get(lv);
		if (sites == null)
		{
			sites = new HashSet<String>();
			locvar2allocsites.put(lv, sites);
		}
		
		sites.add(allocSite);
	}
	
	public static Set<String> getAllocSitesForLocalVar(FieldName fn)
	{
		FieldName lv = new FieldName("", "", fn.methodSig, fn.localVarNo);
		
		return locvar2allocsites.get(lv);
	}
	
	public static Set<String> getAllocSitesForLocalVar(String methodSig, int localVarNo)
	{
		FieldName lv = new FieldName("", "", methodSig, localVarNo);
		
		return locvar2allocsites.get(lv);
	}	
	
	public static void addThreadSharedObject(String allocSite)
	{
		if (DEBUG) System.out.println("[DEBUG AllocationSiteManager.addThreadSharedObject] allocSite = " + allocSite);
			
		escapedAllocSites.add(allocSite);
	}
	
	public static boolean isThreadLocalObject(String allocSite)
	{
		if (allocSite == null) return false;
		
		return ( ! escapedAllocSites.contains(allocSite) );
	}
	
	private static boolean involvesCollections(String objAllocInfo)
	{
		if ( ! objAllocInfo.startsWith("java.util") ) return false;

		if (objAllocInfo.indexOf("java.util.Vector") != -1) return true;
		if (objAllocInfo.indexOf("java.util.Hashtable") != -1) return true;
		if (objAllocInfo.indexOf("java.util.HashMap") != -1) return true;
		if (objAllocInfo.indexOf("java.util.TreeMap") != -1) return true;
		if (objAllocInfo.indexOf("java.util.HashSet") != -1) return true;
		if (objAllocInfo.indexOf("java.util.TreeSet") != -1) return true;
		if (objAllocInfo.indexOf("java.util.ArrayList") != -1) return true;
		if (objAllocInfo.indexOf("java.util.LinkedList") != -1) return true;
		if (objAllocInfo.indexOf("java.util.Arrays") != -1) return true;

		return false;
	}

}
