package jpf.vm;

import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Iterator;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.util.IntTable;
import gov.nasa.jpf.util.ObjVector;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.StaticElementInfo;
import gov.nasa.jpf.vm.OVHeap;
import gov.nasa.jpf.vm.KernelState;
import gov.nasa.jpf.vm.SharedObjectPolicy;
import gov.nasa.jpf.vm.TidSet;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.ArrayFields;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ClassLoaderInfo;


/**
 * support for tracking object reachability from threads
 */
public class GCReachabilityHeap extends OVHeap 
{
	protected VM vm;
	
	protected ClassInfo threadGroupCI;
	

	public GCReachabilityHeap(Config config, KernelState ks) 
	{
		super(config, ks);
		vm = VM.getVM();
		
		threadGroupCI = null;
	}
	
	@Override
	protected void mark()
	{
		if (threadGroupCI == null) threadGroupCI = ClassLoaderInfo.getSystemResolvedClassInfo("java.lang.ThreadGroup"); 
			
		// clear old data
		HeapReachabilityPolicy policy = (HeapReachabilityPolicy) SharedObjectPolicy.getPolicy();
		policy.clearData(vm);
		
		super.mark();	
	}
	
	@Override
	public void markStaticRoot(int objRef) 
	{
		if (objRef == -1) return;
		
		ElementInfo ei = get(objRef);
		
		HeapReachabilityPolicy policy = (HeapReachabilityPolicy) SharedObjectPolicy.getPolicy();
		
		// given static object is reachable from all existing threads		
		for (ThreadInfo ti : vm.getThreadList()) 
		{
			markRecursiveHeapReachabilityForThread(policy, ei, ti, new HashSet<Integer>());
			policy.markObjectReachableFromThread(ei, ti);			
		}
		
		queueMark(objRef);
	}

	@Override
	public void markThreadRoot(int objRef, int tid) 
	{
		if (objRef == -1) return; 

		ElementInfo ei = get(objRef);
		
		HeapReachabilityPolicy policy = (HeapReachabilityPolicy) SharedObjectPolicy.getPolicy();
		
		// given object is reachable from the thread identified by "tid"		
		ThreadInfo ti = vm.getThreadList().getThreadInfoForId(tid);		
		markRecursiveHeapReachabilityForThread(policy, ei, ti, new HashSet<Integer>());
		policy.markObjectReachableFromThread(ei, ti);		
		
		queueMark(objRef);
	} 

	private void markRecursiveHeapReachabilityForThread(HeapReachabilityPolicy policy, ElementInfo obj, ThreadInfo ti, Set<Integer> visitedObjs)
	{	
		// if this object is already marked as reachable from the given thread then it was already processed by this method
		// if the recursive processing of this object already started then we do not have to start again (we prevent stack overflow during the mark phase)
		if (policy.isObjectReachableFromThread(obj, ti) || visitedObjs.contains(obj.getObjectRef())) return;
			
		visitedObjs.add(obj.getObjectRef());

		// mark all other heap objects directly reachable from "obj" (via fields and array elements of reference types)
		// call this method recursively to mark all objects reachable from this one
		
		if (obj.isArray())
		{
			ArrayFields objArrFields = (ArrayFields) obj.getFields();
			
			if (objArrFields.isReferenceArray())
			{
				for (int i = 0; i < objArrFields.arrayLength(); i++)
				{
					int arrElemObjRef = objArrFields.getReferenceValue(i);
					
					if (arrElemObjRef != -1) 
					{
						ElementInfo arrElemObj = get(arrElemObjRef);
						
						markRecursiveHeapReachabilityForThread(policy, arrElemObj, ti, visitedObjs);
						
						policy.markObjectReachableFromThread(arrElemObj, ti);
					}
				}
			}
		}
		else
		{
			for (int i = 0; i < obj.getClassInfo().getNumberOfInstanceFields(); i++)
			{
				FieldInfo fi = obj.getClassInfo().getInstanceField(i);
				
				boolean skip = false;
				
				// we break the link from ThreadGroup to individual Thread objects because we must prevent the reachability chain "Thread -> ThreadGroup -> other Threads"
				if (obj.getClassInfo().equals(threadGroupCI))
				{
					if (fi.getName().equals("threads")) skip = true;	
				}
				
				if (fi.isReference() && (!skip))
				{
					int fldTargetObjRef = obj.getReferenceField(fi);
					
					if (fldTargetObjRef != -1)
					{
						ElementInfo fldTargetObj = get(fldTargetObjRef);
						
						markRecursiveHeapReachabilityForThread(policy, fldTargetObj, ti, visitedObjs);
						
						policy.markObjectReachableFromThread(fldTargetObj, ti);
					}
				}
			}			
		}
	}
}

