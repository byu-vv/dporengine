package jpf.vm;

import java.util.List;
import java.util.ArrayList;

import gov.nasa.jpf.util.ObjVector;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.DynamicElementInfo;
import gov.nasa.jpf.vm.StaticElementInfo;
import gov.nasa.jpf.vm.ThreadInfoSet;
import gov.nasa.jpf.vm.SharedObjectPolicy;
import gov.nasa.jpf.vm.Memento;
import gov.nasa.jpf.vm.TidSet;


/**
 * Policy based on heap reachability.
 * Almost the same as overlapping contender policy but should be used only when tracking of heap reachability is turned on.
 */
public class HeapReachabilityPolicy extends SharedObjectPolicy 
{
	ObjVector<ThreadInfoSet> objectsReachabilityData;
	
	public HeapReachabilityPolicy() 
	{
		objectsReachabilityData = new ObjVector<ThreadInfoSet>();
	}
	
	@Override
	public ThreadInfoSet getThreadInfoSet(ThreadInfo allocThread, DynamicElementInfo ei) 
	{
		ThreadInfoSet tiSet = new TidSet(allocThread);
		
		ThreadInfoSet thReachSet = new TidSet(allocThread);
		objectsReachabilityData.set(ei.getObjectRef(), thReachSet);
		
		return tiSet;
	}
	
	@Override
	public ThreadInfoSet getThreadInfoSet(ThreadInfo allocThread, StaticElementInfo ei) 
	{
		ThreadInfoSet tiSet = new TidSet(allocThread);
		
		return tiSet;
	} 
	
	@Override
	public boolean isShared(ElementInfo ei, ThreadInfoSet set) 
	{
		if (ei instanceof StaticElementInfo) return true;
		
		ThreadInfoSet thReachSet = objectsReachabilityData.get(ei.getObjectRef());
		return thReachSet.hasMultipleLiveThreads();
	}
	
	@Override
	public Memento<ThreadInfoSet> getMemento(ThreadInfoSet set) 
	{
		return set.getMemento();
	}

	@Override
	public void cleanupThreadTermination(ThreadInfo ti) 
	{
		// nothing
	}
 
	public void clearData(VM vm)
	{
		for (int i = 0; i < objectsReachabilityData.size(); i++) 
		{
			ThreadInfoSet thSet = objectsReachabilityData.get(i);
			
			if (thSet != null)
			{			
				for (ThreadInfo ti : vm.getThreadList()) thSet.remove(ti);
			}
		}
	}

	public void markObjectReachableFromThread(ElementInfo ei, ThreadInfo ti) 
	{
		ThreadInfoSet thReachSet = objectsReachabilityData.get(ei.getObjectRef());
		thReachSet.add(ti);
	}
	
	public boolean isObjectReachableFromThread(ElementInfo ei, ThreadInfo ti)
	{
		if (ei instanceof StaticElementInfo) return true;
		
		ThreadInfoSet thReachSet = objectsReachabilityData.get(ei.getObjectRef());
		return thReachSet.contains(ti);
	}

	// return indexes of objects reachable from a given thread
	public List<Integer> getObjectsReachableFromThread(ThreadInfo ti) 
	{
		List<Integer> reachableObjects = new ArrayList<Integer>();
		
		for (int i = 0; i < objectsReachabilityData.size(); i++) 
		{
			ThreadInfoSet thSet = objectsReachabilityData.get(i);
			if (thSet != null) 
			{
				if (thSet.contains(ti)) reachableObjects.add(i);
			}
		}
		
		return reachableObjects;
	}
}

