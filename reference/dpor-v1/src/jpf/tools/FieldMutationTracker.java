package jpf.tools;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Stack;

import java.io.PrintStream;
import java.io.File;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.jvm.bytecode.GETFIELD;
import gov.nasa.jpf.jvm.bytecode.GETSTATIC;
import gov.nasa.jpf.jvm.bytecode.PUTFIELD;
import gov.nasa.jpf.jvm.bytecode.PUTSTATIC;
import gov.nasa.jpf.search.Search;


public class FieldMutationTracker extends ListenerAdapter 
{
	// fields written after read by a thread other than the creating one
	// search global information
	private Set<String> fieldsWrittenAfterRead;
	
	// current transition
	private TransitionInfo curTr;
	
	// stack of transitions (current path)
	private Stack<TransitionInfo> curPathTrs;
	
	
	public FieldMutationTracker(Config config, JPF jpf) 
	{
		fieldsWrittenAfterRead = new HashSet<String>();

		curPathTrs = new Stack<TransitionInfo>();		
    }
    

	public void searchStarted(Search search)
	{
		curTr = new TransitionInfo();
	}
	
	public void stateAdvanced(Search search) 
	{
		curPathTrs.push(curTr);
		
		curTr = new TransitionInfo();
	}

	public void stateBacktracked(Search search) 
	{
		TransitionInfo tri = curPathTrs.peek();
	
		curPathTrs.pop();
	}		
	
	public void searchFinished(Search search)
	{
		System.out.println("MUTATED FIELDS");
		System.out.println("==============");

		for (String fieldName : fieldsWrittenAfterRead)
		{
			System.out.println(fieldName);
		}
	}


	public void executeInstruction(VM vm, ThreadInfo ti, Instruction insn)
	{
		if (ti.isFirstStepInsn()) return;
	
		if (insn instanceof GETFIELD)
		{
			GETFIELD gfInsn = (GETFIELD) insn;

			FieldInfo targetFI = gfInsn.getFieldInfo();			
			int targetObjRef = ti.getTopFrame().peek();
			ElementInfo targetObjEI = vm.getHeap().get(targetObjRef);
			
			String fieldID = createFieldID(targetObjEI.getClassInfo().getName(), targetFI.getName(), targetObjRef);

			if (isAlreadyEscapedObject(targetObjRef))
			{
				Set<Integer> readThIDs = curTr.fieldsReadSharedObjects.get(fieldID);
				if (readThIDs == null)
				{
					readThIDs = new HashSet<Integer>();
					curTr.fieldsReadSharedObjects.put(fieldID, readThIDs);
				}
				
				readThIDs.add(ti.getId());
			}
		}
		
		if (insn instanceof PUTFIELD)
		{
			PUTFIELD pfInsn = (PUTFIELD) insn;

			FieldInfo targetFI = pfInsn.getFieldInfo();
			int targetObjRef = (targetFI.getStorageSize() == 1) ? ti.getTopFrame().peek(1) : ti.getTopFrame().peek(2);	
			ElementInfo targetObjEI = vm.getHeap().get(targetObjRef);
			
			String fieldID = createFieldID(targetObjEI.getClassInfo().getName(), targetFI.getName(), targetObjRef);

			if (isAlreadyEscapedObject(targetObjRef))
			{
				int creatingThID = findObjectCreatingThID(targetObjRef);

				if (isFieldReadByOtherThread(fieldID, creatingThID))
				{
					fieldsWrittenAfterRead.add(targetObjEI.getClassInfo().getName() + "." + targetFI.getName());
				}
			}

			if (targetFI.isReference())
			{
				int valObjRef = ti.getTopFrame().peek();
				if (valObjRef != -1) curTr.escapedObjects.add(valObjRef);
			}
		}	
	}

	public void instructionExecuted(VM vm, ThreadInfo ti, Instruction nextInsn, Instruction execInsn)
	{
		if (ti.isFirstStepInsn()) return;

		if (execInsn instanceof PUTSTATIC)
		{
			PUTSTATIC psInsn = (PUTSTATIC) execInsn;
			
			FieldInfo targetFI = psInsn.getFieldInfo();

			if (targetFI.isReference())
			{
				int valObjRef = (int) psInsn.getLastValue();
				if (valObjRef != -1) curTr.escapedObjects.add(valObjRef);
			}
		}
	}

	public void objectCreated(VM vm, ThreadInfo curTh, ElementInfo newObj)
	{
		// initialization (before the search start)
		if (curTr == null) return;

		curTr.newObject2CreatingThID.put(newObj.getObjectRef(), curTh.getId());
	} 


	private boolean isAlreadyEscapedObject(int objRef)
	{
		for (TransitionInfo tr : curPathTrs)
		{
			if (tr.escapedObjects.contains(objRef)) return true;
		}

		if (curTr.escapedObjects.contains(objRef)) return true;

		return false;
	}

	private boolean isFieldReadByOtherThread(String fieldID, int thID)
	{
		Set<Integer> readThIDs = null;

		for (TransitionInfo tr : curPathTrs)
		{
			readThIDs = tr.fieldsReadSharedObjects.get(fieldID);
			if (readThIDs != null)
			{
				for (Integer rThID : readThIDs)
				{
					if (rThID.intValue() != thID) return true;
				}
			}
		}

		readThIDs = curTr.fieldsReadSharedObjects.get(fieldID);
		if (readThIDs != null)
		{
			for (Integer rThID : readThIDs)
			{
				if (rThID.intValue() != thID) return true;
			}
		}

		return false;
	}
	
	private int findObjectCreatingThID(int objRef)
	{
		for (TransitionInfo tr : curPathTrs)
		{
			if (tr.newObject2CreatingThID.containsKey(objRef)) return tr.newObject2CreatingThID.get(objRef);
		}
	
		if (curTr.newObject2CreatingThID.containsKey(objRef)) return curTr.newObject2CreatingThID.get(objRef);

		return 0;
	}
		

	private static String createFieldID(String className, String fieldName, int objRef)
	{
		return className + "@" + objRef + "." + fieldName;
	}


	public static class TransitionInfo
	{
		// creating thread for each new object
		// map from object ID (ref) to thread ID
		public Map<Integer,Integer> newObject2CreatingThID;

		// objects escaped to the heap
		public Set<Integer> escapedObjects;

		// fields read on already escaped objects (reachable from multiple threads)
		// map from field ID (class name + "@" + object ref + "." + field name) to a set of thread IDs
		public Map<String,Set<Integer>> fieldsReadSharedObjects;

		public TransitionInfo()
		{
			newObject2CreatingThID = new HashMap<Integer,Integer>();
			escapedObjects = new HashSet<Integer>();
			fieldsReadSharedObjects = new HashMap<String,Set<Integer>>();
		}
	}

}
