package jpf.tools;

import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.TreeSet;
import java.util.HashSet;

import java.io.PrintStream;
import java.io.File;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.Heap;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.DynamicElementInfo;
import gov.nasa.jpf.vm.StaticElementInfo;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.ThreadChoiceGenerator;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.ClassLoaderInfo;
import gov.nasa.jpf.jvm.bytecode.GETFIELD;
import gov.nasa.jpf.jvm.bytecode.GETSTATIC;
import gov.nasa.jpf.jvm.bytecode.PUTFIELD;
import gov.nasa.jpf.jvm.bytecode.PUTSTATIC;
import gov.nasa.jpf.search.Search;


/**
 * Heap profiler monitors object accesses in the checked Java program and saves important data for further analysis.
 */
public class HeapProfiler extends ListenerAdapter 
{
	// list of object accesses
	private List<AccessInfo> accesses;
	
	// map from object ID to some information
	private Map<String, ObjectInfo> objid2info;
	
	private Set<String> choices;
	
	
	private String outputDir;
	private String outputFileNameTemplate;
	
	private Instruction prevInstr;


	public HeapProfiler(Config config, JPF jpf) 
	{
		accesses = new ArrayList<AccessInfo>();
		objid2info = new HashMap<String, ObjectInfo>();
		choices = new LinkedHashSet<String>();
		
		outputDir = config.getString("tools.heapprofiler.outputdir", "output");
		outputFileNameTemplate = config.getString("tools.heapprofiler.outfiletemplate", "out");

		prevInstr = null;
    }
    	
    public void executeInstruction(VM vm, ThreadInfo ti, Instruction instr) 
	{
		// instruction will be re-executed because new CG was created
		if (instr == prevInstr) return;

		if (instr instanceof GETFIELD)
		{
			GETFIELD gfInstr = (GETFIELD) instr;

			FieldInfo targetFI = gfInstr.getFieldInfo();
			
			int targetObjRef = ti.getTopFrame().peek();
		
			ElementInfo targetObjEI = vm.getHeap().get(targetObjRef);
			
			recordAccessInfo(instr.getMnemonic(), instr.getFileLocation(), ti.getId(), targetObjEI.getClassInfo().getName() + "@" + targetObjRef, targetObjEI.getClassInfo().getName(), targetFI.getName(), targetObjEI.isShared());
			recordObjectRead(targetObjEI.getClassInfo().getName() + "@" + targetObjRef, ti.getId(), targetObjEI.isShared());
		}
		
		if (instr instanceof GETSTATIC)
		{
			GETSTATIC gsInstr = (GETSTATIC) instr;

			FieldInfo targetFI = gsInstr.getFieldInfo();
			
			ClassInfo targetClass = targetFI.getClassInfo();
			
			ElementInfo targetClassEI = targetClass.getStaticElementInfo();
			if (targetClassEI == null) 
			{
				System.out.println("[WARNING.HeapProfiler] element info == null for class '" + targetClass.getName() + "'");
				return;
			}
			
			recordAccessInfo(instr.getMnemonic(), instr.getFileLocation(), ti.getId(), targetClass.getName() + "@" + String.valueOf(-1), targetClass.getName(), targetFI.getName(), targetClassEI.isShared());
			recordObjectRead(targetClass.getName(), ti.getId(), targetClassEI.isShared());
		}

		if (instr instanceof PUTFIELD)
		{
			PUTFIELD pfInstr = (PUTFIELD) instr;

			FieldInfo targetFI = pfInstr.getFieldInfo();
			
			// we look only for references
			if ( ! targetFI.isReference() ) return;
			
			int targetObjRef = -1;
			switch (targetFI.getStorageSize())
			{
				case 1:
					targetObjRef = ti.getTopFrame().peek(1);
					break;
				case 2:
					targetObjRef = ti.getTopFrame().peek(2);
					break;
			}

			ElementInfo targetObjEI = vm.getHeap().get(targetObjRef);
			
			recordAccessInfo(instr.getMnemonic(), instr.getFileLocation(), ti.getId(), targetObjEI.getClassInfo().getName() + "@" + targetObjRef, targetObjEI.getClassInfo().getName(), targetFI.getName(), targetObjEI.isShared());
			recordObjectWrite(targetObjEI.getClassInfo().getName() + "@" + targetObjRef, ti.getId(), targetObjEI.isShared());
		}
		
		if (instr instanceof PUTSTATIC)
		{
			PUTSTATIC psInstr = (PUTSTATIC) instr;
			
			FieldInfo targetFI = psInstr.getFieldInfo();
			
			ClassInfo targetClass = targetFI.getClassInfo();
			
			ElementInfo targetClassEI = targetClass.getStaticElementInfo();
			
			recordAccessInfo(instr.getMnemonic(), instr.getFileLocation(), ti.getId(), targetClass.getName() + "@" + String.valueOf(-1), targetClass.getName(), targetFI.getName(), targetClassEI.isShared());
			recordObjectWrite(targetClass.getName(), ti.getId(), targetClassEI.isShared());
		}

		prevInstr = instr;
	}
	
	public void objectCreated(VM vm, ThreadInfo ti, ElementInfo objEI)
	{
		Instruction instr = ti.getPC();		

		int objRef = objEI.getObjectRef();
		
		String objAllocSite = "";
		
		if (instr != null) objAllocSite = instr.getMethodInfo().getBaseName() + ":" + instr.getInstructionIndex();

		recordObjectInfo(objEI.getClassInfo().getName() + "@" + objRef, objAllocSite, objEI.getClassInfo().getName(), objEI.isShared());
	}
	
	public void choiceGeneratorSet(VM vm, ChoiceGenerator<?> cg) 
	{
		ThreadInfo ti = vm.getCurrentThread();
		Instruction instr = ti.getPC();
				
		if (instr == null) return;
		
		if (cg instanceof ThreadChoiceGenerator)
		{			
			if (instr instanceof GETFIELD)
			{
				GETFIELD gfInstr = (GETFIELD) instr;
	
				FieldInfo targetFI = gfInstr.getFieldInfo();
				
				int targetObjRef = ti.getTopFrame().peek();
			
				ElementInfo targetObjEI = vm.getHeap().get(targetObjRef);
				
				recordThreadChoice(instr.getMnemonic(), instr.getFileLocation(), targetObjEI.getClassInfo().getName() + "@" + targetObjRef, targetObjEI.getClassInfo().getName(), targetFI.getName(), targetObjEI.isShared());
			}
			
			if (instr instanceof GETSTATIC)
			{
				GETSTATIC gsInstr = (GETSTATIC) instr;
	
				FieldInfo targetFI = gsInstr.getFieldInfo();
				
				ClassInfo targetClass = targetFI.getClassInfo();
				
				ElementInfo targetClassEI = targetClass.getStaticElementInfo();
				if (targetClassEI == null) return;
				
				recordThreadChoice(instr.getMnemonic(), instr.getFileLocation(), targetClass.getName(), targetClass.getName(), targetFI.getName(), targetClassEI.isShared());
			}
	
			if (instr instanceof PUTFIELD)
			{
				PUTFIELD pfInstr = (PUTFIELD) instr;
	
				FieldInfo targetFI = pfInstr.getFieldInfo();
				
				// we look only for references
				if ( ! targetFI.isReference() ) return;
				
				int targetObjRef = -1;
				switch (targetFI.getStorageSize())
				{
					case 1:
						targetObjRef = ti.getTopFrame().peek(1);
						break;
					case 2:
						targetObjRef = ti.getTopFrame().peek(2);
						break;
				}
	
				ElementInfo targetObjEI = vm.getHeap().get(targetObjRef);
				
				recordThreadChoice(instr.getMnemonic(), instr.getFileLocation(), targetObjEI.getClassInfo().getName() + "@" + targetObjRef, targetObjEI.getClassInfo().getName(), targetFI.getName(), targetObjEI.isShared());
			}
			
			if (instr instanceof PUTSTATIC)
			{
				PUTSTATIC psInstr = (PUTSTATIC) instr;
				
				FieldInfo targetFI = psInstr.getFieldInfo();
				
				ClassInfo targetClass = targetFI.getClassInfo();
				
				ElementInfo targetClassEI = targetClass.getStaticElementInfo();
				
				recordThreadChoice(instr.getMnemonic(), instr.getFileLocation(), targetClass.getName(), targetClass.getName(), targetFI.getName(), targetClassEI.isShared());
			}
		}			
	}
	
	public void searchStarted(Search search) 
	{
		int stateIndex = search.getStateId() + 1;
		
		HeapGraph graph = createHeapGraph(search.getVM());
		
		try
		{
			saveHeapGraphToDOT(stateIndex, graph);
		}
		catch (Exception ex)
		{
			System.out.println("[ERROR.HeapProfiler] saving heap graph failed");
			ex.printStackTrace();
		}
	}
  
	public void stateAdvanced(Search search) 
	{
		if (search.isNewState())
		{
			int stateIndex = search.getStateId() + 1;
			
			HeapGraph graph = createHeapGraph(search.getVM());
			
			try
			{
				saveHeapGraphToDOT(stateIndex, graph);
			}
			catch (Exception ex)
			{
				System.out.println("[ERROR.HeapProfiler] saving heap graph failed");
				ex.printStackTrace();
			}
		}
	}  
	
	public void searchFinished(Search search) 
	{
		try
		{
			saveRecordedData();
		}
		catch (Exception ex)
		{
			System.out.println("[ERROR.HeapProfiler] saving recorded data failed");
			ex.printStackTrace();
		}		
	}
	
	private void recordAccessInfo(String insnName, String srcCodeLoc, int thIndex, String objID, String objClassName, String tgtFieldName, boolean objReachable)
	{
		AccessInfo ai = new AccessInfo();

		ai.insnName = insnName;
		ai.sourceCodeLoc = srcCodeLoc;
		
		ai.oid = objID;
		ai.className = objClassName;
		ai.fieldName = tgtFieldName;
		ai.reachable = objReachable;
		
		ai.threadIndex = thIndex;
		
		accesses.add(ai);		
	}

	private void recordObjectInfo(String objID, String allocSite, String objClassName, boolean objReachable)
	{		
		ObjectInfo oi = objid2info.get(objID);
		
		if (oi == null)
		{
			oi = new ObjectInfo(objID, allocSite, objClassName, objReachable);
			objid2info.put(objID, oi);
		}
		
		if (objReachable) oi.reachable = true;
	}
	
	private void recordObjectRead(String objID, int thIndex, boolean objReachable)
	{
		ObjectInfo oi = objid2info.get(objID);
		if (oi == null) return;
		
		if (thIndex != -1) oi.readThreads.add(thIndex);
		
		if (objReachable) oi.reachable = true;
	}
	
	private void recordObjectWrite(String objID, int thIndex, boolean objReachable)
	{
		ObjectInfo oi = objid2info.get(objID);
		if (oi == null) return;
		
		if (thIndex != -1) oi.writeThreads.add(thIndex);
		
		if (objReachable) oi.reachable = true;
	}

	private void recordThreadChoice(String insnName, String srcCodeLoc, String objID, String className, String fieldName, boolean reachable)
	{
		ObjectInfo oi = objid2info.get(objID);
		
		String readThreads = "(";		
		boolean firstR = true;
		for (Integer thIndex : oi.readThreads)
		{
			if (firstR) firstR = false;
			else readThreads += ",";
			readThreads += thIndex.toString();
		}
		readThreads += ")";
		
		String writeThreads = "(";		
		boolean firstW = true;
		for (Integer thIndex : oi.writeThreads)
		{
			if (firstW) firstW = false;
			else writeThreads += ",";
			writeThreads += thIndex.toString();
		}
		writeThreads += ")";
		
		choices.add(insnName + " " + objID + "." + fieldName + " [reachable = " + reachable + ", read threads: " + readThreads + ", write threads: " + writeThreads + "] : location = " + srcCodeLoc);		
	}
	
	private void saveRecordedData() throws Exception
	{
		PrintStream outAccess = new PrintStream(outputDir + File.separator + outputFileNameTemplate + "_access");
		for (AccessInfo ai : accesses)
		{
			outAccess.print(ai.insnName + " ");
			outAccess.print(ai.oid + "." + ai.fieldName);
			outAccess.print(" : ");
			outAccess.print("thread = " + ai.threadIndex);
			outAccess.print(", reachable = " + ai.reachable);
			outAccess.print(", location = " + ai.sourceCodeLoc);  
			
			outAccess.println("");
		}
		outAccess.close();
		
		
		// field access statistics 
		Map<String, Set<Integer>> fieldtype2thacc = new TreeMap<String, Set<Integer>>();
		Map<String, Set<Integer>> fieldobj2thacc = new TreeMap<String, Set<Integer>>();
		for (ObjectInfo oi : objid2info.values())
		{
			for (AccessInfo ai : accesses)
			{
				if (oi.className.equals(ai.className))
				{
					String fieldType = oi.className + "." + ai.fieldName;
					String fieldObj = oi.id + "." + ai.fieldName;
					
					fieldtype2thacc.put(fieldType, new TreeSet<Integer>());
					fieldobj2thacc.put(fieldObj, new TreeSet<Integer>());
				}
			}
		}
		for (AccessInfo ai : accesses)
		{
			String fieldType = ai.className + "." + ai.fieldName;
			String fieldObj = ai.oid + "." + ai.fieldName;
			
			Set<Integer> thAccessFT = fieldtype2thacc.get(fieldType);
			if (thAccessFT == null)
			{
				thAccessFT = new TreeSet<Integer>();
				fieldtype2thacc.put(fieldType, thAccessFT);
			}
			thAccessFT.add(ai.threadIndex);
			
			Set<Integer> thAccessFO = fieldobj2thacc.get(fieldObj);
			if (thAccessFO == null)
			{
				thAccessFO = new TreeSet<Integer>();
				fieldobj2thacc.put(fieldObj, thAccessFO);
			}
			thAccessFO.add(ai.threadIndex);
		}
		PrintStream outStats = new PrintStream(outputDir + File.separator + outputFileNameTemplate + "_stats");
		for (Map.Entry<String, Set<Integer>> me : fieldtype2thacc.entrySet()) 
		{
			outStats.print(me.getKey() + " : ");
			boolean first = true;
			for (Integer thAcc : me.getValue())
			{
				if (first) first = false;
				else outStats.print(",");
				outStats.print(thAcc);
			}
			outStats.println("");
		}
		for (Map.Entry<String, Set<Integer>> me : fieldobj2thacc.entrySet()) 
		{
			outStats.print(me.getKey() + " : ");
			boolean first = true;
			for (Integer thAcc : me.getValue())
			{
				if (first) first = false;
				else outStats.print(",");
				outStats.print(thAcc);
			}
			outStats.println("");
		}
		outStats.close();
		
		
		PrintStream outObjects = new PrintStream(outputDir + File.separator + outputFileNameTemplate + "_object");		
		for (ObjectInfo oi : objid2info.values())
		{
			printObjectInfoToLine(oi, outObjects);
		}		
		outObjects.close();
		
		PrintStream outReachable = new PrintStream(outputDir + File.separator + outputFileNameTemplate + "_reachable");		
		for (ObjectInfo oi : objid2info.values())
		{
			if ( ! oi.reachable ) continue;			
			printObjectInfoToLine(oi, outReachable);
		}		
		outReachable.close();
		
		PrintStream outNotShared = new PrintStream(outputDir + File.separator + outputFileNameTemplate + "_notshared");		
		for (ObjectInfo oi : objid2info.values())
		{
			if ((oi.readThreads.size() != 1) || (oi.writeThreads.size() != 1)) continue;
			printObjectInfoToLine(oi, outNotShared);
		}		
		outNotShared.close();
			
		PrintStream outShared = new PrintStream(outputDir + File.separator + outputFileNameTemplate + "_shared");		
		for (ObjectInfo oi : objid2info.values())
		{
			if ((oi.readThreads.size() < 2) && (oi.writeThreads.size() < 2)) continue;
			printObjectInfoToLine(oi, outShared);
		}		
		outShared.close();
		

		// thread choices
		
		PrintStream outChoices = new PrintStream(outputDir + File.separator + outputFileNameTemplate + "_choice");
		for (String choiceStr : choices)
		{
			outChoices.println(choiceStr);	
		}		
		outChoices.close();
	}
	
	private void printObjectInfoToLine(ObjectInfo oi, PrintStream out)
	{
		out.print(oi.id);
		out.print(" : ");
			
		out.print("allocation site = '" + oi.allocationSite + "'");
		
		out.print(", reachable = " + oi.reachable);
		
		out.print(", reading threads = (");		
		boolean firstR = true;
		for (Integer thIndex : oi.readThreads)
		{
			if (firstR) firstR = false;
			else out.print(",");
			out.print(thIndex);
		}
		out.print(")");
		
		out.print(", writing threads = (");		
		boolean firstW = true;
		for (Integer thIndex : oi.writeThreads)
		{
			if (firstW) firstW = false;
			else out.print(",");
			out.print(thIndex);
		}
		out.print(")");
		
		out.println("");
	}

	private HeapGraph createHeapGraph(VM vm)
	{
		HeapGraph graph = new HeapGraph();

		Heap heap = vm.getHeap();
		ThreadInfo[] threads = vm.getThreadList().getThreads();
		
		for (int i = 0; i < threads.length; i++)
		{
			if (threads[i].isAlive())
			{
				HeapGraphNode nodeTh = new HeapGraphNode(threads[i].getClassInfo().getName() + "@" + threads[i].getThreadObjectRef(), threads[i].getClassInfo().getName());
				nodeTh.reachThreads.add(threads[i].getId());
				graph.roots.put(nodeTh.objID, threads[i].getThreadObjectRef());
				graph.objid2node.put(nodeTh.objID, nodeTh);
				
				if (threads[i].getRunnableRef() != -1)
				{
					HeapGraphNode nodeTgt = new HeapGraphNode(heap.get(threads[i].getRunnableRef()).getClassInfo().getName() + "@" + threads[i].getRunnableRef(), heap.get(threads[i].getRunnableRef()).getClassInfo().getName());
					nodeTgt.reachThreads.add(threads[i].getId());
					graph.roots.put(nodeTgt.objID, threads[i].getRunnableRef());
					graph.objid2node.put(nodeTgt.objID, nodeTgt);
				}
				
				for (StackFrame sf : threads[i])
				{
					for (int k = 0; k <= sf.getTopPos(); k++)
					{
						if (sf.isReferenceSlot(k))
						{
							int slotObjRef = sf.getSlot(k);
							
							if (slotObjRef == -1) continue;
							
							String slotObjID = heap.get(slotObjRef).getClassInfo().getName() + "@" + slotObjRef;
							
							HeapGraphNode nodeSlot = graph.objid2node.get(slotObjID);
							
							if (nodeSlot == null)
							{
								nodeSlot = new HeapGraphNode(slotObjID, heap.get(slotObjRef).getClassInfo().getName());
								graph.objid2node.put(nodeSlot.objID, nodeSlot);
							}
							
							nodeSlot.reachThreads.add(threads[i].getId());
							graph.roots.put(nodeSlot.objID, slotObjRef);
						}
					}				
				}
			}
		}
		
		for (ClassLoaderInfo cli : vm.getClassLoaderList())
		{
			for (ElementInfo ei : cli.getStatics())
			{
				if (ei == null) continue;
				
				StaticElementInfo sei = (StaticElementInfo) ei;
	
				HeapGraphNode nodeCI = new HeapGraphNode(sei.getClassInfo().getName(), sei.getClassInfo().getName());
				for (int k = 0; k < threads.length; k++) 
				{
					if (threads[k].isAlive()) nodeCI.reachThreads.add(threads[k].getId());
				}
				graph.roots.put(nodeCI.objID, -1);
				graph.objid2node.put(nodeCI.objID, nodeCI);
				
				for (int j = 0; j < sei.getClassInfo().getNumberOfStaticFields(); j++)
				{
					FieldInfo seiFI = sei.getFieldInfo(j);
					if (seiFI.isReference())
					{
						int fieldObjRef = sei.getReferenceField(seiFI.getName());
						
						if (fieldObjRef == -1) continue;
						
						String fieldObjID = heap.get(fieldObjRef).getClassInfo().getName() + "@" + fieldObjRef;
						
						HeapGraphNode nodeFI = graph.objid2node.get(fieldObjID);
								
						if (nodeFI == null)
						{
							nodeFI = new HeapGraphNode(fieldObjID, heap.get(fieldObjRef).getClassInfo().getName());
							graph.objid2node.put(nodeFI.objID, nodeFI);
						}
						
						for (int k = 0; k < threads.length; k++) 
						{	
							if (threads[k].isAlive()) nodeFI.reachThreads.add(threads[k].getId());
						}
						
						graph.roots.put(nodeFI.objID, fieldObjRef);
					}
				}
			}
		}
		
		Set<String> processedObjs = new HashSet<String>();
		for (Map.Entry<String,Integer> me : graph.roots.entrySet())
		{
			String rootObjID = me.getKey();
			Integer rootObjRef = me.getValue();
			
			processReferences(rootObjRef, rootObjID, graph, heap, processedObjs);
		}
		
		return graph;
	}
	
	private void processReferences(int objRef, String objID, HeapGraph graph, Heap heap, Set<String> processedObjs)
	{
		ElementInfo ei = heap.get(objRef);
		
		if (ei == null) return;
		
		if (processedObjs.contains(objID)) return;
		
		processedObjs.add(objID);
		
		
		HeapGraphNode nodeObj = graph.objid2node.get(objID);
		
		for (int i = 0; i < ei.getNumberOfFields(); i++)
		{
			FieldInfo fi = ei.getFieldInfo(i);
			if (fi.isReference())
			{
				int fieldObjRef = ei.getReferenceField(fi.getName());
				
				if (fieldObjRef == -1) continue;
				
				String fieldObjID = heap.get(fieldObjRef).getClassInfo().getName();
				
				HeapGraphNode nodeFI = graph.objid2node.get(fieldObjID);
						
				if (nodeFI == null)
				{
					nodeFI = new HeapGraphNode(fieldObjID, heap.get(fieldObjRef).getClassInfo().getName());
					graph.objid2node.put(nodeFI.objID, nodeFI);
				}
				
				nodeFI.reachThreads.addAll(nodeObj.reachThreads);

				nodeObj.pointers.add(nodeFI.objID);
				
				processReferences(fieldObjRef, fieldObjID, graph, heap, processedObjs);
			}
		}
	}
	
	private void saveHeapGraphToDOT(int stateIndex, HeapGraph graph) throws Exception
	{	
		PrintStream outGraph = new PrintStream(outputDir + File.separator + outputFileNameTemplate + "_heap_" + stateIndex + ".dot");
		
		outGraph.println("digraph Heap"+stateIndex+" {");

		for (HeapGraphNode node : graph.objid2node.values())
		{
			String nodeID = getShortClassNameForDOT(node.className) + "_" + node.objID;
			
			String nodeLabel = getShortClassNameForDOT(node.className) + ":" + node.objID;
			nodeLabel += "\\n reachable from ";
			boolean first = true;
			for (Integer thIndex : node.reachThreads)
			{
				if (first) first = false;
				else nodeLabel += ",";
				nodeLabel += ("T"+thIndex.toString());
			}			
			
			String nodeStyle = "color=black";
			if (graph.roots.containsKey(node.objID)) nodeStyle = "color=red,style=bold";
			
			outGraph.println(nodeID + " [shape=box,label=\""+nodeLabel+"\","+nodeStyle+"];");
		}
		
		for (HeapGraphNode parentNode : graph.objid2node.values())
		{	
			String parentNodeShortID = getShortClassNameForDOT(parentNode.className) + "_" + parentNode.objID;
			
			for (String childNodeID : parentNode.pointers)
			{
				HeapGraphNode childNode = graph.objid2node.get(childNodeID);
				if (childNode == null) continue;

				String childNodeShortID = getShortClassNameForDOT(childNode.className) + "_" + childNode.objID;

				outGraph.println(parentNodeShortID + " -> " + childNodeShortID);				
			}
		}
		
		outGraph.println("}");
		
		outGraph.close();
	}
	
	private String getShortClassNameForDOT(String fullClassName)
	{
		if (fullClassName.startsWith("[L")) fullClassName = "[" + fullClassName.substring(2, fullClassName.length()-1);
		
		String shortClassName = fullClassName;
		int k = fullClassName.lastIndexOf('.');
		if (k != -1) shortClassName = fullClassName.substring(k+1);
		
		shortClassName = shortClassName.replace('$', '_');

		if (shortClassName.startsWith("[")) shortClassName = shortClassName.substring(1) + "_array";
		
		return shortClassName;
	}
	

	public static class AccessInfo
	{
		public String oid; // class name + heap address
		
		public String className;
		
		public String fieldName;
		
		// true -> object reachable from multiple threads
		public boolean reachable;
		
		public String insnName;
		
		public String sourceCodeLoc; // file name + line number
		
		public int threadIndex;		
	}
	
	public static class ObjectInfo
	{
		public String id; // class name + heap address
		
		public String allocationSite;
		
		public String className;
		
		public boolean reachable;
		
		public Set<Integer> readThreads;
		public Set<Integer> writeThreads;
		
		public ObjectInfo(String id, String allocSite, String clsName, boolean isReachable)
		{
			this.id = id;
			this.allocationSite = allocSite;
			this.className = clsName;
			this.reachable = isReachable;
			
			readThreads = new TreeSet<Integer>();
			writeThreads = new TreeSet<Integer>();
		}
	}

	
	public static class HeapGraphNode
	{
		public String objID;
		
		public String className;
		
		 // list of threads that can reach this object
		public Set<Integer> reachThreads;
		
		public Set<String> pointers;
		
		public HeapGraphNode(String oid, String clsName)
		{
			this.objID = oid;
			this.className = clsName;
			this.reachThreads = new TreeSet<Integer>();
			this.pointers = new TreeSet<String>();
		}
	}
	
	public static class HeapGraph
	{
		public Map<String, HeapGraphNode> objid2node;
		
		public Map<String, Integer> roots; 
		
		public HeapGraph()
		{
			objid2node = new HashMap<String, HeapGraphNode>();
			roots = new TreeMap<String, Integer>();
		}
	}
}
