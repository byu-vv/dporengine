package jpf.tools;

import java.util.Set;
import java.util.HashSet;
import java.util.TreeSet;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.choice.ThreadChoiceFromSet;
import gov.nasa.jpf.jvm.bytecode.FieldInstruction;


public class ChoiceGenMonitor extends ListenerAdapter 
{
	private Set<String> fieldThreadChoices = new TreeSet<String>();
	
	
	public ChoiceGenMonitor(Config config, JPF jpf) 
	{
	}
	
	public void choiceGeneratorRegistered(VM vm, ChoiceGenerator<?> nextCG, ThreadInfo curTh, Instruction insn) 
	{
		if ( ! (nextCG instanceof ThreadChoiceFromSet) ) return;

		if (insn == null) return;
		
		if ( ! (insn instanceof FieldInstruction) ) return;

		StringBuffer thChoiceInfo = new StringBuffer();

		MethodInfo mi = insn.getMethodInfo();
		if (mi == null) return;
		
		ClassInfo mci = mi.getClassInfo();
		if (mci == null) return;

		thChoiceInfo.append(curTh.getId());
		thChoiceInfo.append(":");
		thChoiceInfo.append(mci.getName());
		thChoiceInfo.append(".");
		thChoiceInfo.append(mi.getUniqueName());
		thChoiceInfo.append("[");
		thChoiceInfo.append(insn.getPosition());
		thChoiceInfo.append("]");
	
		FieldInstruction fieldInsn = (FieldInstruction) insn;
		
		thChoiceInfo.append(": ");
		thChoiceInfo.append(fieldInsn.getFieldInfo().getFullName());
		
		// program counter of all threads
		for (ThreadInfo th : vm.getThreadList())
		{
			Instruction thpcInsn = th.getPC();
			if (thpcInsn == null) continue;
			
			MethodInfo thpcMI = thpcInsn.getMethodInfo();
			if (thpcMI == null) continue;
			
			ClassInfo thpcMCI = thpcMI.getClassInfo();
			if (thpcMCI == null) continue;
	
			thChoiceInfo.append("\n\t");
			thChoiceInfo.append(th.getId());
			thChoiceInfo.append(":");
			thChoiceInfo.append(thpcMCI.getName());
			thChoiceInfo.append(".");
			thChoiceInfo.append(thpcMI.getUniqueName());
			thChoiceInfo.append("[");
			thChoiceInfo.append(thpcInsn.getPosition());
			thChoiceInfo.append("]");
		}

		fieldThreadChoices.add(thChoiceInfo.toString());
	}
	
	public void searchFinished(Search search) 
	{
		System.out.println("[THREAD CHOICES (FIELD ACCESS)]");
		
		for (String thChoiceStr : fieldThreadChoices)
		{
			System.out.println(thChoiceStr);
		}  
	}	

}

