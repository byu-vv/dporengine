package jpf.tools;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.jvm.bytecode.FieldInstruction;


public class ExecTracker extends ListenerAdapter 
{
	public ExecTracker(Config config, JPF jpf) 
	{
	}
	 
	public void choiceGeneratorRegistered(VM vm, ChoiceGenerator<?> nextCG, ThreadInfo ti, Instruction insn) 
	{
		System.out.println("\t # new choice: " + nextCG);

		if (insn == null) return;
		
		MethodInfo mi = insn.getMethodInfo();
		if (mi == null) return;
		
		ClassInfo mci = mi.getClassInfo();
		if (mci == null) return;

		System.out.print("\t\t");

		System.out.print("tid=" + Integer.toString(ti.getId()) + " : ");
		
		System.out.print(mci.getName());
		System.out.print(".");
		System.out.print(mi.getUniqueName());
		
		System.out.print("[");
		System.out.print(insn.getInstructionIndex());
		System.out.print("] ");
		  
		System.out.print(insn);
		
		if (insn instanceof FieldInstruction)
		{
			FieldInstruction fieldInsn = (FieldInstruction) insn;
			System.out.print(" " + fieldInsn.getFieldInfo().getFullName());
		}
	
		System.out.println();
	}

	public void stateAdvanced(Search search) 
	{
		System.out.println("\t # new state: id = " + search.getStateId());
	}	

}

