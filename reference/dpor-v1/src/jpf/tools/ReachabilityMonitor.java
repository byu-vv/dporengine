package jpf.tools;

import java.util.List;
import java.util.Set;
import java.util.HashSet;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.Heap;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.SharedObjectPolicy;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.ArrayFields;

import jpf.vm.HeapReachabilityPolicy;


public class ReachabilityMonitor extends ListenerAdapter 
{
	public ReachabilityMonitor(Config config, JPF jpf) 
	{
	}

	public void stateAdvanced(Search search) 
	{
		VM vm = search.getVM();

		System.out.println("[HEAP REACHABILITY] state id = " + search.getStateId());

		HeapReachabilityPolicy policy = (HeapReachabilityPolicy) SharedObjectPolicy.getPolicy();

		Heap progHeap = vm.getHeap();

		// print data for each thread
		for (ThreadInfo th : vm.getThreadList())
		{
			System.out.println("thread id = " + th.getId());
	
			// print objects reachable from the thread
	
			List<Integer> reachObjRefs = policy.getObjectsReachableFromThread(th);
				
			for (Integer objRef : reachObjRefs)
			{
				ElementInfo obj = progHeap.get(objRef);
					
				if (obj == null) continue;
					
				if (obj.getClassInfo() == null) 
				{
					System.out.println("  object: id = " + objRef + ", class name = null");
					continue;
				}
				
				if (isIgnoredClass(obj.getClassInfo().getName())) continue;
				
				System.out.println("  object: id = " + objRef + ", class name = " + obj.getClassInfo().getName());
				
				// print "reachability chains" of the length 1	
				// loop over all fields of reference type and print target objects
				
				if (obj.isArray())
				{
					ArrayFields objArrFields = (ArrayFields) obj.getFields();
					
					if (objArrFields.isReferenceArray())
					{
						for (int i = 0; i < objArrFields.arrayLength(); i++)
						{
							int arrElemObjRef = objArrFields.getReferenceValue(i);
							
							if (arrElemObjRef != -1) System.out.println("\t array element ["+i+"]: id = " + arrElemObjRef);
							else System.out.println("\t array element ["+i+"]: null");
						}
					}
				}
				else
				{
					for (int i = 0; i < obj.getClassInfo().getNumberOfInstanceFields(); i++)
					{
						FieldInfo fi = obj.getClassInfo().getInstanceField(i); 
						
						if (fi.isReference())
						{
							int fldTargetObjRef = obj.getReferenceField(fi);
							
							if (fldTargetObjRef != -1) System.out.println("\t field \"" + fi.getName() + "\": id = " + fldTargetObjRef);
							else System.out.println("\t field \"" + fi.getName() + "\": null");
						}					
					}				
				}
			}
		}
	}
	
	private boolean isIgnoredClass(String clsName)
	{
		if (clsName.equals("java.lang.Class")) return true;
		if (clsName.equals("java.lang.ClassLoader")) return true;
		if (clsName.equals("java.lang.String")) return true;
		if (clsName.equals("java.lang.Integer")) return true;
		if (clsName.equals("java.lang.Long")) return true;
		if (clsName.equals("java.lang.Boolean")) return true;
		
		if (clsName.equals("[C")) return true;
		if (clsName.equals("[Ljava.lang.Integer;")) return true;
		if (clsName.equals("[Ljava.lang.Long;")) return true;
		
		return false;	
	}
}

