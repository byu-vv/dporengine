package jpf.dynpor;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Stack;
import java.util.Collections;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.jvm.bytecode.FieldInstruction;
import gov.nasa.jpf.jvm.bytecode.GETFIELD;
import gov.nasa.jpf.jvm.bytecode.GETSTATIC;
import gov.nasa.jpf.jvm.bytecode.PUTFIELD;
import gov.nasa.jpf.jvm.bytecode.PUTSTATIC;
import gov.nasa.jpf.jvm.bytecode.DUP;
import gov.nasa.jpf.jvm.bytecode.ASTORE;
import gov.nasa.jpf.jvm.bytecode.MONITORENTER;
import gov.nasa.jpf.jvm.bytecode.MONITOREXIT;
import gov.nasa.jpf.jvm.bytecode.InvokeInstruction;
import gov.nasa.jpf.jvm.bytecode.ReturnInstruction;


public class HappensBeforeOrdering extends ListenerAdapter
{
	// map from thread ID to a list of visible instructions
	private static List<VisibleInsn>[] th2insnlist;
	
	// visible instructions performed in the current transition
	private static TransitionInfo curTr;
	
	// stack of transitions (current path)
	private static Stack<TransitionInfo> curPathTrs;
	
	// stack of field access instructions (current path)
	private static Stack<VisibleInsn> curPathFAs;
	
	private static Stack<Integer> curPathStates;

	// map from state id to the list of visible instructions that may happen after the state
	private static List<VisibleInsn>[] state2futaccs;

	
	public HappensBeforeOrdering(Config cfg, JPF jpf)
	{
		th2insnlist = new List[2];
		
		curPathTrs = new Stack<TransitionInfo>();
		
		curPathFAs = new Stack<VisibleInsn>();
		
		curPathStates = new Stack<Integer>();
		
		state2futaccs = new List[1024];
	}
	
	
	/**
	 * We collect instructions GETFIELD and PUTFIELD in this method so that everything is on the stack.
	 */
	public void executeInstruction(VM vm, ThreadInfo ti, Instruction insn)
	{
		if (ti.isFirstStepInsn()) curTr.threadId = ti.getId();
		
		if (insn instanceof FieldInstruction)
		{
			// we ignore field access instructions that do not correspond to any dynamic thread choice
			if ( ! ti.isFirstStepInsn() ) return;
		}
			
		VisibleInsn vi = null;
		
		if (insn instanceof GETFIELD)
		{
			GETFIELD gfInsn = (GETFIELD) insn;

			FieldInfo targetFI = gfInsn.getFieldInfo();
			
			if (SearchDriver.isRelevantFieldAccess(targetFI, ti, gfInsn.getMethodInfo(), gfInsn.getInstructionIndex()))
			{			
				int targetObjRef = ti.getTopFrame().peek();
			
				ElementInfo targetObjEI = vm.getHeap().get(targetObjRef);
				
				vi = new VisibleInsn(VisibleInsn.Type.FREAD, gfInsn.getMethodInfo(), gfInsn.getPosition(), targetObjRef, VisibleInsn.getNameFromCache(targetObjEI.getClassInfo().getName() + "." + targetFI.getName()), ti.getId());
			}
		}
		
		if (insn instanceof PUTFIELD)
		{
			PUTFIELD pfInsn = (PUTFIELD) insn;

			FieldInfo targetFI = pfInsn.getFieldInfo();
			
			if (SearchDriver.isRelevantFieldAccess(targetFI, ti, pfInsn.getMethodInfo(), pfInsn.getInstructionIndex()))
			{
				int targetObjRef = -1;
				switch (targetFI.getStorageSize())
				{
					case 1:
						targetObjRef = ti.getTopFrame().peek(1);
						break;
					case 2:
						targetObjRef = ti.getTopFrame().peek(2);
						break;
				}
	
				ElementInfo targetObjEI = vm.getHeap().get(targetObjRef);
				
				vi = new VisibleInsn(VisibleInsn.Type.FWRITE, pfInsn.getMethodInfo(), pfInsn.getPosition(), targetObjRef, VisibleInsn.getNameFromCache(targetObjEI.getClassInfo().getName() + "." + targetFI.getName()), ti.getId());
			}
		}
		
		if (insn instanceof MONITORENTER)
		{
			MONITORENTER menInsn = (MONITORENTER) insn;
			
			int targetObjRef = ti.getTopFrame().peek();
			
			ElementInfo targetObjEI = vm.getHeap().get(targetObjRef);
			
			vi = new VisibleInsn(VisibleInsn.Type.LOCK, menInsn.getMethodInfo(), menInsn.getPosition(), targetObjRef, targetObjEI.getClassInfo().getName(), ti.getId());
		}
		
		if (insn instanceof MONITOREXIT)
		{
			MONITOREXIT mexInsn = (MONITOREXIT) insn;
			
			int targetObjRef = ti.getTopFrame().peek();
			
			ElementInfo targetObjEI = vm.getHeap().get(targetObjRef);
			
			vi = new VisibleInsn(VisibleInsn.Type.UNLOCK, mexInsn.getMethodInfo(), mexInsn.getPosition(), targetObjRef, targetObjEI.getClassInfo().getName(), ti.getId());
		}
		
		if (insn instanceof ReturnInstruction)
		{
			ReturnInstruction retInsn = (ReturnInstruction) insn;
			
			if (retInsn.getMethodInfo().isSynchronized())
			{
				int targetObjRef = -1;
				
				if (retInsn.getMethodInfo().isStatic())
				{
					targetObjRef = retInsn.getMethodInfo().getClassInfo().getClassObjectRef();	
				}
				else
				{
					targetObjRef = ti.getThis();
				} 
				
				ElementInfo targetObjEI = ti.getElementInfo(targetObjRef);
			
				vi = new VisibleInsn(VisibleInsn.Type.UNLOCK, retInsn.getMethodInfo(), retInsn.getPosition(), targetObjRef, targetObjEI.getClassInfo().getName(), ti.getId());
			}
		}
				
		if (vi != null)
		{
			updateHappensBeforeData(vi);
			
			curTr.insnList.add(vi);			
		}
	}
	
	/**
	 * We collect instructions GETSTATIC and PUTSTATIC in this method so that class objects are initialized already.
	 */
	public void instructionExecuted(VM vm, ThreadInfo ti, Instruction nextInsn, Instruction execInsn)
	{
		if (ti.isFirstStepInsn()) curTr.threadId = ti.getId();
		
		if (execInsn instanceof FieldInstruction)
		{
			// we ignore field access instructions that do not correspond to any dynamic thread choice
			if ( ! ti.isFirstStepInsn() ) return;
		}
		
		VisibleInsn vi = null;
		
		if (execInsn instanceof GETSTATIC)
		{
			GETSTATIC gsInsn = (GETSTATIC) execInsn;

			FieldInfo targetFI = gsInsn.getFieldInfo();
			
			if (SearchDriver.isRelevantFieldAccess(targetFI, ti, gsInsn.getMethodInfo(), gsInsn.getInstructionIndex()))
			{
				ClassInfo targetClass = targetFI.getClassInfo();
				
				ElementInfo targetClassEI = targetClass.getStaticElementInfo();
				
				vi = new VisibleInsn(VisibleInsn.Type.FREAD, gsInsn.getMethodInfo(), gsInsn.getPosition(), -1, VisibleInsn.getNameFromCache(targetClass.getName() + "." + targetFI.getName()), ti.getId());				
			}
		}
		
		if (execInsn instanceof PUTSTATIC)
		{
			PUTSTATIC psInsn = (PUTSTATIC) execInsn;
			
			FieldInfo targetFI = psInsn.getFieldInfo();
			
			if (SearchDriver.isRelevantFieldAccess(targetFI, ti, psInsn.getMethodInfo(), psInsn.getInstructionIndex()))
			{			
				ClassInfo targetClass = targetFI.getClassInfo();
								
				ElementInfo targetClassEI = targetClass.getStaticElementInfo();
				
				vi = new VisibleInsn(VisibleInsn.Type.FWRITE, psInsn.getMethodInfo(), psInsn.getPosition(), -1, VisibleInsn.getNameFromCache(targetClass.getName() + "." + targetFI.getName()), ti.getId());
			}
		}
		
		if (execInsn instanceof InvokeInstruction)
		{
			InvokeInstruction invokeInsn = (InvokeInstruction) execInsn;
		
			MethodInfo tgtMethod = invokeInsn.getInvokedMethod();
		
			int targetObjRef = -1; 
			ElementInfo targetObjEI = null;
			
			if (tgtMethod.isStatic())
			{
				targetObjEI = tgtMethod.getClassInfo().getStaticElementInfo();
				targetObjRef = -1;
			}
			else
			{
				targetObjRef = ti.getCalleeThis(invokeInsn.getArgSize());
				targetObjEI = vm.getHeap().get(targetObjRef);
			}
			
			// class object not yet initialized
			if (targetObjEI == null) return;
			
			String tgtMthName = tgtMethod.getName();
			
			ClassInfo tgtMthCI = tgtMethod.getClassInfo();
			
			if (tgtMthName.equals("wait"))
			{
				vi = new VisibleInsn(VisibleInsn.Type.WAIT, invokeInsn.getMethodInfo(), invokeInsn.getPosition(), targetObjRef, targetObjEI.getClassInfo().getName(), ti.getId());
			}
			
			if (tgtMthName.equals("notify"))
			{
				vi = new VisibleInsn(VisibleInsn.Type.NOTIFY, invokeInsn.getMethodInfo(), invokeInsn.getPosition(), targetObjRef, targetObjEI.getClassInfo().getName(), ti.getId());
			}
			
			if (tgtMthName.equals("start") && isThreadClass(tgtMthCI))
			{
				vi = new VisibleInsn(VisibleInsn.Type.TSTART, invokeInsn.getMethodInfo(), invokeInsn.getPosition(), targetObjRef, targetObjEI.getClassInfo().getName(), ti.getId());
			}
			
			if (tgtMthName.equals("join") && isThreadClass(tgtMthCI))
			{
				vi = new VisibleInsn(VisibleInsn.Type.TJOIN, invokeInsn.getMethodInfo(), invokeInsn.getPosition(), targetObjRef, targetObjEI.getClassInfo().getName(), ti.getId());
			}
			
			if (tgtMethod.isSynchronized())
			{
				vi = new VisibleInsn(VisibleInsn.Type.LOCK, invokeInsn.getMethodInfo(), invokeInsn.getPosition(), targetObjRef, targetObjEI.getClassInfo().getName(), ti.getId());
			}
		}

		if (vi != null)
		{
			updateHappensBeforeData(vi);
			
			curTr.insnList.add(vi);
		}
	}
	
	public void searchStarted(Search search) 
	{
		curTr = new TransitionInfo();
		
		curPathStates.push(-1);
	}
	
	public void stateAdvanced(Search search) 
	{
		List<VisibleInsn> thInsnList = null;
	
		if (curTr.threadId >= th2insnlist.length)
		{
			List<VisibleInsn>[] newT2IL = new List[curTr.threadId + 1];
			System.arraycopy(th2insnlist, 0, newT2IL, 0, th2insnlist.length);
			th2insnlist = newT2IL;
		}
		else
		{
			thInsnList = th2insnlist[curTr.threadId];
		}
		
		if (thInsnList == null)
		{
			thInsnList = new ArrayList<VisibleInsn>();
			th2insnlist[curTr.threadId] = thInsnList;
		}
		
		thInsnList.addAll(curTr.insnList);

		for (VisibleInsn vi : curTr.insnList)
		{
			if (vi.isFieldAccessEvent()) curPathFAs.push(vi);
		}		
		
		curPathTrs.push(curTr);
		
		curTr = new TransitionInfo();
		
		curPathStates.push(search.getStateId());
	}
	
	public void stateBacktracked(Search search) 
	{
		TransitionInfo tri = curPathTrs.peek();
		
		// remove instructions that belong to the backtracked transitions		
		List<VisibleInsn> thInsnList = th2insnlist[tri.threadId];
		for (VisibleInsn vi : tri.insnList)
		{
			thInsnList.remove(thInsnList.size()-1);
			if (vi.isFieldAccessEvent()) curPathFAs.pop();
		}
		
		curPathTrs.pop();
		
		Integer bkfromStateId = curPathStates.peek();		
		curPathStates.pop();
		Integer bktoStateId = curPathStates.peek();

		// initial state
		if (bktoStateId.intValue() == -1) return;

		// get field accesses that may happen after the state we backtrack from		
		List<VisibleInsn> bkfromStateFutureAccesses = null;
		if (bkfromStateId.intValue() < state2futaccs.length) bkfromStateFutureAccesses = state2futaccs[bkfromStateId.intValue()];

		List<VisibleInsn> bktoStateFutureAccesses = null;
		if (bktoStateId.intValue() < state2futaccs.length) bktoStateFutureAccesses = state2futaccs[bktoStateId.intValue()];

		// compute the new set of future field accesses
		
		List<VisibleInsn> futureAccesses = null;
		
		boolean bktoChoiceWithSingleOption = false;
		ChoiceGenerator cg = search.getVM().getChoiceGenerator();
		if (cg instanceof DynamicThreadChoice)
		{
			DynamicThreadChoice dcg = (DynamicThreadChoice) cg;
			if ( ! dcg.allThreadsEnabled() ) bktoChoiceWithSingleOption = true;
		}
		
		if (bktoChoiceWithSingleOption)
		{
			// field access that is associated with this dynamic thread choice is not a globally visible instruction
				// it was not marked as globally relevant when field accesses on the current path were analyzed (SearchDriver)
			// we can keep the value from the single next state (from which JPF backtracks here)
			futureAccesses = bkfromStateFutureAccesses;
		}
		else
		{
			if (bktoStateFutureAccesses != null) futureAccesses = bktoStateFutureAccesses;
			
			if (futureAccesses == null) futureAccesses = new ArrayList<VisibleInsn>();
			
			if (bkfromStateFutureAccesses != null) 
			{
				for (VisibleInsn vi : bkfromStateFutureAccesses)
				{
					if ( ! futureAccesses.contains(vi) ) futureAccesses.add(vi);
				}
			}
		
			for (VisibleInsn vi : tri.insnList)
			{
				if (vi.isFieldAccessEvent()) 
				{
					if ( ! futureAccesses.contains(vi) ) futureAccesses.add(vi);
				}
			}
			
			if (futureAccesses.isEmpty()) futureAccesses = null;
		}
		
		if (bktoStateId.intValue() >= state2futaccs.length)
		{
			List<VisibleInsn>[] newS2FA = new List[bktoStateId.intValue() + 16384];
			System.arraycopy(state2futaccs, 0, newS2FA, 0, state2futaccs.length);
			state2futaccs = newS2FA;
		}
	
		// update field accesses that may happen after the state we backtrack to
		state2futaccs[bktoStateId.intValue()] = futureAccesses;
	}
	
	public static List<VisibleInsn> getFieldAccessesOnCurrentPath()
	{
		List<VisibleInsn> faList = new ArrayList<VisibleInsn>();
		
		faList.addAll(curPathFAs);
		
		for (VisibleInsn vi : curTr.insnList)
		{
			if (vi.isFieldAccessEvent()) faList.add(vi);
		}
		
		return faList;
	}
	
	public static List<VisibleInsn> getFutureFieldAccessesForCurrentState(int curStateId)
	{
		List<VisibleInsn> faSet = null;
		
		// this condition is not true when there is a loop in the state space (backjump)
		if (curStateId < state2futaccs.length)
		{		
			faSet = state2futaccs[curStateId];
		}
		
		if (faSet == null) faSet = Collections.emptyList();
		
		return faSet;
	}
		
	private static void updateHappensBeforeData(VisibleInsn curVI)
	{
		for (int thId = 0; thId < th2insnlist.length; thId++)
		{
			// skip non-existent thread
			if (th2insnlist[thId] == null) continue;
			
			// skip current thread which executed the given visible instruction
			if (thId == curVI.threadId) continue;
			
			List<VisibleInsn> insnList = th2insnlist[thId];
			
			VisibleInsn hbVI = null;
			
			for (int i = insnList.size() - 1; i >= 0; i--)
			{
				VisibleInsn vi = insnList.get(i);
				
				// test whether 'vi' must happen before 'curVI'
				if (curVI.isConflictingWith(vi))
				{
					hbVI = vi;
					break;
				}
			}
			
			if (hbVI != null)
			{
				if (curVI.th2prevhb.length <= thId)
				{
					VisibleInsn[] newT2PHB = new VisibleInsn[thId+1];
					System.arraycopy(curVI.th2prevhb, 0, newT2PHB, 0, curVI.th2prevhb.length);
					curVI.th2prevhb = newT2PHB;
				}
				
				curVI.th2prevhb[thId] = hbVI;
			}
		}		
	}
	
	private static boolean isThreadClass(ClassInfo ci)
	{
		if (ci.isThreadClassInfo()) return true;
		
		ClassInfo ciSuper = ci.getSuperClass();
		while (ciSuper != null)
		{
			if (ciSuper.isThreadClassInfo()) return true;
			ciSuper = ciSuper.getSuperClass();
		}
		
		return false;
	}
	
	
	static class TransitionInfo
	{
		// thread which executed this transition
		public int threadId;
		
		// visible instructions
		public List<VisibleInsn> insnList;
		
		
		public TransitionInfo()
		{
			insnList = new ArrayList<VisibleInsn>();			
		}
		
		public boolean equals(Object obj)
		{
			if (obj == null) return false;
			
			if ( ! (obj instanceof TransitionInfo) ) return false;
			
			TransitionInfo other = (TransitionInfo) obj;
			
			if (this.threadId != other.threadId) return false;			
			if ( ! this.insnList.equals(other.insnList) ) return false;
			
			return true;
		}
		
		public int hashCode()
		{
			int hash = 0;
			
			hash = hash * 31 + this.threadId;
			hash = hash * 31 + this.insnList.hashCode();
			
			return hash;
		} 
	}
} 
