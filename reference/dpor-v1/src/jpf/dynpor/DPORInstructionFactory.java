package jpf.dynpor;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPFException; 
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.jvm.bytecode.InstructionFactory;


public class DPORInstructionFactory extends InstructionFactory
{
	public DPORInstructionFactory(Config conf)
	{
	}

	public Instruction getfield(String fieldName, String clsName, String fieldDescriptor)
	{
		return new jpf.dynpor.bytecode.GETFIELD(fieldName, clsName, fieldDescriptor);
	}

	public Instruction getstatic(String fieldName, String clsName, String fieldDescriptor)
	{
		return new jpf.dynpor.bytecode.GETSTATIC(fieldName, clsName, fieldDescriptor);
	}
	
	public Instruction putfield(String fieldName, String clsName, String fieldDescriptor)
	{
		return new jpf.dynpor.bytecode.PUTFIELD(fieldName, clsName, fieldDescriptor);
	}

	public Instruction putstatic(String fieldName, String clsName, String fieldDescriptor)
	{
		return new jpf.dynpor.bytecode.PUTSTATIC(fieldName, clsName, fieldDescriptor);
	}
}
