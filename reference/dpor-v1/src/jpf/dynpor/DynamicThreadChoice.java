package jpf.dynpor;

import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.io.PrintWriter;

import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.choice.ThreadChoiceFromSet;
import gov.nasa.jpf.jvm.bytecode.FieldInstruction;


/**
 * Special thread choice generator to be used only at field accesses.
 * All choices except the current thread are disabled initially.
 */
public class DynamicThreadChoice extends ThreadChoiceFromSet  
{
	private static final boolean DEBUG = false;
	
	// choice ID
	private int idNum;
	
	// thread current when the choice is being created 
	private int curThID;
	
	// IDs of all enabled threads (choices)
	private int[] enabledThs;
	private int enabledThsCount;
	
	// IDs of all enabled but unexplored threads
	// we pick only from the beginning of the list
	private int[] unexploredThs;
	private int unexploredThsCount;
	
	// current position in the list of unexplored threads
	// possible values: -1, 0
	private int unexThPos = -1;
	
	// total number of thread choices with a multiple options
	public static int totalNumWithMultipleOptions;
	
	static
	{
		totalNumWithMultipleOptions = 0;
	}
	
	
	// we assume that the current thread is also in the set
	public DynamicThreadChoice(int num, ThreadInfo[] thSet, ThreadInfo curTh, ElementInfo ei)
	{
		super("dpor", thSet, true);
		
		this.idNum = num;
		
		curThID = curTh.getId();
		
		enabledThs = new int[thSet.length];
		enabledThs[0] = curTh.getId();
		enabledThsCount = 1;
		
		unexploredThs = new int[thSet.length];
		unexploredThs[0] = curTh.getId();
		unexploredThsCount = 1;
		
		if (DEBUG)
		{
			FieldInfo fi = ((FieldInstruction) curTh.getPC()).getFieldInfo();
				
			System.out.println("[DEBUG DynamicThreadChoice.<init>] new choice id = " + idNum);
			System.out.println("\t target field: name = " + fi.getClassInfo().getName() + "." + fi.getName() + ", object index = " + ei.getObjectRef());
			System.out.println("\t enabled threads = " + enabledThs);
			System.out.println("\t unexplored threads = " + unexploredThs);
		}
	}

	public void reset()
	{
		// the number of really explored choices
		count = 0;
		
		isDone = false;
		
		unexploredThsCount = 1;
		unexploredThs[0] = curThID;
		
		for (int i = 0; i < enabledThsCount; i++)
		{
			if (enabledThs[i] != curThID) 
			{
				unexploredThs[unexploredThsCount] = enabledThs[i];
				unexploredThsCount++;
			}
		}
			
		unexThPos = -1;
	}
	
	public ThreadInfo getNextChoice()
	{
		if (unexThPos < unexploredThsCount)
		{
			// we already know that a thread with the index equal to "unexThPos" is now enabled
			
			int nextThID = unexploredThs[unexThPos];
				
			for (int i = 0; i < values.length; i++)
			{
				if (values[i].getId() == nextThID) return values[i];
			}
		}
		
		return null;
	}
	
	public boolean hasMoreChoices()
	{
		if (isDone) return false;
		
		if ((unexThPos == -1) || (unexploredThsCount > 1)) return true;
		
		return false;
	}
	
	public void advance()
	{
		if (unexThPos == -1)
		{
			unexThPos = 0;
		}
		else
		{
			// remove the previously explored choice
			for (int i = unexThPos; i < unexploredThsCount - 1; i++) unexploredThs[i] = unexploredThs[i+1];
			unexploredThsCount--;
		}
		
		if (DEBUG)
		{
			System.out.println("[DEBUG DynamicThreadChoice.advance] choice id = " + idNum);
			if (unexploredThsCount > 0) System.out.println("\t next thread = " + unexploredThs[unexThPos]);
			else System.out.println("\t fully explored");
		}
		
		if (count < enabledThsCount) count++; 
	}
			
	public int getTotalNumberOfChoices() 
	{
		return enabledThsCount;
	}

	public int getProcessedNumberOfChoices() 
	{
		return count;
	}

	public Object getNextChoiceObject() 
	{
		return getNextChoice();
	}
  
	public ThreadInfo[] getChoices()
	{
		ThreadInfo[] enabledThObjs = new ThreadInfo[enabledThsCount];
		int pos = 0;
		
		for (int i = 0; i < values.length; i++)
		{
			for (int j = 0; j < enabledThsCount; j++)
			{
				if (values[i].getId() == enabledThs[j])
				{					
					enabledThObjs[pos++] = values[i];
					break;
				}
			}
		}
		
		return enabledThObjs;
	}
	
	public boolean supportsReordering()
	{
		return false;
	}
	
	public void printOn(PrintWriter pw) 
	{
		pw.print(getClass().getName());
		pw.append("[id=\"");
		pw.append(id);
		pw.append('"');

		pw.append(",isCascaded:");
		pw.append(Boolean.toString(isCascaded));

		pw.print(",{");
		for (int i = 0; i < values.length; i++) 
		{
			if (i > 0) pw.print(',');
			pw.print(values[i].getName()+"("+values[i].getId()+")");
		}		
		pw.print("}");
    
		pw.print(",enabled:{");
		for (int i = 0; i < enabledThsCount; i++)
		{
			if (i > 0) pw.print(',');
			pw.print(enabledThs[i]);
		}
		pw.print("}");
			
		pw.print(",unexplored:{");
		for (int i = 0; i < unexploredThsCount; i++)
		{
			if (i > 0) pw.print(',');
			if (i == unexThPos) pw.print(MARKER);			
			pw.print(unexploredThs[i]);
		}
		pw.print("}]");
	}
			
	public ThreadInfo[] getAllThreadChoices() 
	{
		return getChoices();
	}
  
	@Override
	public boolean contains(ThreadInfo th)
	{
		for (int i = 0; i < values.length; i++)
		{
			if (values[i] == th)
			{
				for (int j = 0; j < enabledThsCount; j++)
				{
					if (enabledThs[j] == th.getId()) return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean allThreadsEnabled()
	{
		return (enabledThsCount == values.length);	
	}
	
	public void enableAllThreads()
	{
		if (DEBUG) System.out.println("[DEBUG DynamicThreadChoice.enableAllThreads] choice id = " + idNum);

		if (enabledThsCount < values.length)
		{
			// some new threads will be enabled here
			totalNumWithMultipleOptions++;
		}
		
		for (int i = 0; i < values.length; i++)
		{
			int thId = values[i].getId();
			
			boolean isAlreadyEnabled = false;
			
			for (int j = 0; j < enabledThsCount; j++)
			{
				if (enabledThs[j] == thId) isAlreadyEnabled = true;
			}
			
			// we do not want to enable some thread twice
			if ( ! isAlreadyEnabled )
			{
				enabledThs[enabledThsCount++] = thId;		
				unexploredThs[unexploredThsCount++] = thId;
				
				if (DEBUG) 
				{
					System.out.println("\t thread ID = " + thId);
					System.out.println("\t enabled threads = " + enabledThs);
					System.out.println("\t unexplored threads = " + unexploredThs);
				}
			}
		}
	}
}
