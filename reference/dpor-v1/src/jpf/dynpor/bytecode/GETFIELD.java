package jpf.dynpor.bytecode;

import java.util.List;
import java.util.Set;

import gov.nasa.jpf.JPFException;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.SystemState;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ThreadList;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.Instruction;

import jpf.dynpor.SearchDriver;
import jpf.analysis.JPFUtils;


public class GETFIELD extends gov.nasa.jpf.jvm.bytecode.GETFIELD 
{
	public GETFIELD(String fieldName, String classType, String fieldDescriptor)
	{
		super(fieldName, classType, fieldDescriptor);
	}

	public Instruction execute(ThreadInfo ti) 
	{
		StackFrame frame = ti.getModifiableTopFrame();
		
		int objRef = frame.peek(); // don't pop yet, we might re-enter
		lastThis = objRef;
		if (objRef == -1) return ti.createAndThrowException("java.lang.NullPointerException", "referencing field '" + fname + "' on null object");
		
		ElementInfo ei = ti.getElementInfo(objRef);
		
		FieldInfo fi = getFieldInfo();
		if (fi == null) return ti.createAndThrowException("java.lang.NoSuchFieldError", "referencing field '" + fname + "' in " + ei);

		// check if this breaks the current transition
		if ( (!ti.isFirstStepInsn()) && SearchDriver.isRelevantFieldAccess(fi, ti, mi, insnIndex) )
		{
			boolean skipCG = false;
			
			if (SearchDriver.useFieldAnalysisResults)
			{		
				String className = fi.getClassInfo().getName();
				String fieldName = fi.getName();
				
				String curMthSig = JPFUtils.getMethodSignature(mi);
				
				boolean normalInstanceMth = ( ! mi.isCtor() ) && ( ! mi.isStatic() ) && ( ! mi.isInternalMethod() );
				
				if ( ! JPFUtils.existsFutureWrite(ti, curMthSig, getPosition(), className, fieldName, false, objRef, normalInstanceMth) ) skipCG = true;
			}
			
			if ( ! skipCG )
			{
				if (createAndSetSharedFieldAccessCG(ei, ti)) return this;
			}
		}
		
		frame.pop(); // Ok, now we can remove the object ref from the stack
		Object attr = ei.getFieldAttr(fi);
		
		// We could encapsulate the push in ElementInfo, but not the GET, so we keep it at a similiar level
		if (fi.getStorageSize() == 1) // 1 slotter
		{
			int ival = ei.get1SlotField(fi);
			lastValue = ival;
			
			if (fi.isReference()) frame.pushRef(ival);
			else frame.push(ival);
			
			if (attr != null) frame.setOperandAttr(attr);
			
		} 
		else // 2 slotter
		{
			long lval = ei.get2SlotField(fi);
			lastValue = lval;
			
			frame.pushLong( lval);
			if (attr != null) frame.setLongOperandAttr(attr);
		}
		
		return getNext(ti);
	}

}
