package jpf.dynpor.bytecode;

import java.util.List;
import java.util.Set;

import gov.nasa.jpf.JPFException;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.SystemState;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ThreadList;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.Instruction;

import jpf.dynpor.SearchDriver;
import jpf.analysis.JPFUtils;


public class PUTFIELD extends gov.nasa.jpf.jvm.bytecode.PUTFIELD
{
	public PUTFIELD() 
	{
	}

	public PUTFIELD(String fieldName, String clsDescriptor, String fieldDescriptor)
	{
		super(fieldName, clsDescriptor, fieldDescriptor);
	}

	public Instruction execute(ThreadInfo ti) 
	{
		StackFrame frame = ti.getTopFrame();
		int objRef = frame.peek(size);
		lastThis = objRef;
		
		if (!ti.isFirstStepInsn()) // top half
		{
			// if this produces an NPE, force the error w/o further ado
			if (objRef == -1) return ti.createAndThrowException("java.lang.NullPointerException", "referencing field '" + fname + "' on null object");
			
			ElementInfo ei = ti.getElementInfo(objRef);
			FieldInfo fi = getFieldInfo();
			if (fi == null) return ti.createAndThrowException("java.lang.NoSuchFieldError", "no field " + fname + " in " + ei);
			
			// check if this breaks the current transition
			// note this will also set the shared attribute of the field owner
			if (SearchDriver.isRelevantFieldAccess(fi, ti, mi, insnIndex)) 
			{
				boolean skipCG = false;
			
				if (SearchDriver.useFieldAnalysisResults)
				{		
					String className = fi.getClassInfo().getName();
					String fieldName = fi.getName();
					
					String curMthSig = JPFUtils.getMethodSignature(getMethodInfo());			
	
					if ( ! JPFUtils.existsFutureRead(ti, curMthSig, getPosition(), className, fieldName, false, objRef) ) skipCG = true;
				}
				
				if ( ! skipCG )
				{
					if (createAndSetSharedFieldAccessCG(ei, ti)) return this;
				}
			}
			
			return put(ti, frame, ei);
		} 
		else // re-execution
		{
			// no need to redo the exception checks, we already had them in the top half
			ElementInfo ei = ti.getElementInfo(objRef);
			
			return put(ti, frame, ei);
		} 
	}
	
}

