package jpf.dynpor;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.SystemState;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.DefaultSchedulerFactory;
import gov.nasa.jpf.vm.choice.ThreadChoiceFromSet;


/**
 * We use our custom thread choice generator only at field accesses.
 * It preserves all choices that JPF would create at instructions other than field accesses (thread start, monitor enter).
 */
public class DPORSchedulerFactory extends DefaultSchedulerFactory 
{
	private int nextID;
	
	public DPORSchedulerFactory(Config config, VM vm, SystemState ss)
	{
		super(config, vm, ss);
		
		nextID = 1;
	}
	
	public ChoiceGenerator<ThreadInfo> createSharedFieldAccessCG(ElementInfo ei, ThreadInfo ti) 
	{
		if (ss.isAtomic()) return null;
		
		ThreadInfo[] runThSet = getRunnablesIfChoices(ti);
		
		if (runThSet != null) 
		{
			return new DynamicThreadChoice(nextID++, runThSet, ti, ei);			
		}
		else 
		{
			return null;
		}
	}
}
