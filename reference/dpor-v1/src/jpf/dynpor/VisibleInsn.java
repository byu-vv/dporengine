package jpf.dynpor;

import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

import gov.nasa.jpf.vm.MethodInfo;


public class VisibleInsn
{
	public Type insnType;
	
	// code location of this instruction
	public MethodInfo mth;
	public int insnPos;

	// heap address of target object		
	public int targetObjectRef;
	
	// class name and field name for field access instructions, only class name for all other instructions
	public String targetName;
	
	// thread which executed this instruction
	public int threadId;
	
	// map from thread ID to the nearest previous instruction in the given thread that must happen before this instruction
	public VisibleInsn[] th2prevhb;
	
	private boolean processed;
	
	private int hc = 0;
	
	
	public static Map<String,String> nameCache;
	
	
	static enum Type
	{
		FREAD,
		FWRITE,
		LOCK,
		UNLOCK,
		WAIT,
		NOTIFY,
		TSTART,
		TJOIN
	}
	
	static
	{
		nameCache = new HashMap<String, String>();	
	}
	
	
	public VisibleInsn(Type type, MethodInfo m, int pos, int objRef, String tgtName, int thId)
	{
		insnType = type;
		mth = m;
		insnPos = pos;
		targetObjectRef = objRef;
		targetName = tgtName;
		threadId = thId;
		
		th2prevhb = new VisibleInsn[2];
		
		processed = false;
	}
	
	public boolean isConflictingWith(VisibleInsn other)
	{
		if (this.threadId == other.threadId) return false;
		
		if (this.insnType == Type.FREAD)
		{
			if ((other.insnType == Type.FWRITE) && (this.targetObjectRef == other.targetObjectRef) && this.targetName.equals(other.targetName)) return true;
		}
		
		if (this.insnType == Type.FWRITE)
		{
			if ((other.insnType == Type.FREAD) && (this.targetObjectRef == other.targetObjectRef) && this.targetName.equals(other.targetName)) return true;
		}
		
		if (this.isSynchronizationEvent())
		{
			if (other.isSynchronizationEvent() && (this.targetObjectRef == other.targetObjectRef)) return true;
		}
		
		return false;			
	}
	
	public boolean isFieldAccessEvent()
	{
		return ((insnType == Type.FREAD) || (insnType == Type.FWRITE));
	}
	
	public boolean isSynchronizationEvent()
	{
		return ((insnType == Type.LOCK) || (insnType == Type.UNLOCK) || (insnType == Type.WAIT) || (insnType == Type.NOTIFY));
	}
	
	public boolean mustHappenAfter(VisibleInsn other)
	{
		if (other.threadId >= th2prevhb.length) return false;
		
		VisibleInsn thPrev = th2prevhb[other.threadId];
		
		if (thPrev == other) return true;
		
		return false;
	}
	
	public void setProcessed(boolean b)
	{
		processed = b;
	}
	
	public boolean isProcessed()
	{
		return processed;
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof VisibleInsn) ) return false;
		
		VisibleInsn other = (VisibleInsn) obj;
		
		if (this.insnType != other.insnType) return false;
		if ( ! this.mth.getClassName().equals(other.mth.getClassName()) ) return false;
		if ( ! this.mth.getUniqueName().equals(other.mth.getUniqueName()) ) return false;
		if (this.insnPos != other.insnPos) return false;
		if (this.targetObjectRef != other.targetObjectRef) return false;
		if ( ! this.targetName.equals(other.targetName) ) return false;
		if (this.threadId != other.threadId) return false;
		
		if ( ! Arrays.equals(this.th2prevhb, other.th2prevhb) ) return false;
		
		return true;
	}
	
	public int hashCode()
	{
		if (hc != 0) return hc;
		
		hc = hc * 31 + this.insnType.hashCode();		
		hc = hc * 31 + this.mth.getClassName().hashCode();
		hc = hc * 31 + this.mth.getUniqueName().hashCode();		
		hc = hc * 31 + this.insnPos;
		hc = hc * 31 + this.targetObjectRef;
		hc = hc * 31 + this.targetName.hashCode();
		hc = hc * 31 + this.threadId;
		hc = hc * 31 + Arrays.hashCode(this.th2prevhb);
		
		return hc;
	} 
	
	public String toString()
	{
		return printToString(true);
	}
	
	public String printToString(boolean includeHB)
	{
		StringBuffer strbuf = new StringBuffer();
		
		if (insnType == Type.FREAD) strbuf.append("read field ");
		if (insnType == Type.FWRITE) strbuf.append("write field ");
		if (insnType == Type.LOCK) strbuf.append("lock ");
		if (insnType == Type.UNLOCK) strbuf.append("unlock ");
		if (insnType == Type.WAIT) strbuf.append("wait ");
		if (insnType == Type.NOTIFY) strbuf.append("notify ");
		if (insnType == Type.TSTART) strbuf.append("start thread ");
		if (insnType == Type.TJOIN) strbuf.append("join thread ");
					
		strbuf.append(targetName);
		strbuf.append("@");
		strbuf.append(targetObjectRef);
		
		strbuf.append(" at ");
		
		strbuf.append(threadId);
		strbuf.append(":");
		strbuf.append(mth.getFullName());
		strbuf.append(":");
		strbuf.append(insnPos);
		
		if (includeHB)
		{
			for (VisibleInsn vi : th2prevhb)
			{
				strbuf.append("\t ");
				strbuf.append(vi.printToString(false));
			}
		}
		
		return strbuf.toString();
	}
	
	public static String getNameFromCache(String name)
	{
		if (nameCache.containsKey(name))
		{
			return nameCache.get(name);
		}
		else
		{
			nameCache.put(name, name);
			return name;
		}		
	}
}
