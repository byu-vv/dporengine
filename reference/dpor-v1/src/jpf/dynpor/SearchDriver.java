package jpf.dynpor;

import java.util.List;
import java.util.ArrayList;
import java.util.Stack;
import java.util.Map;
import java.util.TreeMap;
import java.util.Set;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.jvm.bytecode.DUP;
import gov.nasa.jpf.jvm.bytecode.ASTORE;
import gov.nasa.jpf.jvm.bytecode.MONITORENTER;


public class SearchDriver extends ListenerAdapter
{
	private static final boolean DEBUG = false;
	
	public static boolean useFieldAnalysisResults = false;
	
	
	public SearchDriver(Config cfg, JPF jpf)
	{
		useFieldAnalysisResults = cfg.getBoolean("dynpor.field_analysis", false);
	}
	
	public void stateAdvanced(Search search) 
	{
		// modify choices based on recorded field accesses
		if (search.isEndState() || search.isVisitedState())
		{
			if (DEBUG) System.out.println("[DEBUG SearchDriver.stateAdvanced] end of the current path reached (state id = " + search.getStateId() + ")");
			
			// get all field accesses on the current path
			List<VisibleInsn> curPathFieldAccesses = HappensBeforeOrdering.getFieldAccessesOnCurrentPath();
			
			// get all future field accesses that can happen after the current state
			List<VisibleInsn> curStateFutureAccesses = HappensBeforeOrdering.getFutureFieldAccessesForCurrentState(search.getStateId());

			// get all choice generators associated with field accesses on the current path			
			DynamicThreadChoice[] curPathDynThChoices = search.getVM().getChoiceGeneratorsOfType(DynamicThreadChoice.class);
			
			if (DEBUG)
			{
				System.out.println("[DEBUG SearchDriver.stateAdvanced] current path: field accesses = " + curPathFieldAccesses.size() + ", dynamic thread choices = " + curPathDynThChoices.length);
			}
			
			// process future field acceses
			for (int recAccessIdx = curPathFieldAccesses.size() - 1; recAccessIdx >= 0; recAccessIdx--)
			{
				VisibleInsn recFieldAccess = curPathFieldAccesses.get(recAccessIdx);
				
				// the prefix of this execution path was explored before
				if (recFieldAccess.isProcessed()) break;
				
				boolean existsConflict = false;
				
				for (VisibleInsn futureFieldAccess : curStateFutureAccesses)
				{
					if (recFieldAccess.isConflictingWith(futureFieldAccess)) 
					{
						existsConflict = true;
						break;
					}
				}
				
				if (existsConflict)
				{
					curPathDynThChoices[recAccessIdx].enableAllThreads();	
				}
			}			
			
			// loop over the recorded field accesses
			for (int recAccessIdx = curPathFieldAccesses.size() - 1; recAccessIdx >= 0; recAccessIdx--)
			{
				VisibleInsn recFieldAccess = curPathFieldAccesses.get(recAccessIdx);
				
				// the prefix of this execution path was explored before
				if (recFieldAccess.isProcessed()) break;
				
				// for the recorded field access, get every preceding access to the same field on the same object that (1) is in a conflict with the recorded field access but (2) does not happen before some other action preceding the recorded field access in the same thread, and get the associated choice generator too
					// we use indexes to find relevant choice generators (this works because our dynamic choice generator is created for every field access so the indexes match)
				
				for (int prevAccessIdx = 0; prevAccessIdx < recAccessIdx; prevAccessIdx++)
				{
					// we skip this previous access because the associated choice has all threads enabled already
					if (curPathDynThChoices[prevAccessIdx].allThreadsEnabled()) continue;					
					
					VisibleInsn prevFieldAccess = curPathFieldAccesses.get(prevAccessIdx);
					
					if (recFieldAccess.isConflictingWith(prevFieldAccess))
					{
						boolean conflictWithOtherAction = false;
						
						// loop over instructions from the thread executing 'recFieldAccess' 
						for (int recThOtherAccessIdx = prevAccessIdx + 1; recThOtherAccessIdx < recAccessIdx; recThOtherAccessIdx++)
						{
							VisibleInsn recThOtherFieldAccess = curPathFieldAccesses.get(recThOtherAccessIdx);
							
							if (recThOtherFieldAccess.threadId != recFieldAccess.threadId) continue;
						
							// loop over instructions from the thread executing 'prevFieldAccess' 
							for (int prevThOtherAccessIdx = prevAccessIdx + 1; prevThOtherAccessIdx < recThOtherAccessIdx; prevThOtherAccessIdx++)
							{
								VisibleInsn prevThOtherFieldAccess = curPathFieldAccesses.get(prevThOtherAccessIdx);
							
								if (prevThOtherFieldAccess.threadId != prevFieldAccess.threadId) continue;
						
								if (recThOtherFieldAccess.mustHappenAfter(prevThOtherFieldAccess)) 
								{
									conflictWithOtherAction = true;
									break;
								}
							}
							
							if (conflictWithOtherAction) break;
						}
				
						if ( ! conflictWithOtherAction )
						{
							if (DEBUG) 
							{
								System.out.println("[DEBUG SearchDriver.stateAdvanced] recorded field access (" + recAccessIdx + "):\n\t " + recFieldAccess.printToString(false));							
								System.out.println("[DEBUG SearchDriver.stateAdvanced] conflicting previous field access (" + prevAccessIdx + "):\n\t " + prevFieldAccess.printToString(false));
							}
					
							// we must enable all remaining threads in the choice generator
							curPathDynThChoices[prevAccessIdx].enableAllThreads();
						}
					}
				}
			
				recFieldAccess.setProcessed(true);
			}
		}
	}
	
	public void searchFinished(Search search)
	{
		System.out.println("[INFO] dynamic thread choices with multiple options: " + DynamicThreadChoice.totalNumWithMultipleOptions);
	}

	public static boolean isRelevantFieldAccess(FieldInfo fi, ThreadInfo ti, MethodInfo mi, int insnPos)
	{
		// choice generator is always created inside the application classes
			// exceptions: class init, monitor enter prologue
			
		if (fi.neverBreak()) return false;
		
		if (mi.isClinit() && (fi.getClassInfo().equals(mi.getClassInfo()))) return false;
		
		// we also check for monitor enter prologue		
		boolean existsPrologue = false;
		
		Instruction[] code = mi.getInstructions();
		
		insnPos++;
		
		if (insnPos < (code.length - 3))
		{ 
			if (code[insnPos] instanceof DUP) 
			{ 
				insnPos++;
				
				if (code[insnPos] instanceof ASTORE) insnPos++;
					
				if (code[insnPos] instanceof MONITORENTER) existsPrologue = true;
			}
		}
		
		if (existsPrologue) return false;
		
		return true;
	}
		
}
