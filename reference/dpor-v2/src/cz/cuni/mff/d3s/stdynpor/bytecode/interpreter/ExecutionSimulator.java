/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.bytecode.interpreter;

import java.util.List;
import java.util.ArrayList;

import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.shrikeBT.*;
import com.ibm.wala.types.TypeReference;

import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.SymbolicValue;
import cz.cuni.mff.d3s.stdynpor.data.ArrayElementIndex;
import cz.cuni.mff.d3s.stdynpor.data.ClassName;
import cz.cuni.mff.d3s.stdynpor.wala.WALAUtils;


public class ExecutionSimulator
{
	public static void processMethod(IBytecodeMethod mth, InterpretationContext ctx, ExecutionVisitor execVisitor) throws Exception
	{
		String mthSig = WALAUtils.getMethodSignature(mth);
		
		IInstruction[] mthInstructions = mth.getInstructions();
		
		int insnIndex = 0;

		// loop through all Shrike bytecode instructions and process each relevant one (some are ignored)
		while (insnIndex < mthInstructions.length)
		{
			Instruction insn = (Instruction) mthInstructions[insnIndex];
			
			short insnOpcode = insn.getOpcode();
			
			int insnPos = WALAUtils.getInsnBytecodePos(mth, insnIndex);
			
			ProgramPoint insnPP = new ProgramPoint(mthSig, insnPos);
			
			int nextInsnIndex = insnIndex + 1;
			
			if (insn instanceof ArrayLengthInstruction)
			{
				// remove "arrayObj" from the top of the stack and put there "arrayObj.length"
				
				Expression arrayObj = ctx.removeExprFromStack();
				
				ctx.addExprToStack(new FieldAccessExpression(arrayObj, "length", ClassName.createFromString("int"), arrayObj.type));
				
				// modeling query for array length by a read field access
				execVisitor.visitGetInsn(insnPP, new SymbolicValue(arrayObj.toString()), "length");
			}
			
			if (insn instanceof ArrayLoadInstruction)
			{
				Expression indexExpr = ctx.removeExprFromStack();
				
				Expression arrayObj = ctx.removeExprFromStack();
				
				// add "arrayObj[index]" to the stack
				ctx.addExprToStack(new ArrayAccessExpression(arrayObj, indexExpr));
				
				execVisitor.visitArrayLoadInsn(insnPP, new SymbolicValue(arrayObj.toString()), ExpressionUtils.createArrayElementIndex(indexExpr, mthSig, insnIndex));
			}
			
			if (insn instanceof ArrayStoreInstruction)
			{
				Expression newValue = ctx.removeExprFromStack();
				
				Expression indexExpr = ctx.removeExprFromStack();
					
				Expression arrayObj = ctx.removeExprFromStack();
				
				execVisitor.visitArrayStoreInsn(insnPP, new SymbolicValue(arrayObj.toString()), ExpressionUtils.createArrayElementIndex(indexExpr, mthSig, insnIndex), new SymbolicValue(newValue.toString()));
			}
			
			if (insn instanceof BinaryOpInstruction)
			{
				BinaryOpInstruction binOpInsn = (BinaryOpInstruction) insn;
				
				Expression value2 = ctx.removeExprFromStack();
				
				Expression value1 = ctx.removeExprFromStack();
				
				// add whole expression to the stack
				
				String arithmOp = "";
				switch (binOpInsn.getOperator())
				{
					case ADD:
						arithmOp = "+";
						break;
					case SUB:
						arithmOp = "-";
						break;
					case MUL:
						arithmOp = "*";
						break;
					case DIV:
						arithmOp = "/";
						break;
					case REM:
						arithmOp = "%";
						break;
					case AND:
						arithmOp = "&";
						break;
					case OR:
						arithmOp = "|";
						break;
					case XOR:
						arithmOp = "^";
						break;
					default:
						arithmOp = "";
				}
				
				ctx.addExprToStack(new ArithmeticExpression(arithmOp, value1, value2));
			}
			
			if (insn instanceof ComparisonInstruction)
			{
				Expression value2 = ctx.removeExprFromStack();
				
				Expression value1 = ctx.removeExprFromStack();
				
				// we model this using subtraction
				ctx.addExprToStack(new ArithmeticExpression("-", value1, value2));
			}
			
			if (insn instanceof ConditionalBranchInstruction)
			{
				ConditionalBranchInstruction condbrInsn = (ConditionalBranchInstruction) insn;
				
				Expression value2 = null;
				if ((insnOpcode >= 153) && (insnOpcode <= 158)) value2 = new ConstantExpression("0", ClassName.createFromString("int")); 
				else value2 = ctx.removeExprFromStack();
								
				Expression value1 = ctx.removeExprFromStack();
				
				// create conditional expression
				
				String relOp = "";
				switch (condbrInsn.getOperator()) 
				{
					case EQ:
						relOp = "=";
						break;
					case GE:
						relOp = ">=";
						break;
					case GT:
						relOp = ">";
						break;
					case LE:
						relOp = "<=";
						break;
					case LT:
						relOp = "<";
						break;
					case NE:
						relOp = "!=";
						break;
					default:
						relOp = "";
				}
			}
			
			if (insn instanceof ConstantInstruction)
			{
				ConstantInstruction constInsn = (ConstantInstruction) insn;
				
				// add string representation of the constant to the stack
				
				if (constInsn.getValue() == null)
				{
					ctx.addExprToStack(SpecialExpression.NULL);
				}
				else
				{
					String constValueStr = constInsn.getValue().toString();
					ClassName constValueType = ClassName.createFromString(WALAUtils.getPlainClassName(constInsn.getType()));
					
					ctx.addExprToStack(new ConstantExpression(constValueStr, constValueType));
				}
			}
			
			if (insn instanceof DupInstruction)
			{
				boolean skip = false;
				
				// this takes care of the way javac compiles synchronized blocks
				if ((insnIndex + 2 < mthInstructions.length) && (mthInstructions[insnIndex + 2] instanceof MonitorInstruction)) skip = true;
				
				if ( ! skip )
				{
					DupInstruction dupInsn = (DupInstruction) insn;
					
					// dup
					if (dupInsn.getSize() == 1)
					{
						Expression dupValue = ctx.getExprFromStack();
					
						// make duplicate
						ctx.addExprToStack(dupValue);
					}
					
					// dup2
					if (dupInsn.getSize() == 2)
					{
						// duplicate two values at the top
							// template: ..., value2, value1 -> ..., value2, value1, value2, value1

						Expression dupValue1 = ctx.getExprFromStack(0);
						Expression dupValue2 = ctx.getExprFromStack(1);

						ctx.addExprToStack(dupValue2);
						ctx.addExprToStack(dupValue1);
					}
					
					// we ignore all the obscure variants of DUP (e.g., dup_x1, dup2_x1, etc) with non-zero 'delta'
				}
			}
			
			if (insn instanceof GetInstruction)
			{
				GetInstruction getInsn = (GetInstruction) insn;
				
				String fieldName = getInsn.getFieldName();
				
				Expression obj = null;
				
				if (getInsn.isStatic())
				{
					String classNameStr = WALAUtils.getPlainClassName(getInsn.getClassType());
					
					obj = new ClassNameExpression(classNameStr, ClassName.createFromString("java.lang.Class"));
				}
				else
				{
					obj = ctx.removeExprFromStack();
				}
				
				ClassName fieldType = ClassName.createFromString(WALAUtils.getPlainClassName(getInsn.getFieldType()));				
				ClassName ownerClassType = ClassName.createFromString(WALAUtils.getPlainClassName(getInsn.getClassType()));
				
				// put "obj.fieldname" to the stack
				ctx.addExprToStack(new FieldAccessExpression(obj, fieldName, fieldType, ownerClassType));
				
				execVisitor.visitGetInsn(insnPP, new SymbolicValue(obj.toString()), fieldName);
			}
			
			if (insn instanceof GotoInstruction)
			{
				GotoInstruction gotoInsn = (GotoInstruction) insn;
				
				Instruction prevInsn = (Instruction) mthInstructions[insnIndex - 1];
				short prevInsnOpcode = prevInsn.getOpcode();

				Instruction nextInsn = null;
				if (insnIndex + 1 < mthInstructions.length) nextInsn = (Instruction) mthInstructions[insnIndex + 1];

				if ((prevInsnOpcode == com.ibm.wala.shrikeBT.Constants.OP_monitorexit) && (gotoInsn.getLabel() > insnIndex))
				{
					// skip monitor exit for exceptions thrown inside the synchronized block
					// we do not have to consider backjumps here
					nextInsnIndex = gotoInsn.getLabel();
				}
				else if ((nextInsn != null) && (nextInsn instanceof StoreInstruction))
				{
					// handle catch blocks (where the exception object simply appears on the stack)
					ctx.addExprToStack(SpecialExpression.EXCEPTION);
				}
			}
			
			if (insn instanceof InstanceofInstruction)
			{
				Expression obj = ctx.removeExprFromStack();
				
				// we model this with a constant value 1 (true) that represents a successful comparison
				// we ignore the possibility of a negative result (0, false)
				ctx.addExprToStack(new ConstantExpression("1", ClassName.createFromString("int")));
			}
			
			if (insn instanceof InvokeInstruction)
			{
				InvokeInstruction invokeInsn = (InvokeInstruction) insn;
				
				String ownerClassNameStr = WALAUtils.getPlainClassName(invokeInsn.getClassType());
				
				String tgtMethodName = invokeInsn.getMethodName();
		
				String tgtMethodSig = ownerClassNameStr + "." + tgtMethodName + invokeInsn.getMethodSignature();

				boolean isStaticCall = (invokeInsn.getInvocationCode() == IInvokeInstruction.Dispatch.STATIC);
				
				int tgtMethodParamCount = 0;
				if (isStaticCall) tgtMethodParamCount = invokeInsn.getPoppedCount();
				else tgtMethodParamCount = invokeInsn.getPoppedCount() - 1; 
				
				// remove actual parameters
				for (int i = 1; i <= tgtMethodParamCount; i++) ctx.removeExprFromStack();
				
				Expression targetObj = null;		
				if (isStaticCall) targetObj = new ClassNameExpression(ownerClassNameStr, ClassName.createFromString("java.lang.Class"));
				else targetObj = ctx.removeExprFromStack();
				
				if (WALAUtils.hasMethodReturnValue(tgtMethodSig, ownerClassNameStr))
				{
					// put the symbol "[retval]" on the stack (indicates presence of a returned value) 
					ctx.addExprToStack(SpecialExpression.RETURN_VALUE);
				}
			}
           
			if (insn instanceof LoadInstruction)
			{
				LoadInstruction loadInsn = (LoadInstruction) insn;
				
				boolean skip = false;
				
				// this takes care of the way javac compiles synchronized blocks
				if ((insnIndex + 1 < mthInstructions.length) && (mthInstructions[insnIndex + 1] instanceof MonitorInstruction)) skip = true;
				
				if ( ! skip )
				{
					int varIndex = loadInsn.getVarIndex();
					
					// we use artificial local variable names ("localX")
					String varName = "local" + varIndex;
					
					ClassName varType = ClassName.createFromString(WALAUtils.getPlainClassName(loadInsn.getType()));
					
					Expression varExpr = new LocalVarExpression(varIndex, varName, varType);
					
					// add variable name to the stack
					ctx.addExprToStack(varExpr);
					
					execVisitor.visitLoadInsn(insnPP, new SymbolicValue(varExpr.toString()));
				}
			}
			
			if (insn instanceof MonitorInstruction)
			{
				Expression lockObj = null;
				
				// bytecode instructions load, store, and dup just preceding the monitor instruction (enter/exit) do not have any effect (ignored, skipped)
				// we just have to remove the lock object from the expression stack

				if (insnOpcode == com.ibm.wala.shrikeBT.Constants.OP_monitorenter)
				{
					lockObj = ctx.removeExprFromStack();
				}
			}
			
			if (insn instanceof NewInstruction)
			{
				// we use the symbol "[newobj]" to representing newly allocated objects
				Expression newObj = SpecialExpression.NEW_OBJECT;
				
				if ((insnOpcode == com.ibm.wala.shrikeBT.Constants.OP_anewarray) || (insnOpcode == com.ibm.wala.shrikeBT.Constants.OP_newarray)) 
				{
					Expression arraySizeExpr = ctx.removeExprFromStack();
					
					ctx.addExprToStack(newObj);
					
					// modeling update of array length by a write field access
					execVisitor.visitPutInsn(insnPP, new SymbolicValue(newObj.toString()), "length", new SymbolicValue(arraySizeExpr.toString()));
				}
				else
				{
					ctx.addExprToStack(newObj);
				}
			}
			
			if (insn instanceof PopInstruction)
			{
				ctx.removeExprFromStack();
			}
			
			if (insn instanceof PutInstruction)
			{
				PutInstruction putInsn = (PutInstruction) insn;
				
				String fieldName = putInsn.getFieldName();
				
				Expression newValue = ctx.removeExprFromStack();
				
				Expression obj = null;				
				if (putInsn.isStatic())
				{					
					String classNameStr = WALAUtils.getPlainClassName(putInsn.getClassType());
					
					obj = new ClassNameExpression(classNameStr, ClassName.createFromString("java.lang.Class"));
				}					
				else
				{
					obj = ctx.removeExprFromStack();
				}
				
				execVisitor.visitPutInsn(insnPP, new SymbolicValue(obj.toString()), fieldName, new SymbolicValue(newValue.toString()));
			}
			
			if (insn instanceof ReturnInstruction)
			{
				// check whether this method returns something
				if (mth.getReturnType() != TypeReference.Void)
				{					
					// pop "returned expression" from the top of the stack
					ctx.removeExprFromStack();
				}

				Instruction nextInsn = null;
				if (insnIndex + 1 < mthInstructions.length) nextInsn = (Instruction) mthInstructions[insnIndex + 1];

				// handle catch blocks (where the exception object simply appears on the stack)
				if ((nextInsn != null) && (nextInsn instanceof StoreInstruction))
				{
					ctx.addExprToStack(SpecialExpression.EXCEPTION);
				}
			}
			
			if (insn instanceof ShiftInstruction)
			{
				ShiftInstruction shiftInsn = (ShiftInstruction) insn;
				
				Expression value2 = ctx.removeExprFromStack();
				
				Expression value1 = ctx.removeExprFromStack();
				
				// add whole expression to the stack
				
				String shiftOp = "";
				switch (shiftInsn.getOperator())
				{
					case SHL:
						shiftOp = "<<";
						break;
					case SHR:
						shiftOp = ">>";
						break;
					case USHR:
						shiftOp = ">>";
						break;
					default:
						shiftOp = "";
				}
				
				ctx.addExprToStack(new ArithmeticExpression(shiftOp, value1, value2));
			}
			
			if (insn instanceof StoreInstruction)
			{
				StoreInstruction storeInsn = (StoreInstruction) insn;
				
				boolean skip = false;
				
				// this takes care of the way javac compiles synchronized blocks
				if ((insnIndex + 1 < mthInstructions.length) && (mthInstructions[insnIndex + 1] instanceof MonitorInstruction)) skip = true;
				
				if ( ! skip )
				{					 
					int varIndex = storeInsn.getVarIndex();
					
					// we use artificial local variable names ("localX")
					String varName = "local" + varIndex;
					
					ClassName varType = ClassName.createFromString(WALAUtils.getPlainClassName(storeInsn.getType()));
					
					Expression varExpr = new LocalVarExpression(varIndex, varName, varType);
					
					Expression newValue = ctx.removeExprFromStack();

					execVisitor.visitStoreInsn(insnPP, new SymbolicValue(varExpr.toString()), new SymbolicValue(newValue.toString()));
				}
			}
			
			if (insn instanceof SwapInstruction)
			{
				Expression value1 = ctx.removeExprFromStack();
				
				Expression value2 = ctx.removeExprFromStack();
				
				// put values back in swapped order
				ctx.addExprToStack(value1);
				ctx.addExprToStack(value2);
			}
			
			if (insn instanceof SwitchInstruction)
			{
				SwitchInstruction switchInsn = (SwitchInstruction) insn;
				
				Expression testedValue = ctx.removeExprFromStack();
				
				List<Integer> caseValues = new ArrayList<Integer>();
				List<Integer> caseTargets = new ArrayList<Integer>();
				
				int casesNum = switchInsn.getCasesAndLabels().length / 2;
				
				for (int i = 0; i < casesNum; i++)
				{
					caseValues.add(switchInsn.getCasesAndLabels()[i*2]);
					caseTargets.add(switchInsn.getCasesAndLabels()[i*2+1]);
				}
			}
			
			if (insn instanceof ThrowInstruction)
			{
				Expression excObj = ctx.removeExprFromStack();
				
				Instruction nextInsn = null;
				if (insnIndex + 1 < mthInstructions.length) nextInsn = (Instruction) mthInstructions[insnIndex + 1];

				if ((nextInsn != null) && (nextInsn instanceof StoreInstruction))
				{
					// handle catch blocks (where the exception object simply appears on the stack)
					ctx.addExprToStack(SpecialExpression.EXCEPTION);
				}
			}
			
			if (insn instanceof UnaryOpInstruction)
			{
				Expression value = ctx.removeExprFromStack();

				// add expression "(0 - value)" to the stack 
				ctx.addExprToStack(new ArithmeticExpression("-", new ConstantExpression("0", ClassName.createFromString("int")), value));
			}
			
			insnIndex = nextInsnIndex;	
		}
	}
}
