/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.util;


public class Pair<T1,T2>
{
	private T1 first;
	private T2 second;
	
	public Pair(T1 first, T2 second)
	{
		this.first = first;
		this.second = second;
	}
	
    public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof Pair) ) return false;
		
		Pair other = (Pair) obj;
		
		if ( ! this.first.equals(other.first) ) return false;
		if ( ! this.second.equals(other.second) ) return false;
		
		return true;
	}
	
	public int hashCode()
	{
		return first.hashCode() * 31 + second.hashCode();
	}
}

