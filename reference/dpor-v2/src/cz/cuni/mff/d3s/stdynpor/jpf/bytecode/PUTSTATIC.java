/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.jpf.bytecode;

import java.util.List;
import java.util.Set;

import gov.nasa.jpf.JPFException;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.SystemState;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ThreadList;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.LoadOnJPFRequired;
import gov.nasa.jpf.vm.Scheduler;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.util.InstructionState;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.AnalysisResultProcessor;
import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.jpf.JPFUtils;


public class PUTSTATIC extends gov.nasa.jpf.jvm.bytecode.PUTSTATIC
{
	public PUTSTATIC(String fieldName, String clsDescriptor, String fieldDescriptor)
	{
		super(fieldName, clsDescriptor, fieldDescriptor);
	}

	public Instruction execute(ThreadInfo ti) 
	{
		StackFrame frame = ti.getModifiableTopFrame();
		FieldInfo fieldInfo;

		try 
		{
			fieldInfo = getFieldInfo();
		} 
		catch (LoadOnJPFRequired lre) 
		{
			return ti.getPC();
		}
      
		if (fieldInfo == null) return ti.createAndThrowException("java.lang.NoSuchFieldError", (className + '.' + fname));
			
		ClassInfo ciField = fi.getClassInfo();

		if (!mi.isClinit(ciField) && ciField.initializeClass(ti)) return ti.getPC();
			
		ElementInfo ei = ciField.getModifiableStaticElementInfo();

		String className = getFieldInfo().getClassInfo().getName();
		String fieldName = getFieldInfo().getName();
				
		String curMethodSig = JPFUtils.getMethodSignature(getMethodInfo());			

		Scheduler scheduler = ti.getScheduler();

		if ( Configuration.usingFieldAccess && ( ! Configuration.usingDynamicPOR ) )
		{
			if (scheduler.canHaveSharedClassCG(ti, this, ei, fieldInfo))
			{
				ei = scheduler.updateClassSharedness(ti, ei, fi);

				if (AnalysisResultProcessor.existsConflictingFieldRead(ti, curMethodSig, getPosition(), className, fieldName, true, -1))
				{
					if (scheduler.setsSharedClassCG(ti, this, ei, fieldInfo)) return this;
				}
			} 
		}
		else if (Configuration.usingDynamicPOR)
		{
			if (scheduler.canHaveSharedClassCG(ti, this, ei, fieldInfo))
			{
				ei = scheduler.updateClassSharedness(ti, ei, fi);

				boolean skipCG = false;
					
				if (Configuration.usingFieldAccess)
				{	
					if ( ! AnalysisResultProcessor.existsConflictingFieldRead(ti, curMethodSig, getPosition(), className, fieldName, true, -1) ) skipCG = true;
				}
					
				if ( ! skipCG )
				{
					if (scheduler.setsSharedClassCG(ti, this, ei, fieldInfo)) return this;
				}
			}
		}

		if (frame.getAndResetFrameAttr(InstructionState.class) == null)
		{
			int fieldSize = fieldInfo.getStorageSize();
			
			if (fieldSize == 1)
			{
				Object valAttr = frame.getOperandAttr();
				int val = frame.peek();
				ei.set1SlotField(fieldInfo, val);
				ei.setFieldAttr(fieldInfo, valAttr);
				lastValue = val;
			}
			else 
			{
				Object valAttr = frame.getLongOperandAttr();
				long val = frame.peekLong();
				ei.set2SlotField(fieldInfo, val);
				ei.setFieldAttr(fieldInfo, valAttr);
				lastValue = val;
			}  
		}
		
		if (isReferenceField())
		{
			int refValue = frame.peek();
			if (refValue != MJIEnv.NULL)
			{
				ElementInfo eiExposed = ti.getElementInfo(refValue);
				if (scheduler.setsSharedClassExposureCG(ti, this, ei, fieldInfo, eiExposed))
				{
					frame.addFrameAttr(InstructionState.processed);
					return this;
				}
			}
		}
		
		popOperands(frame);
		return getNext(); 
	}
}
