/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.data;

import java.util.List;

public class FieldAccessPath
{
	public List<FieldName> fieldNameList;
	
	// LocalVarName for instance fields
	// ClassName for static fields
	public ObjectID rootObjectID;
	
	private int hc;


	public FieldAccessPath(List<FieldName> fieldNames, ObjectID objID)
	{
		this.fieldNameList = fieldNames;
		
		this.rootObjectID = objID;
		
		hc = 0;
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof FieldAccessPath) ) return false;
		
		FieldAccessPath other = (FieldAccessPath) obj;
		
		if ( ! this.fieldNameList.equals(other.fieldNameList) ) return false;
		if ( ! this.rootObjectID.equals(other.rootObjectID) ) return false;
		
		return true;
	}
	
	public int hashCode()
	{
		if (hc == 0)
		{
			hc = hc * 31 + this.fieldNameList.hashCode();
			hc = hc * 31 + this.rootObjectID.hashCode();
		}
		
		return hc;
	}
	
	public String toString()
	{
		StringBuffer strbuf = new StringBuffer();

		strbuf.append(rootObjectID.toString());
		
		for (FieldName fn : fieldNameList)
		{
			strbuf.append(".");
			strbuf.append(fn.fieldNameStr);
		}
		
		return strbuf.toString();
	}
}

