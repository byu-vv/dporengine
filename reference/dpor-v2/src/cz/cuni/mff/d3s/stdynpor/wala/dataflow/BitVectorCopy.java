/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.wala.dataflow;

import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.UnaryOperator;


public class BitVectorCopy extends UnaryOperator<BitVectorVariable> 
{
	private int srcVar;
	private int destVar;


	private BitVectorCopy() {}
	
	public BitVectorCopy(int src, int dest)
	{
		this.srcVar = src;
		this.destVar = dest;
	}
	
	@Override
	public String toString() 
	{
		return "COPY " + srcVar + " " + destVar;
	}
  
	@Override
	public int hashCode() 
	{
		return (9907 + srcVar) * 31 + destVar;
	}

	@Override
	public boolean equals(Object o) 
	{
		if (o instanceof BitVectorCopy)
		{
			BitVectorCopy other = (BitVectorCopy) o;
			
			if (this.srcVar != other.srcVar) return false;
			if (this.destVar != other.destVar) return false;
			
			return true;
		}
		
		return false;
	}

	
	@Override
	public byte evaluate(BitVectorVariable lhs, BitVectorVariable rhs) throws IllegalArgumentException 
	{
		if (lhs == null) throw new IllegalArgumentException("lhs == null");
		if (rhs == null) throw new IllegalArgumentException("rhs == null");
	
		BitVectorVariable C = new BitVectorVariable();
		C.copyState(rhs);
		
		if ((srcVar != -1) && (destVar != -1))
		{
			if (rhs.get(srcVar)) C.set(destVar);
			else C.clear(destVar);
		}

		if (!lhs.sameValue(C)) 
		{
			lhs.copyState(C);
			return CHANGED;
		} 
		else 
		{
			return NOT_CHANGED;
		}
	}
}
