/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.bytecode.interpreter;

import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

import cz.cuni.mff.d3s.stdynpor.data.ObjectID;
import cz.cuni.mff.d3s.stdynpor.data.ClassName;
import cz.cuni.mff.d3s.stdynpor.data.AllocationSite;
import cz.cuni.mff.d3s.stdynpor.data.FieldID;
import cz.cuni.mff.d3s.stdynpor.data.FieldName;
import cz.cuni.mff.d3s.stdynpor.data.FieldAccessPath;
import cz.cuni.mff.d3s.stdynpor.data.ArrayElementIndex;
import cz.cuni.mff.d3s.stdynpor.data.LocalVarName;
import cz.cuni.mff.d3s.stdynpor.wala.WALAUtils;
import cz.cuni.mff.d3s.stdynpor.wala.PointerAnalysisData;


public class ExpressionUtils
{
	public static ArrayElementIndex createArrayElementIndex(Expression expr, String curMthSig, int curInsnIndex)
	{
		ArrayElementIndex aeIndexObj = new ArrayElementIndex();
		
		extractInfoRecursively(expr, aeIndexObj, curMthSig, curInsnIndex);
		
		aeIndexObj.numericConstant = extractIntegerConstantRecursively(expr);
		
		return aeIndexObj;
	}
	
	private static void extractInfoRecursively(Expression expr, ArrayElementIndex aeIndexObj, String curMthSig, int curInsnIndex)
	{
		if (expr instanceof LocalVarExpression)
		{
			LocalVarExpression lvExpr = (LocalVarExpression) expr;
			
			aeIndexObj.localVariables.add(LocalVarName.createFromIndex(lvExpr.varIndex));
			aeIndexObj.exprSN.add("L"+aeIndexObj.localVariables.size());
		}

		if (isFieldAccessPath(expr))
		{
			FieldAccessExpression faExpr = (FieldAccessExpression) expr;
			
			FieldAccessPath fap = extractFieldAccessPath(faExpr, aeIndexObj, curMthSig, curInsnIndex);
			
			aeIndexObj.fieldPaths.add(fap);
			aeIndexObj.exprSN.add("F"+aeIndexObj.fieldPaths.size());
		}
		
		if (expr instanceof ArrayAccessExpression)
		{
			ArrayAccessExpression arrayExpr = (ArrayAccessExpression) expr;
			
			extractInfoRecursively(arrayExpr.targetArrayObj, aeIndexObj, curMthSig, curInsnIndex);
			extractInfoRecursively(arrayExpr.elementIndex, aeIndexObj, curMthSig, curInsnIndex);
			
			aeIndexObj.exprSN.add("[]");
		}
		
		if (expr instanceof ConstantExpression)
		{
			ConstantExpression constExpr = (ConstantExpression) expr;
			
			aeIndexObj.exprSN.add(constExpr.valueStr);			
		}
		
		if (expr instanceof ArithmeticExpression)
		{
			ArithmeticExpression arithmExpr = (ArithmeticExpression) expr;
			
			extractInfoRecursively(arithmExpr.value1, aeIndexObj, curMthSig, curInsnIndex);
			extractInfoRecursively(arithmExpr.value2, aeIndexObj, curMthSig, curInsnIndex);
			
			aeIndexObj.exprSN.add(arithmExpr.operator);
		}
		
		if (expr instanceof SpecialExpression)
		{
			aeIndexObj.exprSN.add("U");
		}
	}
	
	private static Integer extractIntegerConstantRecursively(Expression expr)
	{
		if (expr instanceof ConstantExpression)
		{
			ConstantExpression constExpr = (ConstantExpression) expr;
			
			try
			{
				return Integer.parseInt(constExpr.valueStr);
			}
			catch (NumberFormatException ex)
			{
				return null;
			}
		}
		
		if (expr instanceof ArithmeticExpression)
		{
			ArithmeticExpression arithmExpr = (ArithmeticExpression) expr;
			
			Integer res1 = extractIntegerConstantRecursively(arithmExpr.value1);
			Integer res2 = extractIntegerConstantRecursively(arithmExpr.value2);
			
			// not integer constant
			if ((res1 == null) || (res2 == null)) return null;

			if (arithmExpr.operator.equals("+")) return new Integer(res1.intValue() + res2.intValue());
			if (arithmExpr.operator.equals("-")) return new Integer(res1.intValue() - res2.intValue());
			if (arithmExpr.operator.equals("*")) return new Integer(res1.intValue() * res2.intValue());
			if (arithmExpr.operator.equals("/")) return new Integer(res1.intValue() / res2.intValue());
			if (arithmExpr.operator.equals("%")) return new Integer(res1.intValue() % res2.intValue());
			if (arithmExpr.operator.equals("<<")) return new Integer(res1.intValue() << res2.intValue());
			if (arithmExpr.operator.equals(">>")) return new Integer(res1.intValue() >> res2.intValue());
			if (arithmExpr.operator.equals("&")) return new Integer(res1.intValue() & res2.intValue());
			if (arithmExpr.operator.equals("|")) return new Integer(res1.intValue() | res2.intValue());
			if (arithmExpr.operator.equals("^")) return new Integer(res1.intValue() ^ res2.intValue());
		}

		return null;
	}
	
	private static boolean isFieldAccessPath(Expression expr)
	{
		// check whether it is field access path in the form "<var>.f1.f2...fN"
		// we require at least one field access in the path (e.g., not just local variable)

		// other cases: local variable, class name, array access, arithmetic expression, special
		if ( ! (expr instanceof FieldAccessExpression) ) return false;
		
		FieldAccessExpression faExpr = (FieldAccessExpression) expr;
		
		return isValidFieldAccessTarget(faExpr.targetObj);
	}

	private static boolean isValidFieldAccessTarget(Expression expr)
	{
		// root expression (object)
		if (expr instanceof LocalVarExpression) return true;
		if (expr instanceof ClassNameExpression) return true;
		if (expr instanceof SpecialExpression) return true;
		
		// other cases: array access, arithmetic expression
		if ( ! (expr instanceof FieldAccessExpression) ) return false;
		
		FieldAccessExpression faExpr = (FieldAccessExpression) expr;
		
		return isValidFieldAccessTarget(faExpr.targetObj);
	}

	private static FieldAccessPath extractFieldAccessPath(FieldAccessExpression faExpr, ArrayElementIndex aeIndexObj, String curMthSig, int curInsnIndex)
	{
		List<FieldName> fieldNameList = new ArrayList<FieldName>();
		
		extractFieldsRecursively(faExpr, aeIndexObj.allFields, fieldNameList, curMthSig, curInsnIndex);

		ObjectID rootObjID = extractFieldPathRootObject(faExpr);
	
		return new FieldAccessPath(fieldNameList, rootObjID);
	}
	
	private static void extractFieldsRecursively(Expression expr, Set<FieldID> fields, List<FieldName> fieldNameList, String curMthSig, int curInsnIndex)
	{
		// root object does not contain any field
		if ( ! (expr instanceof FieldAccessExpression) ) return;

		FieldAccessExpression faExpr = (FieldAccessExpression) expr;
		
		int tgtObjRef = WALAUtils.getTargetRefForPreviousFieldAccess(curMthSig, curInsnIndex, fieldNameList.size());
		
		Set<AllocationSite> tgtObjAllocSites = PointerAnalysisData.getObjectAllocSites(faExpr.ownerClassType.getAsString(), curMthSig, tgtObjRef);
		
		for (AllocationSite objAS : tgtObjAllocSites) fields.add(new FieldID(objAS, faExpr.fieldName));
		
		fieldNameList.add(0, FieldName.createFromString(faExpr.ownerClassType.getAsString(), faExpr.fieldName));
		
		extractFieldsRecursively(faExpr.targetObj, fields, fieldNameList, curMthSig, curInsnIndex);	
	}
	
	private static ObjectID extractFieldPathRootObject(Expression expr)
	{
		// this really should not happen
		if ( ! (expr instanceof FieldAccessExpression) ) return null;
		
		FieldAccessExpression faExpr = (FieldAccessExpression) expr;
		
		if (faExpr.targetObj instanceof LocalVarExpression)
		{
			// instance fields
			
			LocalVarExpression lvExpr = (LocalVarExpression) faExpr.targetObj;
			
			// we do not consider the root of a field access path ("local0/this") as local variable
			
			return LocalVarName.createFromIndex(lvExpr.varIndex);
		}
		
		if (faExpr.targetObj instanceof ClassNameExpression)
		{
			// static fields
			
			ClassNameExpression cnExpr = (ClassNameExpression) faExpr.targetObj;
			
			return ClassName.createFromString(cnExpr.className);
		}
		
		return extractFieldPathRootObject(faExpr.targetObj);
	}
}

