/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.jpf.bytecode;

import java.util.List;
import java.util.Set;

import gov.nasa.jpf.JPFException;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.StaticElementInfo;
import gov.nasa.jpf.vm.SystemState;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ThreadList;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.LoadOnJPFRequired;
import gov.nasa.jpf.vm.Scheduler;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.AnalysisResultProcessor;
import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.jpf.JPFUtils;


public class GETSTATIC extends gov.nasa.jpf.jvm.bytecode.GETSTATIC
{
	public GETSTATIC(String fieldName, String clsDescriptor, String fieldDescriptor)
	{
		super(fieldName, clsDescriptor, fieldDescriptor);
	}

	public Instruction execute(ThreadInfo ti) 
	{
		ClassInfo ciField;
		FieldInfo fieldInfo;
		
		try 
		{
			fieldInfo = getFieldInfo();
		} 
		catch (LoadOnJPFRequired lre) 
		{
			return ti.getPC();
		}
		
		if (fieldInfo == null) return ti.createAndThrowException("java.lang.NoSuchFieldError", (className + '.' + fname));
		
		// this can be actually different (can be a base)
		ciField = fieldInfo.getClassInfo();

		// note - this returns the next insn in the topmost clinit that just got pushed		
		if (!mi.isClinit(ciField) && ciField.initializeClass(ti)) return ti.getPC();
		
		ElementInfo ei = ciField.getStaticElementInfo();
		
		String className = ciField.getName();
		String fieldName = fieldInfo.getName();
			
		MethodInfo mi = getMethodInfo();			
		String curMethodSig = JPFUtils.getMethodSignature(mi);

		Scheduler scheduler = ti.getScheduler();

		if ( Configuration.usingFieldAccess && ( ! Configuration.usingDynamicPOR ) )
		{
			if (scheduler.canHaveSharedClassCG(ti, this, ei, fieldInfo))
			{
				ei = scheduler.updateClassSharedness(ti, ei, fieldInfo);
				
				if (AnalysisResultProcessor.existsConflictingFieldWrite(ti, curMethodSig, getPosition(), className, fieldName, true, -1))
				{
					if (scheduler.setsSharedClassCG(ti, this, ei, fieldInfo)) return this;
				}
			}
		}
		else if (Configuration.usingDynamicPOR)
		{
			if (scheduler.canHaveSharedClassCG(ti, this, ei, fieldInfo))
			{
				ei = scheduler.updateClassSharedness(ti, ei, fieldInfo);

				boolean skipCG = false;
				
				if (Configuration.usingFieldAccess)
				{	
					if ( ! AnalysisResultProcessor.existsConflictingFieldWrite(ti, curMethodSig, getPosition(), className, fieldName, true, -1) ) skipCG = true;
				}
				
				if ( ! skipCG )
				{
					if (scheduler.setsSharedClassCG(ti, this, ei, fieldInfo)) return this;
				}
			}
		}
		
		Object attr = ei.getFieldAttr(fieldInfo);
		
		StackFrame frame = ti.getModifiableTopFrame();
		
		if (size == 1) 
		{
			int ival = ei.get1SlotField(fieldInfo);
			lastValue = ival;
			
			if (fieldInfo.isReference()) frame.pushRef(ival);
			else frame.push(ival);
			
			if (attr != null) frame.setOperandAttr(attr);			
		} 
		else 
		{
			long lval = ei.get2SlotField(fieldInfo);
			lastValue = lval;
			
			frame.pushLong(lval);
			
			if (attr != null) frame.setLongOperandAttr(attr);
		}

		return getNext(ti);   
	} 
}
