/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.wala;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAFieldAccessInstruction;
import com.ibm.wala.ssa.SSAGetInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.AllocationSite;
import cz.cuni.mff.d3s.stdynpor.data.ClassName;
import cz.cuni.mff.d3s.stdynpor.data.FieldID;
import cz.cuni.mff.d3s.stdynpor.data.FieldName;
import cz.cuni.mff.d3s.stdynpor.data.FieldAccess;
import cz.cuni.mff.d3s.stdynpor.data.LocalVarID;
import cz.cuni.mff.d3s.stdynpor.data.LocalVarAccess;


public class FieldAccessCodeInfo
{
	// map from program point (method signature and instruction position) that represents a field access instruction to the SSA variable number that represents a target object reference
	protected static Map<ProgramPoint, Integer> pp2FieldAccessTgtRef;
	
	static
	{
		pp2FieldAccessTgtRef = new HashMap<ProgramPoint, Integer>();
	}
	
	
	public static void saveTargetRefsForFieldAccesses(CallGraph clGraph)
	{
		Set<String> processedMethods = new HashSet<String>();
				
		for (CGNode node : clGraph) 
		{
			String methodSig = WALAUtils.getMethodSignature(node.getMethod());
			
			if (processedMethods.contains(methodSig)) continue;
			
			IR methodIR = node.getIR();
			
			if (methodIR == null) continue;
					
			SSAInstruction[] instructions = methodIR.getInstructions();
			for (int idx = 0; idx < instructions.length; idx++)
			{
				if (instructions[idx] == null) continue;
				
				SSAInstruction insn = instructions[idx];
	
				if (insn instanceof SSAFieldAccessInstruction) 
				{
					SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) insn;
					
					int faInsnPos = WALAUtils.getInsnBytecodePos(node, idx);
					
					ProgramPoint faInsnPP = new ProgramPoint(methodSig, faInsnPos);
					
					pp2FieldAccessTgtRef.put(faInsnPP, faInsn.getRef());
				}
			}
		}
	}
	
	public static int getFieldAccessTargetRef(String mthSig, int insnPos)
	{
		ProgramPoint pp = new ProgramPoint(mthSig, insnPos);
		
		return pp2FieldAccessTgtRef.get(pp);		
	}
	
	public static Set<FieldID> getFieldsForInsn(CGNode mthNode, SSAFieldAccessInstruction faInsn, int ssaInsnIndex)
	{
		Set<FieldID> fields = new HashSet<FieldID>();
			
		try
		{
			String fieldName = faInsn.getDeclaredField().getName().toUnicodeString();
			
			String className = WALAUtils.getDeclaringClassNameForField(faInsn.getDeclaredField());
			
			String methodSig = WALAUtils.getMethodSignature(mthNode.getMethod());

			if (Configuration.saveFALocalVarAccess)
			{
				int insnPos = WALAUtils.getInsnBytecodePos(mthNode, ssaInsnIndex);
				
				LocalVarAccess tgtVarAccess = new LocalVarAccess(new LocalVarID(methodSig, faInsn.getRef(), className), new ProgramPoint(methodSig, insnPos));
				
				fields.add(new FieldID(tgtVarAccess, fieldName));
			}
			else if (Configuration.usingPointerAlias)
			{
				if (Configuration.enabledPAExhaustive || Configuration.enabledPAExhaustObjCtx)
				{					
					Set<AllocationSite> objectAllocSites = PointerAnalysisData.getObjectAllocSites(className, methodSig, faInsn.getRef());
			
					for (AllocationSite objAS : objectAllocSites) fields.add(new FieldID(objAS, fieldName));
				}
				else if (Configuration.enabledPADemandDrv || Configuration.enabledPADemandDrvCtx)
				{
					if ( ! faInsn.isStatic() )
					{
						// here we consider the local variable pointing to the object instance
						fields.add(new FieldID(new LocalVarID(methodSig, faInsn.getRef(), className), fieldName));
					}
					else
					{
						fields.add(new FieldID(ClassName.createFromString(className), fieldName));
					}
				}
			}
			else
			{
				fields.add(new FieldID(ClassName.createFromString(className), fieldName));
			}
		}
		catch (Exception ex) 
		{
			ex.printStackTrace(); 
		}
			
		return fields;
	}
	
	public static FieldName getFieldNameForInsn(SSAFieldAccessInstruction faInsn) throws Exception
	{
		String fieldNameStr = faInsn.getDeclaredField().getName().toUnicodeString();

		String classNameStr = WALAUtils.getDeclaringClassNameForField(faInsn.getDeclaredField());
						
		return FieldName.createFromString(classNameStr, fieldNameStr);
	}
	
	public static Set<FieldAccess> getFieldAccessesForInsn(CGNode mthNode, SSAFieldAccessInstruction faInsn, int ssaInsnIndex) throws Exception
	{
		// field accesses performed by the current instruction
		Set<FieldAccess> accesses = new HashSet<FieldAccess>();
		
		String fieldName = faInsn.getDeclaredField().getName().toUnicodeString();

		String className = WALAUtils.getDeclaringClassNameForField(faInsn.getDeclaredField());					
	
		String methodSig = WALAUtils.getMethodSignature(mthNode.getMethod());
		
		int insnPos = WALAUtils.getInsnBytecodePos(mthNode, ssaInsnIndex);

		Set<AllocationSite> objectAllocSites = PointerAnalysisData.getObjectAllocSites(className, methodSig, faInsn.getRef());
		
		for (AllocationSite objAS : objectAllocSites)
		{
			FieldID fid = new FieldID(objAS, fieldName);
			ProgramPoint pp = new ProgramPoint(methodSig, insnPos);
			
			accesses.add(new FieldAccess(fid, pp));
		}
			
		return accesses;
	}	

}

