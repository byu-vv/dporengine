/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.wala;

import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

import java.io.File;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IField;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.classLoader.CallSiteReference;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.util.strings.Atom;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAFieldAccessInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.ssa.analysis.IExplodedBasicBlock;
import com.ibm.wala.types.TypeName;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.types.FieldReference;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.callgraph.impl.Everywhere;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.AllocationSiteInNode;
import com.ibm.wala.ipa.callgraph.propagation.cfa.CallerSiteContext;
import com.ibm.wala.ipa.callgraph.propagation.cfa.CallStringContext;
import com.ibm.wala.ipa.callgraph.propagation.cfa.CallStringContextSelector;
import com.ibm.wala.ipa.callgraph.propagation.cfa.CallString;
import com.ibm.wala.ipa.callgraph.propagation.ReceiverInstanceContext;
import com.ibm.wala.ipa.cfg.BasicBlockInContext;
import com.ibm.wala.analysis.typeInference.TypeInference;
import com.ibm.wala.analysis.typeInference.TypeAbstraction;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.Debug;
import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.AllocationSite;
import cz.cuni.mff.d3s.stdynpor.data.Context;
import cz.cuni.mff.d3s.stdynpor.data.ObjectStringContext;
import cz.cuni.mff.d3s.stdynpor.data.ClassName;
import cz.cuni.mff.d3s.stdynpor.bytecode.ByteCodeUtils;
import cz.cuni.mff.d3s.stdynpor.wala.jvm.*;
import cz.cuni.mff.d3s.stdynpor.JPFDecoratorFactory;


public class WALAUtils
{
	public static Map<String, IClass> clsName2Obj;
	
	public static Map<TypeReference, String> typeRef2Name;
	
	public static Map<IClass, String> clsObj2Name;
	
	public static Map<String, CGNode> mthSig2CGNode;

	
	// globally useful data structures (context)
	public static IClassHierarchy classHierarchy;
	public static AnalysisScope scope;
	public static AnalysisOptions options;
	public static AnalysisCache cache;
	public static Iterable<Entrypoint> entryPoints;
	

	static
	{
		clsName2Obj = new HashMap<String, IClass>();
		
		typeRef2Name = new HashMap<TypeReference, String>();
		
		clsObj2Name = new HashMap<IClass, String>();
		
		mthSig2CGNode = new HashMap<String, CGNode>();
	}
	
	
	public static void initLibrary(String mainClassName, String targetClassPath, String walaExclusionFilePath) throws Exception
	{
		createAnalysisScope(targetClassPath, walaExclusionFilePath);

		makeClassHierarchy();

		entryPoints = Util.makeMainEntrypoints(scope, classHierarchy, getInternalClassName(mainClassName));	
			
		options = new AnalysisOptions(scope, entryPoints);
		options.setHandleStaticInit(true);
		
		cache = new AnalysisCache();
	}
	
	private static void createAnalysisScope(String targetClassPath, String exclusionFilePath) throws Exception
	{
		JVMFactory jvmFactory = new JPFDecoratorFactory(AnalysisScopeReaderCustomJVM.getDefaultJVMFactory(), "lib/jpf8/jpf-core/build/jpf-classes.jar");
		
		scope = AnalysisScopeReaderCustomJVM.readJavaScope("wala-jpf.txt", new File(exclusionFilePath), WALAUtils.class.getClassLoader(), jvmFactory);

		AnalysisScopeReaderCustomJVM.processScopeDefLine(scope, WALAUtils.class.getClassLoader(), jvmFactory, "Application,Java,binaryDir," + targetClassPath);
	}

	private static void makeClassHierarchy() throws Exception
	{	
		classHierarchy = ClassHierarchy.make(scope);
	}
	
	
	public static String getPlainClassName(String internalClassName)
	{
		int curPos = 0;
		
		// skip array dimensions
		while (internalClassName.charAt(curPos) == '[') curPos++;
		
		if (internalClassName.charAt(curPos) == 'L') curPos++;
		
		// omit the ";" character at the end
		String plainClassName = internalClassName.substring(curPos, internalClassName.length() - 1).replace('/', '.');
		
		return plainClassName;
	}
	
	public static String getInternalClassName(String plainClassName)
	{
		return "L"+plainClassName.replace('.', '/');	
	}
	
	public static String getPackageName(String fullClassName)
	{
		String packageName = "";
		int k = fullClassName.lastIndexOf('.');
		if (k != -1) packageName = fullClassName.substring(0, k);
		return packageName;
	}

	public static String getClassName(TypeReference typeRef)
	{
		return getTypeNameStr(typeRef);
	}
	
	public static String getClassName(IClass cls) throws Exception
	{
		if (cls == null) return null;
		
		if (clsObj2Name.containsKey(cls)) return clsObj2Name.get(cls);
		
		String cnStr = getTypeNameStr(cls.getName());
		
		clsObj2Name.put(cls, cnStr);
					
		return cnStr;
	}
	
	public static String getDeclaringClassNameForField(FieldReference tgtField) throws Exception
	{
		if (tgtField == null) return null;
		
		// input class name: loaded from bytecode instruction (field access)
		String inClassName = getClassName(tgtField.getDeclaringClass());

		IClass tgtCls = null;
	
		// find the class which really declares the given field

		IClass cls = findClass(inClassName);

		// unknown class (e.g., due to exclusions)
		if (cls == null)
		{
			// we use the class name loaded from the bytecode instruction as the fallback result	
			return inClassName;	
		}
		
		while (cls != null)
		{
			for (IField fld : cls.getDeclaredStaticFields()) 
			{
				if (fld.getName().equals(tgtField.getName()))
				{
					tgtCls = cls;
					break;
				}
			}
			
			if (tgtCls != null) break;
			
			for (IField fld : cls.getDeclaredInstanceFields()) 
			{
				if (fld.getName().equals(tgtField.getName()))
				{
					tgtCls = cls;
					break;
				}
			}
			
			if (tgtCls != null) break;
			
			cls = cls.getSuperclass();
		}
		
		// unknown field name (this may happen, for example, when the field is defined and used only in the built-in model of a native method)
		if (tgtCls == null)
		{
			// we use the class name loaded from the bytecode instruction as the fallback result
			return inClassName;			
		}
		
		TypeName tgtClassType = tgtCls.getName();
		
		return getTypeNameStr(tgtClassType);
	}				
	
	public static String getDeclaringClassNameForField(IField field) throws Exception
	{
		if (field == null) return null;
		
		return getDeclaringClassNameForField(field.getReference());
	}
	
	public static String getFieldTypeClassName(IField field) throws Exception
	{
		if (field == null) return null;
		
		TypeReference fieldTypeRef = field.getFieldTypeReference();
		
		TypeName fieldTypeClass = fieldTypeRef.getName();

		return getTypeNameStr(fieldTypeClass);
	}
	
	public static String getTypeNameStr(TypeName typeName) throws Exception
	{
		if (typeName.isPrimitiveType())
		{
			if (typeName == TypeReference.IntName) return "int";	
			if (typeName == TypeReference.BooleanName) return "boolean";
			if (typeName == TypeReference.ByteName) return "byte";
			if (typeName == TypeReference.CharName) return "char";
			if (typeName == TypeReference.LongName) return "long";
			if (typeName == TypeReference.DoubleName) return "double";
			if (typeName == TypeReference.FloatName) return "float";
			if (typeName == TypeReference.ShortName) return "short";
		}
		
		if (typeName.isArrayType())
		{
			StringBuffer arrayTypeStrBuf = new StringBuffer();
			
			String innerElemTypeStr = getTypeNameStr(typeName.getInnermostElementType());
			
			arrayTypeStrBuf.append(innerElemTypeStr);
			
			for (int i = 0; i < typeName.getDerivedMask(); i++) arrayTypeStrBuf.append("[]");			
			
			String arrayTypeStr = arrayTypeStrBuf.toString();
			
			return arrayTypeStr;			
		}
			
		// reference types
		
		StringBuffer refTypeStrBuf = new StringBuffer();
			
		if (typeName.getPackage() != null) refTypeStrBuf.append(typeName.getPackage().toUnicodeString().replace('/', '.')).append(".");

		refTypeStrBuf.append(typeName.getClassName().toUnicodeString());
			
		String refTypeStr = refTypeStrBuf.toString();

		return refTypeStr;
	}

	public static String getTypeNameStr(TypeReference typeRef)
	{
		if (typeRef == null) return null;
		
		if (typeRef2Name.containsKey(typeRef)) 
		{
			return typeRef2Name.get(typeRef);
		}
		
		try
		{
			TypeName typeClass = typeRef.getName();
			
			String typeStr = getTypeNameStr(typeClass);

			typeRef2Name.put(typeRef, typeStr);
	
			return typeStr;
		}
		catch (Exception ex) { ex.printStackTrace(); }
		
		return null;
	}
	
	public static String getMethodSignature(IMethod mth)
	{
		return mth.getSignature();
	}

	public static String getMethodSignature(MethodReference mthRef)
	{
		return mthRef.getSignature();
	}	

	public static String getShortMethodName(MethodReference mthRef) throws Exception
	{
		return mthRef.getName().toUnicodeString();
	}
	
	public static String getFullMethodNameFromCGNode(CGNode node) throws Exception
	{	
		IClass cls = node.getMethod().getDeclaringClass();
		
		String className = getClassName(cls);
		
		String methodName = node.getMethod().getReference().getName().toUnicodeString();
				
		StringBuffer mnStrBuf = new StringBuffer();

		mnStrBuf.append(className).append(".").append(methodName);

		return mnStrBuf.toString();
	}
	
	public static String getClassNameFromCGNode(CGNode node) throws Exception
	{	
		IClass cls = node.getMethod().getDeclaringClass();
		
		return getClassName(cls);
	}
	
	public static String getMethodNameFromCGNode(CGNode node) throws Exception
	{	
		return node.getMethod().getReference().getName().toUnicodeString();
	}
	
	public static String getDeclaringClassNameForMethod(IMethod tgtMethod) throws Exception
	{
		return getDeclaringClassNameForMethod(tgtMethod.getReference());
	}
	
	public static String getDeclaringClassNameForMethod(MethodReference tgtMethod) throws Exception
	{		
		String tgtMthSig = getMethodSignature(tgtMethod);
		
		// input class name: loaded from bytecode instruction (method call)
		String inClassName = getClassName(tgtMethod.getDeclaringClass());
		
		IClass tgtCls = null;
		
		// find the class which really declares the given method

		IClass cls = findClass(inClassName);

		// unknown class (e.g., due to exclusions)
		if (cls == null)
		{
			// we use the class name loaded from the bytecode instruction as the fallback result	
			return inClassName;	
		}
		
		while (cls != null)
		{
			for (IMethod mth : cls.getDeclaredMethods()) 
			{
				String mthSig = getMethodSignature(mth);
				
				if (mthSig.equals(tgtMthSig))
				{
					tgtCls = cls;
					break;
				}
			}
			
			if (tgtCls != null) break;
			
			cls = cls.getSuperclass();
		}
	
		return getClassName(tgtCls);
	}
	
	public static boolean isMethodConstructor(CGNode node) throws Exception
	{	
		return node.getMethod().isInit();
	}
	
	public static boolean hasMethodReturnValue(String tgtMethodSig, String ownerClassName) throws Exception
	{
		IMethod tgtMth = null;

		CGNode cgn = mthSig2CGNode.get(tgtMethodSig);

		if (cgn != null) 
		{
			// typical case (fast path)
			tgtMth = cgn.getMethod();
		}
		else // cgn == null
		{
			// may happen for methods that are not in the call graph
				// example: InterruptedException.printStackTrace() that returns void (nothing)
			
			IClass cls = findClass(ownerClassName);

			// cls == null for unknown class (e.g., due to exclusions)

			while (cls != null)
			{
				for (IMethod mth : cls.getDeclaredMethods()) 
				{
					String mthSig = getMethodSignature(mth);
				
					if (mthSig.equals(tgtMethodSig))
					{
						tgtMth = mth;
						break;
					}
				}

				if (tgtMth != null) break;
			
				cls = cls.getSuperclass();
			}		
		}

		// safe default answer if we do not know for sure
		// unknown class or strange library class/method
		if (tgtMth == null) return true;

		return (tgtMth.getReturnType() != TypeReference.Void);
	}	
	
	public static IClass findClass(String className) throws Exception
	{
		if (className.endsWith("[]")) return null;
		
		if (clsName2Obj.containsKey(className)) 
		{
			return clsName2Obj.get(className);
		}
		
		Iterator<IClass> clsIt = classHierarchy.iterator();
		while (clsIt.hasNext())
		{
			IClass cls = clsIt.next();
			
			if (className.equals(getClassName(cls)))
			{
				clsName2Obj.put(className, cls);
				return cls;
			}
		}
		
		return null;
	}
	
	public static boolean isThreadClass(TypeReference typeRef) throws Exception
	{
		String clsName = getClassName(typeRef);

		if (clsName.equals("java.lang.Thread")) return true;
		
		IClass cls = findClass(clsName);
	
		if (cls != null)
		{
			IClass clsSuper = cls.getSuperclass();
			while (clsSuper != null)
			{
				String clsSuperName = getClassName(clsSuper);						
				if (clsSuperName.equals("java.lang.Thread")) return true;
				clsSuper = clsSuper.getSuperclass();
			}
		}
		
		return false;
	}
	
	public static boolean isThreadStartCall(SSAInstruction insn)
	{
		if (insn instanceof SSAInvokeInstruction)
		{
			SSAInvokeInstruction invokeInsn = (SSAInvokeInstruction) insn;

			try
			{
				String targetMthName = getShortMethodName(invokeInsn.getDeclaredTarget());
						
				if (targetMthName.equals("start") && isThreadClass(invokeInsn.getDeclaredTarget().getDeclaringClass())) return true;
			}
			catch (Exception ex) { ex.printStackTrace(); }
		}
		
		return false;
	}	
	
	public static List<IMethod> findTargetMethods(CGNode curMthNode, SSAInvokeInstruction invokeInsn, CallGraph clGraph) throws Exception
	{
		List<IMethod> methods = new ArrayList<IMethod>();

		Set<CGNode> invokeTargetNodes = clGraph.getPossibleTargets(curMthNode, invokeInsn.getCallSite());
		
		for (CGNode invokeTgtMthNode : invokeTargetNodes)
		{
			methods.add(invokeTgtMthNode.getMethod());
		}
		
		return methods;
	}
	
	public static AllocationSite getAllocationSite(AllocationSiteInNode allocKey)
	{
		String methodSig = getMethodSignature(allocKey.getNode().getMethod());
				
		int insnPos = allocKey.getSite().getProgramCounter();
		
		Context ctx = null;
		
		if (Configuration.enabledPAExhaustObjCtx)
		{
			ObjectStringContext objStringCtx = new ObjectStringContext();
			
			// potentially recursive
			extendAllocSiteContext(objStringCtx, allocKey);
			
			ctx = objStringCtx;
		}
				
		return new AllocationSite(methodSig, insnPos, ctx);
	}
	
	private static void extendAllocSiteContext(ObjectStringContext objStringCtx, AllocationSiteInNode allocKey)
	{
		// process the next outer level
		
		com.ibm.wala.ipa.callgraph.Context akNodeCtx = allocKey.getNode().getContext();
		
		if (akNodeCtx instanceof CallerSiteContext)
		{
			CallerSiteContext csCtx = (CallerSiteContext) akNodeCtx;
			
			objStringCtx.addFirstElement(new AllocationSite(getMethodSignature(csCtx.getCaller().getMethod()), csCtx.getCallSite().getProgramCounter()));
		}
		else if (akNodeCtx instanceof ReceiverInstanceContext)
		{
			ReceiverInstanceContext riCtx = (ReceiverInstanceContext) akNodeCtx;
			
			InstanceKey ik = (InstanceKey) riCtx.get(com.ibm.wala.ipa.callgraph.ContextKey.RECEIVER);
			
			if (ik instanceof AllocationSiteInNode)
			{
				// nak = Next Alloc Key
				AllocationSiteInNode nak = (AllocationSiteInNode) ik;
				
				String nakMethodSig = getMethodSignature(nak.getNode().getMethod());				
				int nakInsnPos = nak.getSite().getProgramCounter();
				
				objStringCtx.addFirstElement(new AllocationSite(nakMethodSig, nakInsnPos));				
				
				extendAllocSiteContext(objStringCtx, nak);
			}
		}
	}
	
	public static AllocationSite getAllocationSiteForClass(String className)
	{
		return new AllocationSite(className+".<clinit>", -1);
	}
	
	public static ClassName getAllocatedObjectClassName(ProgramPoint pp)
	{
		CGNode mthNode = mthSig2CGNode.get(pp.methodSig);
		
		IR mthIR = mthNode.getIR();			
		if (mthIR == null) return null;
					
		SSAInstruction[] instructions = mthIR.getInstructions();
		for (int idx = 0; idx < instructions.length; idx++)
		{
			if (instructions[idx] == null) continue;
				
			int curInsnPos = WALAUtils.getInsnBytecodePos(mthNode, idx);
			
			if (pp.insnPos == curInsnPos)
			{
				if (instructions[idx] instanceof SSANewInstruction)
				{
					SSANewInstruction newInsn = (SSANewInstruction) instructions[idx];
					
					String classNameStr = getClassName(newInsn.getConcreteType());
					
					return ClassName.createFromString(classNameStr);
				}
				else return null;
			}
		}
		
		return null;
	}
	
	public static boolean checkTypeAliasing(String typeName1, String typeName2) throws Exception
	{
		if (typeName1.equals(typeName2)) return true;
		
		// two variables are may-aliased if their types (classes) are on one path in the class hierarchy (if one type is a subtype of the other)
		// for two variables of interface types, we also check if they have a common implementing class
			
		IClass typeCls1 = findClass(typeName1);
		IClass typeCls2 = findClass(typeName2);

		if ((typeCls1 != null) && (typeCls2 != null))
		{
			if (classHierarchy.isSubclassOf(typeCls1, typeCls2)) return true;
			if (classHierarchy.isSubclassOf(typeCls2, typeCls1)) return true;
				
			if (typeCls1.isInterface() && typeCls2.isInterface())
			{
				Set<IClass> implClasses1 = classHierarchy.getImplementors(typeCls1.getReference());
				Set<IClass> implClasses2 = classHierarchy.getImplementors(typeCls2.getReference());
				
				for (IClass implCls1 : implClasses1)
				{
					String implClsName1 = getClassName(implCls1);
					
					for (IClass implCls2 : implClasses2)
					{
						String implClsName2 = getClassName(implCls2);
						
						if (implClsName1.equals(implClsName2)) return true;
					}
				}
			}
			else
			{				
				if (classHierarchy.implementsInterface(typeCls1, typeCls2)) return true;
				if (classHierarchy.implementsInterface(typeCls2, typeCls1)) return true;
			}
		}
		
		return false;
	}
	
	public static boolean checkTypeAliasing(TypeInference mthTypeInfo, int tgtValNum, int otherValNum)
	{	
		TypeReference tgtValTypeRef = mthTypeInfo.getType(tgtValNum).getTypeReference();							
		if ((tgtValTypeRef != null) && tgtValTypeRef.isReferenceType())
		{
			String tgtValTypeName = getTypeNameStr(tgtValTypeRef);
			
			TypeReference otherValTypeRef = mthTypeInfo.getType(otherValNum).getTypeReference();							
			if ((otherValTypeRef != null) && otherValTypeRef.isReferenceType())
			{
				String otherValTypeName = getTypeNameStr(otherValTypeRef);
					
				try
				{
					// we use type-based aliasing
					if (checkTypeAliasing(tgtValTypeName, otherValTypeName)) return true;
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
					return false;
				}
			}
		}
		
		return false;
	}	
	
	public static Set<Integer> getLocalVariablesWithRefType(IR mthIR, TypeInference mthTypeInfo)
	{
		Set<Integer> valNums = new HashSet<Integer>();
		
		// method parameters
				
		int[] methodParams = mthIR.getParameterValueNumbers();
		for (int i = 0; i < methodParams.length; i++) 
		{
			TypeReference valTypeRef = mthTypeInfo.getType(methodParams[i]).getTypeReference();
			if ((valTypeRef != null) && valTypeRef.isReferenceType()) valNums.add(methodParams[i]);
		}

		// local variables 
				
		SSAInstruction[] instructions = mthIR.getInstructions();
		for (int i = 0; i < instructions.length; i++) 
		{
			SSAInstruction insn = instructions[i];					
			if (insn == null) continue;
					
			for (int j = 0; j < insn.getNumberOfDefs(); j++) 
			{
				TypeReference valTypeRef = null;
				
				TypeAbstraction valTypeAbs = mthTypeInfo.getType(insn.getDef(j));
				if (valTypeAbs != null) valTypeRef = valTypeAbs.getTypeReference();
				
				if ((valTypeRef != null) && valTypeRef.isReferenceType()) valNums.add(insn.getDef(j));
			}
		}
		
		return valNums;
	}
	
	public static boolean isFieldAccessInsn(CGNode mthNode, int insnIndex)
	{
		if (insnIndex == -1) return false;
		
		IR methodIR = mthNode.getIR();			
		if (methodIR == null) return false;
		
		SSAInstruction[] instructions = methodIR.getInstructions();
		
		if (instructions[insnIndex] instanceof SSAFieldAccessInstruction) return true;
		else return false;
	}
	
	public static int getTargetRefForPreviousFieldAccess(String mthSig, int startInsnIndex, int skipCount)
	{
		// we assume that all relevant fields access instructions are consecutive without any jump target in the middle
		
		CGNode mthNode = getNodeForMethod(mthSig);
		IR mthIR = mthNode.getIR();
	
		SSAInstruction[] instructions = mthIR.getInstructions();
		
		int curInsnIndex = startInsnIndex;
		
		// get the first instruction that performs field access		
		while ( ! (instructions[curInsnIndex] instanceof SSAFieldAccessInstruction) ) curInsnIndex--;
		
		// skip the requested number of field accesses
		for (int i = 0; i < skipCount; i++)
		{
			curInsnIndex--;			
			while ( ! (instructions[curInsnIndex] instanceof SSAFieldAccessInstruction) ) curInsnIndex--;
		}
		
		SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) instructions[curInsnIndex];
		
		return faInsn.getRef();			
	}
	
	public static int getTargetRefForFieldAccess(String mthSig, int fieldInsnPos) throws Exception
	{
		IMethod mth = null;
		IR mthIR = null;
	
		CGNode mthNode = getNodeForMethod(mthSig);

		if (mthNode != null)
		{
			// typical code path			
			mth = mthNode.getMethod();
			mthIR = mthNode.getIR();
		}
		else
		{
			// when the method is not in the static call graph
			// should happen very rarely (for special method calls by JPF VM)			
			mth = findMethod(mthSig);
			mthIR = cache.getIRFactory().makeIR(mth, Everywhere.EVERYWHERE, SSAOptions.defaultOptions());
		}
	
		SSAInstruction[] instructions = mthIR.getInstructions();
		
		for (int insnIndex = 0; insnIndex < instructions.length; insnIndex++) 
		{
			SSAInstruction insn = instructions[insnIndex];
			if (insn == null) continue;
			
			if (getInsnBytecodePos(mth, insnIndex) == fieldInsnPos)
			{
				SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) instructions[insnIndex];
				return faInsn.getRef();
			}
		}
		
		// should not happen
		return -1;
	}	
	
	public static int getArrayDimensionsCount(TypeReference arrayTypeRef)
	{
		return arrayTypeRef.getDimensionality();
	}
	
	public static int getInsnBytecodePos(CGNode mthNode, int insnIndex)
	{
		return getInsnBytecodePos(mthNode.getMethod(), insnIndex);
	}
	
	public static int getInsnBytecodePos(IMethod mth, int insnIndex)
	{
		try
		{
			return ((IBytecodeMethod) mth).getBytecodeIndex(insnIndex);
		}
		catch (Exception ex)
		{
			return -1;
		}
	}
	
	public static void loadMethodNodesCache(CallGraph clGraph)
	{
		for (CGNode node : clGraph)
		{
			String methodSig = getMethodSignature(node.getMethod());
			
			mthSig2CGNode.put(methodSig, node);
		}
	}
	
	public static CGNode getNodeForMethod(String methodSig)
	{
		return mthSig2CGNode.get(methodSig);
	}
	
	public static IMethod findMethod(String tgtMethodSig) throws Exception
	{
		// extract class name from target method signature
		String tgtClassName = tgtMethodSig.substring(0, tgtMethodSig.lastIndexOf('.'));

		IClass tgtCls = findClass(tgtClassName);
		
		// find method with the given signature
		IMethod tgtMth = null;
		for (IMethod mth : tgtCls.getAllMethods())
		{
			String mthSig = getMethodSignature(mth);
			
			if (tgtMethodSig.equals(mthSig))
			{
				tgtMth = mth;
				break;
			}
		}
		
		return tgtMth;
	}
	
	public static boolean isAbstractedMethod(String methodSig)
	{
		// WALA uses abstraction for Thread.<init> that omits call of ThreadGroup.add
		if (methodSig.startsWith("java.lang.ThreadGroup.add")) return true;
		
		return false;
	}
	
	public static List<ProgramPoint> getAllProgramPoints(CallGraph clGraph)
	{
		List<ProgramPoint> allPPs = new ArrayList<ProgramPoint>();
		
		Set<String> processedMethods = new HashSet<String>();
				
		for (CGNode node : clGraph) 
		{
			String methodSig = getMethodSignature(node.getMethod());
			
			if (processedMethods.contains(methodSig)) continue;
			
			IR methodIR = node.getIR();
			
			if (methodIR == null) continue;
					
			SSAInstruction[] instructions = methodIR.getInstructions();
			for (int idx = 0; idx < instructions.length; idx++)
			{
				if (instructions[idx] == null) continue;
				
				int insnPos = WALAUtils.getInsnBytecodePos(node, idx);
		
				ProgramPoint pp = new ProgramPoint(methodSig, insnPos);
				
				allPPs.add(pp);		
			}			
		
			processedMethods.add(methodSig);
		}
		
		return allPPs;
	}			
	
	public static void printCallGraph(CallGraph clGraph, int maxLevel)
	{
		System.out.println("CALL GRAPH");
		System.out.println("==========");

		CGNode entryNode = clGraph.getFakeRootNode(); 
		printCallGraphNode(clGraph, entryNode, "method: ", maxLevel, 0);

		System.out.println("");
	}

	private static void printCallGraphNode(CallGraph clGraph, CGNode node, String prefix, int maxLevel, int curLevel)
	{
		if (curLevel > maxLevel) return;
		
		System.out.println(prefix + getMethodSignature(node.getMethod()));
		
		Iterator<CallSiteReference> callSitesIt = node.iterateCallSites();
		while (callSitesIt.hasNext())
		{
			CallSiteReference callSite = callSitesIt.next();
			
			Set<CGNode> targetNodes = clGraph.getPossibleTargets(node, callSite);
			
			for (CGNode tgtNode : targetNodes) printCallGraphNode(clGraph, tgtNode, "\t" + prefix, maxLevel, curLevel + 1);
		}
	}

	public static void printMethodIR(CGNode mthNode, String mthSig)
	{
		IR methodIR = mthNode.getIR();
					
		if (methodIR == null) return;
					
		System.out.println("method signature = " + mthSig);
	   
		SSAInstruction[] instructions = methodIR.getInstructions();
		for (int ssaIndex = 0; ssaIndex < instructions.length; ssaIndex++)
		{
			int insnPos = WALAUtils.getInsnBytecodePos(mthNode, ssaIndex);
			if (instructions[ssaIndex] == null) System.out.println("\t " + insnPos + ": null");
			else System.out.println("\t " + insnPos + ": " + instructions[ssaIndex].toString());
		}
	}
	
	public static void printAllMethodsIR(Graph<BasicBlockInContext<IExplodedBasicBlock>> icfg)
	{
		Set<String> printedMethods = new HashSet<String>();
				
		System.out.println("METHOD SSA IR");
		System.out.println("============="); 
		
		for (BasicBlockInContext<IExplodedBasicBlock> bb : icfg) 
		{
			CGNode node = bb.getNode();
					
			String methodSig = getMethodSignature(node.getMethod());
			
			if ( ! Debug.isWatchedEntity(methodSig) ) continue;
			
			if (printedMethods.contains(methodSig)) continue;
			
			printMethodIR(node, methodSig);

			printedMethods.add(methodSig);
		}

		System.out.println("");
	}

	public static void printAllMethodsIR(CallGraph clGraph)
	{
		Set<String> printedMethods = new HashSet<String>();
				
		System.out.println("METHOD SSA IR");
		System.out.println("============="); 
		
		for (CGNode node : clGraph) 
		{
			String methodSig = getMethodSignature(node.getMethod());
			
			if ( ! Debug.isWatchedEntity(methodSig) ) continue;
						
			if (printedMethods.contains(methodSig)) continue;
			
			printMethodIR(node, methodSig);

			printedMethods.add(methodSig);
		}

		System.out.println("");
	}
	
	public static void printInterProcCFG(Graph<BasicBlockInContext<IExplodedBasicBlock>> icfg)
	{
		System.out.println("INTERPROC CFG");
		System.out.println("============="); 
		
		for (BasicBlockInContext<IExplodedBasicBlock> bb : icfg) 
		{
			for (Iterator<BasicBlockInContext<IExplodedBasicBlock>> succIt = icfg.getSuccNodes(bb); succIt.hasNext(); )
			{
				BasicBlockInContext<IExplodedBasicBlock> succ = succIt.next();
			
				String methodSigFrom = getMethodSignature(bb.getNode().getMethod());
				String methodSigTo = getMethodSignature(succ.getNode().getMethod());
				
				if ( ! (Debug.isWatchedEntity(methodSigFrom) || Debug.isWatchedEntity(methodSigTo)) ) continue;
				
				System.out.println(bb.toString() + " ==> " + succ.toString());
			}
		}
			
		System.out.println("");
	}
}

