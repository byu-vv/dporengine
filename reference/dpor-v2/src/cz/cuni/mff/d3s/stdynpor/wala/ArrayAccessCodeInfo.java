/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.wala;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAArrayReferenceInstruction;
import com.ibm.wala.analysis.typeInference.TypeInference;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.AllocationSite;
import cz.cuni.mff.d3s.stdynpor.data.ClassName;
import cz.cuni.mff.d3s.stdynpor.data.ArrayID;
import cz.cuni.mff.d3s.stdynpor.data.ArrayElementAccess;


public class ArrayAccessCodeInfo
{
	public static Set<ArrayID> getArraysAccessedByInsn(CGNode mthNode, SSAArrayReferenceInstruction arrInsn)
	{
		Set<ArrayID> arrayIDs = new HashSet<ArrayID>();
			
		try
		{
			String methodSig = WALAUtils.getMethodSignature(mthNode.getMethod());
			
			TypeInference mthTypeInfo = TypeAnalysisData.getMethodTypeInfo(methodSig, mthNode.getIR());			
			TypeReference arrayObjTypeRef = mthTypeInfo.getType(arrInsn.getArrayRef()).getTypeReference();
			
			String arrayClassName = WALAUtils.getTypeNameStr(arrayObjTypeRef);
			
			int arrayDimensions = WALAUtils.getArrayDimensionsCount(arrayObjTypeRef);
			
			if (Configuration.usingPointerAlias)
			{
				Set<AllocationSite> objectAllocSites = PointerAnalysisData.getObjectAllocSites(arrayClassName, methodSig, arrInsn.getArrayRef());
			
				for (AllocationSite objAS : objectAllocSites) arrayIDs.add(new ArrayID(objAS, arrayDimensions));
			}
			else
			{
				arrayIDs.add(new ArrayID(ClassName.createFromString(arrayClassName), arrayDimensions));
			}
		}
		catch (Exception ex) 
		{
			ex.printStackTrace(); 
		}
			
		return arrayIDs;
	}

	public static Set<ArrayElementAccess> getArrayElementAccessesForInsn(CGNode mthNode, SSAArrayReferenceInstruction arrInsn, int ssaInsnIndex)
	{
		Set<ArrayElementAccess> accesses = new HashSet<ArrayElementAccess>();
			
		try
		{
			String methodSig = WALAUtils.getMethodSignature(mthNode.getMethod());
			
			int insnPos = WALAUtils.getInsnBytecodePos(mthNode, ssaInsnIndex);
			
			TypeInference mthTypeInfo = TypeAnalysisData.getMethodTypeInfo(methodSig, mthNode.getIR());			
			TypeReference arrayObjTypeRef = mthTypeInfo.getType(arrInsn.getArrayRef()).getTypeReference();
			
			String arrayClassName = WALAUtils.getTypeNameStr(arrayObjTypeRef);
			
			int arrayDimensions = WALAUtils.getArrayDimensionsCount(arrayObjTypeRef);
			
			Set<ArrayID> arrays = new HashSet<ArrayID>();
			
			if (Configuration.usingPointerAlias)
			{
				Set<AllocationSite> objectAllocSites = PointerAnalysisData.getObjectAllocSites(arrayClassName, methodSig, arrInsn.getArrayRef());
			
				for (AllocationSite objAS : objectAllocSites) arrays.add(new ArrayID(objAS, arrayDimensions));
			}
			else
			{
				arrays.add(new ArrayID(ClassName.createFromString(arrayClassName), arrayDimensions));
			}
			
			for (ArrayID aid : arrays)
			{
				ProgramPoint pp = new ProgramPoint(methodSig, insnPos);
			
				accesses.add(new ArrayElementAccess(aid, pp));
			}
		}
		catch (Exception ex) 
		{
			ex.printStackTrace(); 
		}
			
		return accesses;
	}

}

