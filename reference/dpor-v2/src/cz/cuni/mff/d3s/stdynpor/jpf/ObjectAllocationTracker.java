/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.jpf;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.Stack;
import java.util.Collection;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StaticElementInfo;
import gov.nasa.jpf.vm.ClassLoaderInfo;
import gov.nasa.jpf.search.Search;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.data.AllocationSite;
import cz.cuni.mff.d3s.stdynpor.data.ObjectStringContext;
import cz.cuni.mff.d3s.stdynpor.bytecode.ByteCodeUtils;


public class ObjectAllocationTracker extends ListenerAdapter
{
	// map from object index to context-insensitive allocation site
	private static Map<Integer, AllocationSite> obj2AllocSite;
	
	// map from a class object index (ref) to the name of the associated Java class
	private static Map<Integer, String> clsObjRef2Name;
	
	// map from a Java class name to the JPF class info
	private static Map<String, ClassInfo> clsName2Info;
	
	// map from allocation site to the number of objects allocated at the given site
	private static Map<AllocationSite, Integer> allocSite2Count;
	
	private Stack<Map<Integer,AllocationSite>> currentPathAS;
	private Stack<Map<Integer,String>> currentPathCRN;
	private Stack<Map<String,ClassInfo>> currentPathCNI;
	private Stack<Map<AllocationSite,Integer>> currentPathOAC;
	private boolean changedAllocData;
	
	
	static
	{		
		obj2AllocSite = new HashMap<Integer, AllocationSite>();
		clsObjRef2Name = new HashMap<Integer, String>();
		clsName2Info = new HashMap<String, ClassInfo>();
		allocSite2Count = new HashMap<AllocationSite, Integer>();
	}
	
	
	public ObjectAllocationTracker(Config cfg)
	{
		currentPathAS = new Stack<Map<Integer,AllocationSite>>();
		currentPathCRN = new Stack<Map<Integer,String>>();
		currentPathCNI = new Stack<Map<String,ClassInfo>>();
		currentPathOAC = new Stack<Map<AllocationSite,Integer>>();
		changedAllocData = false;
	}
	
	public void objectCreated(VM vm, ThreadInfo curTh, ElementInfo objEI)
	{
		if (curTh == null) return;

		Instruction insn = curTh.getPC();
		if (insn == null) return;		

		int objRef = objEI.getObjectRef();

		// default option (used also when there is no pointer analysis enabled)
		AllocationSite allocSite = JPFUtils.getAllocSiteFromInsn(insn);
		
		if (Configuration.enabledPAExhaustObjCtx)
		{
			// get an object string that is ordered from the outermost receiver object (main) to the receiver object for the top stack frame	 
			ObjectStringContext objStringCtx = JPFUtils.getReceiverObjectStringForThreadStack(curTh);
			
			int lastCollPos = -1;

			for (int i = objStringCtx.length() - 1; i >= 0; i--)
			{
				AllocationSite ctxElem = objStringCtx.getElement(i);

				if (ByteCodeUtils.isCollectionsMethod(ctxElem.progPoint.methodSig)) 
				{
					lastCollPos = i;
					break;
				}
			}

			if (ByteCodeUtils.isCollectionsMethod(allocSite.progPoint.methodSig))
			{
				// allocation site involves collections -> use one level of context
				
				if (objStringCtx.length() > 0) objStringCtx.keepOnlyLastElement();
			}
			else if (lastCollPos != -1)
			{
				// object string involves collections and allocation site does not involve collections -> use one level of context prior to the first occurrence
				
				for (int i = 0; i < lastCollPos - 1; i++)
				{
					objStringCtx.dropFirstElement();
				}
			}
			else 
			{
				// no collections anywhere
				
				objStringCtx.dropAllElements();
			}
			
			allocSite = AllocationSite.makeCopyWithContext(allocSite, objStringCtx);
		}
		
		obj2AllocSite.put(objRef, allocSite);
		
		Integer oldCount = allocSite2Count.get(allocSite);
		if (oldCount == null) oldCount = new Integer(0);
		allocSite2Count.put(allocSite, oldCount.intValue() + 1);

		changedAllocData = true;
	}

	// java.lang.Class objects (for static fields)
	public void classLoaded(VM vm, ClassInfo ci)
	{
		Instruction insn = vm.getInstruction();
		if (insn == null) return;
		
		int objRef = ci.getClassObjectRef();
		
		AllocationSite allocSite = JPFUtils.getAllocSiteForClass(ci.getName());
		
		obj2AllocSite.put(objRef, allocSite);
		
		clsObjRef2Name.put(objRef, ci.getName());
		
		clsName2Info.put(ci.getName(), ci);
		
		Integer oldCount = allocSite2Count.get(allocSite);
		if (oldCount == null) oldCount = new Integer(0);
		allocSite2Count.put(allocSite, oldCount.intValue() + 1);

		changedAllocData = true;
	}
	
	public void searchStarted(Search search)
	{
		Map<Integer,AllocationSite> asCopy = new HashMap<Integer,AllocationSite>();
		asCopy.putAll(obj2AllocSite);
		currentPathAS.push(asCopy);
		
		Map<Integer,String> crnCopy = new HashMap<Integer,String>();
		crnCopy.putAll(clsObjRef2Name);
		currentPathCRN.push(crnCopy);
		
		Map<String,ClassInfo> cniCopy = new HashMap<String,ClassInfo>();
		cniCopy.putAll(clsName2Info);
		currentPathCNI.push(cniCopy);
		
		Map<AllocationSite,Integer> oacCopy = new HashMap<AllocationSite,Integer>();
		oacCopy.putAll(allocSite2Count);
		currentPathOAC.push(oacCopy);
		
		changedAllocData = false;
	}
	
	public void stateAdvanced(Search search)
	{
		Map<Integer,AllocationSite> asCopy = null;
		if (changedAllocData) 
		{
			asCopy = new HashMap<Integer,AllocationSite>();
			asCopy.putAll(obj2AllocSite);
		}
		else
		{
			asCopy = currentPathAS.peek();
		}
		currentPathAS.push(asCopy);
		
		Map<Integer,String> crnCopy = null;
		if (changedAllocData) 
		{
			crnCopy = new HashMap<Integer,String>();
			crnCopy.putAll(clsObjRef2Name);
		}
		else
		{
			crnCopy = currentPathCRN.peek();
		}
		currentPathCRN.push(crnCopy);
		
		Map<String,ClassInfo> cniCopy = null;
		if (changedAllocData) 
		{
			cniCopy = new HashMap<String,ClassInfo>();
			cniCopy.putAll(clsName2Info);
		}
		else
		{
			cniCopy = currentPathCNI.peek();
		}
		currentPathCNI.push(cniCopy);
		
		Map<AllocationSite,Integer> oacCopy = null;
		if (changedAllocData) 
		{
			oacCopy = new HashMap<AllocationSite,Integer>();
			oacCopy.putAll(allocSite2Count);
		}
		else
		{
			oacCopy = currentPathOAC.peek();
		}
		currentPathOAC.push(oacCopy);
		
		changedAllocData = false;
	}
	
	public void stateBacktracked(Search search) 
	{
		Map<Integer,AllocationSite> curAS = currentPathAS.pop();
		Map<Integer,AllocationSite> prevAS = currentPathAS.peek();
		if (curAS != prevAS)
		{
			obj2AllocSite = new HashMap<Integer,AllocationSite>();
			obj2AllocSite.putAll(prevAS);
		}
		
		Map<Integer,String> curCRN = currentPathCRN.pop();
		Map<Integer,String> prevCRN = currentPathCRN.peek();
		if (curCRN != prevCRN)
		{
			clsObjRef2Name = new HashMap<Integer,String>();
			clsObjRef2Name.putAll(prevCRN);
		}
		
		Map<String,ClassInfo> curCNI = currentPathCNI.pop();
		Map<String,ClassInfo> prevCNI = currentPathCNI.peek();
		if (curCNI != prevCNI)
		{
			clsName2Info = new HashMap<String,ClassInfo>();
			clsName2Info.putAll(prevCNI);
		}
		
		Map<AllocationSite,Integer> curOAC = currentPathOAC.pop();
		Map<AllocationSite,Integer> prevOAC = currentPathOAC.peek();
		if (curOAC != prevOAC)
		{
			allocSite2Count = new HashMap<AllocationSite,Integer>();
			allocSite2Count.putAll(prevOAC);
		}
		
		changedAllocData = false;
	}

	public static AllocationSite getAllocSiteForObject(int objRef)
	{
		return obj2AllocSite.get(objRef);
	}
	
	public static String getClassNameForClassObject(int clsObjRef)
	{
		String clsName = clsObjRef2Name.get(clsObjRef);
		
		// probably some library class (initialized before listeners are notified)
		if (clsName == null)
		{
			// find the corresponding StaticElementInfo and get the class name from it
		
			for (ClassLoaderInfo cli : VM.getVM().getClassLoaderList())
			{
				for (ElementInfo ei : cli.getStatics())
				{
					if (ei == null) continue;
					
					StaticElementInfo sei = (StaticElementInfo) ei;
					
					if (sei.getClassObjectRef() == clsObjRef)
					{
						clsName = sei.getClassInfo().getName();													
					}
				}
			}
			
			clsObjRef2Name.put(clsObjRef, clsName);			
		}
		
		return clsName;
	}
	
	public static ClassInfo getClassInformationForName(String className)
	{
		ClassInfo clsInfo = clsName2Info.get(className);
		
		// probably some library class (initialized before listeners are notified)
		if (clsInfo == null)
		{
			// find the proper ClassInfo with the given class name
		
			for (ClassLoaderInfo clInfo : VM.getVM().getClassLoaderList())
			{
				for (ClassInfo ci : clInfo)
				{
					if (ci.getName().equals(className)) clsInfo = ci;
				}
			}
			
			clsName2Info.put(className, clsInfo);
		}
		
		return clsInfo;
	}
	
	public static int getPastUsageCountForAllocSite(AllocationSite as)
	{
		Integer countObj = allocSite2Count.get(as);

		if (countObj == null)
		{
			// objID is allocation site for class or a system object
			return 1;
		}
		
		return countObj.intValue();
	}

}
