/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.jpf.dpor;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Stack;
import java.util.Collections;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.bytecode.FieldInstruction;
import gov.nasa.jpf.vm.bytecode.ReturnInstruction;
import gov.nasa.jpf.vm.bytecode.ArrayElementInstruction;
import gov.nasa.jpf.jvm.bytecode.GETFIELD;
import gov.nasa.jpf.jvm.bytecode.GETSTATIC;
import gov.nasa.jpf.jvm.bytecode.PUTFIELD;
import gov.nasa.jpf.jvm.bytecode.PUTSTATIC;
import gov.nasa.jpf.jvm.bytecode.DUP;
import gov.nasa.jpf.jvm.bytecode.ASTORE;
import gov.nasa.jpf.jvm.bytecode.MONITORENTER;
import gov.nasa.jpf.jvm.bytecode.MONITOREXIT;
import gov.nasa.jpf.jvm.bytecode.JVMInvokeInstruction;
import gov.nasa.jpf.jvm.bytecode.ArrayLoadInstruction;
import gov.nasa.jpf.jvm.bytecode.ArrayStoreInstruction;

import cz.cuni.mff.d3s.stdynpor.jpf.ArraySharednessPolicy;
import cz.cuni.mff.d3s.stdynpor.jpf.JPFUtils;
import cz.cuni.mff.d3s.stdynpor.jpf.DynamicThreadChoice;


public class HappensBeforeOrdering extends ListenerAdapter
{
	// map from thread ID to a list of visible actions
	private static List<VisibleAction>[] th2VisibleActions;
	
	// visible actions performed in the current transition
	private static TransitionInfo curTr;
	
	// stack of transitions (current path)
	private static Stack<TransitionInfo> curPathTrs;
	
	// index for the next visible action
	private static int nextActionID;
	
	// stack of all visible actions on the current path
	private static Stack<VisibleAction> curPathActions;
	
	// stack of visible accesses (field, array) on the current path
	private static Stack<VisibleAction> curPathAccesses;
	
	private static Stack<Integer> curPathStates;

	// map from state id to the list of visible accesses that may happen after the state
	private static List<VisibleAction>[] state2FutureAccesses;

	
	public HappensBeforeOrdering(Config cfg)
	{
		th2VisibleActions = new List[2];
		
		curPathTrs = new Stack<TransitionInfo>();
		
		nextActionID = 0;
		
		curPathActions = new Stack<VisibleAction>();
		
		curPathAccesses = new Stack<VisibleAction>();
		
		curPathStates = new Stack<Integer>();
		
		state2FutureAccesses = new List[1024];
	}
	
	
	/**
	 * We collect instructions GETFIELD and PUTFIELD in this method so that everything is on the stack.
	 */
	public void executeInstruction(VM vm, ThreadInfo ti, Instruction insn)
	{
		if (ti.isFirstStepInsn()) curTr.threadId = ti.getId();
		
		if (insn instanceof FieldInstruction)
		{
			// we ignore field access instructions that do not correspond to any dynamic thread choice
			if ( ! ti.isFirstStepInsn() ) return;
		}
		
		if (insn instanceof ArrayElementInstruction)
		{
			// we ignore array element access instructions that do not correspond to any dynamic thread choice
			if ( ! ti.isFirstStepInsn() ) return;
		}
			
		VisibleAction act = null;
		
		if (insn instanceof GETFIELD)
		{
			GETFIELD gfInsn = (GETFIELD) insn;

			FieldInfo targetFI = gfInsn.getFieldInfo();
			
			if (JPFUtils.isSchedulingPointAtFieldAccess(targetFI, ti, gfInsn.getMethodInfo(), gfInsn.getInstructionIndex()))
			{			
				int targetObjRef = ti.getTopFrame().peek();
			
				ElementInfo targetObjEI = vm.getHeap().get(targetObjRef);
				
				// we can safely ignore field accesses to heap objects that are not reachable from multiple threads
				if (targetObjEI.isShared())
				{
					act = new VisibleAction(VisibleAction.Type.FREAD, targetObjRef, targetFI.getFieldIndex(), ti.getId());
				}
			}
		}
		
		if (insn instanceof PUTFIELD)
		{
			PUTFIELD pfInsn = (PUTFIELD) insn;

			FieldInfo targetFI = pfInsn.getFieldInfo();
			
			if (JPFUtils.isSchedulingPointAtFieldAccess(targetFI, ti, pfInsn.getMethodInfo(), pfInsn.getInstructionIndex()))
			{
				int targetObjRef = -1;
				switch (targetFI.getStorageSize())
				{
					case 1:
						targetObjRef = ti.getTopFrame().peek(1);
						break;
					case 2:
						targetObjRef = ti.getTopFrame().peek(2);
						break;
				}
	
				ElementInfo targetObjEI = vm.getHeap().get(targetObjRef);
				
				// we can safely ignore field accesses to heap objects that are not reachable from multiple threads
				if (targetObjEI.isShared())
				{
					act = new VisibleAction(VisibleAction.Type.FWRITE, targetObjRef, targetFI.getFieldIndex(), ti.getId());
				}
			}
		}
		
		if (insn instanceof ArrayLoadInstruction)
		{
			ArrayLoadInstruction areadInsn = (ArrayLoadInstruction) insn;
			
			ElementInfo targetArrayEI = vm.getHeap().get(areadInsn.getArrayRef(ti));
			
			if ( ! ArraySharednessPolicy.isNeverBreakArrayElementAccess(areadInsn) )
			{
				// we can safely ignore accesses to array objects that are not reachable from multiple threads
				if (targetArrayEI.isShared())
				{
					act = new VisibleAction(VisibleAction.Type.AREAD, areadInsn.getArrayRef(ti), areadInsn.getIndex(ti), ti.getId());
				}
			}
		}
		
		if (insn instanceof ArrayStoreInstruction)
		{
			ArrayStoreInstruction awriteInsn = (ArrayStoreInstruction) insn;
			
			ElementInfo targetArrayEI = vm.getHeap().get(awriteInsn.getArrayRef(ti));
			
			if ( ! ArraySharednessPolicy.isNeverBreakArrayElementAccess(awriteInsn) )
			{
				// we can safely ignore accesses to array objects that are not reachable from multiple threads
				if (targetArrayEI.isShared())
				{
					act = new VisibleAction(VisibleAction.Type.AWRITE, awriteInsn.getArrayRef(ti), awriteInsn.getIndex(ti), ti.getId());
				}
			}
		}
		
		if (insn instanceof MONITORENTER)
		{
			MONITORENTER menInsn = (MONITORENTER) insn;
			
			int targetObjRef = ti.getTopFrame().peek();
			
			ElementInfo targetObjEI = vm.getHeap().get(targetObjRef);
			
			act = new VisibleAction(VisibleAction.Type.LOCK, targetObjRef, -1, ti.getId());
		}
		
		if (insn instanceof MONITOREXIT)
		{
			MONITOREXIT mexInsn = (MONITOREXIT) insn;
			
			int targetObjRef = ti.getTopFrame().peek();
			
			ElementInfo targetObjEI = vm.getHeap().get(targetObjRef);
			
			act = new VisibleAction(VisibleAction.Type.UNLOCK, targetObjRef, -1, ti.getId());
		}
		
		if (insn instanceof ReturnInstruction)
		{
			ReturnInstruction retInsn = (ReturnInstruction) insn;
			
			if (retInsn.getMethodInfo().isSynchronized())
			{
				int targetObjRef = -1;
				
				if (retInsn.getMethodInfo().isStatic())
				{
					targetObjRef = retInsn.getMethodInfo().getClassInfo().getClassObjectRef();	
				}
				else
				{
					targetObjRef = ti.getThis();
				} 
				
				ElementInfo targetObjEI = ti.getElementInfo(targetObjRef);
			
				act = new VisibleAction(VisibleAction.Type.UNLOCK, targetObjRef, -1, ti.getId());
			}
		}
				
		if (act != null)
		{
			act.index = nextActionID++;
			
			updateHappensBeforeData(act);
			
			curTr.actions.add(act);
		}
	}
	
	/**
	 * We collect instructions GETSTATIC and PUTSTATIC in this method so that class objects are initialized already.
	 */
	public void instructionExecuted(VM vm, ThreadInfo ti, Instruction nextInsn, Instruction execInsn)
	{
		if (ti.isFirstStepInsn()) curTr.threadId = ti.getId();
		
		if (execInsn instanceof FieldInstruction)
		{
			// we ignore field access instructions that do not correspond to any dynamic thread choice
			if ( ! ti.isFirstStepInsn() ) return;
		}
		
		if (execInsn instanceof ArrayElementInstruction)
		{
			// we ignore array element access instructions that do not correspond to any dynamic thread choice
			if ( ! ti.isFirstStepInsn() ) return;
		}
		
		VisibleAction act = null;
		
		if (execInsn instanceof GETSTATIC)
		{
			GETSTATIC gsInsn = (GETSTATIC) execInsn;

			FieldInfo targetFI = gsInsn.getFieldInfo();
			
			if (JPFUtils.isSchedulingPointAtFieldAccess(targetFI, ti, gsInsn.getMethodInfo(), gsInsn.getInstructionIndex()))
			{
				ClassInfo targetClass = targetFI.getClassInfo();
				
				ElementInfo targetClassEI = targetClass.getStaticElementInfo();
				
				act = new VisibleAction(VisibleAction.Type.FREAD, -1, targetFI.getFieldIndex(), ti.getId());
			}
		}
		
		if (execInsn instanceof PUTSTATIC)
		{
			PUTSTATIC psInsn = (PUTSTATIC) execInsn;
			
			FieldInfo targetFI = psInsn.getFieldInfo();
			
			if (JPFUtils.isSchedulingPointAtFieldAccess(targetFI, ti, psInsn.getMethodInfo(), psInsn.getInstructionIndex()))
			{			
				ClassInfo targetClass = targetFI.getClassInfo();
								
				ElementInfo targetClassEI = targetClass.getStaticElementInfo();
				
				act = new VisibleAction(VisibleAction.Type.FWRITE, -1, targetFI.getFieldIndex(), ti.getId());
			}
		}
		
		if (execInsn instanceof JVMInvokeInstruction)
		{
			JVMInvokeInstruction invokeInsn = (JVMInvokeInstruction) execInsn;
		
			MethodInfo tgtMethod = invokeInsn.getInvokedMethod();
		
			int targetObjRef = -1; 
			ElementInfo targetObjEI = null;
			
			if (tgtMethod.isStatic())
			{
				targetObjEI = tgtMethod.getClassInfo().getStaticElementInfo();
				targetObjRef = -1;
			}
			else
			{
				targetObjRef = ti.getCalleeThis(invokeInsn.getArgSize());
				targetObjEI = vm.getHeap().get(targetObjRef);
			}
			
			// class object not yet initialized
			if (targetObjEI == null) return;
			
			String tgtMthName = tgtMethod.getName();
			
			ClassInfo tgtMthCI = tgtMethod.getClassInfo();
			
			if (tgtMthName.equals("wait"))
			{
				act = new VisibleAction(VisibleAction.Type.WAIT, targetObjRef, -1, ti.getId());
			}
			
			if (tgtMthName.equals("notify"))
			{
				act = new VisibleAction(VisibleAction.Type.NOTIFY, targetObjRef, -1, ti.getId());
			}
			
			if (tgtMthName.equals("start") && JPFUtils.isThreadClass(tgtMthCI))
			{
				ThreadInfo newTh = vm.getThreadList().getThreadInfoForObjRef(targetObjRef);

				act = new VisibleAction(VisibleAction.Type.TSTART, targetObjRef, newTh.getId(), ti.getId());
			}
			
			if (tgtMthName.equals("join") && JPFUtils.isThreadClass(tgtMthCI))
			{
				act = new VisibleAction(VisibleAction.Type.TJOIN, targetObjRef, -1, ti.getId());
			}
			
			if (tgtMethod.isSynchronized())
			{
				act = new VisibleAction(VisibleAction.Type.LOCK, targetObjRef, -1, ti.getId());
			}
		}

		if (act != null)
		{
			act.index = nextActionID++;
			
			updateHappensBeforeData(act);
			
			curTr.actions.add(act);
		}
	}
	
	public void searchStarted(Search search) 
	{
		curTr = new TransitionInfo();
		
		curPathStates.push(-1);
	}
	
	public void stateAdvanced(Search search) 
	{
		List<VisibleAction> thActions = null;
	
		if (curTr.threadId >= th2VisibleActions.length)
		{
			List<VisibleAction>[] newT2VA = new List[curTr.threadId + 1];
			System.arraycopy(th2VisibleActions, 0, newT2VA, 0, th2VisibleActions.length);
			th2VisibleActions = newT2VA;
		}
		else
		{
			thActions = th2VisibleActions[curTr.threadId];
		}
		
		if (thActions == null)
		{
			thActions = new ArrayList<VisibleAction>();
			th2VisibleActions[curTr.threadId] = thActions;
		}
		
		thActions.addAll(curTr.actions);

		for (VisibleAction act : curTr.actions)
		{
			curPathActions.push(act);
			if (act.isFieldAccess() || act.isArrayAccess()) curPathAccesses.push(act);
		}		
		
		curPathTrs.push(curTr);
		
		curTr = new TransitionInfo();
		
		curPathStates.push(search.getStateId());
	}
	
	public void stateBacktracked(Search search) 
	{
		TransitionInfo tri = curPathTrs.peek();
		
		// remove actions that belong to the backtracked transitions		
		List<VisibleAction> thActions = th2VisibleActions[tri.threadId];
		for (VisibleAction act : tri.actions)
		{
			thActions.remove(thActions.size()-1);
			nextActionID--;
			curPathActions.pop();
			if (act.isFieldAccess() || act.isArrayAccess()) curPathAccesses.pop();
		}
		
		curPathTrs.pop();
		
		Integer bkfromStateId = curPathStates.peek();
		curPathStates.pop();
		Integer bktoStateId = curPathStates.peek();

		// initial state
		if (bktoStateId.intValue() == -1) return;

		// get visible accesses that may happen after the state we backtrack from		
		List<VisibleAction> bkfromStateFutureAccesses = null;
		if (bkfromStateId.intValue() < state2FutureAccesses.length) bkfromStateFutureAccesses = state2FutureAccesses[bkfromStateId.intValue()];

		List<VisibleAction> bktoStateFutureAccesses = null;
		if (bktoStateId.intValue() < state2FutureAccesses.length) bktoStateFutureAccesses = state2FutureAccesses[bktoStateId.intValue()];

		// compute the new set of future field accesses
		
		List<VisibleAction> futureAccesses = null;
		
		boolean bktoChoiceWithSingleOption = false;
		ChoiceGenerator cg = search.getVM().getChoiceGenerator();
		if (cg instanceof DynamicThreadChoice)
		{
			DynamicThreadChoice dcg = (DynamicThreadChoice) cg;
			if ( ! dcg.allThreadsEnabled() ) bktoChoiceWithSingleOption = true;
		}
		
		if (bktoChoiceWithSingleOption)
		{
			// visible access that is associated with this dynamic thread choice is not a globally visible instruction
				// it was not marked as globally relevant when all accesses on the current path were analyzed (SearchDriver)
			// we can keep the value from the single next state (from which JPF backtracks here)
			futureAccesses = bkfromStateFutureAccesses;
		}
		else
		{
			if (bktoStateFutureAccesses != null) futureAccesses = bktoStateFutureAccesses;
			
			if (futureAccesses == null) futureAccesses = new ArrayList<VisibleAction>();
			
			// optimize the speed of inclusion test (avoid loops in "List.contains()" below)
			Set<Integer> futaccHashCodes = new HashSet<Integer>();
			for (VisibleAction act : futureAccesses) futaccHashCodes.add(act.hashCode());
			
			if (bkfromStateFutureAccesses != null)
			{
				for (VisibleAction act : bkfromStateFutureAccesses)
				{
					if ( ! futaccHashCodes.contains(act.hashCode()) ) futureAccesses.add(act);
				}
			}
		
			for (VisibleAction act : tri.actions)
			{
				if (act.isFieldAccess() || act.isArrayAccess())
				{
					if ( ! futaccHashCodes.contains(act.hashCode()) ) futureAccesses.add(act);
				}
			}
			
			if (futureAccesses.isEmpty()) futureAccesses = null;
		}
		
		if (bktoStateId.intValue() >= state2FutureAccesses.length)
		{
			List<VisibleAction>[] newS2FA = new List[bktoStateId.intValue() + 16384];
			System.arraycopy(state2FutureAccesses, 0, newS2FA, 0, state2FutureAccesses.length);
			state2FutureAccesses = newS2FA;
		}
	
		// update field accesses that may happen after the state we backtrack to
		state2FutureAccesses[bktoStateId.intValue()] = futureAccesses;
	}
	
	public static List<VisibleAction> getVisibleAccessesOnCurrentPath()
	{
		List<VisibleAction> accesses = new ArrayList<VisibleAction>();
		
		accesses.addAll(curPathAccesses);
		
		for (VisibleAction act : curTr.actions)
		{
			if (act.isFieldAccess() || act.isArrayAccess()) accesses.add(act);
		}
		
		return accesses;
	}
	
	public static List<VisibleAction> getFutureAccessesForCurrentState(int curStateId)
	{
		List<VisibleAction> accesses = null;
		
		// this condition is not true when there is a loop in the state space (backjump)
		if (curStateId < state2FutureAccesses.length)
		{		
			accesses = state2FutureAccesses[curStateId];
		}
		
		if (accesses == null) accesses = Collections.emptyList();
		
		return accesses;
	}
	
	private static void updateHappensBeforeData(VisibleAction curVA)
	{
		for (int thId = 0; thId < th2VisibleActions.length; thId++)
		{
			// skip non-existent thread
			if (th2VisibleActions[thId] == null) continue;
			
			// skip current thread which performed the given visible action
			if (thId == curVA.threadId) continue;
			
			List<VisibleAction> actions = th2VisibleActions[thId];
			
			VisibleAction hbVA = null;
			
			for (int i = actions.size() - 1; i >= 0; i--)
			{
				VisibleAction act = actions.get(i);
				
				// test whether 'act' must happen before 'curVA'
				if (curVA.isConflictingWith(act))
				{
					hbVA = act;
					break;
				}
			}
			
			if (hbVA != null)
			{
				if (curVA.th2PrevHB.length <= thId)
				{
					VisibleAction[] newT2PHB = new VisibleAction[thId+1];
					System.arraycopy(curVA.th2PrevHB, 0, newT2PHB, 0, curVA.th2PrevHB.length);
					curVA.th2PrevHB = newT2PHB;
				}
				
				curVA.th2PrevHB[thId] = hbVA;
			}
		}		
	}
	
	private static VisibleAction getVisibleAction(int actIdx)
	{
		if (actIdx < curPathActions.size())
		{
			return curPathActions.get(actIdx);	
		}
		
		// how many actions in the current transition we must skip before we reach the target one
		int skipActionCount = actIdx - curPathActions.size();
		
		return curTr.actions.get(skipActionCount);
	}
	
	public static boolean existsOrderBetweenActions(int firstActIdx, int secondActIdx)
	{
		VisibleAction firstAct = getVisibleAction(firstActIdx);
		VisibleAction secondAct = getVisibleAction(secondActIdx);
		
		for (int i = firstActIdx; i < secondActIdx; i++)
		{
			VisibleAction actI = getVisibleAction(i);
			
			if (actI.threadId != firstAct.threadId) continue;

			for (int j = i + 1; j <= secondActIdx; j++)
			{
				VisibleAction actJ = getVisibleAction(j);
				
				if (actJ.threadId != secondAct.threadId) continue;
				
				if (actJ.mustHappenAfter(actI)) return true;
			}
		}
		
		return false;
	}
	
	
	static class TransitionInfo
	{
		// thread which executed this transition
		public int threadId;
		
		// visible actions
		public List<VisibleAction> actions;
		
		
		public TransitionInfo()
		{
			actions = new ArrayList<VisibleAction>();			
		}
		
		public boolean equals(Object obj)
		{
			if (obj == null) return false;
			
			if ( ! (obj instanceof TransitionInfo) ) return false;
			
			TransitionInfo other = (TransitionInfo) obj;
			
			if (this.threadId != other.threadId) return false;			
			if ( ! this.actions.equals(other.actions) ) return false;
			
			return true;
		}
		
		public int hashCode()
		{
			int hash = 0;
			
			hash = hash * 31 + this.threadId;
			hash = hash * 31 + this.actions.hashCode();
			
			return hash;
		} 
	}
}
