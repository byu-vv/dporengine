/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Collection;

import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.KernelState;
import gov.nasa.jpf.vm.ThreadList;
import gov.nasa.jpf.vm.Instruction;

import cz.cuni.mff.d3s.stdynpor.data.*;
import cz.cuni.mff.d3s.stdynpor.bytecode.ByteCodeUtils;
import cz.cuni.mff.d3s.stdynpor.wala.*;
import cz.cuni.mff.d3s.stdynpor.jpf.*;
import cz.cuni.mff.d3s.stdynpor.analysis.*;
import cz.cuni.mff.d3s.stdynpor.analysis.fieldaccess.*;
import cz.cuni.mff.d3s.stdynpor.analysis.pointeralias.*;
import cz.cuni.mff.d3s.stdynpor.analysis.arrayaccess.*;
import cz.cuni.mff.d3s.stdynpor.analysis.localvariable.*;
import cz.cuni.mff.d3s.stdynpor.analysis.methodcall.*;
import cz.cuni.mff.d3s.stdynpor.util.TreeNode;


public class AnalysisResultProcessor
{
	public static boolean printResultsFull = false;
	public static boolean printChoiceDecision = false;
	
	
	public static boolean existsConflictingFieldRead(ThreadInfo curTh, String curMthSig, int curInsnPos, String className, String fieldName, boolean isStatic, int objRef)
	{
		return existsConflictingFieldAccess('r', curTh, curMthSig, curInsnPos, className, fieldName, isStatic, objRef);
	}
	
	public static boolean existsConflictingFieldWrite(ThreadInfo curTh, String curMthSig, int curInsnPos, String className, String fieldName, boolean isStatic, int objRef)
	{
		return existsConflictingFieldAccess('w', curTh, curMthSig, curInsnPos, className, fieldName, isStatic, objRef);
	}	
	
	// look for conflicts with the given access to the field <className/objRef>.<fieldName> at the program point <curMthSig>:<curInsnPos> in the thread <curTh>
	private static boolean existsConflictingFieldAccess(char accessType, ThreadInfo curTh, String curMthSig, int curInsnPos, String classNameStr, String fieldNameStr, boolean isStatic, int objRef)
	{
		if (printChoiceDecision)
		{
			if (Debug.isWatchedEntity(curMthSig))
			{			
				System.out.println("[CHOICE DECISION] new query (field access): curTh = " + curTh.getId() + ", curMthSig = " + curMthSig + ", curInsnPos = " + curInsnPos + ", className = " + classNameStr + ", fieldName = " + fieldNameStr + ", static = " + isStatic + ", objRef = " + objRef + ", access type = " + accessType);
			}
		}

		ObjectID targetObject = null;
		
		if (Configuration.usingPointerAlias)
		{
			if (Configuration.enabledPAExhaustive || Configuration.enabledPAExhaustObjCtx)
			{
				if (isStatic)
				{
					targetObject = JPFUtils.getAllocSiteForClass(classNameStr);
				}
				else
				{
					targetObject = ObjectAllocationTracker.getAllocSiteForObject(objRef);
				}
			}
			else if (Configuration.enabledPADemandDrv || Configuration.enabledPADemandDrvCtx)
			{
				if ( ! isStatic )
				{
					int faInsnRef = FieldAccessCodeInfo.getFieldAccessTargetRef(curMthSig, curInsnPos);
					
					// here we consider the local variable pointing to the object instance
					targetObject = new LocalVarID(curMthSig, faInsnRef, classNameStr);
				}
				else
				{
					targetObject = ClassName.createFromString(classNameStr);
				}
			}
		}
		else
		{
			// possible only when the happens-before analysis is turned off
			targetObject = ClassName.createFromString(classNameStr);
		}

		FieldID targetField = new FieldID(targetObject, fieldNameStr);		
		FieldName targetFieldName = FieldName.createFromString(classNameStr, fieldNameStr);
		
		if (printChoiceDecision)
		{
			System.out.println("[CHOICE DECISION] target field = " + targetField + ", target object = " + targetObject);
		}
		
		AllocationSite curThObjID = ObjectAllocationTracker.getAllocSiteForObject(curTh.getThreadObjectRef());
		
		if (printChoiceDecision)
		{
			if (Debug.isWatchedEntity(curMthSig))
			{
				System.out.println("[CHOICE DECISION] current thread: object reference = " + curTh.getThreadObjectRef() + ", allocation site = " + curThObjID);
			}
		}
		
		List<AllocationSite> curThLocks = JPFUtils.getCurrentlyHeldLocksForThread(curTh);
		
		if (printChoiceDecision)
		{
			if (Debug.isWatchedEntity(curMthSig))
			{			
				System.out.println("[CHOICE DECISION] locks held by the current thread (allocation sites)");
		
				for (AllocationSite as : curThLocks)
				{
					if ( ! Debug.isWatchedEntity(as.toString()) ) continue;
				
					System.out.println("\t " + as.toString());
				}
			}
		}
		
		ThreadInfo[] threadList = curTh.getVM().getThreadList().getThreads();
		
		Map<Integer,List<ProgramPoint>> cacheTh2PPs = new HashMap<Integer,List<ProgramPoint>>();
		
		// get information about possible future allocations
		
		Set<AllocationSite> futureAllocSites = new HashSet<AllocationSite>();
		
		if (Configuration.usingAllocationSites)
		{
			for (int j = 0; j < threadList.length; j++)
			{
				ThreadInfo th = threadList[j];
				
				List<ProgramPoint> thPPs = getCachedProgramPointsForThreadStack(th, cacheTh2PPs, false);
				
				Set<AllocationSite> thAllocSites = getMergedFutureAllocationSites(thPPs);
			
				// safe result (missing analysis data)
				if (thAllocSites == null) return true;
				
				futureAllocSites.addAll(thAllocSites);
			}
				
			if (printResultsFull)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{			
					System.out.println("[FULL RESULTS] future allocation sites for all threads");
		
					for (AllocationSite as : futureAllocSites)
					{
						if ( ! Debug.isWatchedEntity(as.toString()) ) continue;
				
						System.out.println("\t " + as.toString());
					}
				}
			}
		}
		
		// look for possible conflicting field accesses
		
		boolean existsConflictAccess = false;
		
		boolean skipAllThreads = false;
		
		if (Configuration.usingImmutableFields)
		{
			// check results of the immutable fields analysis
				// we can do it here because they are not specific to a single thread
				
			boolean detectedImmField = false;
			
			if (ImmutableFieldsDetector.checkFieldImmutability(targetFieldName))
			{
				// there is no future write access to this field by a thread other than the one that created the encapsulating object

				detectedImmField = true;
				
				// we do not have to check the analysis results for them
				skipAllThreads = true;
			}
			
			if (printChoiceDecision)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{
					System.out.println("[CHOICE DECISION] detected immutable field = " + detectedImmField);					
				}
			}
		}
		
		// check the analysis results for individual threads
		
		for (int i = 0; i < threadList.length; i++)
		{
			ThreadInfo th = threadList[i];
			
			// we do not have to check anything here
			if (skipAllThreads) break;

			// skip current thread
			if (th.getId() == curTh.getId()) continue;

			List<ProgramPoint> thPPs = getCachedProgramPointsForThreadStack(th, cacheTh2PPs, false);
			ProgramPoint thTopPP = JPFUtils.getProgramPointForThreadPC(th, false);
			
			if (printChoiceDecision)
			{
				if (thTopPP != ProgramPoint.INVALID)
				{
					System.out.println("[CHOICE DECISION] thread = " + th.getId() + ", methodSig = " + thTopPP.methodSig + ", insnPos = " + thTopPP.insnPos + ", call stack = " + thPPs);
				}
				else
				{
					System.out.println("[CHOICE DECISION] thread = " + th.getId() + ", empty stack of program points");
				}
			}
			
			if ( ! Configuration.usingFieldAccess ) 
			{
				// we cannot use results of the field access analysis
				// safely assume that there is a conflicting field access
				existsConflictAccess = true;
				
				// we do not have to check other threads
				break;
			}

			// get complete results of the field access analysis and use them
		
			Set<FieldID> futureFieldAccesses = null;
			
			if (Configuration.enabledFACtxSensitive)
			{
				// get merged field accesses for the whole dynamic thread call stack
				futureFieldAccesses = getMergedFutureFieldAccessesForThreadStack(accessType, thPPs, false);
			}
			else if (Configuration.enabledFACalleeCtx || Configuration.enabledFAInsensitive)
			{
				// get field accesses for the current PC (top frame of the call stack)
				futureFieldAccesses = getFutureFieldAccessesForThreadPC(accessType, thTopPP, false);
			}
			
			// safe result (missing analysis data)
			if (futureFieldAccesses == null) return true;
		
			if (printResultsFull)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{			
					System.out.print("[FULL RESULTS] future field ");
					if (accessType == 'r') System.out.print("read");
					if (accessType == 'w') System.out.print("write");
					System.out.println(" accesses for thread " + th.getId());
			
					for (FieldID fid : futureFieldAccesses)
					{
						if ( ! Debug.isWatchedEntity(fid.tgtObjectID.toString()) ) continue;
					
						System.out.println("\t " + fid.toString());
					}
				}
			}
	
			boolean existsFutureAccess = false;
			
			ThreadInfo accessTh = null;
			List<ProgramPoint> accessThPPs = null;
			
			// it does not make sense to use demand-driven pointer analysis for static fields
			if ( (Configuration.enabledPADemandDrv || Configuration.enabledPADemandDrvCtx) && ( ! isStatic ) )
			{
				// for each local variable 'l' pointing to objects of the class 'C' such that 'C.f' is accessed through it (i.e., field access analysis results contain 'l:C.f'), check whether the intersection of points-to set for 'l' with points-to set for 'u' (variable pointing to 'o') is not empty
				// if this holds for some local variable 'l', then the analysis claims that there is a possible future access to 'o.f'
				
				LocalVarID targetFieldLV = (LocalVarID) targetField.tgtObjectID;
				
				for (FieldID fid : futureFieldAccesses)
				{
					if (targetField.fieldName.equals(fid.fieldName))
					{
						if (fid.tgtObjectID instanceof LocalVarID)
						{
							// future access to instance field

							LocalVarID fidLV = (LocalVarID) fid.tgtObjectID;
						
							// compare class names for local variables
							if (targetFieldLV.typeName.equals(fidLV.typeName))
							{
								if (PointerAnalysisData.checkLocalVariableAliasing(targetFieldLV, fidLV)) existsFutureAccess = true; 
							}
						}
						else if (fid.tgtObjectID instanceof ClassName)
						{
							// future access to static field
							
							// conservative safe answer: we cannot use demand-driver pointer analysis for future accesses to static fields
							existsFutureAccess = true;
						}
					}
				}
			}
			else 
			{
				// other cases: no pointer analysis used, exhaustive pointer analysis
				
				if (futureFieldAccesses.contains(targetField)) existsFutureAccess = true;
			}
			
			if (existsFutureAccess)
			{
				accessTh = th;
				accessThPPs = thPPs;
			}
			
			if (printChoiceDecision)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{
					System.out.print("[CHOICE DECISION] thread = " + th.getId() + ", exists future ");
					if (accessType == 'r') System.out.print("read");
					if (accessType == 'w') System.out.print("write");
					System.out.println(" = " + existsFutureAccess);					
				}
			}
			
			// no future access of the given kind to the target field -> continue with another thread
			if ( ! existsFutureAccess ) continue;
			
			// we consider just results of the field access analysis
			existsConflictAccess = existsFutureAccess;
				
			// we do not have to check other threads if we found some conflict already
			if (existsConflictAccess) break;
			else continue;
		}
		
		// we must have a thread choice
		if (existsConflictAccess) 
		{
			if (printChoiceDecision)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{
					System.out.print("[CHOICE DECISION] result: some threads perform conflicting ");
					if (accessType == 'r') System.out.print("read");
					if (accessType == 'w') System.out.print("write");
					System.out.println(" field access -> we must create a new thread choice");
					
					System.out.println("");
				}
			}
			
			return true;
		}
		
		if ( ! existsConflictAccess )
		{
			if (printChoiceDecision)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{
					System.out.print("[CHOICE DECISION] result: other threads do not perform conflicting ");
					if (accessType == 'r') System.out.print("read");
					if (accessType == 'w') System.out.print("write");
					System.out.println(" field access -> no thread choice needed");
					
					System.out.println("");
				}
			}
			
			return false;
		}
		
		// safe default
		return true;
	}
	
	
	public static boolean existsConflictingArrayRead(ThreadInfo curTh, String curMthSig, int curInsnPos, String arrayClassName, int arrayObjRef, int arrayDimensions, int dynElementIndex)
	{
		return existsConflictingArrayAccess('r', curTh, curMthSig, curInsnPos, arrayClassName, arrayObjRef, arrayDimensions, dynElementIndex);
	}
	
	public static boolean existsConflictingArrayWrite(ThreadInfo curTh, String curMthSig, int curInsnPos, String arrayClassName, int arrayObjRef, int arrayDimensions, int dynElementIndex)
	{
		return existsConflictingArrayAccess('w', curTh, curMthSig, curInsnPos, arrayClassName, arrayObjRef, arrayDimensions, dynElementIndex);
	}
	
	// look for conflicts with the given access to the array object and element <arrayClassName/arrayObjRef/dynElementIndex> at the program point <curMthSig>:<curInsnPos> in the thread <curTh>
	private static boolean existsConflictingArrayAccess(char accessType, ThreadInfo curTh, String curMthSig, int curInsnPos, String arrayClassName, int arrayObjRef, int arrayDimensions, int dynElementIndex)
	{
		if (printChoiceDecision)
		{
			if (Debug.isWatchedEntity(curMthSig))
			{			
				System.out.println("[CHOICE DECISION] new query (array access): curTh = " + curTh.getId() + ", curMthSig = " + curMthSig + ", curInsnPos = " + curInsnPos + ", arrayClassName = " + arrayClassName + ", arrayObjRef = " + arrayObjRef + ", arrayDimensions = " + arrayDimensions + ", dynElementIndex = " + dynElementIndex + ", access type = " + accessType);
			}
		}

		ObjectID targetObject = null;
		
		if (Configuration.usingPointerAlias)
		{
			// there cannot be a static array object (unlike for fields)
			
			targetObject = ObjectAllocationTracker.getAllocSiteForObject(arrayObjRef);
		}
		else
		{			
			targetObject = ClassName.createFromString(arrayClassName);
		}
		
		ArrayID targetArrayObject = new ArrayID(targetObject, arrayDimensions);
		
		if (printChoiceDecision)
		{
			System.out.println("[CHOICE DECISION] target array = " + targetArrayObject);
		}
		
		ProgramPoint currentAccessPP = new ProgramPoint(curMthSig, curInsnPos);
		
		ThreadInfo[] threadList = curTh.getVM().getThreadList().getThreads();
		
		// get information about possible future allocations
		
		Set<AllocationSite> futureAllocSites = new HashSet<AllocationSite>();
		
		if (Configuration.usingAllocationSites)
		{
			for (int j = 0; j < threadList.length; j++)
			{
				ThreadInfo th = threadList[j];
				
				List<ProgramPoint> thPPs = JPFUtils.getProgramPointsForThreadStack(th, false);
				
				Set<AllocationSite> thAllocSites = getMergedFutureAllocationSites(thPPs);
			
				// safe result (missing analysis data)
				if (thAllocSites == null) return true;
				
				futureAllocSites.addAll(thAllocSites);
			}
				
			if (printResultsFull)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{			
					System.out.println("[FULL RESULTS] future allocation sites for all threads");
		
					for (AllocationSite as : futureAllocSites)
					{
						if ( ! Debug.isWatchedEntity(as.toString()) ) continue;
				
						System.out.println("\t " + as.toString());
					}
				}
			}
		}
		
		// look for possible conflicting array accesses
		
		boolean existsConflictAccess = false;
		
		for (int i = 0; i < threadList.length; i++)
		{
			ThreadInfo th = threadList[i];

			// skip current thread
			if (th.getId() == curTh.getId()) continue;

			List<ProgramPoint> thPPs = JPFUtils.getProgramPointsForThreadStack(th, false);
			
			if (printChoiceDecision)
			{
				if (thPPs.size() > 0)
				{
					ProgramPoint thTopPP = thPPs.get(0);
				
					System.out.println("[CHOICE DECISION] thread = " + th.getId() + ", methodSig = " + thTopPP.methodSig + ", insnPos = " + thTopPP.insnPos + ", call stack = " + thPPs);
				}
				else
				{
					System.out.println("[CHOICE DECISION] thread = " + th.getId() + ", empty stack of program points");
				} 
			}
			
			if ( ! Configuration.usingArrayAccess )
			{
				// we cannot use results of the array object access analysis
				// safely assume that there is a conflicting array access
				existsConflictAccess = true;
				
				// we do not have to check other threads
				break;
			}

			// get complete results of the array object access analysis and use them
		
			Set<ArrayID> futureArrayAccesses = getMergedFutureArrayObjectAccesses(accessType, thPPs, false);
					
			// safe result (missing analysis data)
			if (futureArrayAccesses == null) return true;
		
			if (printResultsFull)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{			
					System.out.print("[FULL RESULTS] future array ");
					if (accessType == 'r') System.out.print("read");
					if (accessType == 'w') System.out.print("write");
					System.out.println(" accesses for thread " + th.getId());
			
					for (ArrayID aid : futureArrayAccesses)
					{
						if ( ! Debug.isWatchedEntity(aid.heapObjectID.toString()) ) continue;
					
						System.out.println("\t " + aid.toString());
					}
				}
			}
	
			boolean existsFutureAccess = false;
			
			ThreadInfo accessTh = null;
			List<ProgramPoint> accessThPPs = null;
			
			if (futureArrayAccesses.contains(targetArrayObject))
			{
				existsFutureAccess = true;
				accessTh = th;
				accessThPPs = thPPs;
			}
			
			if (printChoiceDecision)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{
					System.out.print("[CHOICE DECISION] thread = " + th.getId() + ", exists future ");
					if (accessType == 'r') System.out.print("read");
					if (accessType == 'w') System.out.print("write");
					System.out.println(" = " + existsFutureAccess);					
				}
			}
			
			// no future access of the given kind to the target array object -> continue with another thread
			if ( ! existsFutureAccess ) continue;
			
			if ( ! Configuration.usingArrayElements ) 
			{
				// we cannot use results of the array elements access analyses
				// we consider just results of the array object access analysis
				existsConflictAccess = existsFutureAccess;
				
				// we do not have to check other threads if we found some conflict already
				if (existsConflictAccess) break;
				else continue;
			}

			// we analyze the specific array element indexes only for array objects that may be accessed from some other thread
			
			// step 1: get complete results of the array element access analysis and use them
				// it computes the set of possibly conflicting future accesses to array elements (read or write) for the given program point

			Set<ArrayElementAccess> futureArrayElementAccesses = getMergedFutureArrayElementAccesses(accessType, accessThPPs);
					
			// safe result (missing analysis data)
			if (futureArrayElementAccesses == null) return true;
		
			if (printResultsFull)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{			
					System.out.print("[FULL RESULTS] future array element ");
					if (accessType == 'r') System.out.print("read");
					if (accessType == 'w') System.out.print("write");
					System.out.println(" accesses for thread " + th.getId());
			
					for (ArrayElementAccess aea : futureArrayElementAccesses)
					{
						if ( ! Debug.isWatchedEntity(aea.progPoint.methodSig) ) continue;
					
						System.out.println("\t " + aea.toString());
					}
				}
			}
			
			// get the symbolic element index used in the current array access (next instruction of the current thread)
			ArrayElementIndex currentAccessSymbIndex = SymbolicArrayElementIndexAnalysis.getSymbolicElementIndexForProgramPoint(currentAccessPP);
			
			if (printChoiceDecision)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{
					System.out.println("[CHOICE DECISION] symbolic element index used for the current access to target array: " + currentAccessSymbIndex);
				}
			}
			
			// extract future conflicting accesses to "targetArray" and get the symbolic element indexes used in the respective accesses

			Set<ArrayElementAccess> futureAccessesTargetArray = new HashSet<ArrayElementAccess>();
			
			Map<ArrayElementAccess, ArrayElementIndex> futureElementAccess2SymbolicIndex = new HashMap<ArrayElementAccess, ArrayElementIndex>();
			
			for (ArrayElementAccess elementAccess : futureArrayElementAccesses)
			{
				if (elementAccess.targetArray.equals(targetArrayObject))
				{
					futureAccessesTargetArray.add(elementAccess);
					
					ArrayElementIndex symbIndex = SymbolicArrayElementIndexAnalysis.getSymbolicElementIndexForProgramPoint(elementAccess.progPoint);
					
					futureElementAccess2SymbolicIndex.put(elementAccess, symbIndex);
				}
			}
			
			if (printChoiceDecision)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{					
					System.out.print("[CHOICE DECISION] symbolic element indexes used in future ");
					if (accessType == 'r') System.out.print("read");
					if (accessType == 'w') System.out.print("write");
					System.out.println(" accesses to target array");
					
					for (ArrayElementAccess aea : futureAccessesTargetArray)
					{
						if ( ! Debug.isWatchedEntity(aea.progPoint.methodSig) ) continue;
						
						ArrayElementIndex sidx = futureElementAccess2SymbolicIndex.get(aea);
					
						System.out.println("\t " + aea.progPoint.toString() + " # " + sidx);
					}
				}
			}
			
			// step 2: inspect symbolic values of array element indexes
			
			// get results of the field access analysis (only writes) for the current program point of the given other thread (in the current dynamic state)
				
			Set<FieldID> futureFieldWritesAccessTh = getMergedFutureFieldAccessesForThreadStack('w', accessThPPs, false);
				
			if (printResultsFull)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{			
					System.out.println("[FULL RESULTS] future field write accesses for thread " + accessTh.getId());
			
					for (FieldID fid : futureFieldWritesAccessTh)
					{
						if ( ! Debug.isWatchedEntity(fid.tgtObjectID.toString()) ) continue;
					
						System.out.println("\t " + fid.toString());
					}
				}
			}
			
			// indexes to this list correspond to stack frame offsets (top is 0) 
			List<Set<LocalVarName>> futureLocalVarWritesAccessThPPs = null;
			
			Set<String> futureMethodCallsAccessTh = null;
			
			if (Configuration.usingLocalVarAccess)
			{
				// get results of the local variable access analysis (writes) for the current program point in each stack frame (corresponding method) of the conflicting thread (in the current dynamic state)
				
				futureLocalVarWritesAccessThPPs = new ArrayList<Set<LocalVarName>>();
				
				for (int j = 0; j < accessThPPs.size(); j++)
				{
					Set<LocalVarName> pp2FutureLocalVarWrites = getFutureLocalVariableAccesses('w', accessThPPs.get(j));
					
					futureLocalVarWritesAccessThPPs.add(pp2FutureLocalVarWrites);
				}
				
				if (printResultsFull)
				{
					if (Debug.isWatchedEntity(curMthSig))
					{			
						System.out.println("[FULL RESULTS] future local variable write accesses for the current method of thread " + accessTh.getId());
						
						for (int j = 0; j < futureLocalVarWritesAccessThPPs.size(); j++)
						{
							Set<LocalVarName> pp2FutureLocalVarWrites = futureLocalVarWritesAccessThPPs.get(j);
							
							System.out.println("\t stack frame: " + j);
							
							for (LocalVarName lv : pp2FutureLocalVarWrites)
							{
								System.out.println("\t\t " + lv.toString());
							}
						}
					}
				}
				
				if (Configuration.usingMethodCalls)
				{
					// get results of the method calls analysis for the current program point in the current method of the conflicting thread (in the current dynamic state)
						
					futureMethodCallsAccessTh = getMergedFutureMethodCalls(accessThPPs);
						
					if (printResultsFull)
					{
						if (Debug.isWatchedEntity(curMthSig))
						{			
							System.out.println("[FULL RESULTS] future method calls for the current method of thread " + accessTh.getId());
					
							for (String mthSig : futureMethodCallsAccessTh)
							{
								if ( ! Debug.isWatchedEntity(mthSig) ) continue;
								
								System.out.println("\t " + mthSig);
							}
						}
					}
				}
			}

			boolean existsArrayIndexWithSameConstant = false;
			
			boolean existsArrayIndexWithUpdatedField = false;
			
			boolean existsArrayIndexWithUpdatedLocalVar = false;
			
			// inspect future conflicting accesses together with associated symbolic element indexes
			for (ArrayElementAccess aea : futureAccessesTargetArray)
			{
				ArrayElementIndex symbIndex = futureElementAccess2SymbolicIndex.get(aea);
				
				String symbIndexMthSig = aea.progPoint.methodSig;
				
				// check whether the given symbolic index corresponds to a numeric constant that is equal to the dynamic index used by the current active thread in the next access (instruction)
				if (symbIndex.isNumericConstant())
				{
					if (dynElementIndex == symbIndex.getIntegerValue().intValue()) existsArrayIndexWithSameConstant = true;

					if (printChoiceDecision)
					{
						if (Debug.isWatchedEntity(curMthSig))
						{
							System.out.println("[CHOICE DECISION] thread = " + accessTh.getId() + ", symbolic index = " + symbIndex + ", exists array index with same constant = " + existsArrayIndexWithSameConstant);
						}
					}
					
					// we do not have to check future field updates and local variables in this case
					continue;
				}
				
				// check whether the symbolic index contains some field possibly updated in the future
					// this also includes possible future allocations of heap objects because constructors update fields 
				
				boolean existsUpdatedField = false;
				
				if (printResultsFull)
				{
					if (Debug.isWatchedEntity(curMthSig))
					{			
						System.out.println("[FULL RESULTS] fields accessed in the symbolic index \"" + symbIndex + "\"");
				
						for (FieldID fid : symbIndex.allFields)
						{
							if ( ! Debug.isWatchedEntity(fid.tgtObjectID.toString()) ) continue;
						
							System.out.println("\t " + fid.toString());
						}
					}
				}
				
				for (FieldID fid : symbIndex.allFields)
				{
					if (futureFieldWritesAccessTh.contains(fid)) existsUpdatedField = true;
				}
				
				for (AllocationSite as : futureAllocSites)
				{
					ObjectID futureHeapObjID = null;
					
					if (Configuration.usingPointerAlias)
					{
						futureHeapObjID = as;
					}
					else
					{
						// get class (type) of heap objects allocated at the given site						
						futureHeapObjID = WALAUtils.getAllocatedObjectClassName(as.progPoint);
					}
						
					for (FieldID fid : symbIndex.allFields)
					{
						if (fid.tgtObjectID.equals(futureHeapObjID)) existsUpdatedField = true;
					}
				}
				
				if (existsUpdatedField) existsArrayIndexWithUpdatedField = true;
				
				// optionally check whether the symbolic index contains some local variables of methods already on the call stack that are possibly updated in the future during program execution
					// this includes possible future calls of the currently executing methods (on the call stack) in the conflicting thread

				boolean existsUpdatedLocalVar = false;
				
				if (Configuration.usingLocalVarAccess)
				{
					if (printResultsFull)
					{
						if (Debug.isWatchedEntity(curMthSig))
						{			
							System.out.println("[FULL RESULTS] local variables accessed in the symbolic index \"" + symbIndex + "\"");
					
							for (LocalVarName lv : symbIndex.localVariables)
							{
								System.out.println("\t " + lv.toString());
							}
						}
					}
					
					for (LocalVarName lv : symbIndex.localVariables)
					{
						for (int j = 0; j < futureLocalVarWritesAccessThPPs.size(); j++)
						{
							String ppMthSig = accessThPPs.get(j).methodSig;
							
							if (symbIndexMthSig.equals(ppMthSig))
							{
								Set<LocalVarName> pp2FutureLocalVarWrites = futureLocalVarWritesAccessThPPs.get(j);
															
								// some local variable may be possibly updated
								if (pp2FutureLocalVarWrites.contains(lv)) existsUpdatedLocalVar = true;
							}
						}
					}
					
					if (Configuration.usingMethodCalls)
					{
						if (symbIndex.containsLocalVariables())
						{
							// there may occur some future call to the given method (which may update some local variable)
							if (futureMethodCallsAccessTh.contains(symbIndexMthSig)) existsUpdatedLocalVar = true;
						}
					}
					else
					{
						if (symbIndex.containsLocalVariables())
						{
							// default (future method call is possible)
							existsUpdatedLocalVar = true;
						}
					}
				}
				else
				{
					if (symbIndex.containsLocalVariables())
					{
						// default (local variable can be updated later)
						existsUpdatedLocalVar = true;
					}
				}
				
				if (existsUpdatedLocalVar) existsArrayIndexWithUpdatedLocalVar = true;
				
				// if the symbolic index does not contain fields updated in future and does not contain updated local variables (in any method on the dynamic call stack) then we can use its current dynamic value 
										
				if ( ( ! existsUpdatedField ) && ( ! existsUpdatedLocalVar ) )
				{
					// check feasibility of symbolic array element index
						// we consider only field access paths that start either in the local variable "this" (for any method on the dynamic call stack of the given thread) or in any static field
						// in case of local variables, we consider only symbolic indexes associated with methods already on the dynamic call stack of "accessTh"
						// method associated with the symbolic index cannot appear twice on the dynamic call stack

					// set to "false" when the symbolic array element index is not just arithmetic expression over field access paths, local variables of some method on the dynamic call stack, and integer constants
					boolean feasibleSymbIndex = true;
					
					// set to "true" if the symbolic array element index contains a field access path with a local variable (as the root)
					boolean existsFAPwithLocalVar = false;

					for (FieldAccessPath fap : symbIndex.fieldPaths)
					{
						if (fap.rootObjectID instanceof LocalVarName)
						{
							LocalVarName lvName = (LocalVarName) fap.rootObjectID;
							
							String lvNameStr = lvName.getAsString();
							
							// local variable "this" has the index 0
							if ( ! lvNameStr.equals("local0") ) feasibleSymbIndex = false;

							existsFAPwithLocalVar = true;
						}
					}
					
					// set to "true" if the symbolic array element index belongs to a method that is not already on the dynamic call stack					
					boolean symbIndexForNonStackMethod = true;
					
					// how many times the method appears on the dynamic call stack
					int symbIndexMthCount = 0;
										
					for (ProgramPoint pp : accessThPPs)
					{
						if (symbIndexMthSig.equals(pp.methodSig))
						{
							symbIndexForNonStackMethod = false;
							symbIndexMthCount++;
						}
					}
					
					// if the symbolic index belongs to a method that is not on the dynamic call stack (of some thread) then 
						// the local variable "this" in field access paths may not point to the same heap object as in the current state
						// symbolic index may contain local variables whose values are not available from the dynamic state (call stack)
					// it does not make sense to compute and use the current dynamic value of the field access paths (starting with a local variable) and current dynamic values of local variables in that case
					// we can safely use the dynamic values of static fields (array element indexes that contain static fields and constants) in this case
					// dynamic value of the symbolic index cannot be used also when the associated method appears twice on the call stack 
					if ( (symbIndex.containsLocalVariables() || existsFAPwithLocalVar) && (symbIndexForNonStackMethod  || (symbIndexMthCount > 1)) ) feasibleSymbIndex = false;
					
					// get current dynamic value of the array element index from the dynamic program state

					// the value "null" is produced for infeasible symbolic array element index	(when the field access path starts with a different local variable than "this", symbolic index associated with a method that is not on the call stack, no field access path, etc)
					
					Integer symbIndexValue = null;
					
					if (feasibleSymbIndex)
					{
						symbIndexValue = computeDynamicValueForSymbolicIndex(symbIndex, symbIndexMthSig, accessTh);
					}
					
					if (printChoiceDecision)
					{
						if (Debug.isWatchedEntity(curMthSig))
						{
							System.out.println("[CHOICE DECISION] symbolic index = " + symbIndex + ", dynamic value = " + symbIndexValue);
						}
					}
					
					if (( ! feasibleSymbIndex ) || (symbIndexValue == null) || (dynElementIndex == symbIndexValue.intValue())) existsArrayIndexWithSameConstant = true;
				}
				
				if (printChoiceDecision)
				{
					if (Debug.isWatchedEntity(curMthSig))
					{
						System.out.println("[CHOICE DECISION] thread = " + accessTh.getId() + ", symbolic index = " + symbIndex + ", exists array index with updated field = " + existsArrayIndexWithUpdatedField + ", exists array index with updated local variable = " + existsArrayIndexWithUpdatedLocalVar + ", exists array index with same constant = " + existsArrayIndexWithSameConstant);
					}
				}
			}

			// use results the step 2
			
			// we have a possibly conflicting access to array object
			// access to the target array element may be performed over the same concrete index value on some control flow path			
			if (existsArrayIndexWithSameConstant || existsArrayIndexWithUpdatedField || existsArrayIndexWithUpdatedLocalVar) existsConflictAccess = true;
			
			// we do not have to check other threads if we found some conflict already
			if (existsConflictAccess) break;
		}
		
		// we must have a thread choice
		if (existsConflictAccess)
		{
			if (printChoiceDecision)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{
					System.out.print("[CHOICE DECISION] result: some threads perform conflicting ");
					if (accessType == 'r') System.out.print("read");
					if (accessType == 'w') System.out.print("write");
					System.out.println(" array access -> we must create a new thread choice");
					
					System.out.println("");
				}
			}
			
			return true;
		}
		
		if ( ! existsConflictAccess )
		{
			if (printChoiceDecision)
			{
				if (Debug.isWatchedEntity(curMthSig))
				{
					System.out.print("[CHOICE DECISION] result: other threads do not perform conflicting ");
					if (accessType == 'r') System.out.print("read");
					if (accessType == 'w') System.out.print("write");
					System.out.println(" array access -> no thread choice needed");
					
					System.out.println("");
				}
			}
			
			return false;
		}
		
		// safe default
		return true;
	}

	public static List<ThreadInfo> getThreadsPerformingConflictFieldAccess(ThreadInfo[] allThreads, Set<FieldID> targetFieldsRead, Set<FieldID> targetFieldsWrite, boolean excludeChildThreads)
	{
		List<ThreadInfo> conflictingThreads = new ArrayList<ThreadInfo>();
		
		// process analysis results for individual threads
		
		for (int i = 0; i < allThreads.length; i++)
		{
			ThreadInfo th = allThreads[i];
			
			List<ProgramPoint> thPPs = JPFUtils.getProgramPointsForThreadStack(th, excludeChildThreads);

			if ( ! Configuration.usingFieldAccess ) 
			{
				// we cannot use results of the field access analysis
				// safely assume that there is a conflicting field access
				conflictingThreads.add(th);
				
				continue;
			}

			// we assume that the context-sensitive field access analysis is enabled here

			// get merged field accesses (i.e. complete results) for the whole dynamic thread call stack
			// we want results that exclude child threads when it is allowed
			Set<FieldID> futureFieldReadAccesses = getMergedFutureFieldAccessesForThreadStack('r', thPPs, excludeChildThreads);
			Set<FieldID> futureFieldWriteAccesses = getMergedFutureFieldAccessesForThreadStack('w', thPPs, excludeChildThreads);
			
			// safe result (missing analysis data)
			if ((futureFieldReadAccesses == null) || (futureFieldWriteAccesses == null))
			{
				conflictingThreads.add(th);
				continue;
			}
			
			if (printResultsFull)
			{
				System.out.println("[FULL RESULTS] future field read accesses for thread " + th.getId());			
				for (FieldID fid : futureFieldReadAccesses)
				{
					if ( ! Debug.isWatchedEntity(fid.tgtObjectID.toString()) ) continue;
					System.out.println("\t " + fid.toString());
				}
				
				System.out.println("[FULL RESULTS] future field write accesses for thread " + th.getId());			
				for (FieldID fid : futureFieldWriteAccesses)
				{
					if ( ! Debug.isWatchedEntity(fid.tgtObjectID.toString()) ) continue;
					System.out.println("\t " + fid.toString());
				}
			}
		
			boolean existsConflictingFutureAccess = false;
			
			for (FieldID tgtFR : targetFieldsRead)
			{
				if (futureFieldWriteAccesses.contains(tgtFR)) existsConflictingFutureAccess = true;
				
				if (existsConflictingFutureAccess) break;
			}
			
			for (FieldID tgtFW : targetFieldsWrite)
			{
				if (futureFieldReadAccesses.contains(tgtFW)) existsConflictingFutureAccess = true;
				
				if (existsConflictingFutureAccess) break;
			}
			
			if (existsConflictingFutureAccess)
			{
				conflictingThreads.add(th);
			}
		}
		
		return conflictingThreads;
	}

	public static List<ThreadInfo> getThreadsPerformingConflictArrayAccess(ThreadInfo[] allThreads, Set<ArrayID> targetArraysRead, Set<ArrayID> targetArraysWrite, boolean excludeChildThreads)
	{
		List<ThreadInfo> conflictingThreads = new ArrayList<ThreadInfo>();
		
		// process analysis results for individual threads
		
		for (int i = 0; i < allThreads.length; i++)
		{
			ThreadInfo th = allThreads[i];
			
			List<ProgramPoint> thPPs = JPFUtils.getProgramPointsForThreadStack(th, excludeChildThreads);
			
			if ( ! Configuration.usingArrayAccess ) 
			{
				// we cannot use results of the array object access analysis
				// safely assume that there may be a conflicting array access
				conflictingThreads.add(th);
				
				continue;
			}

			// get merged array object accesses (i.e. complete results) for the whole dynamic thread call stack
			// we want results that exclude child threads when it is allowed
			Set<ArrayID> futureArrayReadAccesses = getMergedFutureArrayObjectAccesses('r', thPPs, excludeChildThreads);
			Set<ArrayID> futureArrayWriteAccesses = getMergedFutureArrayObjectAccesses('w', thPPs, excludeChildThreads);

			// safe result (missing analysis data)
			if ((futureArrayReadAccesses == null) || (futureArrayWriteAccesses == null))
			{
				conflictingThreads.add(th);
				continue;
			}

			if (printResultsFull)
			{
				System.out.println("[FULL RESULTS] future array read accesses for thread " + th.getId());			
				for (ArrayID aid : futureArrayReadAccesses)
				{
					if ( ! Debug.isWatchedEntity(aid.heapObjectID.toString()) ) continue;
					System.out.println("\t " + aid.toString());
				}
				
				System.out.println("[FULL RESULTS] future array write accesses for thread " + th.getId());			
				for (ArrayID aid : futureArrayWriteAccesses)
				{
					if ( ! Debug.isWatchedEntity(aid.heapObjectID.toString()) ) continue;
					System.out.println("\t " + aid.toString());
				}
			}
			
			boolean existsConflictingFutureAccess = false;
			
			for (ArrayID tgtAR : targetArraysRead)
			{
				if (futureArrayWriteAccesses.contains(tgtAR)) existsConflictingFutureAccess = true;
				
				if (existsConflictingFutureAccess) break;
			}
			
			for (ArrayID tgtAW : targetArraysWrite)
			{
				if (futureArrayReadAccesses.contains(tgtAW)) existsConflictingFutureAccess = true;
				
				if (existsConflictingFutureAccess) break;
			}
			
			if (existsConflictingFutureAccess)
			{
				conflictingThreads.add(th);
			}
		}
		
		return conflictingThreads;
	}
	

	private static List<ProgramPoint> getCachedProgramPointsForThreadStack(ThreadInfo th, Map<Integer,List<ProgramPoint>> cacheTh2PPs, boolean excludeChildThreads)
	{
		List<ProgramPoint> thPPs = cacheTh2PPs.get(th.getId());
		
		if (thPPs == null)
		{
			thPPs = JPFUtils.getProgramPointsForThreadStack(th, excludeChildThreads);
			
			cacheTh2PPs.put(th.getId(), thPPs);
		}
		
		return thPPs;
	}
	

	private static Set<FieldID> getFutureFieldAccessesForProgPoint(char accessType, ProgramPoint pp, boolean excludeChildThreads)
	{
		Set<FieldID> ppFieldAccesses = null;

		// empty set
		if (pp == ProgramPoint.INVALID) return new HashSet<FieldID>();
				
		if (accessType == 'r') ppFieldAccesses = FieldAccessAnalysisBase.getFieldReadsForProgramPoint(pp, excludeChildThreads);
		if (accessType == 'w') ppFieldAccesses = FieldAccessAnalysisBase.getFieldWritesForProgramPoint(pp, excludeChildThreads);
						
		// conservative solution: program point was not analyzed, so we return a safe value by default
		if (ppFieldAccesses == null) 
		{
			// handling ThreadGroup.add()
			if (WALAUtils.isAbstractedMethod(pp.methodSig)) return new HashSet<FieldID>();
				
			if (printResultsFull) System.out.println("[FULL RESULTS] no data available for the program point " + pp);
				
			return null;
		}
			
		return ppFieldAccesses;
	}
	
	private static Set<FieldID> getFutureFieldAccessesForThreadPC(char accessType, ProgramPoint thTopPP, boolean excludeChildThreads)
	{
		return getFutureFieldAccessesForProgPoint(accessType, thTopPP, excludeChildThreads);
	}
	
	private static Set<FieldID> getMergedFutureFieldAccessesForThreadStack(char accessType, List<ProgramPoint> thProgPoints, boolean excludeChildThreads)
	{
		Set<FieldID> fieldAccesses = new HashSet<FieldID>();

		for (int i = 0; i < thProgPoints.size(); i++)
		{
			ProgramPoint pp = thProgPoints.get(i);
			if (pp == ProgramPoint.INVALID) continue;
			
			Set<FieldID> ppFieldAccesses = getFutureFieldAccessesForProgPoint(accessType, pp, excludeChildThreads);
			
			// conservative solution: program point was not analyzed, so we return a safe value by default
			if (ppFieldAccesses == null) return null;
			
			fieldAccesses.addAll(ppFieldAccesses);
		}
		
		return fieldAccesses;
	}
	
	private static Set<AllocationSite> getMergedFutureAllocationSites(List<ProgramPoint> thProgPoints)
	{
		Set<AllocationSite> allocSites = new HashSet<AllocationSite>();

		for (int i = 0; i < thProgPoints.size(); i++)
		{
			ProgramPoint pp = thProgPoints.get(i);
			if (pp == ProgramPoint.INVALID) continue;
				
			Set<AllocationSite> ppAllocSites = AllocationSitesAnalysis.getAllocationSitesForProgramPoint(pp);
			
			// conservative solution: program point was not analyzed, so we return a safe value by default
			if (ppAllocSites == null) 
			{
				// handling ThreadGroup.add()
				if (WALAUtils.isAbstractedMethod(pp.methodSig)) return new HashSet<AllocationSite>();
			
				if (printResultsFull) System.out.println("[FULL RESULTS] no data available for the program point " + pp);
				
				return null;
			}
			
			allocSites.addAll(ppAllocSites);
		}
		
		return allocSites;
	}

	private static Set<ArrayID> getMergedFutureArrayObjectAccesses(char accessType, List<ProgramPoint> thProgPoints, boolean excludeChildThreads)
	{
		Set<ArrayID> arrayAccesses = new HashSet<ArrayID>();

		for (int i = 0; i < thProgPoints.size(); i++)
		{
			ProgramPoint pp = thProgPoints.get(i);
			if (pp == ProgramPoint.INVALID) continue;
				
			Set<ArrayID> ppArrayAccesses = null;
			if (accessType == 'r') ppArrayAccesses = ArrayObjectAccessAnalysis.getArrayReadsForProgramPoint(pp, excludeChildThreads);
			if (accessType == 'w') ppArrayAccesses = ArrayObjectAccessAnalysis.getArrayWritesForProgramPoint(pp, excludeChildThreads);
						
			// conservative solution: program point was not analyzed, so we return a safe value by default
			if (ppArrayAccesses == null) 
			{
				// handling ThreadGroup.add()
				if (WALAUtils.isAbstractedMethod(pp.methodSig)) return new HashSet<ArrayID>();
				
				if (printResultsFull) System.out.println("[FULL RESULTS] no data available for the program point " + pp);
				
				return null;
			}
			
			arrayAccesses.addAll(ppArrayAccesses);
		}
		
		return arrayAccesses;
	}
	
	private static Set<ArrayElementAccess> getMergedFutureArrayElementAccesses(char accessType, List<ProgramPoint> thProgPoints)
	{
		Set<ArrayElementAccess> elementAccesses = new HashSet<ArrayElementAccess>();

		for (int i = 0; i < thProgPoints.size(); i++)
		{
			ProgramPoint pp = thProgPoints.get(i);
			if (pp == ProgramPoint.INVALID) continue;
				
			Set<ArrayElementAccess> ppElementAccesses = null;
			if (accessType == 'r') ppElementAccesses = ArrayElementAccessAnalysis.getArrayElementReadsForProgramPoint(pp);
			if (accessType == 'w') ppElementAccesses = ArrayElementAccessAnalysis.getArrayElementWritesForProgramPoint(pp);
						
			// conservative solution: program point was not analyzed, so we return a safe value by default
			if (ppElementAccesses == null) 
			{
				// handling ThreadGroup.add()
				if (WALAUtils.isAbstractedMethod(pp.methodSig)) return new HashSet<ArrayElementAccess>();
				
				if (printResultsFull) System.out.println("[FULL RESULTS] no data available for the program point " + pp);
				
				return null;
			}
			
			elementAccesses.addAll(ppElementAccesses);
		}
		
		return elementAccesses;
	}
	
	private static Set<LocalVarName> getFutureLocalVariableAccesses(char accessType, ProgramPoint pp)
	{
		Set<LocalVarName> ppLocalVarNames = null;
		
		if (accessType == 'r') ppLocalVarNames = LocalVariableAccessAnalysis.getLocalVarReadsForProgramPoint(pp);
		if (accessType == 'w') ppLocalVarNames = LocalVariableAccessAnalysis.getLocalVarWritesForProgramPoint(pp);
		
		// conservative solution: program point was not analyzed, so we return a safe value by default
		if (ppLocalVarNames == null) 
		{
			// handling ThreadGroup.add()
			if (WALAUtils.isAbstractedMethod(pp.methodSig)) return new HashSet<LocalVarName>();
				
			if (printResultsFull) System.out.println("[FULL RESULTS] no data available for the program point " + pp);
			
			return null;
		}
			
		return ppLocalVarNames;
	}
	
	private static Set<String> getMergedFutureMethodCalls(List<ProgramPoint> thProgPoints)
	{
		Set<String> methodCalls = new HashSet<String>();

		for (int i = 0; i < thProgPoints.size(); i++)
		{
			ProgramPoint pp = thProgPoints.get(i);
			if (pp == ProgramPoint.INVALID) continue;
				
			Set<String> ppMethodCalls = MethodCallsAnalysis.getMethodCallsForProgramPoint(pp);
						
			// conservative solution: program point was not analyzed, so we return a safe value by default
			if (ppMethodCalls == null) 
			{
				// handling ThreadGroup.add()
				if (WALAUtils.isAbstractedMethod(pp.methodSig)) return new HashSet<String>();
				
				if (printResultsFull) System.out.println("[FULL RESULTS] no data available for the program point " + pp);
				
				return null;
			}
			
			methodCalls.addAll(ppMethodCalls);
		}
		
		return methodCalls;
	}
	
	private static Integer computeDynamicValueForSymbolicIndex(ArrayElementIndex symbIndex, String symbIndexMthSig, ThreadInfo accessTh)
	{
		// process arithmetic expression in the suffix notation (reverse polish notation)
		// returns "null" if it contains some unsupported element (e.g., array access)
		
		Integer result = null;
		
		// get the first value (it must be there)
		
		String firstValueStr = symbIndex.exprSN.get(0);
		
		if (firstValueStr.charAt(0) == 'U')
		{
			// unknown index (e.g., method return value)
			
			result = null;
		}
		else if (firstValueStr.charAt(0) == 'L')
		{
			// local variable
			
			int lvNum = Integer.parseInt(firstValueStr.substring(1));
			
			LocalVarName lv = symbIndex.localVariables.get(lvNum - 1);
			
			result = JPFUtils.getCurrentValueLocalVariable(accessTh, symbIndexMthSig, lv);
		}
		else if (firstValueStr.charAt(0) == 'F')
		{
			// field access path
			
			int fapNum = Integer.parseInt(firstValueStr.substring(1));
			
			FieldAccessPath fap = symbIndex.fieldPaths.get(fapNum - 1);
			
			result = JPFUtils.getCurrentValueFieldAccessPath(accessTh, symbIndexMthSig, fap);
		}
		else
		{
			// integer constant
			
			result = Integer.parseInt(firstValueStr);
		}
		
		if (result == null) return null;
		
		// evaluate individual operators
		
		int exprPos = 1;
		while (exprPos < symbIndex.exprSN.size())
		{
			String nextValueStr = symbIndex.exprSN.get(exprPos);
			
			Integer nextValue = null;
			
			if (nextValueStr.charAt(0) == 'U')
			{
				nextValue = null;
			}
			else if (nextValueStr.charAt(0) == 'L')
			{
				int lvNum = Integer.parseInt(firstValueStr.substring(1));
			
				LocalVarName lv = symbIndex.localVariables.get(lvNum - 1);
			
				nextValue = JPFUtils.getCurrentValueLocalVariable(accessTh, symbIndexMthSig, lv);
			}
			else if (nextValueStr.charAt(0) == 'F')
			{
				int fapNum = Integer.parseInt(nextValueStr.substring(1));
				
				FieldAccessPath fap = symbIndex.fieldPaths.get(fapNum - 1);
				
				nextValue = JPFUtils.getCurrentValueFieldAccessPath(accessTh, symbIndexMthSig, fap);
			}
			else
			{
				nextValue = Integer.parseInt(nextValueStr);
			}
			
			if (nextValue == null) return null;
			
			exprPos++;
			
			String operator = symbIndex.exprSN.get(exprPos);
			
			if (operator.equals("+")) result = new Integer(result.intValue() + nextValue.intValue());
			if (operator.equals("-")) result = new Integer(result.intValue() - nextValue.intValue());
			if (operator.equals("*")) result = new Integer(result.intValue() * nextValue.intValue());
			if (operator.equals("/")) result = new Integer(result.intValue() / nextValue.intValue());
			if (operator.equals("%")) result = new Integer(result.intValue() % nextValue.intValue());
			if (operator.equals("<<")) result = new Integer(result.intValue() << nextValue.intValue());
			if (operator.equals(">>")) result = new Integer(result.intValue() >> nextValue.intValue());
			if (operator.equals("&")) result = new Integer(result.intValue() & nextValue.intValue());
			if (operator.equals("|")) result = new Integer(result.intValue() | nextValue.intValue());
			if (operator.equals("^")) result = new Integer(result.intValue() ^ nextValue.intValue());
			
			// array access (not supported here)
			if (operator.equals("[]")) return null;
			
			exprPos++;
		}
		
		return result;
	}
}
