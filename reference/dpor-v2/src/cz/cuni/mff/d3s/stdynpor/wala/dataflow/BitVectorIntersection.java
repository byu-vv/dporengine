/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.wala.dataflow;

import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.FixedPointConstants;
import com.ibm.wala.dataflow.graph.AbstractMeetOperator;
import com.ibm.wala.util.intset.IntSet;
import com.ibm.wala.util.intset.IntIterator;


public class BitVectorIntersection extends AbstractMeetOperator<BitVectorVariable> implements FixedPointConstants 
{
	private final static BitVectorIntersection instance = new BitVectorIntersection();

	public static BitVectorIntersection getInstance()
	{
		return instance;
	}

	private BitVectorIntersection() 
	{
	}

	@Override
	public String toString() 
	{
		return "INTERSECTION";
	}

  
	@Override
	public int hashCode() 
	{
		return 9904;
	}

	@Override
	public boolean equals(Object o) 
	{
		return (o instanceof BitVectorIntersection);
	}

	
	@Override
	public byte evaluate(BitVectorVariable lhs, BitVectorVariable[] rhs) throws IllegalArgumentException 
	{
		if (lhs == null) throw new IllegalArgumentException("lhs == null");
		if (rhs == null) throw new IllegalArgumentException("rhs == null");
	
		BitVectorVariable I = new BitVectorVariable();
		I.copyState(lhs);
		
		IntSet lhsSet = lhs.getValue();		
		if (lhsSet != null)
		{
			IntIterator lhsIt = lhsSet.intIterator();
			while (lhsIt.hasNext())
			{
				int vn = lhsIt.next();
				
				boolean someUnset = false;
			
				for (int i = 0; i < rhs.length; i++) 
				{
					if ( ! rhs[i].get(vn) ) someUnset = true;
				}
				
				if (someUnset) I.clear(vn);
			}
		}
		
		if (!lhs.sameValue(I)) 
		{
			lhs.copyState(I);
			return CHANGED;
		} 
		else 
		{
			return NOT_CHANGED;
		}
	}
}
