/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.wala.dataflow;

import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.UnaryOperator;


public class BitVectorKillAll extends UnaryOperator<BitVectorVariable> 
{
	private final static BitVectorKillAll instance = new BitVectorKillAll();

	public static BitVectorKillAll getInstance() 
	{
		return instance;
	}
	
	private BitVectorKillAll() {}
	
	@Override
	public boolean equals(Object o) 
	{
		// there can be only a single instance
		return this == o;
	}
	
	@Override
	public int hashCode() 
	{
		return 9908;
	}
	
	@Override
	public String toString() 
	{
		return "KILLALL";
	}
	
	@Override
	public byte evaluate(BitVectorVariable lhs, BitVectorVariable rhs) 
	{
		BitVectorVariable E = new BitVectorVariable();
		
		if (!lhs.sameValue(E)) 
		{
			lhs.copyState(E);
			return CHANGED;
		}
		else 
		{
			return NOT_CHANGED;
		}
	}
}
