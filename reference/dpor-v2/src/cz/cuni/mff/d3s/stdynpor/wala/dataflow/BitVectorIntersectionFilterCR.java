/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.wala.dataflow;

import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.FixedPointConstants;
import com.ibm.wala.dataflow.graph.AbstractMeetOperator;
import com.ibm.wala.util.intset.IntSet;
import com.ibm.wala.util.intset.IntIterator;


// "CR" in the class name stands for call-return (edge)
public class BitVectorIntersectionFilterCR extends AbstractMeetOperator<BitVectorVariable> implements FixedPointConstants 
{
	// index of the bit that represents the flag marking call-return edges
	private int markCallRetBitPos;
	
	// index of the bit that, when it is set, denotes that no relevant action occurred in the nested method calls
	private int markEmptyNestedMethodsBitPos;
	
	private BitVectorIntersectionFilterCR() {}
	
	public BitVectorIntersectionFilterCR(int markCR, int markEmptyNM)
	{
		this.markCallRetBitPos = markCR;
		this.markEmptyNestedMethodsBitPos = markEmptyNM;
	}		

	@Override
	public String toString() 
	{
		return "INTERSECTION-FILTER-CR: " + String.valueOf(markCallRetBitPos) + " " + String.valueOf(markEmptyNestedMethodsBitPos);
	}
  
	@Override
	public int hashCode() 
	{
		return 9904 + markCallRetBitPos + markEmptyNestedMethodsBitPos;
	}

	@Override
	public boolean equals(Object o) 
	{
		if (o instanceof BitVectorIntersectionFilterCR)
		{
			BitVectorIntersectionFilterCR other = (BitVectorIntersectionFilterCR) o;
			
			if (this.markCallRetBitPos != other.markCallRetBitPos) return false;
			
			if (this.markEmptyNestedMethodsBitPos != other.markEmptyNestedMethodsBitPos) return false;
			
			return true;
		}
		
		return false;
	}

	
	@Override
	public byte evaluate(BitVectorVariable lhs, BitVectorVariable[] rhs) throws IllegalArgumentException 
	{
		if (lhs == null) throw new IllegalArgumentException("lhs == null");
		if (rhs == null) throw new IllegalArgumentException("rhs == null");
	
		BitVectorVariable I = new BitVectorVariable();
		I.copyState(lhs);
		
		// find the "rhs" element that represents a call-return edge
		int idxCR = -1;
		for (int i = 0; i < rhs.length; i++)
		{
			if (rhs[i].get(markCallRetBitPos)) idxCR = i;	
		}
		
		// check whether there is a nested method for which no relevant action occurs during its execution
		// process only data received over call-entry edges
		boolean existsEmptyNestedMethod = false;
		if (idxCR >= 0)
		{
			// we do it only when merging data for call-return edge and possibly several call-entry edges (for nested methods)

			for (int i = 0; i < rhs.length; i++)
			{
				if (i == idxCR) continue;
				
				if (rhs[i].get(markEmptyNestedMethodsBitPos)) existsEmptyNestedMethod = true;	
			}
		}
		
		IntSet lhsSet = lhs.getValue();
		if (lhsSet != null)
		{
			IntIterator lhsIt = lhsSet.intIterator();
			while (lhsIt.hasNext())
			{
				int vn = lhsIt.next();
				
				// "CE" stands for call-entry (into nested method calls)
				boolean someUnsetCE = false;
				
				for (int i = 0; i < rhs.length; i++) 
				{
					// skip data for the call-return edge
					if (i == idxCR) continue;
					
					if ( ! rhs[i].get(vn) ) someUnsetCE = true;
				}
				
				boolean someUnset = someUnsetCE;
				
				// empty intersection for the call-entry edges and no relevant action occurred inside nested methods (during their execution) -> use data for the call-return edge
				if (someUnsetCE && (idxCR >= 0) && existsEmptyNestedMethod)
				{
					if (rhs[idxCR].get(vn)) someUnset = false;
				}
				
				if (someUnset) I.clear(vn);
			}
		}
		
		if (!lhs.sameValue(I)) 
		{
			lhs.copyState(I);
			return CHANGED;
		} 
		else 
		{
			return NOT_CHANGED;
		}
	}
}
