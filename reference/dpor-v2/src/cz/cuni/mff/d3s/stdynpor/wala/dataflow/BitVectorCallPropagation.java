/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.wala.dataflow;

import java.util.Arrays;

import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.UnaryOperator;


public class BitVectorCallPropagation extends UnaryOperator<BitVectorVariable> 
{
	private String callerMethodSig;
	private String calleeMethodSig;
	
	private int[] callerActualParams;
	private int[] calleeFormalParams;
	

	private BitVectorCallPropagation() {}
	
	public BitVectorCallPropagation(String callerSig, String calleeSig, int[] actParams, int[] formParams) 
	{
		this.callerMethodSig = callerSig;
		this.calleeMethodSig = calleeSig;
		
		this.callerActualParams = actParams;
		this.calleeFormalParams = formParams;
	}
	
	@Override
	public String toString() 
	{
		return "CALLPROPAGATION " + Arrays.toString(callerActualParams) + " " + Arrays.toString(calleeFormalParams);
	}
  
	@Override
	public int hashCode() 
	{
		return 9905 + Arrays.hashCode(callerActualParams) + Arrays.hashCode(calleeFormalParams);
	}

	@Override
	public boolean equals(Object o) 
	{
		if (o instanceof BitVectorCallPropagation)
		{
			BitVectorCallPropagation other = (BitVectorCallPropagation) o;
			
			if ( ! Arrays.equals(this.callerActualParams, other.callerActualParams) ) return false;
			if ( ! Arrays.equals(this.calleeFormalParams, other.calleeFormalParams) ) return false;
			
			return true;
		}
		
		return false;
	}

	
	@Override
	public byte evaluate(BitVectorVariable lhs, BitVectorVariable rhs) throws IllegalArgumentException 
	{
		if (lhs == null) throw new IllegalArgumentException("lhs == null");
		if (rhs == null) throw new IllegalArgumentException("rhs == null");
	
		// we must copy 'rhs' (value that flows into the edge)
		BitVectorVariable P = new BitVectorVariable();
		P.copyState(rhs);
		
		// propagate bits associated with actual parameters to formal parameters
		for (int j = 0; j < callerActualParams.length; j++)
		{
			if ((calleeFormalParams[j] != -1) && (callerActualParams[j] != -1))
			{
				if (rhs.get(callerActualParams[j])) P.set(calleeFormalParams[j]);
				else P.clear(calleeFormalParams[j]);
			}
		}
		
		if (!lhs.sameValue(P)) 
		{
			lhs.copyState(P);
			return CHANGED;
		} 
		else 
		{
			return NOT_CHANGED;
		}
	}
}
