/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.data;

public class AllocationSite extends ObjectID implements Comparable<AllocationSite>
{
	public ProgramPoint progPoint;

	// optional (it can be null)	
	public Context context;
	
	private int hc;
	
	
	public AllocationSite(String mthSig, int insnPos)
	{
		this(mthSig, insnPos, null);
	}
	
	public AllocationSite(String mthSig, int insnPos, Context ctx)
	{
		this(new ProgramPoint(mthSig, insnPos), ctx);
	}
	
	private AllocationSite(ProgramPoint pp, Context ctx)
	{
		super();
		
		this.progPoint = pp;
		
		this.context = ctx;
		
		hc = 0;
	}
	
	public static AllocationSite makeCopyWithContext(AllocationSite as, Context ctx)
	{
		return new AllocationSite(as.progPoint, ctx);	
	}

	public static AllocationSite makeCopyWithoutContext(AllocationSite as)
	{
		return new AllocationSite(as.progPoint, null);
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof AllocationSite) ) return false;
		
		AllocationSite other = (AllocationSite) obj;
		
		if ( ! this.progPoint.equals(other.progPoint) ) return false;
		
		if (this.context != null)
		{
			if ( ! this.context.equals(other.context) ) return false;	
		}
		else // this.context == null
		{
			if (other.context != null) return false;	
		}
		
		return true;
	}
	
	public int hashCode()
	{
		if (hc == 0)
		{
			hc = hc * 31 + this.progPoint.hashCode();
			
			if (this.context != null) hc = hc * 31 + this.context.hashCode();
		}
		
		return hc;
	}
	
	public int compareTo(AllocationSite other)
	{
		if (other == null) return 1;
		
		return this.progPoint.compareTo(other.progPoint);
	}

	protected String createStringRepr()
	{
		StringBuffer strbuf = new StringBuffer();

		strbuf.append(this.progPoint.toString());

		if (this.context != null)
		{
			strbuf.append("[").append(this.context.toString()).append("]");
		}

		return strbuf.toString();
	}
}
