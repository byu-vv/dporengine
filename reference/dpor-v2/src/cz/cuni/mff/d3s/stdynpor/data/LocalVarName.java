/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.data;

public class LocalVarName extends ObjectID
{
	// bytecode variable index
	private int index;

	private String nameStr;
	
	private LocalVarName(int idx)
	{
		super();
	
		this.index = idx;
		
		// we use artificial local variable names ("localX")
		this.nameStr = "local"+idx;
	}

	public static LocalVarName createFromIndex(int idx)
	{
		return new LocalVarName(idx);
	}
	
	public int getIndex()
	{
		return this.index;
	}

	public String getAsString()
	{
		return this.nameStr;
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof LocalVarName) ) return false;
		
		LocalVarName other = (LocalVarName) obj;
		
		if (this.index != other.index) return false;
		
		return true;
	}
	
	public int hashCode()
	{
		return this.index;
	}
	
	protected String createStringRepr()
	{
		return this.nameStr;
	}
}
