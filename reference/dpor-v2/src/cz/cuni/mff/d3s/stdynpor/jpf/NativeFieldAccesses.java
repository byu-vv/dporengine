/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.jpf;

import java.util.Set;
import java.util.HashSet;

import cz.cuni.mff.d3s.stdynpor.data.FieldName;


public class NativeFieldAccesses
{
	// fields stored in these sets must be always considered as read/written in the future
	public static Set<FieldName> nativeFieldReads;
	public static Set<FieldName> nativeFieldWrites;

	static
	{
		nativeFieldReads = new HashSet<FieldName>();
		nativeFieldWrites = new HashSet<FieldName>();
		
		nativeFieldReads.add(FieldName.createFromString("java.io.File", "filename"));
		nativeFieldReads.add(FieldName.createFromString("java.io.FileDescriptor", "fd"));
		nativeFieldReads.add(FieldName.createFromString("java.io.FileDescriptor", "off"));
		nativeFieldReads.add(FieldName.createFromString("java.io.FileDescriptor", "mode"));
		nativeFieldReads.add(FieldName.createFromString("java.io.FileDescriptor", "fileName"));
		nativeFieldReads.add(FieldName.createFromString("java.io.FileDescriptor", "state"));
		nativeFieldReads.add(FieldName.createFromString("java.io.OutputStreamWriter", "value"));
		nativeFieldReads.add(FieldName.createFromString("java.io.RandomAccessFile", "filename"));
		nativeFieldReads.add(FieldName.createFromString("java.io.RandomAccessFile", "data_root"));
		nativeFieldReads.add(FieldName.createFromString("java.io.RandomAccessFile", "currentPosition"));
		nativeFieldReads.add(FieldName.createFromString("java.io.RandomAccessFile", "currentLength"));
		nativeFieldReads.add(FieldName.createFromString("java.io.RandomAccessFile", "CHUNK_SIZE"));
		nativeFieldReads.add(FieldName.createFromString("java.io.RandomAccessFile$DataRepresentation", "data"));
		nativeFieldReads.add(FieldName.createFromString("java.io.RandomAccessFile$DataRepresentation", "next"));
		nativeFieldReads.add(FieldName.createFromString("java.io.RandomAccessFile$DataRepresentation", "chunk_index"));
		nativeFieldReads.add(FieldName.createFromString("java.lang.String", "value"));
		nativeFieldReads.add(FieldName.createFromString("java.lang.String", "hash"));
		nativeFieldReads.add(FieldName.createFromString("java.lang.StringBuffer", "value"));
		nativeFieldReads.add(FieldName.createFromString("java.lang.StringBuffer", "count"));
		nativeFieldReads.add(FieldName.createFromString("java.lang.StringBuffer", "shared"));
		nativeFieldReads.add(FieldName.createFromString("java.lang.StringBuilder", "value"));
		nativeFieldReads.add(FieldName.createFromString("java.lang.StringBuilder", "count"));		
		nativeFieldReads.add(FieldName.createFromString("java.lang.Thread", "name"));
		nativeFieldReads.add(FieldName.createFromString("java.lang.Throwable", "snapshot"));
		nativeFieldReads.add(FieldName.createFromString("java.lang.Throwable", "detailMessage"));
		nativeFieldReads.add(FieldName.createFromString("java.util.Date", "fastTime"));
		nativeFieldReads.add(FieldName.createFromString("java.util.Random", "seed"));
				
		nativeFieldWrites.add(FieldName.createFromString("java.io.File", "filename"));
		nativeFieldWrites.add(FieldName.createFromString("java.io.FileDescriptor", "off"));
		nativeFieldWrites.add(FieldName.createFromString("java.io.RandomAccessFile", "currentPosition"));
		nativeFieldWrites.add(FieldName.createFromString("java.io.RandomAccessFile", "currentLength"));
		nativeFieldWrites.add(FieldName.createFromString("java.io.RandomAccessFile", "data_root"));
		nativeFieldWrites.add(FieldName.createFromString("java.io.RandomAccessFile$DataRepresentation", "data"));
		nativeFieldWrites.add(FieldName.createFromString("java.io.RandomAccessFile$DataRepresentation", "next"));
		nativeFieldWrites.add(FieldName.createFromString("java.io.RandomAccessFile$DataRepresentation", "chunk_index"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.Integer", "value"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.Long", "value"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.Float", "value"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.Double", "value"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.Boolean", "value"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.Character", "value"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.Byte", "value"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.Short", "value"));		
		nativeFieldWrites.add(FieldName.createFromString("java.lang.String", "hash"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.StringBuffer", "value"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.StringBuffer", "count"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.StringBuffer", "shared"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.StringBuilder", "value"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.StringBuilder", "count"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.Throwable", "snapshot"));
		nativeFieldWrites.add(FieldName.createFromString("java.lang.reflect.Field", "regIdx"));
		nativeFieldWrites.add(FieldName.createFromString("java.util.Calendar", "firstDayOfWeek"));
		nativeFieldWrites.add(FieldName.createFromString("java.util.Calendar", "minimalDaysInFirstWeek"));
		nativeFieldWrites.add(FieldName.createFromString("java.util.Random", "seed"));		
	}

}

