/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.bytecode.interpreter;

import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.SymbolicValue;
import cz.cuni.mff.d3s.stdynpor.data.ArrayElementIndex;


public interface ExecutionVisitor
{
	void visitArrayLoadInsn(ProgramPoint pp, SymbolicValue arrayObj, ArrayElementIndex elementIndex);
	
	void visitArrayStoreInsn(ProgramPoint pp, SymbolicValue arrayObj, ArrayElementIndex elementIndex, SymbolicValue newValue);

	void visitGetInsn(ProgramPoint pp, SymbolicValue obj, String fieldName);

	void visitLoadInsn(ProgramPoint pp, SymbolicValue localVar);
		
	void visitPutInsn(ProgramPoint pp, SymbolicValue obj, String fieldName, SymbolicValue newValue);
	
	void visitStoreInsn(ProgramPoint pp, SymbolicValue localVar, SymbolicValue newValue);
}
