/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.data;

public class LocalVarAccess extends ObjectID
{
	public LocalVarID targetVar;
	
	public ProgramPoint progPoint;
	
	private int hc;


	public LocalVarAccess(LocalVarID tgtVar, ProgramPoint pp)
	{
		this.targetVar = tgtVar;
		
		this.progPoint = pp;
		
		hc = 0;
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof LocalVarAccess) ) return false;
		
		LocalVarAccess other = (LocalVarAccess) obj;
		
		if ( ! this.targetVar.equals(other.targetVar) ) return false;
		if ( ! this.progPoint.equals(other.progPoint) ) return false;
		
		return true;
	}
	
	public int hashCode()
	{
		if (hc == 0)
		{
			hc = hc * 31 + this.targetVar.hashCode();
			hc = hc * 31 + this.progPoint.hashCode();
		}
		
		return hc;
	}
	
	protected String createStringRepr()
	{
		StringBuffer strbuf = new StringBuffer();

		strbuf.append(targetVar.toString());
		strbuf.append(" # ");
		strbuf.append(progPoint.toString());
		
		return strbuf.toString();
	}	
}


