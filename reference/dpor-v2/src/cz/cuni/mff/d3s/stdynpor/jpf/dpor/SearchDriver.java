/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.jpf.dpor;

import java.util.List;
import java.util.ArrayList;
import java.util.Stack;
import java.util.Map;
import java.util.TreeMap;
import java.util.Set;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.bytecode.FieldInstruction;
import gov.nasa.jpf.report.Statistics;

import cz.cuni.mff.d3s.stdynpor.Debug;
import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.jpf.DynamicThreadChoice;
import cz.cuni.mff.d3s.stdynpor.jpf.JPFUtils;


public class SearchDriver extends ListenerAdapter
{
	public static boolean printChoiceDecision = false;
	

	public SearchDriver(Config cfg)
	{
	}
	
	public void stateAdvanced(Search search) 
	{
		// modify choices based on recorded visible accesses
		if (search.isEndState() || search.isVisitedState())
		{
			// get all visible accesses on the current path
			List<VisibleAction> curPathVisibleAccesses = HappensBeforeOrdering.getVisibleAccessesOnCurrentPath();
			
			// get all future visible accesses that can happen after the current state
			List<VisibleAction> curStateFutureAccesses = HappensBeforeOrdering.getFutureAccessesForCurrentState(search.getStateId());

			// get all choice generators associated with visible accesses on the current path			
			DynamicThreadChoice[] curPathDynThChoices = search.getVM().getChoiceGeneratorsOfType(DynamicThreadChoice.class);

			for (int recAccessIdx = curPathVisibleAccesses.size() - 1; recAccessIdx >= 0; recAccessIdx--)
			{
				VisibleAction recAccess = curPathVisibleAccesses.get(recAccessIdx);
				
				if (curPathDynThChoices[recAccessIdx].allThreadsEnabled()) continue;
				
				// process future visible accesses that can happen after the current state
				
				boolean existsConflict = false;
				
				for (VisibleAction futureAccess : curStateFutureAccesses)
				{
					if (futureAccess.isConflictingWith(recAccess)) 
					{
						if (printChoiceDecision)
						{
							Instruction cgInsn = curPathDynThChoices[recAccessIdx].getInsn();

							String cgMthSig = JPFUtils.getMethodSignature(cgInsn.getMethodInfo());
								
							if (Debug.isWatchedEntity(cgMthSig))
							{
								if (cgInsn instanceof FieldInstruction)
								{
									FieldInstruction cgFieldInsn = (FieldInstruction) cgInsn;
								
									ProgramPoint cgPP = new ProgramPoint(cgMthSig, cgInsn.getPosition());
		
									System.out.print("[CHOICE DECISION] exists future conflict = true");
		
									System.out.print("[CHOICE DECISION] multiple thread choices for current ");
		
									if (cgFieldInsn.isRead()) System.out.print("read ");
									else System.out.print("write ");
		
									System.out.print("access to field ");
									System.out.print(cgFieldInsn.getClassName() + "." + cgFieldInsn.getFieldName());
		
									System.out.print(" at ");
									System.out.print(cgPP.toString());
		
									System.out.println("");
								}
							}
						}
						
						existsConflict = true;
						break;
					}
				}
				
				if (existsConflict)
				{
					curPathDynThChoices[recAccessIdx].enableAllThreads();	
				}
				
				// process visible accesses recorded on the current path				
					// for the recorded visible access, get every preceding access that (1) is in a conflict with the recorded access but (2) does not happen before some other access preceding the recorded access in the same thread, and get the associated choice generator too
					// we use indexes to find relevant choice generators (this works because our dynamic choice generator is created for every visible access so the indexes match)
				
				for (int prevAccessIdx = 0; prevAccessIdx < recAccessIdx; prevAccessIdx++)
				{
					// we skip this previous access because the associated choice has all threads enabled already
					if (curPathDynThChoices[prevAccessIdx].allThreadsEnabled()) continue;					
					
					VisibleAction prevAccess = curPathVisibleAccesses.get(prevAccessIdx);
					
					if (recAccess.isConflictingWith(prevAccess))
					{
						if ( ! HappensBeforeOrdering.existsOrderBetweenActions(prevAccess.index, recAccessIdx - 1) )
						{
							if (printChoiceDecision)
							{
								Instruction cgInsn = curPathDynThChoices[prevAccessIdx].getInsn();
							
								String cgMthSig = JPFUtils.getMethodSignature(cgInsn.getMethodInfo());
							
								if (Debug.isWatchedEntity(cgMthSig))
								{
									if (cgInsn instanceof FieldInstruction)
									{
										FieldInstruction cgFieldInsn = (FieldInstruction) cgInsn;
		
										ProgramPoint cgPP = new ProgramPoint(cgMthSig, cgInsn.getPosition());
		
										System.out.print("[CHOICE DECISION] multiple thread choices for previous ");
		
										if (cgFieldInsn.isRead()) System.out.print("read ");
										else System.out.print("write ");
		
										System.out.print("access to field ");
										System.out.print(cgFieldInsn.getClassName() + "." + cgFieldInsn.getFieldName());
		
										System.out.print(" at ");
										System.out.print(cgPP.toString());
			
										System.out.println("");
									}
								}
							}
							
							// we must enable all remaining threads in the choice generator
							curPathDynThChoices[prevAccessIdx].enableAllThreads();
						}
					}
				}
			}
		}
	}
	
	public void searchFinished(Search search)
	{
		System.out.println("[DYNPOR] dynamic thread choices with multiple options: " + DynamicThreadChoice.totalNumWithMultipleOptions);
		
		Statistics stats = search.getVM().getJPF().getReporter().getStatistics();
		
		long totalChoices = stats.newStates - stats.sharedAccessCGs + DynamicThreadChoice.totalNumWithMultipleOptions;
		
		System.out.println("[DYNPOR] total choices: " + totalChoices);
	}

}
