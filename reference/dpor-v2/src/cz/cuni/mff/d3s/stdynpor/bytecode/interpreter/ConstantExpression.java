/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.bytecode.interpreter;

import cz.cuni.mff.d3s.stdynpor.data.ClassName;


public class ConstantExpression extends Expression
{
	public String valueStr;
	
	public ConstantExpression(String val, ClassName type)
	{
		super(type);
		
		this.valueStr = val;
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof ConstantExpression) ) return false;
		
		ConstantExpression other = (ConstantExpression) obj;
		
		if ( ! this.valueStr.equals(other.valueStr) ) return false;
		
		return true;
	}
	
	public int hashCode()
	{
		return this.valueStr.hashCode();
	}

	protected String createStringRepr()
	{
		return valueStr;
	}

}
