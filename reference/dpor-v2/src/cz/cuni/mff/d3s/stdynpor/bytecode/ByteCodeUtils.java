/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.bytecode;

public class ByteCodeUtils
{
	public static boolean isLibraryEntity(String entityName)
	{
		if (entityName.startsWith("java.")) return true;
		if (entityName.startsWith("javax.")) return true;
		if (entityName.startsWith("sun.")) return true;
		if (entityName.startsWith("com.ibm.wala.")) return true;
		
		return false;
	}

	public static boolean isLibraryEntityForThread(String entityName)
	{
		if (entityName.startsWith("java.lang.Thread.")) return true;
		if (entityName.startsWith("java.lang.ThreadGroup.")) return true;

		return false;
	}
	
	public static boolean isThreadStartMethod(String entityName)
	{
		if (entityName.startsWith("java.lang.Thread.start()")) return true;
		
		return false;
	}
	
	public static boolean isSynchEventMethod(String methodSig)
	{
		if (methodSig.startsWith("java.lang.Thread.start")) return true;
		if (methodSig.startsWith("java.lang.Thread.join")) return true;
		if (methodSig.startsWith("java.lang.Object.wait")) return true;
		if (methodSig.startsWith("java.lang.Object.notify")) return true;
		
		return false;
	}
	
	public static boolean isCollectionsMethod(String methodSig)
	{
		if ( ! methodSig.startsWith("java.util") ) return false;

		if (methodSig.startsWith("java.util.Vector")) return true;
		if (methodSig.startsWith("java.util.Hashtable")) return true;
		if (methodSig.startsWith("java.util.HashMap")) return true;
		if (methodSig.startsWith("java.util.TreeMap")) return true;
		if (methodSig.startsWith("java.util.HashSet")) return true;
		if (methodSig.startsWith("java.util.TreeSet")) return true;
		if (methodSig.startsWith("java.util.ArrayList")) return true;
		if (methodSig.startsWith("java.util.LinkedList")) return true;
		if (methodSig.startsWith("java.util.Arrays")) return true;
		
		return false;
	}

}

