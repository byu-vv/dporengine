/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor;

public class Configuration
{
	public static boolean usingPointerAlias = false;
	public static boolean enabledPAExhaustive = false;
	public static boolean enabledPAExhaustObjCtx = false;
	public static boolean enabledPADemandDrv = false;
	public static boolean enabledPADemandDrvCtx = false;
	
	public static boolean usingFieldAccess = false;
	public static boolean enabledFAInsensitive = false;
	public static boolean enabledFACalleeCtx = false;
	public static boolean enabledFACtxSensitive = false;
	public static boolean saveFALocalVarAccess = false;
	
	public static boolean usingImmutableFields = false;
	
	public static boolean usingAllocationSites = false;
	
	public static boolean usingArrayAccess = false;
	public static boolean usingArrayElements = false;	
	public static boolean usingLocalVarAccess = false;	
	public static boolean usingMethodCalls = false;
	
	public static boolean usingDynamicPOR = false;
	
	public static boolean allowExcludingChildThreads = false;
	
	public static boolean debugIncludeLibs = false;

}
