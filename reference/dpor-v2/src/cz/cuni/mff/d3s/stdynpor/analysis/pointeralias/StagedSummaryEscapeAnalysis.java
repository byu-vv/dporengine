/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.analysis.pointeralias;

import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Arrays;

import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.impl.Everywhere;
import com.ibm.wala.ipa.cfg.ExplodedInterproceduralCFG;
import com.ibm.wala.ipa.cfg.BasicBlockInContext;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.IField;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAFieldAccessInstruction;
import com.ibm.wala.ssa.SSAGetInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.ssa.SSAReturnInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SSAArrayStoreInstruction;
import com.ibm.wala.ssa.SSAArrayLoadInstruction;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.ssa.SSAPhiInstruction;
import com.ibm.wala.ssa.SSACheckCastInstruction;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.ssa.analysis.ExplodedControlFlowGraph;
import com.ibm.wala.ssa.analysis.IExplodedBasicBlock;
import com.ibm.wala.util.intset.BitVector;
import com.ibm.wala.util.intset.OrdinalSetMapping;
import com.ibm.wala.util.intset.MutableMapping;
import com.ibm.wala.util.intset.IntSet; 
import com.ibm.wala.util.intset.IntIterator;
import com.ibm.wala.util.graph.impl.GraphInverter;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.UnaryOperator;
import com.ibm.wala.dataflow.graph.BitVectorSolver;
import com.ibm.wala.dataflow.graph.BitVectorFramework;
import com.ibm.wala.dataflow.graph.IKilldallFramework;
import com.ibm.wala.dataflow.graph.AbstractMeetOperator;
import com.ibm.wala.dataflow.graph.BitVectorKillGen;
import com.ibm.wala.dataflow.graph.BitVectorUnion;
import com.ibm.wala.dataflow.graph.BitVectorIdentity;
import com.ibm.wala.dataflow.graph.ITransferFunctionProvider;
import com.ibm.wala.analysis.typeInference.TypeInference;
import com.ibm.wala.analysis.typeInference.TypeAbstraction;

import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.LocalVarID;
import cz.cuni.mff.d3s.stdynpor.wala.WALAUtils;
import cz.cuni.mff.d3s.stdynpor.wala.TypeAnalysisData;
import cz.cuni.mff.d3s.stdynpor.wala.dataflow.BitVectorIntersection;
import cz.cuni.mff.d3s.stdynpor.wala.dataflow.BitVectorCopy;
import cz.cuni.mff.d3s.stdynpor.wala.dataflow.BitVectorKillAll;


public class StagedSummaryEscapeAnalysis
{
	// method signature to the current summary
	private static Map<String, MethodSummary> mthSig2Summary;
	
	// method signature to SSA IR
	private static Map<String, IR> mthSig2IR;
		
	// method signature to unique reference
	private static Map<String, MethodReference> mthSig2Ref;
	
	// map from program points to the sets of escaped variables
	private static Map<ProgramPoint, Set<LocalVarID>> pp2EscapedVars;

	
	static
	{
		mthSig2Summary = new HashMap<String, MethodSummary>();		
		mthSig2IR = new HashMap<String, IR>();		
		mthSig2Ref = new HashMap<String, MethodReference>();

		pp2EscapedVars = new HashMap<ProgramPoint, Set<LocalVarID>>();
	}

	
	public static void analyzeProgram(CallGraph clGraph) throws Exception
	{
		generateMethodSummaries(clGraph);
		collectEscapedVariables(clGraph);
	}
	
	public static Map<ProgramPoint, Set<LocalVarID>> getEscapedVariables()
	{
		return pp2EscapedVars;
	}
	

	/**
	 * This method performs stage 1 of the immutable fields analysis.
	 */
	public static void generateMethodSummaries(CallGraph clGraph) throws Exception
	{
		// elements: method signatures
		List<String> mthWorklist = new LinkedList<String>();
		
		// put all reachable Java methods into the worklist		
		// we assume that native methods escape everything
		for (Iterator<CGNode> cgnIt = clGraph.iterator(); cgnIt.hasNext(); )
		{
			CGNode cgn = cgnIt.next();
			
			IMethod mth = cgn.getMethod();
			
			String methodSig = WALAUtils.getMethodSignature(mth);
			
			mthSig2Ref.put(methodSig, mth.getReference());
			
			if (mth.isNative())
			{
				// a native method escapes all its parameters and returned value
				// its summary cannot change later (it does not have any callees)
				
				Set<Integer> retParams = new HashSet<Integer>();
				for (int i = 0; i < mth.getNumberOfParameters(); i++) retParams.add(i);
				
				Set<Integer> escapedParams = new HashSet<Integer>();
				for (int i = 0; i < mth.getNumberOfParameters(); i++) escapedParams.add(i);
				
				MethodSummary nativeMthSumm = new MethodSummary(methodSig, escapedParams, new RetvalSourceInfo(retParams, true, true, true));
				
				mthSig2Summary.put(methodSig, nativeMthSumm);
			}			
			else
			{				
				mthSig2IR.put(methodSig, cgn.getIR());
			
				if ( ! mthWorklist.contains(methodSig) ) mthWorklist.add(methodSig);
			}
		}
		
		// perform worklist algorithm over the method signatures
		while ( ! mthWorklist.isEmpty() )
		{
			String curMthSig = mthWorklist.remove(0);
			
			IR curMthIR = mthSig2IR.get(curMthSig);
			
			MethodSummary curMthOrigSumm = getCurrentMethodSummary(curMthSig);

			// perform simple intra-procedural analysis of the given method to compute its summary 
				// find what parameters it may escape and whether returned value escapes
				// consider also nested method calls (available summaries for them)			
			MethodSummary curMthNewSumm = computeSummaryForMethod(curMthSig, curMthIR, clGraph);
			
			if ( ! curMthOrigSumm.equals(curMthNewSumm) )
			{
				// summary for the method changed
				
				// add callers into the worklist
				for ( CGNode curMthNode : clGraph.getNodes(mthSig2Ref.get(curMthSig)) )
				{
					Iterator<CGNode> callerNodesIt = clGraph.getPredNodes(curMthNode);
				
					while (callerNodesIt.hasNext())
					{
						CGNode callerNode = callerNodesIt.next();
						
						String callerMthSig = WALAUtils.getMethodSignature(callerNode.getMethod());
												
						if ( ! mthWorklist.contains(callerMthSig) ) mthWorklist.add(callerMthSig);
					}
				}
			}
			
			mthSig2Summary.put(curMthSig, curMthNewSumm);
		}
	}
	
	public static void printMethodSummaries()
	{
		System.out.println("METHOD SUMMARIES");
		System.out.println("=================");
	
		for (String mthSig : mthSig2Summary.keySet())
		{
			MethodSummary mthSumm = mthSig2Summary.get(mthSig);
	
			// filter core libraries
			if (mthSig.startsWith("java.")) continue;
			if (mthSig.startsWith("com.ibm.wala.")) continue;
			
			System.out.println(mthSig);
			
			System.out.println("\t escaped parameters = " + mthSumm.escapedParams);
			
			System.out.println("\t returned parameters = " + mthSumm.retInfo.params);
			
			System.out.println("\t heap = " + mthSumm.retInfo.heap + ", fresh = " + mthSumm.retInfo.fresh + ", escaped = " + mthSumm.retInfo.escaped);
		}
		
		System.out.println("");
	}

	private static MethodSummary computeSummaryForMethod(String mthSig, IR mthIR, CallGraph clGraph)
	{
		ExplodedControlFlowGraph mthCFG = ExplodedControlFlowGraph.make(mthIR);
		
		RetvalSourceInfo mthRSI = new RetvalSourceInfo(new HashSet<Integer>(), false, false, false);
		
		TypeInference mthTypeInfo = TypeAnalysisData.getMethodTypeInfo(mthSig, mthIR);
		
		// get possible sources of returned value for every return instruction and merge into the result for the whole method
			// not sure if there can be at most one return instruction in the SSA IR or multiple return instructions (but one exit block)
		
		SSAInstruction[] instructions = mthIR.getInstructions();
		for (int i = 0; i < instructions.length; i++) 
		{
			SSAInstruction insn = instructions[i];
					
			if (insn == null) continue;

			if (insn instanceof SSAReturnInstruction)
			{
				SSAReturnInstruction returnInsn = (SSAReturnInstruction) insn;
						
				// does not return void
				if (returnInsn.getResult() != -1)
				{
					RetvalSourceInfo insnRSI = computeRetvalSources(mthSig, mthIR, mthCFG, mthTypeInfo, returnInsn.getResult(), clGraph);
					
					for (Integer retParamIdx : insnRSI.params) mthRSI.params.add(retParamIdx);
					
					if (insnRSI.heap) mthRSI.heap = true;
					if (insnRSI.fresh) mthRSI.fresh = true;
					if (insnRSI.escaped) mthRSI.escaped = true;
				}
			}
		}
		
		Set<Integer> escapedParams = findEscapingParameters(mthSig, mthIR, mthCFG, mthTypeInfo, clGraph);
				
		MethodSummary mthSumm = new MethodSummary(mthSig, escapedParams, mthRSI);
		
		return mthSumm;
	}	
			
	private static RetvalSourceInfo computeRetvalSources(String mthSig, IR mthIR, ExplodedControlFlowGraph mthCFG, TypeInference mthTypeInfo, int retValNum, CallGraph clGraph)
	{
		// these data structures contain SSA value numbers
		Set<Integer> returnedVals = new HashSet<Integer>();
		List<Integer> valWorklist = new LinkedList<Integer>();
		
		valWorklist.add(retValNum);
		
		Set<Integer> rsiParams = new HashSet<Integer>();
		boolean rsiHeap = false;
		boolean rsiFresh = false;
		boolean rsiEscaped = false;
		
		DefUse mthDU = new DefUse(mthIR);

		while ( ! valWorklist.isEmpty() )
		{
			int curValNum = valWorklist.remove(0);
			
			if (returnedVals.contains(curValNum)) continue;
		
			returnedVals.add(curValNum);
			
			SSAInstruction curValDefInsn = mthDU.getDef(curValNum);
		
			if (curValDefInsn == null)
			{
				// method parameter or constant value
				
				if (mthIR.getSymbolTable().isParameter(curValNum))
				{
					// if the "def" location for the returned variable is the start (entry cfg block) then some parameter is returned from the method

					// value numbers for method parameters start at 1
					// parameter indexes start at 0
					rsiParams.add(curValNum - 1);
				}
			}
			
			if (curValDefInsn instanceof SSACheckCastInstruction)
			{
				// typically follows an invoke instruction
				
				SSACheckCastInstruction castInsn = (SSACheckCastInstruction) curValDefInsn;
				
				if ( ! valWorklist.contains(castInsn.getVal()) ) valWorklist.add(castInsn.getVal());	
			}
				
			if (curValDefInsn instanceof SSAInvokeInstruction)
			{
				SSAInvokeInstruction invokeInsn = (SSAInvokeInstruction) curValDefInsn;
				
				// populate summary for the current return variable based on all the possibly called methods
				
				// process callees for this invoke
				for ( CGNode mthNode : clGraph.getNodes(mthSig2Ref.get(mthSig)) )
				{
					for ( CGNode tgtNode : clGraph.getPossibleTargets(mthNode, invokeInsn.getCallSite()) )
					{
						String tgtMthSig = WALAUtils.getMethodSignature(tgtNode.getMethod());
						
						// get current summary for target method (callee)
						MethodSummary tgtMthSumm = getCurrentMethodSummary(tgtMthSig);
						
						if (tgtMthSumm.retInfo.fresh) rsiFresh = true;
						if (tgtMthSumm.retInfo.heap) rsiHeap = true;
					
						// any parameter of the called method could be returned from it						
						// for each index stored in "tgtMthSumm.retInfo.params" add the SSA value of actual parameter to the valWorklist
						for (Integer paramIdx : tgtMthSumm.retInfo.params)
						{
							int actParamValNum = invokeInsn.getUse(paramIdx.intValue());
							
							if ( ! valWorklist.contains(actParamValNum) ) valWorklist.add(actParamValNum);
						}
					}
				}
			}
			
			if (curValDefInsn instanceof SSAPhiInstruction)
			{
				SSAPhiInstruction phiInsn = (SSAPhiInstruction) curValDefInsn;
				
				// insert all source value numbers over all incoming edges ("uses") to the valWorklist
				
				for (int i = 0; i < phiInsn.getNumberOfUses(); i++)
				{
					int sourceValNum = phiInsn.getUse(i);
					
					if ( ! valWorklist.contains(sourceValNum) ) valWorklist.add(sourceValNum);
				}
			}
			
			if (curValDefInsn instanceof SSAGetInstruction)
			{
				// returned value loaded from an object field (heap)
				
				rsiHeap = true;
			}

			if (curValDefInsn instanceof SSAArrayLoadInstruction)
			{
				// returned value loaded from an array element (heap)
				
				rsiHeap = true;
			}
			
			if (curValDefInsn instanceof SSANewInstruction)
			{
				// returned value is a newly allocated object
				
				rsiFresh = true;
			}
		}
		
		// find all escaped variables (through putfield, arraystore, or a nested call) and their sources (definitions)
		Set<Integer> allEscapedVals = findEscapedVariablesWithSources(mthSig, mthIR, mthDU, clGraph);
		
		// detect possible aliasing between escaped values and returned values
		// check emptiness of the intersection between (1) sources for the returned value and (2) escaped variables 
		boolean emptyIntersect = true;
		for (Integer rv : returnedVals)
		{
			if (allEscapedVals.contains(rv)) 
			{
				emptyIntersect = false;
				break;
			}
		}
		
		if ( ! emptyIntersect ) rsiEscaped = true;

		return new RetvalSourceInfo(rsiParams, rsiHeap, rsiFresh, rsiEscaped);
	}

	private static Set<Integer> findEscapedVariablesWithSources(String mthSig, IR mthIR, DefUse mthDU, CallGraph clGraph)
	{
		Set<Integer> allEscapedVals = new HashSet<Integer>();
		
		List<Integer> valWorklist = new LinkedList<Integer>();
		
		// put all variables that may escape into the worklist
		valWorklist.addAll(getDirectlyEscapedVariables(mthSig, mthIR, clGraph));
		
		// find sources (definitions) of escaped variables using backwards traversal
		while ( ! valWorklist.isEmpty() )
		{
			int curValNum = valWorklist.remove(0);
	
			if (allEscapedVals.contains(curValNum)) continue;
		
			allEscapedVals.add(curValNum);
			
			SSAInstruction curValDefInsn = mthDU.getDef(curValNum);
	
			// method parameter or constant value
			if (curValDefInsn == null) continue;
			
			if (curValDefInsn instanceof SSACheckCastInstruction)
			{
				// typically follows an invoke instruction
				
				SSACheckCastInstruction castInsn = (SSACheckCastInstruction) curValDefInsn;
				
				if ( ! valWorklist.contains(castInsn.getVal()) ) valWorklist.add(castInsn.getVal());				
			}
			
			if (curValDefInsn instanceof SSAInvokeInstruction)
			{
				SSAInvokeInstruction invokeInsn = (SSAInvokeInstruction) curValDefInsn;
				
				// process callees for this invoke
				for ( CGNode mthNode : clGraph.getNodes(mthSig2Ref.get(mthSig)) )
				{
					for ( CGNode tgtNode : clGraph.getPossibleTargets(mthNode, invokeInsn.getCallSite()) )
					{
						String tgtMthSig = WALAUtils.getMethodSignature(tgtNode.getMethod());
						
						// get current summary for target method (callee)
						MethodSummary tgtMthSumm = getCurrentMethodSummary(tgtMthSig);
						
						// any parameter of the called method could be returned from it						
						// for each index stored in "tgtMthSumm.retInfo.params" add the SSA value of actual parameter to the valWorklist
						for (Integer paramIdx : tgtMthSumm.retInfo.params)
						{
							int actParamValNum = invokeInsn.getUse(paramIdx.intValue());
							
							if ( ! valWorklist.contains(actParamValNum) ) valWorklist.add(actParamValNum);
						}
					}
				}
			}
			
			if (curValDefInsn instanceof SSAPhiInstruction)
			{
				SSAPhiInstruction phiInsn = (SSAPhiInstruction) curValDefInsn;
				
				// insert all source value numbers over all incoming edges ("uses") to the valWorklist
				
				for (int i = 0; i < phiInsn.getNumberOfUses(); i++)
				{
					int sourceValNum = phiInsn.getUse(i);
					
					if ( ! valWorklist.contains(sourceValNum) ) valWorklist.add(sourceValNum);
				}
			}
		}
		
		return allEscapedVals;
	}
	
	private static Set<Integer> getDirectlyEscapedVariables(String mthSig, IR mthIR, CallGraph clGraph)
	{
		Set<Integer> escapedVars = new HashSet<Integer>();
		
		SSAInstruction[] instructions = mthIR.getInstructions();
		
		// get all variables that may escape (putfield, arraystore, nested method call)
		for (int i = 0; i < instructions.length; i++) 
		{
			SSAInstruction insn = instructions[i];
			
			if (insn == null) continue;

			if (insn instanceof SSAPutInstruction)
			{
				SSAPutInstruction putInsn = (SSAPutInstruction) insn;
				
				int storedValNum = putInsn.getVal();
			
				escapedVars.add(storedValNum);
			}
			
			if (insn instanceof SSAArrayStoreInstruction)
			{
				SSAArrayStoreInstruction arstoreInsn = (SSAArrayStoreInstruction) insn;
				
				int storedValNum = arstoreInsn.getValue();
				
				escapedVars.add(storedValNum);
			}
			
			if (insn instanceof SSAInvokeInstruction)
			{
				SSAInvokeInstruction invokeInsn = (SSAInvokeInstruction) insn;
				
				// process callees for this invoke
				for ( CGNode mthNode : clGraph.getNodes(mthSig2Ref.get(mthSig)) )
				{
					for ( CGNode tgtNode : clGraph.getPossibleTargets(mthNode, invokeInsn.getCallSite()) )
					{
						String tgtMthSig = WALAUtils.getMethodSignature(tgtNode.getMethod());
						
						// get current summary for target method
						MethodSummary tgtMthSumm = getCurrentMethodSummary(tgtMthSig);
					
						// for every parameter that escapes inside the callee method
						for (Integer paramIdx : tgtMthSumm.escapedParams)
						{
							int actParamValNum = invokeInsn.getUse(paramIdx.intValue());
							
							escapedVars.add(actParamValNum);
						}
					}
				}
			}
		}
		
		return escapedVars;
	}
	
	private static Set<Integer> findEscapingParameters(String mthSig, IR mthIR, ExplodedControlFlowGraph mthCFG, TypeInference mthTypeInfo, CallGraph clGraph)
	{
		Set<Integer> escParams = new HashSet<Integer>();
		
		// these data structures contain SSA value numbers
		Set<Integer> processedVals = new HashSet<Integer>();
		List<Integer> valWorklist = new LinkedList<Integer>(); 
		
		DefUse mthDU = new DefUse(mthIR);
		
		// put all variables that may escape into the worklist
		valWorklist.addAll(getDirectlyEscapedVariables(mthSig, mthIR, clGraph));
		
		while ( ! valWorklist.isEmpty() )
		{
			int curValNum = valWorklist.remove(0);
			
			if (processedVals.contains(curValNum)) continue;
		
			processedVals.add(curValNum);
			
			SSAInstruction curValDefInsn = mthDU.getDef(curValNum);
		
			if (curValDefInsn == null)
			{
				// method parameter or constant value
				
				if (mthIR.getSymbolTable().isParameter(curValNum))
				{
					// value numbers for method parameters start at 1
					// parameter indexes start at 0
					escParams.add(curValNum - 1);
				}
			}
			
			if (curValDefInsn instanceof SSACheckCastInstruction)
			{
				// typically follows an invoke instruction
				
				SSACheckCastInstruction castInsn = (SSACheckCastInstruction) curValDefInsn;
				
				if ( ! valWorklist.contains(castInsn.getVal()) ) valWorklist.add(castInsn.getVal());
			}
		
			// we must consider the effects of nested method calls
			if (curValDefInsn instanceof SSAInvokeInstruction)
			{
				SSAInvokeInstruction invokeInsn = (SSAInvokeInstruction) curValDefInsn;
			
				// process callees for this invoke
				for ( CGNode mthNode : clGraph.getNodes(mthSig2Ref.get(mthSig)) )
				{
					for ( CGNode tgtNode : clGraph.getPossibleTargets(mthNode, invokeInsn.getCallSite()) )
					{
						String tgtMthSig = WALAUtils.getMethodSignature(tgtNode.getMethod());
				
						MethodSummary tgtMthSumm = getCurrentMethodSummary(tgtMthSig);
						
						// for each index stored in "tgtMthSumm.retInfo.params" add the SSA value of actual parameter to the valWorklist
						for (Integer paramIdx : tgtMthSumm.retInfo.params)
						{							
							int actParamValNum = invokeInsn.getUse(paramIdx.intValue());
							
							if ( ! valWorklist.contains(actParamValNum) ) valWorklist.add(actParamValNum);
						}
					}
				}
			}
			
			if (curValDefInsn instanceof SSAPhiInstruction)
			{
				SSAPhiInstruction phiInsn = (SSAPhiInstruction) curValDefInsn;
				
				// insert all source value numbers over all incoming edges ("uses") to the valWorklist
				
				for (int i = 0; i < phiInsn.getNumberOfUses(); i++)
				{
					int sourceValNum = phiInsn.getUse(i);
					
					if ( ! valWorklist.contains(sourceValNum) ) valWorklist.add(sourceValNum);
				}
			}
		}
			
		return escParams;
	}
	
	private static MethodSummary getCurrentMethodSummary(String mthSig)
	{
		MethodSummary mthSumm = mthSig2Summary.get(mthSig);
		
		if (mthSumm == null)
		{
			mthSumm = new MethodSummary(mthSig, new HashSet<Integer>(), new RetvalSourceInfo(new HashSet<Integer>(), false, false, false));
			mthSig2Summary.put(mthSig, mthSumm);
		}
		
		return mthSumm;
	}
	
	
	/**
	 * Information about possible sources of the returned value.
	 */
	static class RetvalSourceInfo
	{	
		// parameters that may be returned from the method
		// index for every such parameter
		public Set<Integer> params;
	
		// true iff the returned value may be an object loaded from the heap
		public boolean heap;
		
		// true iff the returned value is a new object (allocated inside)
		public boolean fresh;
		
		// true iff the new object may be stored into the heap and then returned
		public boolean escaped;
		
		public RetvalSourceInfo(Set<Integer> ps, boolean h, boolean f, boolean ef)
		{
			this.params = ps;
			this.heap = h;
			this.fresh = f;
			this.escaped = ef;		
		}
		
		public boolean equals(Object obj)
		{
			if (obj == null) return false;
			
			if ( ! (obj instanceof RetvalSourceInfo) ) return false;
	
			RetvalSourceInfo other = (RetvalSourceInfo) obj;
	
			if ( ! this.params.equals(other.params) ) return false;
	
			if (this.heap != other.heap) return false;
			if (this.fresh != other.fresh) return false;
			if (this.escaped != other.escaped) return false;
	
			return true;		
		}
		
		public int hashCode()
		{
			int hc = 0;
			
			hc = hc + (heap ? 1 : 0);
			hc = hc + (fresh ? 2 : 0);
			hc = hc + (escaped ? 4 : 0);
			
			hc = hc * 31 + params.hashCode();
			
			return hc;	
		}
	}
	
	
	static class MethodSummary
	{
		public String mthSig;
		
		// parameters that may be stored into the heap by the given method (or its callees)
		// index for every such parameter
		public Set<Integer> escapedParams;
		
		public RetvalSourceInfo retInfo;
		
		public MethodSummary(String msig, Set<Integer> params, RetvalSourceInfo ri)
		{
			this.mthSig = msig;
			this.escapedParams = params;
			this.retInfo = ri;
		}
		
		public boolean equals(Object obj)
		{
			if (obj == null) return false;
			
			if ( ! (obj instanceof MethodSummary) ) return false;
	
			MethodSummary other = (MethodSummary) obj;
	
			if ( ! this.mthSig.equals(other.mthSig) ) return false;
			
			if ( ! this.escapedParams.equals(other.escapedParams) ) return false;
	
			if ( ! this.retInfo.equals(other.retInfo) ) return false;
	
			return true;		
		}
		
		public int hashCode()
		{
			int hc = 0;
			
			hc = mthSig.hashCode();
			
			hc = hc * 31 + escapedParams.hashCode();
			
			hc = hc * 31 + retInfo.hashCode();
			
			return hc;
		}
	}
	
	
	/**
	 * This method performs stage 2 of the immutable fields analysis.
	 */
	public static void collectEscapedVariables(CallGraph clGraph) throws Exception
	{
		Graph<BasicBlockInContext<IExplodedBasicBlock>> ipCFG = ExplodedInterproceduralCFG.make(clGraph);
		
		EscapedVariablesCollector evc = new EscapedVariablesCollector(ipCFG);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverEVC = evc.analyze();
		
		// collect analysis results: a set of local variables that may escape
		// for every program point (method signature, bytecode index) there will be a set of local variables of the given method that might have already escaped
		
		for (BasicBlockInContext<IExplodedBasicBlock> bb : ipCFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();

			for (int i = ebb.getFirstInstructionIndex(); i <= ebb.getLastInstructionIndex(); i++)
			{
				int bcPos = WALAUtils.getInsnBytecodePos(bb.getNode(), i);
			
				String bcMethodSig = WALAUtils.getMethodSignature(bb.getNode().getMethod());
			
				ProgramPoint pp = new ProgramPoint(bcMethodSig, bcPos);
				
				Set<LocalVarID> escapedVars = new HashSet<LocalVarID>();
				
				// add variables that escaped before the given program point
				IntSet out = solverEVC.getOut(bb).getValue();
				if (out != null)
				{
					for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
					{
						int bvPos = outIt.next();
						
						Integer varNum = evc.getLocalVarNum(bvPos);
						
						LocalVarID escVar = null;
						if ((varNum != null) && (varNum.intValue() != -1)) escVar = new LocalVarID(bcMethodSig, varNum.intValue());
						
						if (escVar != null) 
						{
							escapedVars.add(escVar);
						}
					}					
				}
				
				pp2EscapedVars.put(pp, escapedVars);
			}
		}
	}
	
	public static void printEscapedVariables()
	{
		System.out.println("ESCAPED VARIABLES");
		System.out.println("=================");
	
		Set<ProgramPoint> progPoints = pp2EscapedVars.keySet();
	
		for (ProgramPoint pp : progPoints)
		{
			Set<LocalVarID> escapedVars = pp2EscapedVars.get(pp);
	
			// filter core libraries
			if (pp.methodSig.startsWith("java.")) continue;
			if (pp.methodSig.startsWith("com.ibm.wala.")) continue;
				
			System.out.println(pp.methodSig + ":" + pp.insnPos);
			
			for (LocalVarID ev : escapedVars) 
			{
				if (ev == null) continue;
				
				// filter core libraries
				if (ev.methodSig.startsWith("java.")) continue;
				if (ev.methodSig.startsWith("com.ibm.wala.")) continue;
				
				System.out.println("\t " + ev.toString());
			}
		}
		
		System.out.println("");
	}
	
	
	/**
	 * Escape analysis for local variables (inter-procedural, flow sensitive).
	 * The data flow information associated with a code location is a set of already escaped variables.
	 */
	static class EscapedVariablesCollector
	{
		private Graph<BasicBlockInContext<IExplodedBasicBlock>> ipCFG;
		
		// mapping between ssa value numbers and bitvector positions
		// shared for all methods (bits for individual methods overlap)
		protected OrdinalSetMapping<Integer> sharedVarsNumbering;
		
		// map from method signature to a set of local variables
		private Map<String, Set<LocalVarID>> method2LocalVars;
		
		// ssa value number used to store result for each method invoke
		private Map<ProgramPoint, Integer> invoke2ResultVar;
		
		
		public EscapedVariablesCollector(Graph<BasicBlockInContext<IExplodedBasicBlock>> ipCFG)
		{
			this.ipCFG = ipCFG;
			
			try
			{
				createVariablesNumbering();
			
				initReturnData();
			}
			catch (Exception ex) { ex.printStackTrace(); }
		}
		
		private void createVariablesNumbering() throws Exception
		{
			sharedVarsNumbering = new MutableMapping<Integer>(new Integer[1]);
			
			method2LocalVars = new HashMap<String, Set<LocalVarID>>();
			
			// we must process each method just once -> keep track of processed method signatures
			Set<String> processedMethods = new HashSet<String>();
		
			for (BasicBlockInContext<IExplodedBasicBlock> bb : ipCFG) 
			{
				CGNode node = bb.getNode();
				
				IR methodIR = node.getIR();
				
				if (methodIR == null) continue;
				
				String methodSig = WALAUtils.getMethodSignature(node.getMethod());
				
				if (methodSig == null) continue;
				
				if (processedMethods.contains(methodSig)) continue;
				
				TypeInference typesInfo = TypeAnalysisData.getMethodTypeInfo(methodSig, methodIR);
				
				Set<LocalVarID> localVars = new HashSet<LocalVarID>();
				
				// take all method parameters
				
				int[] methodParams = methodIR.getParameterValueNumbers();
				for (int i = 0; i < methodParams.length; i++)
				{
					TypeReference paramTypeRef = typesInfo.getType(methodParams[i]).getTypeReference();
					
					if ((paramTypeRef != null) && paramTypeRef.isReferenceType())
					{
						int bvPos = sharedVarsNumbering.add(methodParams[i]);
						
						localVars.add(new LocalVarID(methodSig, methodParams[i]));
					}
				}
				
				// take all local variables 
				
				SSAInstruction[] instructions = methodIR.getInstructions();
				for (int i = 0; i < instructions.length; i++) 
				{
					SSAInstruction insn = instructions[i];
					
					if (insn == null) continue;
					
					for (int j = 0; j < insn.getNumberOfDefs(); j++)
					{
						TypeReference varTypeRef = null;
				
						TypeAbstraction varTypeAbs = typesInfo.getType(insn.getDef(j));
						if (varTypeAbs != null) varTypeRef = varTypeAbs.getTypeReference();
						
						if ((varTypeRef != null) && varTypeRef.isReferenceType())
						{
							int bvPos = sharedVarsNumbering.add(insn.getDef(j));
							
							localVars.add(new LocalVarID(methodSig, insn.getDef(j)));
						}
					}					
				}
				
				method2LocalVars.put(methodSig, localVars);
				
				processedMethods.add(methodSig);
			}
		}
		
		private void initReturnData() throws Exception
		{
			invoke2ResultVar = new HashMap<ProgramPoint, Integer>();			
			
			// we must process each method just once -> keep track of processed method signatures
			Set<String> processedMethods = new HashSet<String>();
			
			for (BasicBlockInContext<IExplodedBasicBlock> bb : ipCFG) 
			{
				CGNode node = bb.getNode();
				
				IR methodIR = node.getIR();
				
				if (methodIR == null) continue;
				
				String methodSig = WALAUtils.getMethodSignature(node.getMethod());
				
				if (methodSig == null) continue;
				
				if (processedMethods.contains(methodSig)) continue;
				
				TypeInference typesInfo = TypeAnalysisData.getMethodTypeInfo(methodSig, methodIR);
   
				SSAInstruction[] instructions = methodIR.getInstructions();
				for (int idx = 0; idx < instructions.length; idx++) 
				{
					SSAInstruction insn = instructions[idx];
					
					if (insn == null) continue;
				
					if (insn instanceof SSAInvokeInstruction)
					{
						SSAInvokeInstruction invokeInsn = (SSAInvokeInstruction) insn;
						
						int resVar = -1;
						
						if (invokeInsn.getNumberOfReturnValues() > 0) resVar = invokeInsn.getReturnValue(0);
						
						// does not return void
						if (resVar != -1)
						{
							// we do not store bytecode index but ssa position
							ProgramPoint pp = new ProgramPoint(methodSig, idx);
							
							TypeReference resVarTypeRef = typesInfo.getType(resVar).getTypeReference();
							
							if ((resVarTypeRef != null) && resVarTypeRef.isReferenceType())
							{
								invoke2ResultVar.put(pp, resVar);
							}
						}
					}
				}
				
				processedMethods.add(methodSig);
			}
		}
		
		public Integer getLocalVarNum(int bvPos)
		{
			return sharedVarsNumbering.getMappedObject(bvPos);
		}
		
		public Set<LocalVarID> getLocalVarsForMethod(String mthSig)
		{
			return method2LocalVars.get(mthSig);
		}
		
		
		class TransferFunctionsEVC implements ITransferFunctionProvider<BasicBlockInContext<IExplodedBasicBlock>, BitVectorVariable> 
		{
			public AbstractMeetOperator<BitVectorVariable> getMeetOperator() 
			{
				return BitVectorUnion.instance();
			}
		
			public UnaryOperator<BitVectorVariable> getNodeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> bb) 
			{
				IExplodedBasicBlock ebb = bb.getDelegate();
				
				SSAInstruction insn = ebb.getInstruction();

				int insnIndex = ebb.getFirstInstructionIndex();
				
				CGNode node = bb.getNode();
				
				IR methodIR = node.getIR();
				
				String methodSig = WALAUtils.getMethodSignature(node.getMethod());
				
				TypeInference mthTypeInfo = TypeAnalysisData.getMethodTypeInfo(methodSig, methodIR);
				
				Set<Integer> allMthVars = WALAUtils.getLocalVariablesWithRefType(methodIR, mthTypeInfo);				
								
				if (insn instanceof SSANewInstruction)
				{
					SSANewInstruction newInsn = (SSANewInstruction) insn;
					
					// allocation statement 'v = new C' -> we mark the variable 'v' as not escaped
					
					// bvPos ~ bitvector position
					int bvPos = sharedVarsNumbering.getMappedIndex(newInsn.getDef());
					
					BitVector kill = new BitVector();
					if (bvPos != -1) kill.set(bvPos);

					BitVector gen = new BitVector();					
					
					return new BitVectorKillGen(kill, gen);
				}

				if (insn instanceof SSACheckCastInstruction)
				{
					// typically follows an invoke instruction
				
					SSACheckCastInstruction castInsn = (SSACheckCastInstruction) insn;

					// propagate information from the source variable to the destination variable
					
					int bvPosSrc = sharedVarsNumbering.getMappedIndex(castInsn.getVal());
					
					int bvPosDest = sharedVarsNumbering.getMappedIndex(castInsn.getResult());

					return new BitVectorCopy(bvPosSrc, bvPosDest);
				}

				if (insn instanceof SSAPutInstruction)
				{
					SSAPutInstruction putInsn = (SSAPutInstruction) insn;
					
					// field write statement 'v.f = r' -> make the variable 'r' always escaped at the code location
					
					// bvPos == -1 for ssa values that do not represent variables of a reference type
					int bvPos = sharedVarsNumbering.getMappedIndex(putInsn.getVal());
					
					BitVector kill = new BitVector();
					
					BitVector gen = new BitVector();
					
					// stored variable 'r'
					if (bvPos != -1) gen.set(bvPos);
					
					if (bvPos != -1)
					{
						// process variables aliased with 'r'
						for (Integer otherMthVar : allMthVars)
						{
							if (WALAUtils.checkTypeAliasing(mthTypeInfo, putInsn.getVal(), otherMthVar.intValue()))
							{
								int bvPosOther = sharedVarsNumbering.getMappedIndex(otherMthVar);						
								if (bvPosOther != -1) gen.set(bvPosOther);
							}
						}
					}
					
					return new BitVectorKillGen(kill, gen);
				}
				
				if (insn instanceof SSAGetInstruction)
				{
					SSAGetInstruction getInsn = (SSAGetInstruction) insn;
					
					// field read statement 'r = v.f' or 'r = v.f.g.h' -> mark the variable 'r' always escaped at the code location
					
					// bvPos == -1 for ssa values that do not represent variables of a reference type
					int bvPos = sharedVarsNumbering.getMappedIndex(getInsn.getDef());

					BitVector kill = new BitVector();
					
					BitVector gen = new BitVector();
					if (bvPos != -1) gen.set(bvPos);
					
					return new BitVectorKillGen(kill, gen);
				}
				
				if (insn instanceof SSAArrayStoreInstruction)
				{
					SSAArrayStoreInstruction astoreInsn = (SSAArrayStoreInstruction) insn;
					
					// array store statement 'a[i] = r' -> make the variable 'r' always escaped at the code location
					
					// bvPos == -1 for ssa values that do not represent variables of a reference type
					int bvPos = sharedVarsNumbering.getMappedIndex(astoreInsn.getValue());
					
					BitVector kill = new BitVector();
					
					BitVector gen = new BitVector();
					
					// stored variable 'r'
					if (bvPos != -1) gen.set(bvPos);
					
					if (bvPos != -1)
					{
						// process variables aliased with 'r'					
						for (Integer otherMthVar : allMthVars)
						{
							if (WALAUtils.checkTypeAliasing(mthTypeInfo, astoreInsn.getValue(), otherMthVar.intValue()))
							{
								int bvPosOther = sharedVarsNumbering.getMappedIndex(otherMthVar);						
								if (bvPosOther != -1) gen.set(bvPosOther);
							}
						}
					}					
					
					return new BitVectorKillGen(kill, gen);
				}
				
				if (insn instanceof SSAArrayLoadInstruction)
				{
					SSAArrayLoadInstruction aloadInsn = (SSAArrayLoadInstruction) insn;
					
					// array load statement 'r = a[i]' -> mark the variable 'r' always escaped at the code location
					
					// bvPos == -1 for ssa values that do not represent variables of a reference type
					int bvPos = sharedVarsNumbering.getMappedIndex(aloadInsn.getDef());

					BitVector kill = new BitVector();
					
					BitVector gen = new BitVector();
					if (bvPos != -1) gen.set(bvPos);
					
					return new BitVectorKillGen(kill, gen);
				}
				
				// identity function for all other instructions
				return BitVectorIdentity.instance();
			}

			public UnaryOperator<BitVectorVariable> getEdgeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> src, BasicBlockInContext<IExplodedBasicBlock> dst) 
			{
				try
				{
					if (dst.isEntryBlock())
					{
						SSAInstruction insn = src.getDelegate().getInstruction();
						
						if (insn instanceof SSAInvokeInstruction)
						{
							SSAInvokeInstruction invokeInsn = (SSAInvokeInstruction) insn;
							
							// upon method call, the flag saying whether an actual parameter (in the caller method) is already escaped must propagate to the corresponding formal parameter (local variable of the callee method)
							
							String callerMethodSig = WALAUtils.getMethodSignature(src.getNode().getMethod());
							
							String targetMethodSig = WALAUtils.getMethodSignature(dst.getNode().getMethod());
							
							// bitvector positions
							int[] actualParamsBVP = new int[invokeInsn.getNumberOfParameters()]; 
							
							for (int i = 0; i < invokeInsn.getNumberOfParameters(); i++)
							{
								// store the real actual parameter
								
								actualParamsBVP[i] = sharedVarsNumbering.getMappedIndex(invokeInsn.getUse(i));
								
								// we do not have to consider aliased variables for actual parameter because they are set correctly upon each write instruction
							}
							
							// bitvector positions
							int[] formalParamsBVP = new int[invokeInsn.getNumberOfParameters()];
							
							int[] formalParamVars = dst.getNode().getIR().getParameterValueNumbers();
							for (int i = 0; i < formalParamVars.length; i++)
							{
								formalParamsBVP[i] = sharedVarsNumbering.getMappedIndex(formalParamVars[i]);
							}
							
							return new BitVectorCallPropagationSumm(callerMethodSig, targetMethodSig, actualParamsBVP, formalParamsBVP);
						}
					}
					
					if (src.isExitBlock())
					{	
						// upon return from method, the flag saying whether a returned value (in the callee method) is already escaped must propagate to the corresponding variable used to store the returned value in the caller method
						// mark escaped parameters and returned value based on the available callee method summary
						
						CGNode calleeNode = src.getNode();
						String calleeMethodSig = WALAUtils.getMethodSignature(calleeNode.getMethod());
	   
						CGNode callerNode = dst.getNode();
						String callerMethodSig = WALAUtils.getMethodSignature(callerNode.getMethod());
						int callerNextInsnIndex = dst.getDelegate().getFirstInstructionIndex();
						
						if ((callerNextInsnIndex > -2) && (dst.getNode().getIR().getInstructions()[callerNextInsnIndex - 1] instanceof SSAInvokeInstruction))
						{						
							SSAInvokeInstruction invokeInsn = (SSAInvokeInstruction) dst.getNode().getIR().getInstructions()[callerNextInsnIndex - 1];
							
							TypeInference callerMthTypeInfo = TypeAnalysisData.getMethodTypeInfo(callerMethodSig, callerNode.getIR());
							
							// bitvector positions
							List<List<Integer>> actualParam2AliasesBVP = new ArrayList<List<Integer>>(); 
							
							for (int i = 0; i < invokeInsn.getNumberOfParameters(); i++)
							{
								List<Integer> paramAliasesBVP = new ArrayList<Integer>(); 
								
								// store the real actual parameter
								
								paramAliasesBVP.add(sharedVarsNumbering.getMappedIndex(invokeInsn.getUse(i)));
								
								// store aliased local variables
								for (LocalVarID lvidA : getLocalVarsForMethod(callerMethodSig))
								{
									if (lvidA == null) continue;
									
									if (WALAUtils.checkTypeAliasing(callerMthTypeInfo, invokeInsn.getUse(i), lvidA.varNumber)) 
									{
										paramAliasesBVP.add(sharedVarsNumbering.getMappedIndex(lvidA.varNumber));
									}
								}
								
								actualParam2AliasesBVP.add(paramAliasesBVP);
							}
							
							// bitvector positions
							int[] formalParamsBVP = new int[src.getNode().getIR().getParameterValueNumbers().length];
								
							int[] formalParamVars = src.getNode().getIR().getParameterValueNumbers();
							for (int i = 0; i < formalParamVars.length; i++)
							{
								formalParamsBVP[i] = sharedVarsNumbering.getMappedIndex(formalParamVars[i]);
							}
		
							ProgramPoint callerPP = new ProgramPoint(callerMethodSig, callerNextInsnIndex - 1);
							Integer callerResVar = invoke2ResultVar.get(callerPP);
						
							MethodSummary calleeMthSumm = mthSig2Summary.get(calleeMethodSig);
							
							return new BitVectorReturnPropagationSumm(callerMethodSig, calleeMethodSig, actualParam2AliasesBVP, formalParamsBVP, sharedVarsNumbering.getMappedIndex(callerResVar), calleeMthSumm.escapedParams, calleeMthSumm.retInfo.heap || calleeMthSumm.retInfo.escaped);
						}
					}
				}
				catch (Exception ex) { ex.printStackTrace(); }

				// other cases
				return BitVectorIdentity.instance();
			}
			
			public boolean hasEdgeTransferFunctions() 
			{
				return true;
			}
			
			public boolean hasNodeTransferFunctions() 
			{
				return true;
			}
		}


		// propagation over method call that uses summaries
		public class BitVectorCallPropagationSumm extends UnaryOperator<BitVectorVariable> 
		{
			private String callerMethodSig;
			private String calleeMethodSig;
	
			// BVP ~ bitvector positions
			private int[] callerActualParamsBVP;
			private int[] calleeFormalParamsBVP;


			private BitVectorCallPropagationSumm() {}
	
			public BitVectorCallPropagationSumm(String callerSig, String calleeSig, int[] actParamsBVP, int[] formParamsBVP) 
			{
				this.callerMethodSig = callerSig;
				this.calleeMethodSig = calleeSig;
				this.callerActualParamsBVP = actParamsBVP;
				this.calleeFormalParamsBVP = formParamsBVP;
			}
	
			@Override
			public String toString() 
			{
				return "CALLPROPAGATIONSUMM " + Arrays.toString(callerActualParamsBVP) + " " + Arrays.toString(calleeFormalParamsBVP);
			}
  
			@Override
			public int hashCode() 
			{
				return 9910 + Arrays.hashCode(callerActualParamsBVP) + Arrays.hashCode(calleeFormalParamsBVP);
			}
			
			@Override
			public boolean equals(Object o) 
			{
				if (o instanceof BitVectorCallPropagationSumm)
				{
					BitVectorCallPropagationSumm other = (BitVectorCallPropagationSumm) o;
					if ( ! Arrays.equals(this.callerActualParamsBVP, other.callerActualParamsBVP) ) return false;
					if ( ! Arrays.equals(this.calleeFormalParamsBVP, other.calleeFormalParamsBVP) ) return false;
					return true;
				}
		
				return false;
			}

			@Override
			public byte evaluate(BitVectorVariable lhs, BitVectorVariable rhs) throws IllegalArgumentException 
			{
				if (lhs == null) throw new IllegalArgumentException("null lhs");
				if (rhs == null) throw new IllegalArgumentException("rhs == null");
	
				BitVectorVariable P = new BitVectorVariable();
				P.copyState(lhs);
		
				// propagate bits for actual parameters in 'rhs' (value that flows into the edge) to formal parameters
				for (int j = 0; j < calleeFormalParamsBVP.length; j++)
				{
					if (calleeFormalParamsBVP[j] != -1)
					{
						boolean actualSet = false;
						
						int actualParamBVP = callerActualParamsBVP[j];
						if (actualParamBVP != -1)
						{
							if (rhs.get(actualParamBVP)) actualSet = true;
						}
						
						if (actualSet) P.set(calleeFormalParamsBVP[j]);
						else P.clear(calleeFormalParamsBVP[j]);						
					}
				}
		
				if (!lhs.sameValue(P)) 
				{
					lhs.copyState(P);
					return CHANGED;
				} 
				else 
				{
					return NOT_CHANGED;
				}
			}
		}
		
		
		// propagation over method return that uses summaries
		public class BitVectorReturnPropagationSumm extends UnaryOperator<BitVectorVariable> 
		{
			private String callerMethodSig;
			private String calleeMethodSig;

			private List<List<Integer>> callerActualParam2AliasesBVP;
			private int[] calleeFormalParamsBVP;
		
			private int callerResultVariableBVP;
			
			private Set<Integer> calleeEscapedParamIdxs;
			private boolean calleeEscapedRetVal;
			
			
			private BitVectorReturnPropagationSumm() {}
			
			public BitVectorReturnPropagationSumm(String callerSig, String calleeSig, List<List<Integer>> actParam2AliasBVP, int[] formParamsBVP, int resVarBVP, Set<Integer> escapedParamIdxs, boolean escapedRetVal) 
			{
				this.callerMethodSig = callerSig;
				this.calleeMethodSig = calleeSig;

				this.callerActualParam2AliasesBVP = actParam2AliasBVP;
				this.calleeFormalParamsBVP = formParamsBVP;
				
				this.callerResultVariableBVP = resVarBVP;
				
				this.calleeEscapedParamIdxs = escapedParamIdxs;
				this.calleeEscapedRetVal = escapedRetVal;
			}
			
			@Override
			public String toString() 
			{
				return "RETURNPROPAGATIONSUMM " + callerActualParam2AliasesBVP.toString() + " " + Arrays.toString(calleeFormalParamsBVP) + " " + callerResultVariableBVP + " " + calleeEscapedParamIdxs + " " + calleeEscapedRetVal;
			}
		  
			@Override
			public int hashCode() 
			{
				return 9911 + callerActualParam2AliasesBVP.hashCode() + Arrays.hashCode(calleeFormalParamsBVP) + callerResultVariableBVP + calleeEscapedParamIdxs.hashCode() + (calleeEscapedRetVal ? 1 : 0);
			}
		
			@Override
			public boolean equals(Object o) 
			{
				if (o instanceof BitVectorReturnPropagationSumm)
				{
					BitVectorReturnPropagationSumm other = (BitVectorReturnPropagationSumm) o;
					if ( ! this.callerActualParam2AliasesBVP.equals(other.callerActualParam2AliasesBVP) ) return false;
					if ( ! Arrays.equals(this.calleeFormalParamsBVP, other.calleeFormalParamsBVP) ) return false;			
					if (this.callerResultVariableBVP != other.callerResultVariableBVP) return false;
					if ( ! this.calleeEscapedParamIdxs.equals(other.calleeEscapedParamIdxs) ) return false;
					if (this.calleeEscapedRetVal != other.calleeEscapedRetVal) return false;
					return true;
				}
				
				return false;
			}
		
			
			@Override
			public byte evaluate(BitVectorVariable lhs, BitVectorVariable rhs) throws IllegalArgumentException 
			{
				if (lhs == null) throw new IllegalArgumentException("null lhs");
				if (rhs == null) throw new IllegalArgumentException("rhs == null");
			
				BitVectorVariable P = new BitVectorVariable();
				
				// we have an empty bitvector P
 								
				// set bits for actual parameters and result variable based on data from the callee method summary
				for (Integer escParamIdx : calleeEscapedParamIdxs)
				{
					List<Integer> actualParamAliasesBVP = callerActualParam2AliasesBVP.get(escParamIdx.intValue());
					
					for (int k = 0; k < actualParamAliasesBVP.size(); k++)
					{
						if (actualParamAliasesBVP.get(k) != -1) P.set(actualParamAliasesBVP.get(k));
					}					
				}
				if (calleeEscapedRetVal) 
				{
					if (callerResultVariableBVP != -1) P.set(callerResultVariableBVP);
				}
						
				if (!lhs.sameValue(P)) 
				{
					lhs.copyState(P);
					return CHANGED;
				} 
				else 
				{
					return NOT_CHANGED;
				}
			}
		}


		public BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> analyze() 
		{
			BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, Integer> framework = new BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, Integer>(ipCFG, new TransferFunctionsEVC(), sharedVarsNumbering);
			
			BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solver = new BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>>(framework);
			
			try
			{
				solver.solve(null);
			}
			catch (Exception ex) { ex.printStackTrace(); }
			
			return solver;
		}		
	}
}
