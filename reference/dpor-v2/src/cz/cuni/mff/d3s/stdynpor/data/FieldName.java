/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.data;

public class FieldName
{
	public String classNameStr;
	
	public String fieldNameStr;
	
	private int hc;

	
	private FieldName(String cn, String fn)
	{
		this.classNameStr = cn;
		
		this.fieldNameStr = fn;

		hc = 0;
	}
	
	public static FieldName createFromString(String classNameStr, String fieldNameStr)
	{
		return new FieldName(classNameStr, fieldNameStr);
	}

	public String getAsString()
	{
		return toString();
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof FieldName) ) return false;
		
		FieldName other = (FieldName) obj;
		
		if ( ! this.classNameStr.equals(other.classNameStr) ) return false;
		if ( ! this.fieldNameStr.equals(other.fieldNameStr) ) return false;
		
		return true;
	}
	
	public int hashCode()
	{
		if (hc == 0)
		{
			hc = hc * 31 + this.classNameStr.hashCode();
			hc = hc * 31 + this.fieldNameStr.hashCode();
		}
		
		return hc;
	}
	
	public String toString()
	{
		StringBuffer strbuf = new StringBuffer();

		strbuf.append(classNameStr);
		strbuf.append(".");
		strbuf.append(fieldNameStr);

		return strbuf.toString();
	}	
}

