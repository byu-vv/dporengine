/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.bytecode.interpreter;

import cz.cuni.mff.d3s.stdynpor.data.ClassName;


public class FieldAccessExpression extends Expression
{
	public Expression targetObj;
	public String fieldName;
	
	public ClassName ownerClassType;
	
	
	public FieldAccessExpression(Expression obj, String fname, ClassName fieldType, ClassName ownerClassType)
	{
		super(fieldType);
		
		this.targetObj = obj;
		this.fieldName = fname;
		
		this.ownerClassType = ownerClassType;
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof FieldAccessExpression) ) return false;
		
		FieldAccessExpression other = (FieldAccessExpression) obj;
		
		if ( ! this.targetObj.equals(other.targetObj) ) return false;
		if ( ! this.fieldName.equals(other.fieldName) ) return false;
		
		return true;
	}
	
	public int hashCode()
	{
		return this.targetObj.hashCode() * 31 + this.fieldName.hashCode();
	}

	protected String createStringRepr()
	{
		return targetObj.toString() + "." + fieldName;
	}

}
