/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.wala;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;
import java.util.Iterator;
import java.util.Collection;

import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.propagation.HeapModel;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.callgraph.propagation.SSAPropagationCallGraphBuilder;
import com.ibm.wala.ipa.callgraph.propagation.PointerKey;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.InstanceFieldKey;
import com.ibm.wala.ipa.callgraph.propagation.StaticFieldKey;
import com.ibm.wala.ipa.callgraph.propagation.LocalPointerKey;
import com.ibm.wala.ipa.callgraph.propagation.AllocationSiteInNode;
import com.ibm.wala.classLoader.IField;
import com.ibm.wala.demandpa.util.MemoryAccessMap;
import com.ibm.wala.demandpa.util.PABasedMemoryAccessMap;
import com.ibm.wala.demandpa.alg.DemandRefinementPointsTo;
import com.ibm.wala.demandpa.alg.ContextSensitiveStateMachine;
import com.ibm.wala.demandpa.alg.statemachine.DummyStateMachine;
import com.ibm.wala.demandpa.alg.statemachine.StateMachineFactory;
import com.ibm.wala.demandpa.alg.refinepolicy.TunedRefinementPolicy;
import com.ibm.wala.demandpa.flowgraph.IFlowLabel;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.data.LocalVarID;
import cz.cuni.mff.d3s.stdynpor.data.AllocationSite;


public class PointerAnalysisData
{
	// map from class name to allocation sites for objects of the class
	private static Map<String, Set<AllocationSite>> clsName2AllocSites;

	// map from local variable to allocation sites (points-to set)
	private static Map<LocalVarID, Set<AllocationSite>> localVar2AllocSites;
	
	// map from allocation site (object) to local variables pointing to it
	private static Map<AllocationSite, Set<LocalVarID>> allocSite2PointingVars;
	
	// precomputed information for demand-driven pointer analysis
	private static DemandRefinementPointsTo demandPointsTo;

	// cache for points-to sets (used with demand-driven pointer analysis)
	private static Map<PointerKey, Collection<InstanceKey>> cacheDemandDrvPointsTo;
	
	private static Map<AllocationSite, Integer> cachePointsToMaxSize;
	

	static
	{
		clsName2AllocSites = new HashMap<String, Set<AllocationSite>>();
		localVar2AllocSites = new HashMap<LocalVarID, Set<AllocationSite>>();
		allocSite2PointingVars = new HashMap<AllocationSite, Set<LocalVarID>>();
		
		cacheDemandDrvPointsTo = new HashMap<PointerKey, Collection<InstanceKey>>();
		
		cachePointsToMaxSize = new HashMap<AllocationSite, Integer>();
	}
	
	
	public static void addAllocSiteForClass(String className, AllocationSite allocSite)
	{
		Set<AllocationSite> sites = clsName2AllocSites.get(className);
		if (sites == null)
		{
			sites = new HashSet<AllocationSite>();
			clsName2AllocSites.put(className, sites);
		}
		
		sites.add(allocSite);
	}
	
	public static Set<AllocationSite> getAllocSitesForClass(String className)
	{
		return clsName2AllocSites.get(className);
	}
	
	public static void addAllocSiteForLocalVar(LocalVarID lv, AllocationSite allocSite)
	{
		Set<AllocationSite> sites = localVar2AllocSites.get(lv);
		if (sites == null)
		{
			sites = new HashSet<AllocationSite>();
			localVar2AllocSites.put(lv, sites);
		}
		
		sites.add(allocSite);
	}
	
	public static Set<AllocationSite> getAllocSitesForLocalVar(LocalVarID lv)
	{
		return localVar2AllocSites.get(lv);
	}
	
	public static Set<AllocationSite> getAllocSitesForLocalVar(String methodSig, int localVarNo)
	{
		LocalVarID lv = new LocalVarID(methodSig, localVarNo);
		
		return localVar2AllocSites.get(lv);
	}	

	public static Set<AllocationSite> getObjectAllocSites(String objClassName, String curMethodSig, int localVarNo)
	{
		Set<AllocationSite> objectAllocSites = new HashSet<AllocationSite>();
		
		// check results of pointer analysis
		Set<AllocationSite> allocSitesLV = getAllocSitesForLocalVar(curMethodSig, localVarNo);
		
		if (allocSitesLV != null) 
		{
			objectAllocSites.addAll(allocSitesLV);
		}
		else
		{
			// we do not have results of pointer analysis (it is a static field or a static method)
			objectAllocSites.add(WALAUtils.getAllocationSiteForClass(objClassName));
		}
		
		return objectAllocSites;
	}
	
	public static int getMaxSizePointsToSetWithObject(AllocationSite objID)
	{
		if (cachePointsToMaxSize.containsKey(objID))
		{
			return cachePointsToMaxSize.get(objID).intValue();
		}
		
		// the value not yet cached
		
		Set<LocalVarID> pointVars = allocSite2PointingVars.get(objID);
		
		if (pointVars == null)
		{
			// objID is allocation site for class or a system object
			
			cachePointsToMaxSize.put(objID, 1);
			
			return 1;
		}
		
		int maxSize = 1;
		
		for (LocalVarID pv : pointVars)
		{
			Set<AllocationSite> sites = localVar2AllocSites.get(pv);
			
			if (sites != null)
			{
				if (sites.size() > maxSize) maxSize = sites.size();
			}
		}
		
		cachePointsToMaxSize.put(objID, maxSize);
		
		return maxSize;		
	}		

	public static void collectAllocationSites(PointerAnalysis pa) throws Exception
	{
		// we cannot iterate directly because concurrent modification exception is thrown in that case
		List<InstanceKey> instKeys = new ArrayList<InstanceKey>();
		instKeys.addAll(pa.getInstanceKeys());
		
		for (InstanceKey ik : instKeys)
		{
			if (ik instanceof AllocationSiteInNode)
			{
				AllocationSiteInNode allocKey = (AllocationSiteInNode) ik;
				
				AllocationSite allocSite = WALAUtils.getAllocationSite(allocKey);
				
				String objClassName = WALAUtils.getClassName(allocKey.getConcreteType());
				
				// static type of the abstract heap object
				addAllocSiteForClass(objClassName, allocSite);
				
				Iterator<Object> ofIt = pa.getHeapGraph().getSuccNodes(ik);
				
				// loop over fields
				while (ofIt.hasNext())
				{
					Object pk = ofIt.next();
					
					IField field = null;
					
					if (pk instanceof InstanceFieldKey) field = ((InstanceFieldKey) pk).getField();
					if (pk instanceof StaticFieldKey) field = ((StaticFieldKey) pk).getField();
					
					// get the class which truly declares the given field
					// it may be an arbitrary superclass of "objClassName" 
					String fieldDeclClsName = WALAUtils.getDeclaringClassNameForField(field);
			
					// other kind of field (pointer key)
					if (fieldDeclClsName == null) continue;
					
					addAllocSiteForClass(fieldDeclClsName, allocSite);
				}
			}
		}
		
		for (Iterator<PointerKey> pkIt = pa.getPointerKeys().iterator(); pkIt.hasNext(); )
		{
			PointerKey pk = pkIt.next();
			
			if (pk instanceof LocalPointerKey)
			{	
				CGNode mthNode = ((LocalPointerKey) pk).getNode();
				int localVarNo = ((LocalPointerKey) pk).getValueNumber();

				String mthSignature = WALAUtils.getMethodSignature(mthNode.getMethod());
				
				LocalVarID lv = new LocalVarID(mthSignature, localVarNo);
				
				Iterator<Object> asIt = pa.getHeapGraph().getSuccNodes(pk);

				while (asIt.hasNext())
				{
					Object ik = asIt.next();
					
					if (ik instanceof AllocationSiteInNode)
					{
						AllocationSiteInNode allocKey = (AllocationSiteInNode) ik;
						
						String objClassName = WALAUtils.getClassName(allocKey.getConcreteType());
							
						AllocationSite allocSite = WALAUtils.getAllocationSite(allocKey);
						
						addAllocSiteForLocalVar(lv, allocSite);
						
						addAllocSiteForClass(objClassName, allocSite);
					}
				}
			}		
		}
	}
	
	public static void computeAliasingInformation()
	{
		for (LocalVarID lv : localVar2AllocSites.keySet())
		{
			Set<AllocationSite> sites = localVar2AllocSites.get(lv);
			
			for (AllocationSite as : sites)
			{
				Set<LocalVarID> pointVars = allocSite2PointingVars.get(as);
				if (pointVars == null)
				{
					pointVars = new HashSet<LocalVarID>();
					allocSite2PointingVars.put(as, pointVars);
				}
				
				pointVars.add(lv);
			}
		}		
	}
	
	public static void initDemandDrivenAnalysis(SSAPropagationCallGraphBuilder cgBuilder, CallGraph clGraph, PointerAnalysis pa)
	{
		MemoryAccessMap mam = new PABasedMemoryAccessMap(clGraph, pa);
		
		StateMachineFactory<IFlowLabel> smf = null;		
		if (Configuration.enabledPADemandDrvCtx) smf = new ContextSensitiveStateMachine.Factory();
		else smf = new DummyStateMachine.Factory<IFlowLabel>();					
		
		demandPointsTo = DemandRefinementPointsTo.makeWithDefaultFlowGraph(clGraph, cgBuilder, mam, WALAUtils.classHierarchy, WALAUtils.options, smf);
		demandPointsTo.setRefinementPolicyFactory(new TunedRefinementPolicy.Factory(WALAUtils.classHierarchy));
	}
	
	public static boolean checkLocalVariableAliasing(LocalVarID lv1, LocalVarID lv2)
	{
		if (Configuration.enabledPAExhaustive || Configuration.enabledPAExhaustObjCtx)
		{
			Set<AllocationSite> lvAllocSites1 = localVar2AllocSites.get(lv1);
			Set<AllocationSite> lvAllocSites2 = localVar2AllocSites.get(lv2);
			
			for (AllocationSite as : lvAllocSites1)
			{
				// check whether possible aliasing exists
				if (lvAllocSites2.contains(as)) return true;
			}
		}
			
		if (Configuration.enabledPADemandDrv || Configuration.enabledPADemandDrvCtx)
		{			
			HeapModel hm = demandPointsTo.getHeapModel();
			
			PointerKey pk1 = hm.getPointerKeyForLocal(WALAUtils.getNodeForMethod(lv1.methodSig), lv1.varNumber);
			Collection<InstanceKey> pkAllocSites1 = retrievePointsToSetOnDemand(pk1);
			
			// we do not know for sure so we must give a safe answer (i.e., there may be a non-empty intersection)
			if (pkAllocSites1 == null) return true;
			
			PointerKey pk2 = hm.getPointerKeyForLocal(WALAUtils.getNodeForMethod(lv2.methodSig), lv2.varNumber);
			Collection<InstanceKey> pkAllocSites2 = retrievePointsToSetOnDemand(pk2);
			
			// we do not know for sure so we must give a safe answer (i.e., there may be a non-empty intersection)
			if (pkAllocSites2 == null) return true;
				
			for (InstanceKey ik : pkAllocSites1)
			{
				// check whether possible aliasing exists
				if (pkAllocSites2.contains(ik)) return true;
			}
		}
		
		return false;
	}
	
	private static Collection<InstanceKey> retrievePointsToSetOnDemand(PointerKey pk)
	{
		// compute the points-to set only if it is not already stored in the cache
		
		Collection<InstanceKey> allocSites = cacheDemandDrvPointsTo.get(pk);

		if (allocSites == null)
		{	
			try
			{
				allocSites = demandPointsTo.getPointsTo(pk);
			}
			catch (Exception ex)
			{
				return null;
			}

			cacheDemandDrvPointsTo.put(pk, allocSites);
		}
		
		return allocSites;
	}

}

