/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.wala.dataflow;

import java.util.Arrays;

import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.UnaryOperator;


public class BitVectorReturnPropagation extends UnaryOperator<BitVectorVariable> 
{
	private int[] callerActualParams;
	private int[] calleeFormalParams;

	private int[] calleeReturnedValues;
	private int callerResultVariable;
	
	
	private BitVectorReturnPropagation() {}
	
	public BitVectorReturnPropagation(int[] actParams, int[] formParams, int[] retVals, int resVar) 
	{
		this.callerActualParams = actParams;
		this.calleeFormalParams = formParams;
				
		this.calleeReturnedValues = retVals;
		this.callerResultVariable = resVar;
	}
	
	@Override
	public String toString() 
	{
		return "RETURNPROPAGATION " + Arrays.toString(callerActualParams) + " " + Arrays.toString(calleeFormalParams) + " " + Arrays.toString(calleeReturnedValues) + " " + callerResultVariable;
	}
  
	@Override
	public int hashCode() 
	{
		return 9906 + Arrays.hashCode(callerActualParams) + Arrays.hashCode(calleeFormalParams) + Arrays.hashCode(calleeReturnedValues) + callerResultVariable;
	}

	@Override
	public boolean equals(Object o) 
	{
		if (o instanceof BitVectorReturnPropagation)
		{
			BitVectorReturnPropagation other = (BitVectorReturnPropagation) o;
			
			if ( ! Arrays.equals(this.callerActualParams, other.callerActualParams) ) return false;
			if ( ! Arrays.equals(this.calleeFormalParams, other.calleeFormalParams) ) return false;
			if ( ! Arrays.equals(this.calleeReturnedValues, other.calleeReturnedValues) ) return false;
			if (this.callerResultVariable != other.callerResultVariable) return false;
			
			return true;
		}
		
		return false;
	}

	
	@Override
	public byte evaluate(BitVectorVariable lhs, BitVectorVariable rhs) throws IllegalArgumentException 
	{
		if (lhs == null) throw new IllegalArgumentException("lhs == null");
		if (rhs == null) throw new IllegalArgumentException("rhs == null");
	
		// we must copy 'rhs' (value that flows into the edge)
		BitVectorVariable P = new BitVectorVariable();
		P.copyState(rhs);
		
		// propagate bits associated with formal parameters to actual parameters	
		for (int j = 0; j < calleeFormalParams.length; j++)
		{
			if (callerActualParams[j] != -1)
			{
				if (rhs.get(calleeFormalParams[j])) P.set(callerActualParams[j]);
				else P.clear(callerActualParams[j]);
			}
		}
		
		boolean allRetValSet = true;
		for (int i = 0; i < calleeReturnedValues.length; i++)
		{
			if (calleeReturnedValues[i] != -1)
			{
				if ( ! rhs.get(calleeReturnedValues[i]) ) allRetValSet = false;
			}
		}
		
		if (callerResultVariable != -1)
		{
			if (allRetValSet) P.set(callerResultVariable);
			else P.clear(callerResultVariable);
		}
		
		if (!lhs.sameValue(P)) 
		{
			lhs.copyState(P);
			return CHANGED;
		} 
		else 
		{
			return NOT_CHANGED;
		}
	}
}