/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.analysis.arrayaccess;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.TreeSet;

import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CallGraph;

import cz.cuni.mff.d3s.stdynpor.Debug;
import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.SymbolicValue;
import cz.cuni.mff.d3s.stdynpor.data.ArrayElementIndex;
import cz.cuni.mff.d3s.stdynpor.bytecode.interpreter.SymbolicByteCodeInterpreter;
import cz.cuni.mff.d3s.stdynpor.bytecode.interpreter.ExecutionVisitor;


public class SymbolicArrayElementIndexAnalysis
{
	// map from program point to the symbolic expression that represents an array element index used there (by the array element access)	
	protected static Map<ProgramPoint, ArrayElementIndex> pp2SymbElementIndex;
	
	static
	{
		pp2SymbElementIndex = new LinkedHashMap<ProgramPoint, ArrayElementIndex>();
	}
	
	
	public static void analyzeProgram(CallGraph clGraph) throws Exception
	{
		// for each program point that corresponds to array element access (xALOAD, xASTORE), collect the symbolic expression that represents an element index 
		
		ArrayElementIndexCollector indexCollector = new ArrayElementIndexCollector();
		
		SymbolicByteCodeInterpreter.processReachableMethods(clGraph, indexCollector);  
	}
	
	public static ArrayElementIndex getSymbolicElementIndexForProgramPoint(ProgramPoint pp)
	{
		return pp2SymbElementIndex.get(pp);
	}
	
	public static void printSymbolicElementIndexes()
	{
		System.out.println("SYMBOLIC ARRAY ELEMENT INDEXES");
		System.out.println("==============================");
		
		Set<ProgramPoint> progPoints = new TreeSet<ProgramPoint>();
		progPoints.addAll(pp2SymbElementIndex.keySet());
		
		for (ProgramPoint pp : progPoints)
		{
			if ( ! Debug.isWatchedEntity(pp.methodSig) ) continue;
			
			ArrayElementIndex symbIndex = pp2SymbElementIndex.get(pp);

			System.out.println(pp.methodSig + ":" + pp.insnPos);
			
			System.out.println("\t " + symbIndex.toString());
		}
		
		System.out.println("");
	}
	
	
	static class ArrayElementIndexCollector implements ExecutionVisitor
	{
		public void visitArrayLoadInsn(ProgramPoint pp, SymbolicValue arrayObj, ArrayElementIndex elementIndex)
		{
			pp2SymbElementIndex.put(pp, elementIndex);
		}
	
		public void visitArrayStoreInsn(ProgramPoint pp, SymbolicValue arrayObj, ArrayElementIndex elementIndex, SymbolicValue newValue)
		{
			pp2SymbElementIndex.put(pp, elementIndex);
		}
		
		public void visitGetInsn(ProgramPoint pp, SymbolicValue obj, String fieldName)
		{
		}
		
		public void visitLoadInsn(ProgramPoint pp, SymbolicValue localVar)
		{
		}
		
		public void visitPutInsn(ProgramPoint pp, SymbolicValue obj, String fieldName, SymbolicValue newValue)
		{
		}
	
		public void visitStoreInsn(ProgramPoint pp, SymbolicValue localVar, SymbolicValue newValue)
		{
		}		
	}
	
}
