/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.bytecode.interpreter;


public class ArrayAccessExpression extends Expression
{
	public Expression targetArrayObj;
	public Expression elementIndex;
	
	public ArrayAccessExpression(Expression arrayObj, Expression idx)
	{
		// type not important here
		super(null);
		
		this.targetArrayObj = arrayObj;
		this.elementIndex = idx;
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof ArrayAccessExpression) ) return false;
		
		ArrayAccessExpression other = (ArrayAccessExpression) obj;
		
		if ( ! this.targetArrayObj.equals(other.targetArrayObj) ) return false;
		if ( ! this.elementIndex.equals(other.elementIndex) ) return false;
		
		return true;
	}
	
	public int hashCode()
	{
		return this.targetArrayObj.hashCode() * 31 + this.elementIndex.hashCode();
	}

	protected String createStringRepr()
	{
		return targetArrayObj.toString() + "[" + elementIndex.toString() + "]";
	}

}
