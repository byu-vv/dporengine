/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.jpf;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPFException; 
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.jvm.bytecode.InstructionFactory;


public class SharedAccessInstructionFactory extends InstructionFactory
{
	public SharedAccessInstructionFactory(Config conf)
	{
	}

	// fields
	
	@Override
	public Instruction getfield(String fieldName, String clsName, String fieldDescriptor)
	{
		return new cz.cuni.mff.d3s.stdynpor.jpf.bytecode.GETFIELD(fieldName, clsName, fieldDescriptor);
	}

	@Override
	public Instruction getstatic(String fieldName, String clsName, String fieldDescriptor)
	{
		return new cz.cuni.mff.d3s.stdynpor.jpf.bytecode.GETSTATIC(fieldName, clsName, fieldDescriptor);
	}
	
	@Override
	public Instruction putfield(String fieldName, String clsName, String fieldDescriptor)
	{
		return new cz.cuni.mff.d3s.stdynpor.jpf.bytecode.PUTFIELD(fieldName, clsName, fieldDescriptor);
	}

	@Override
	public Instruction putstatic(String fieldName, String clsName, String fieldDescriptor)
	{
		return new cz.cuni.mff.d3s.stdynpor.jpf.bytecode.PUTSTATIC(fieldName, clsName, fieldDescriptor);
	}

}
