/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.analysis.pointeralias;

import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.List;

import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.impl.Everywhere;
import com.ibm.wala.ipa.cfg.ExplodedInterproceduralCFG;
import com.ibm.wala.ipa.cfg.BasicBlockInContext;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.ssa.analysis.ExplodedControlFlowGraph;
import com.ibm.wala.ssa.analysis.IExplodedBasicBlock;
import com.ibm.wala.util.intset.BitVector;
import com.ibm.wala.util.intset.OrdinalSetMapping;
import com.ibm.wala.util.intset.MutableMapping;
import com.ibm.wala.util.intset.IntSet; 
import com.ibm.wala.util.intset.IntIterator;
import com.ibm.wala.util.graph.impl.GraphInverter;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.UnaryOperator;
import com.ibm.wala.dataflow.graph.BitVectorSolver;
import com.ibm.wala.dataflow.graph.BitVectorFramework;
import com.ibm.wala.dataflow.graph.AbstractMeetOperator;
import com.ibm.wala.dataflow.graph.BitVectorKillGen;
import com.ibm.wala.dataflow.graph.BitVectorUnion;
import com.ibm.wala.dataflow.graph.BitVectorIdentity;
import com.ibm.wala.dataflow.graph.ITransferFunctionProvider;

import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.AllocationSite;
import cz.cuni.mff.d3s.stdynpor.bytecode.ByteCodeUtils;
import cz.cuni.mff.d3s.stdynpor.wala.WALAUtils;
import cz.cuni.mff.d3s.stdynpor.wala.dataflow.BitVectorKillAll;
import cz.cuni.mff.d3s.stdynpor.Debug;


public class AllocationSitesAnalysis
{
	// map from program point (method signature and instruction position) to the set of allocation sites
	protected static Map<ProgramPoint, Set<AllocationSite>> pp2FutureAllocSites;
	
	static
	{
		pp2FutureAllocSites = new LinkedHashMap<ProgramPoint, Set<AllocationSite>>();
	}
	

	public static void analyzeProgram(CallGraph clGraph, ExplodedInterproceduralCFG icfg) throws Exception
	{
		// create the backwards-oriented control-flow graph of the program
		Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG = GraphInverter.invert(icfg);

		AllocSitesCollector asc = new AllocSitesCollector(bwICFG);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverASC = asc.analyze();

		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();

			int insnPos = WALAUtils.getInsnBytecodePos(bb.getNode(), ebb.getFirstInstructionIndex());
		
			String fullMethodSig = WALAUtils.getMethodSignature(bb.getNode().getMethod());
		
			ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);

			Set<AllocationSite> sites = new HashSet<AllocationSite>();
		
			IntSet out = solverASC.getOut(bb).getValue();
			if (out != null)
			{
				for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
				{
					int siteNum = outIt.next();
				
					AllocationSite site = asc.getSiteForNum(siteNum);
				
					sites.add(site);
				}	
			}
			
			pp2FutureAllocSites.put(pp, sites);
		}
	}
	
	public static Set<AllocationSite> getAllocationSitesForProgramPoint(ProgramPoint pp)
	{
		return pp2FutureAllocSites.get(pp);
	}

	public static void printAllocationSites()
	{
		System.out.println("ALLOCATION SITES");
		System.out.println("================");
		
		Set<ProgramPoint> progPoints = new TreeSet<ProgramPoint>();
		progPoints.addAll(pp2FutureAllocSites.keySet());
		
		for (ProgramPoint pp : progPoints)
		{
			if ( ! Debug.isWatchedEntity(pp.methodSig) ) continue;
			
			Set<AllocationSite> sites = pp2FutureAllocSites.get(pp);

			System.out.println(pp.methodSig + ":" + pp.insnPos);
			
			for (AllocationSite site : sites)
			{
				if ( ! Debug.isWatchedEntity(site.progPoint.methodSig) ) continue;
				
				System.out.println("\t " + site.toString());
			}
		}
		
		System.out.println("");
	}

	
	// collecting allocation sites visited before return from the method
	static class AllocSitesCollector
	{
		private Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG;
		
		// mapping between allocation sites and integer numbers (used in bitvectors)
		protected OrdinalSetMapping<AllocationSite> sitesNumbering;
		
		public AllocSitesCollector(Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG)
		{
			this.bwICFG = bwICFG;
			
			this.sitesNumbering = createAllocSitesNumbering(bwICFG);
		}
		
		protected Set<AllocationSite> getAllocSitesAtInsn(CGNode mthNode, IR methodIR, SSAInstruction insn, int insnIndex) throws Exception
		{
			// allocation sites represented by the current instruction
			Set<AllocationSite> sites = new HashSet<AllocationSite>();
			
			if (insn instanceof SSANewInstruction)
			{
				SSANewInstruction newInsn = (SSANewInstruction) insn;
				
				String methodSig = WALAUtils.getMethodSignature(mthNode.getMethod());
				
				int insnPos = WALAUtils.getInsnBytecodePos(mthNode, insnIndex);
				
				sites.add(new AllocationSite(methodSig, insnPos));
			}
				
			return sites;
		}
		
		protected OrdinalSetMapping<AllocationSite> createAllocSitesNumbering(Graph<BasicBlockInContext<IExplodedBasicBlock>> icfg)
		{
			MutableMapping<AllocationSite> siteNums = new MutableMapping<AllocationSite>(new AllocationSite[1]);
			
			for (BasicBlockInContext<IExplodedBasicBlock> bb : icfg)
			{
				CGNode mthNode = bb.getNode();
					
				IR methodIR = mthNode.getIR();
					
				if (methodIR == null) continue;
	
				int firstInsnIdx = bb.getFirstInstructionIndex();
				int lastInsnIdx = bb.getLastInstructionIndex();
	   
				// basic block without instructions
				if ((firstInsnIdx < 0) || (lastInsnIdx < 0)) continue;
	
				SSAInstruction[] instructions = methodIR.getInstructions();
	
				for (int idx = firstInsnIdx; idx <= lastInsnIdx; idx++) 
				{
					SSAInstruction insn = instructions[idx];
					
					try
					{			
						// get allocation sites represented by the current instruction
						Set<AllocationSite> sites = getAllocSitesAtInsn(mthNode, methodIR, insn, idx);
						
						for (AllocationSite as : sites) siteNums.add(as);
					}
					catch (Exception ex) { ex.printStackTrace(); }
				}
			}
				
			return siteNums;
		}
			
		protected AllocationSite getSiteForNum(int siteNum)
		{
			return sitesNumbering.getMappedObject(siteNum);
		}
		
		class TransferFunctionsASC implements ITransferFunctionProvider<BasicBlockInContext<IExplodedBasicBlock>, BitVectorVariable> 
		{
			public AbstractMeetOperator<BitVectorVariable> getMeetOperator() 
			{
				return BitVectorUnion.instance();
			}

			public UnaryOperator<BitVectorVariable> getEdgeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> src, BasicBlockInContext<IExplodedBasicBlock> dst) 
			{
				// we do not propagate through return-to-exit edges 
				
				if (dst.getDelegate().isExitBlock()) 
				{
					return BitVectorKillAll.getInstance();
				}
				else
				{
					return BitVectorIdentity.instance();
				}
			}
	
			public UnaryOperator<BitVectorVariable> getNodeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> bb) 
			{
				IExplodedBasicBlock ebb = bb.getDelegate();
				
				SSAInstruction insn = ebb.getInstruction();

				if (insn instanceof SSANewInstruction)
				{
					// this must be empty -> we do not need to kill anything
					BitVector kill = new BitVector();

					BitVector gen = new BitVector();
					
					try
					{
						Set<AllocationSite> sites = getAllocSitesAtInsn(bb.getNode(), bb.getNode().getIR(), insn, bb.getFirstInstructionIndex());
						
						for (AllocationSite as : sites)
						{
							int siteNum = sitesNumbering.getMappedIndex(as);							
							gen.set(siteNum);
						}							
					}
					catch (Exception ex) { ex.printStackTrace(); }
					
					return new BitVectorKillGen(kill, gen);
				}
				else
				{
					// identity function for all other instructions
					return BitVectorIdentity.instance();
				}
			}
			
			public boolean hasEdgeTransferFunctions() 
			{
				return true;
			}
			
			public boolean hasNodeTransferFunctions() 
			{
				return true;
			}
		}
	
		public BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> analyze() 
		{
			BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, AllocationSite> framework = new BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, AllocationSite>(bwICFG, new TransferFunctionsASC(), sitesNumbering);
			
			BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solver = new BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>>(framework);

			try
			{
				solver.solve(null);
			}
			catch (Exception ex) { ex.printStackTrace(); }
			
			return solver;
		}
	}

}

