/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.analysis.arrayaccess;

import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.List;

import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.cfg.ExplodedInterproceduralCFG;
import com.ibm.wala.ipa.cfg.BasicBlockInContext;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAArrayReferenceInstruction;
import com.ibm.wala.ssa.SSAArrayLoadInstruction;
import com.ibm.wala.ssa.SSAArrayStoreInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.analysis.ExplodedControlFlowGraph;
import com.ibm.wala.ssa.analysis.IExplodedBasicBlock;
import com.ibm.wala.util.intset.BitVector;
import com.ibm.wala.util.intset.OrdinalSetMapping;
import com.ibm.wala.util.intset.MutableMapping;
import com.ibm.wala.util.intset.IntSet; 
import com.ibm.wala.util.intset.IntIterator;
import com.ibm.wala.util.graph.impl.GraphInverter;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.UnaryOperator;
import com.ibm.wala.dataflow.graph.BitVectorSolver;
import com.ibm.wala.dataflow.graph.BitVectorFramework;
import com.ibm.wala.dataflow.graph.AbstractMeetOperator;
import com.ibm.wala.dataflow.graph.BitVectorKillGen;
import com.ibm.wala.dataflow.graph.BitVectorUnion;
import com.ibm.wala.dataflow.graph.BitVectorIdentity;
import com.ibm.wala.dataflow.graph.ITransferFunctionProvider;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.Debug;
import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.AllocationSite;
import cz.cuni.mff.d3s.stdynpor.data.ClassName;
import cz.cuni.mff.d3s.stdynpor.data.ArrayID;
import cz.cuni.mff.d3s.stdynpor.bytecode.ByteCodeUtils;
import cz.cuni.mff.d3s.stdynpor.wala.WALAUtils;
import cz.cuni.mff.d3s.stdynpor.wala.PointerAnalysisData;
import cz.cuni.mff.d3s.stdynpor.wala.ArrayAccessCodeInfo;
import cz.cuni.mff.d3s.stdynpor.wala.dataflow.BitVectorKillAll;


public class ArrayObjectAccessAnalysis
{
	// map from program point (method signature and instruction position) to the set of array objects accessed for reading (xALOAD)
	protected static Map<ProgramPoint, Set<ArrayID>> pp2FutureArrayReads; 
	
	// map from program point (method signature and instruction position) to the set of array objects accessed for writing (xASTORE)
	protected static Map<ProgramPoint, Set<ArrayID>> pp2FutureArrayWrites;

	// results when child threads are excluded
	protected static Map<ProgramPoint, Set<ArrayID>> pp2FutureArrayReadsECT; 
	protected static Map<ProgramPoint, Set<ArrayID>> pp2FutureArrayWritesECT;
	
	// mapping between array object IDs and integer numbers (used in bitvectors)
	private static OrdinalSetMapping<ArrayID> arraysNumbering;

	static
	{
		pp2FutureArrayReads = new LinkedHashMap<ProgramPoint, Set<ArrayID>>();
		pp2FutureArrayWrites = new LinkedHashMap<ProgramPoint, Set<ArrayID>>();
		
		pp2FutureArrayReadsECT = new LinkedHashMap<ProgramPoint, Set<ArrayID>>();
		pp2FutureArrayWritesECT = new LinkedHashMap<ProgramPoint, Set<ArrayID>>();
		
		arraysNumbering = null;
	}
	
	
	public static void analyzeProgram(CallGraph clGraph, ExplodedInterproceduralCFG icfg, boolean excludeChildThreads) throws Exception
	{
		// create the backwards-oriented control-flow graph of the whole program
		Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG = GraphInverter.invert(icfg);
		
		// prepare common data structures
		
		createArrayObjectsNumbering(bwICFG);

		// perform the analysis of array reads
		
		ArrayObjectAccesses aoReads = new ArrayObjectAccesses(bwICFG, true, false, excludeChildThreads);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverReads = aoReads.analyze();

		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();

			int insnPos = WALAUtils.getInsnBytecodePos(bb.getNode(), ebb.getFirstInstructionIndex());
		
			String fullMethodSig = WALAUtils.getMethodSignature(bb.getNode().getMethod());
		
			ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);

			Set<ArrayID> arrayReads = new HashSet<ArrayID>();
		
			IntSet out = solverReads.getOut(bb).getValue();
			if (out != null)
			{
				for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
				{
					int arrayNum = outIt.next();
				
					ArrayID arr = arraysNumbering.getMappedObject(arrayNum);
				
					arrayReads.add(arr);
				}	
			}
			
			if (excludeChildThreads) pp2FutureArrayReadsECT.put(pp, arrayReads);
			else pp2FutureArrayReads.put(pp, arrayReads);			
		}
		
		// perform the analysis of array writes
		
		ArrayObjectAccesses aoWrites = new ArrayObjectAccesses(bwICFG, false, true, excludeChildThreads);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverWrites = aoWrites.analyze();

		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();

			int insnPos = WALAUtils.getInsnBytecodePos(bb.getNode(), ebb.getFirstInstructionIndex());
		
			String fullMethodSig = WALAUtils.getMethodSignature(bb.getNode().getMethod());
		
			ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);

			Set<ArrayID> arrayWrites = new HashSet<ArrayID>();
		
			IntSet out = solverWrites.getOut(bb).getValue();
			if (out != null)
			{
				for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
				{
					int arrayNum = outIt.next();
				
					ArrayID arr = arraysNumbering.getMappedObject(arrayNum);
				
					arrayWrites.add(arr);
				}	
			}
			
			if (excludeChildThreads) pp2FutureArrayWritesECT.put(pp, arrayWrites);
			else pp2FutureArrayWrites.put(pp, arrayWrites);
		}		
	}
	
	public static Set<ArrayID> getArrayReadsForProgramPoint(ProgramPoint pp, boolean withECT)
	{
		Set<ArrayID> arrayReads = null;
		
		if (withECT) arrayReads = pp2FutureArrayReadsECT.get(pp);
		else arrayReads = pp2FutureArrayReads.get(pp);
					
		// "null" for invalid program point or no data available
		return arrayReads;
	}

	public static Set<ArrayID> getArrayWritesForProgramPoint(ProgramPoint pp, boolean withECT)
	{
		Set<ArrayID> arrayWrites = null;
		
		if (withECT) arrayWrites = pp2FutureArrayWritesECT.get(pp);
		else arrayWrites = pp2FutureArrayWrites.get(pp);
					
		// "null" for invalid program point or no data available
		return arrayWrites;
	}
	
	public static void printArrayAccesses()
	{
		System.out.println("ARRAY ACCESSES");
		System.out.println("==============");
		
		// the key set is the same for both reads and writes
		Set<ProgramPoint> progPoints = new TreeSet<ProgramPoint>();
		progPoints.addAll(pp2FutureArrayReads.keySet());
		
		for (ProgramPoint pp : progPoints)
		{
			if ( ! Debug.isWatchedEntity(pp.methodSig) ) continue;
			
			Set<ArrayID> arrayReads = pp2FutureArrayReads.get(pp);
			Set<ArrayID> arrayWrites = pp2FutureArrayWrites.get(pp);			

			Set<ArrayID> arrayAccesses = new HashSet<ArrayID>();
			arrayAccesses.addAll(arrayReads);
			arrayAccesses.addAll(arrayWrites);
			
			System.out.println(pp.methodSig + ":" + pp.insnPos);
			
			for (ArrayID arr : arrayAccesses)
			{
				if ( ! Debug.isWatchedEntity(arr.heapObjectID.toString()) ) continue;
				
				if (arrayReads.contains(arr) && arrayWrites.contains(arr)) System.out.println("\t both: " + arr.toString());
				else if (arrayReads.contains(arr)) System.out.println("\t read: " + arr.toString());
				else if (arrayWrites.contains(arr)) System.out.println("\t write: " + arr.toString());
			}
		}
		
		System.out.println("");
	}
	
	private static void createArrayObjectsNumbering(Graph<BasicBlockInContext<IExplodedBasicBlock>> icfg)
	{
		arraysNumbering = new MutableMapping<ArrayID>(new ArrayID[1]);
		
		for (BasicBlockInContext<IExplodedBasicBlock> bb : icfg)
		{
			CGNode mthNode = bb.getNode();
				
			IR methodIR = mthNode.getIR();
				
			if (methodIR == null) continue;

			int firstInsnIdx = bb.getFirstInstructionIndex();
			int lastInsnIdx = bb.getLastInstructionIndex();
   
			// basic block without instructions
			if ((firstInsnIdx < 0) || (lastInsnIdx < 0)) continue;

			SSAInstruction[] instructions = methodIR.getInstructions();

			for (int i = firstInsnIdx; i <= lastInsnIdx; i++) 
			{
				SSAInstruction insn = instructions[i];

				if (insn instanceof SSAArrayReferenceInstruction) 
				{
					SSAArrayReferenceInstruction arrInsn = (SSAArrayReferenceInstruction) insn;
						
					try
					{
						Set<ArrayID> arrayIDs = ArrayAccessCodeInfo.getArraysAccessedByInsn(mthNode, arrInsn);
						for (ArrayID aid : arrayIDs) arraysNumbering.add(aid);
					}
					catch (Exception ex) { ex.printStackTrace(); }						
				}
			}
		}
	}


	static class ArrayObjectAccesses
	{
		private Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG;
		
		private boolean considerReads;
		private boolean considerWrites;
		
		private boolean excludeChildThreads;
		
		public ArrayObjectAccesses(Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG, boolean reads, boolean writes, boolean ect)
		{
			this.bwICFG = bwICFG;
			
			this.considerReads = reads;
			this.considerWrites = writes;
			
			this.excludeChildThreads = ect;
		}

		
		class TransferFunctionsAO implements ITransferFunctionProvider<BasicBlockInContext<IExplodedBasicBlock>, BitVectorVariable> 
		{
			public AbstractMeetOperator<BitVectorVariable> getMeetOperator() 
			{
				return BitVectorUnion.instance();
			}

			public UnaryOperator<BitVectorVariable> getEdgeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> src, BasicBlockInContext<IExplodedBasicBlock> dst) 
			{
				// we do not propagate through return-to-exit edges
				if (dst.getDelegate().isExitBlock()) 
				{
					return BitVectorKillAll.getInstance();
				}
				
				if (excludeChildThreads)
				{
					// in this configuration we do not propagate any data flow facts over the calls of java.lang.Thread.start()
					
					SSAInstruction dstInsn = dst.getDelegate().getInstruction();
					
					if (WALAUtils.isThreadStartCall(dstInsn) && src.isEntryBlock()) return BitVectorKillAll.getInstance();
				}
				
				// default
				return BitVectorIdentity.instance();
			}
	
			public UnaryOperator<BitVectorVariable> getNodeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> bb) 
			{
				IExplodedBasicBlock ebb = bb.getDelegate();
				
				SSAInstruction insn = ebb.getInstruction();

				if (insn instanceof SSAArrayReferenceInstruction) 
				{
					SSAArrayReferenceInstruction arrInsn = (SSAArrayReferenceInstruction) insn;
					
					if ((considerReads && (arrInsn instanceof SSAArrayLoadInstruction)) || (considerWrites && (arrInsn instanceof SSAArrayStoreInstruction)))
					{
						// this must be empty -> we do not need to kill anything
						BitVector kill = new BitVector();

						BitVector gen = new BitVector();

						Set<ArrayID> arrayIDs = ArrayAccessCodeInfo.getArraysAccessedByInsn(bb.getNode(), arrInsn);							

						try
						{
							for (ArrayID aid : arrayIDs)
							{
								int arrayNum = arraysNumbering.getMappedIndex(aid);
								
								gen.set(arrayNum);
							}
						}
						catch (Exception ex) { ex.printStackTrace(); }
						
						return new BitVectorKillGen(kill, gen);
					}
				}
				
				// identity function for all other cases and instructions
				return BitVectorIdentity.instance();
			}
			
			public boolean hasEdgeTransferFunctions() 
			{
				return true;
			}
			
			public boolean hasNodeTransferFunctions() 
			{
				return true;
			}
		}
	
		public BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> analyze() 
		{
			BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, ArrayID> framework = new BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, ArrayID>(bwICFG, new TransferFunctionsAO(), arraysNumbering);
			
			BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solver = new BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>>(framework);

			try
			{
				solver.solve(null);
			}
			catch (Exception ex) { ex.printStackTrace(); }
			
			return solver;
		}
	}
}
