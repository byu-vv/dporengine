/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.util;

import java.util.List;
import java.util.ArrayList;


public class TreeNode<T>
{
	public T item;
	public List<TreeNode<T>> children;
	
	public TreeNode(T item)
	{
		this.item = item;
		this.children = null;
	}

	public boolean isEmpty()
	{
		if (this.item == null)
		{
			if (this.children == null) return true;
			if (this.children.isEmpty()) return true;
		}
		
		return false;
	}
	
	public boolean hasChildren()
	{
		if (this.children == null) return false;
		if (this.children.isEmpty()) return false;
		
		return true;
	}
}

