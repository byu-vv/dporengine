/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.bytecode.interpreter;

import java.util.LinkedList;


public class InterpretationContext
{
	private LinkedList<Expression> exprStack;
	
	
	public void resetForCurrentMethod(String mthSig)
	{
		exprStack = new LinkedList<Expression>();
	}
	
	public void addExprToStack(Expression expr)
	{
		exprStack.addFirst(expr);
	}
	
	public void insertExprToStack(Expression expr, int depth)
	{
		exprStack.add(depth, expr);
	}
	
	public Expression removeExprFromStack()
	{
		return exprStack.removeFirst();
	}
	
	public Expression getExprFromStack()
	{
		return exprStack.getFirst();
	}

	public Expression getExprFromStack(int offset)
	{
		return exprStack.get(offset);
	}

	public void printExprStack(String prefix)
	{
		System.out.println(prefix);
		
		for (int i = 0; i < exprStack.size(); i++)
		{
			Expression expr = exprStack.get(i);

			if (i == 0) System.out.println("top:    " + expr);
			else if (i == exprStack.size() - 1) System.out.println("bot:    " + expr);
			else System.out.println("        " + expr);
		}
	}
}

