/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.jpf.dpor;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.SystemState;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.choice.ThreadChoiceFromSet;

import cz.cuni.mff.d3s.stdynpor.jpf.ArraySharednessPolicy;
import cz.cuni.mff.d3s.stdynpor.jpf.DynamicThreadChoice;


/**
 * We use our custom thread choice generator only at shared field accesses and shared array element accesses.
 * It preserves all choices that JPF would create at other bytecode instructions (thread start, monitor enter).
 */
// Inherits from the ArraySharednessPolicy to ensure proper setup of thread choices at shared array element accesses.
public class DynamicArraySharednessPolicy extends ArraySharednessPolicy
{
	private int nextID;
	
	
	public DynamicArraySharednessPolicy(Config config)
	{
		super(config);
		
		nextID = 1;
	}

	protected ChoiceGenerator<ThreadInfo> getRunnableCG(String id, ThreadInfo curTh)
	{
		// no CG if we are in a atomic section
		if (vm.getSystemState().isAtomic()) return null;
		
		ThreadInfo[] runThSet = getRunnables(curTh.getApplicationContext());
		if (runThSet.length <= 1) return null; // field access does not block, i.e. the current thread is always runnable
    
		return new DynamicThreadChoice(nextID++, runThSet, curTh);
	}
}

