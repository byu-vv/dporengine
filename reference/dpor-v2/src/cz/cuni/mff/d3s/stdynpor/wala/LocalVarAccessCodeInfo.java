/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.wala;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.shrikeBT.IInstruction;
import com.ibm.wala.shrikeBT.Instruction;
import com.ibm.wala.shrikeBT.LoadInstruction;
import com.ibm.wala.shrikeBT.StoreInstruction;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.LocalVarName;


public class LocalVarAccessCodeInfo
{
	public static Set<LocalVarName> getLocalVarNamesForInsn(IMethod mth, int ssaInsnIndex, boolean considerReads, boolean considerWrites)
	{
		Set<LocalVarName> localVars = new HashSet<LocalVarName>();
		
		try
		{
			IBytecodeMethod bcMth = (IBytecodeMethod) mth;
			
			IInstruction[] bcInstructions = bcMth.getInstructions();
			
			Instruction bcInsn = (Instruction) bcInstructions[ssaInsnIndex];
	
			// we use artificial local variable names ("localX")
			
			if ((bcInsn instanceof LoadInstruction) && considerReads)
			{
				LoadInstruction loadInsn = (LoadInstruction) bcInsn;
				
				LocalVarName lv = LocalVarName.createFromIndex(loadInsn.getVarIndex());
				
				localVars.add(lv);
			}
			
			if ((bcInsn instanceof StoreInstruction) && considerWrites)
			{
				StoreInstruction storeInsn = (StoreInstruction) bcInsn;
				
				LocalVarName lv = LocalVarName.createFromIndex(storeInsn.getVarIndex());
				
				localVars.add(lv);
			}
		}
		catch (Exception ex) 
		{
			ex.printStackTrace(); 
		}
		
		return localVars;
	}
}

