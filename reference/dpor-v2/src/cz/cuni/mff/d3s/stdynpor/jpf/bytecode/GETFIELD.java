/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.jpf.bytecode;

import java.util.List;
import java.util.Set;

import gov.nasa.jpf.JPFException;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ThreadList;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.Scheduler;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.AnalysisResultProcessor;
import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.jpf.JPFUtils;


public class GETFIELD extends gov.nasa.jpf.jvm.bytecode.GETFIELD 
{
	public GETFIELD(String fieldName, String classType, String fieldDescriptor)
	{
		super(fieldName, classType, fieldDescriptor);
	}

	public Instruction execute(ThreadInfo ti) 
	{
		StackFrame frame = ti.getModifiableTopFrame();
    
		int objRef = frame.peek();
		lastThis = objRef;
		if (objRef == MJIEnv.NULL) return ti.createAndThrowException("java.lang.NullPointerException", "referencing field '" + fname + "' on null object");

		ElementInfo ei = ti.getElementInfo(objRef);
		
		FieldInfo fi = getFieldInfo();
		if (fi == null) return ti.createAndThrowException("java.lang.NoSuchFieldError", "referencing unknown field '" + fname + "' in " + ei);

		String className = fi.getClassInfo().getName();
		String fieldName = fi.getName();
			
		MethodInfo mi = getMethodInfo();
		String curMethodSig = JPFUtils.getMethodSignature(mi);

		Scheduler scheduler = ti.getScheduler();

		if ( Configuration.usingFieldAccess && ( ! Configuration.usingDynamicPOR ) )
		{
			if (scheduler.canHaveSharedObjectCG(ti, this, ei, fi))
			{
				ei = scheduler.updateObjectSharedness(ti, ei, fi); 
			
				if (AnalysisResultProcessor.existsConflictingFieldWrite(ti, curMethodSig, getPosition(), className, fieldName, false, objRef))
				{
					if (scheduler.setsSharedObjectCG(ti, this, ei, fi)) return this;
				}
			}
		}
		else if (Configuration.usingDynamicPOR)
		{
			// we can safely ignore field accesses to heap objects that are not reachable from multiple threads
			if (scheduler.canHaveSharedObjectCG(ti, this, ei, fi))
			{
				ei = scheduler.updateObjectSharedness(ti, ei, fi); 

				boolean skipCG = false;
				
				if (Configuration.usingFieldAccess)
				{		
					if ( ! AnalysisResultProcessor.existsConflictingFieldWrite(ti, curMethodSig, getPosition(), className, fieldName, false, objRef) ) skipCG = true;
				}
				
				if ( ! skipCG )
				{
					if (scheduler.setsSharedObjectCG(ti, this, ei, fi)) return this;
				}
			}			
		}
		
		frame.pop();
		
		Object attr = ei.getFieldAttr(fi);
		
		if (fi.getStorageSize() == 1)
		{
			int ival = ei.get1SlotField(fi);
			lastValue = ival;
			
			if (fi.isReference()) frame.pushRef(ival);				
			else frame.push(ival);
			
			if (attr != null) frame.setOperandAttr(attr);			
		} 
		else
		{
			long lval = ei.get2SlotField(fi);
			lastValue = lval;
			
			frame.pushLong(lval);
			
			if (attr != null) frame.setLongOperandAttr(attr);
		}
		
		return getNext(ti); 
	}
}
