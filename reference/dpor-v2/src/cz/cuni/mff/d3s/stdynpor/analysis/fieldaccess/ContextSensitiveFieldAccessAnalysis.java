/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.analysis.fieldaccess;

import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;

import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.cfg.ExplodedInterproceduralCFG;
import com.ibm.wala.ipa.cfg.BasicBlockInContext;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.analysis.ExplodedControlFlowGraph;
import com.ibm.wala.ssa.analysis.IExplodedBasicBlock;
import com.ibm.wala.util.intset.IntSet; 
import com.ibm.wala.util.intset.IntIterator;
import com.ibm.wala.util.graph.impl.GraphInverter;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.dataflow.graph.BitVectorSolver;
import com.ibm.wala.dataflow.graph.BitVectorFramework;

import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.FieldID;
import cz.cuni.mff.d3s.stdynpor.wala.WALAUtils;


public class ContextSensitiveFieldAccessAnalysis extends FieldAccessAnalysisBase
{
	public static void analyzeProgram(CallGraph clGraph, ExplodedInterproceduralCFG icfg, boolean excludeChildThreads) throws Exception
	{
		// create the backwards-oriented control-flow graph of the program
		Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG = GraphInverter.invert(icfg);

		// prepare common data structures
		
		createFieldsNumbering(bwICFG);

		// perform the analysis of field reads
		
		FieldAccessesBR pfaReadsBR = new FieldAccessesBR(bwICFG, true, false, excludeChildThreads);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverReadsBR = pfaReadsBR.analyze();

		// collect analysis results: create sets of field accesses for all program points (instructions) 
		// the set for a program point will contain all field names for which there is '1' in the bit vector associated with the basic block (cfg node)
		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();

			int insnPos = WALAUtils.getInsnBytecodePos(bb.getNode(), ebb.getFirstInstructionIndex());
		
			String fullMethodSig = WALAUtils.getMethodSignature(bb.getNode().getMethod());
		
			ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);

			Set<FieldID> fieldReads = new HashSet<FieldID>();
		
			IntSet out = solverReadsBR.getOut(bb).getValue();
			if (out != null)
			{
				for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
				{
					int fieldNum = outIt.next();
				
					FieldID fid = getFieldForNum(fieldNum);
				
					fieldReads.add(fid);
				}	
			}
			
			if (excludeChildThreads) pp2FutureFieldReadsECT.put(pp, fieldReads);
			else pp2FutureFieldReads.put(pp, fieldReads);
		}
		
		// perform the analysis of field writes
		
		FieldAccessesBR pfaWritesBR = new FieldAccessesBR(bwICFG, false, true, excludeChildThreads);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverWritesBR = pfaWritesBR.analyze();

		// collect analysis results: create sets of field accesses for all program points (instructions) 
		// the set for a program point will contain all field names for which there is '1' in the bit vector associated with the basic block (cfg node)
		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();

			int insnPos = WALAUtils.getInsnBytecodePos(bb.getNode(), ebb.getFirstInstructionIndex());
		
			String fullMethodSig = WALAUtils.getMethodSignature(bb.getNode().getMethod());
		
			ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);

			Set<FieldID> fieldWrites = new HashSet<FieldID>();
		
			IntSet out = solverWritesBR.getOut(bb).getValue();
			if (out != null)
			{
				for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
				{
					int fieldNum = outIt.next();
				
					FieldID fid = getFieldForNum(fieldNum);
				
					fieldWrites.add(fid);
				}	
			}
			
			if (excludeChildThreads) pp2FutureFieldWritesECT.put(pp, fieldWrites);
			else pp2FutureFieldWrites.put(pp, fieldWrites);
		}
	}

}

