/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.data;

import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;


public class ArrayElementIndex
{
	// SN means "suffix notation" (Reverse Polish Notation)
	// components: arithmetic operators, integer constants, array element selection operator '[]', symbols 'Fx' indicating field access paths ('x' representing index to the list of field access paths), symbols 'Lx' indicating local variables, and symbol 'U' represents unknown index (e.g., method return value)
	public List<String> exprSN;
	
	public List<LocalVarName> localVariables;
	
	public List<FieldAccessPath> fieldPaths;
	
	public Set<FieldID> allFields;
	
	public Integer numericConstant;

	private int hc;
	private String strRep;
	
	
	public ArrayElementIndex()
	{
		exprSN = new ArrayList<String>();
		
		localVariables = new ArrayList<LocalVarName>();
		fieldPaths = new ArrayList<FieldAccessPath>();
		
		allFields = new HashSet<FieldID>();
		
		numericConstant = null;
		
		hc = 0;
		strRep = null;
	}
	
	public boolean containsLocalVariables()
	{
		int countLocVars = this.localVariables.size();
		
		return (countLocVars > 0);
	}
	
	public boolean isNumericConstant()
	{
		return (localVariables.isEmpty() && allFields.isEmpty() && (numericConstant != null));
	}
	
	public Integer getIntegerValue()
	{
		return numericConstant;		
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof ArrayElementIndex) ) return false;
		
		ArrayElementIndex other = (ArrayElementIndex) obj;
		
		if ( ! this.exprSN.equals(other.exprSN) ) return false;
		if ( ! this.localVariables.equals(other.localVariables) ) return false;
		if ( ! this.fieldPaths.equals(other.fieldPaths) ) return false;
		if ( ! this.allFields.equals(other.allFields) ) return false;
		if ( ! this.numericConstant.equals(other.numericConstant) ) return false;
		
		return true;
	}
	
	public int hashCode()
	{
		if (hc == 0)
		{
			hc = hc * 31 + this.exprSN.hashCode();
			hc = hc * 31 + this.localVariables.hashCode();
			hc = hc * 31 + this.fieldPaths.hashCode();
			hc = hc * 31 + this.allFields.hashCode();
			hc = hc * 31 + this.numericConstant.hashCode();
		}
		
		return hc;
	}
	
	public String toString()
	{
		if (strRep == null)
		{
			StringBuffer strBuf = new StringBuffer();
			
			for (String snElem : exprSN)
			{
				strBuf.append(" ");
				
				// local variable
				if (snElem.charAt(0) == 'L')
				{
					int lvIdx = Integer.parseInt(snElem.substring(1));
					LocalVarName lvn = localVariables.get(lvIdx - 1);
					strBuf.append(lvn.toString());
					continue;
				}
				
				// field access path
				if (snElem.charAt(0) == 'F')
				{
					int fapIdx = Integer.parseInt(snElem.substring(1));
					FieldAccessPath fap = fieldPaths.get(fapIdx - 1);
					strBuf.append(fap.toString());
					continue;
				}
				
				// unknown
				if (snElem.charAt(0) == 'U')
				{
					strBuf.append("<unknown>");
					continue;
				}
				
				// other cases: integer constant, arithmetic operator, array element selection
				strBuf.append(snElem);				
			}
			
			strRep = strBuf.toString().trim();
		}
		
		return strRep;
	}
}
