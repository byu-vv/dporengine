/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.analysis.fieldaccess;

import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.List;

import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.impl.Everywhere;
import com.ibm.wala.ipa.cfg.ExplodedInterproceduralCFG;
import com.ibm.wala.ipa.cfg.BasicBlockInContext;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAFieldAccessInstruction;
import com.ibm.wala.ssa.SSAGetInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.analysis.ExplodedControlFlowGraph;
import com.ibm.wala.ssa.analysis.IExplodedBasicBlock;
import com.ibm.wala.util.intset.BitVector;
import com.ibm.wala.util.intset.OrdinalSetMapping;
import com.ibm.wala.util.intset.MutableMapping;
import com.ibm.wala.util.intset.IntSet; 
import com.ibm.wala.util.intset.IntIterator;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.UnaryOperator;
import com.ibm.wala.dataflow.graph.BitVectorSolver;
import com.ibm.wala.dataflow.graph.BitVectorFramework;
import com.ibm.wala.dataflow.graph.AbstractMeetOperator;
import com.ibm.wala.dataflow.graph.BitVectorKillGen;
import com.ibm.wala.dataflow.graph.BitVectorUnion;
import com.ibm.wala.dataflow.graph.BitVectorIdentity;
import com.ibm.wala.dataflow.graph.ITransferFunctionProvider;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.Debug;
import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.AllocationSite;
import cz.cuni.mff.d3s.stdynpor.data.ClassName;
import cz.cuni.mff.d3s.stdynpor.data.FieldID;
import cz.cuni.mff.d3s.stdynpor.data.LocalVarID;
import cz.cuni.mff.d3s.stdynpor.bytecode.ByteCodeUtils;
import cz.cuni.mff.d3s.stdynpor.wala.WALAUtils;
import cz.cuni.mff.d3s.stdynpor.wala.PointerAnalysisData;
import cz.cuni.mff.d3s.stdynpor.wala.FieldAccessCodeInfo;
import cz.cuni.mff.d3s.stdynpor.wala.dataflow.BitVectorKillAll;


public class FieldAccessAnalysisBase
{
	// map from program point (method signature and instruction position) to the set of field read accesses (getfield, getstatic)
	protected static Map<ProgramPoint, Set<FieldID>> pp2FutureFieldReads; 
	
	// map from program point (method signature and instruction position) to the set of field write accesses (putfield, putstatic)
	protected static Map<ProgramPoint, Set<FieldID>> pp2FutureFieldWrites;

	// results when child threads are excluded
	protected static Map<ProgramPoint, Set<FieldID>> pp2FutureFieldReadsECT; 
	protected static Map<ProgramPoint, Set<FieldID>> pp2FutureFieldWritesECT;
	
	// mapping between fields and integer numbers (used in bitvectors)
	private static OrdinalSetMapping<FieldID> fieldsNumbering;

	
	static
	{
		pp2FutureFieldReads = new LinkedHashMap<ProgramPoint, Set<FieldID>>();
		pp2FutureFieldWrites = new LinkedHashMap<ProgramPoint, Set<FieldID>>();
		
		pp2FutureFieldReadsECT = new LinkedHashMap<ProgramPoint, Set<FieldID>>();
		pp2FutureFieldWritesECT = new LinkedHashMap<ProgramPoint, Set<FieldID>>();
		
		fieldsNumbering = null;
	}


	public static Set<FieldID> getFieldReadsForProgramPoint(ProgramPoint pp, boolean withECT)
	{
		Set<FieldID> fieldReads = null;
		
		if (withECT) fieldReads = pp2FutureFieldReadsECT.get(pp);
		else fieldReads = pp2FutureFieldReads.get(pp);
		
		// "null" for invalid program point or no data available
		return fieldReads;
	}

	public static Set<FieldID> getFieldWritesForProgramPoint(ProgramPoint pp, boolean withECT)
	{
		Set<FieldID> fieldWrites = null;
		
		if (withECT) fieldWrites = pp2FutureFieldWritesECT.get(pp);
		else fieldWrites = pp2FutureFieldWrites.get(pp);

		// "null" for invalid program point or no data available
		return fieldWrites;
	}

	public static Map<ProgramPoint, Set<FieldID>> getFutureFieldAccessesData(boolean withECT)
	{
		Map<ProgramPoint, Set<FieldID>> pp2FutureFieldAccesses = new HashMap<ProgramPoint, Set<FieldID>>();

		// the key set is the same for both reads and writes (also w/o ECT)
		Set<ProgramPoint> progPoints = pp2FutureFieldReads.keySet();
		
		for (ProgramPoint pp : progPoints)
		{		
			Set<FieldID> fieldAccesses = new HashSet<FieldID>();
			
			if (withECT)
			{
				fieldAccesses.addAll(pp2FutureFieldReadsECT.get(pp));
				fieldAccesses.addAll(pp2FutureFieldWritesECT.get(pp));
			}
			else
			{			
				fieldAccesses.addAll(pp2FutureFieldReads.get(pp));
				fieldAccesses.addAll(pp2FutureFieldWrites.get(pp));
			}
			
			pp2FutureFieldAccesses.put(pp, fieldAccesses);
		}
		
		return pp2FutureFieldAccesses;
	}
	
	public static Map<ProgramPoint, Set<FieldID>> getFutureFieldReadsData(boolean withECT)
	{
		if (withECT) return pp2FutureFieldReadsECT;
		else return pp2FutureFieldReads;
	}
	
	public static Map<ProgramPoint, Set<FieldID>> getFutureFieldWritesData(boolean withECT)
	{
		if (withECT) return pp2FutureFieldWritesECT;
		else return pp2FutureFieldWrites;
	}
	
	public static void printFieldAccesses(boolean firstMthInsnOnly)
	{
		System.out.println("FIELD ACCESSES");
		System.out.println("==============");
		
		// the key set is the same for both reads and writes
		Set<ProgramPoint> progPoints = new TreeSet<ProgramPoint>();
		progPoints.addAll(pp2FutureFieldReads.keySet());
		
		for (ProgramPoint pp : progPoints)
		{
			if (firstMthInsnOnly && (pp.insnPos != 0)) continue;
			
			if ( ! Debug.isWatchedEntity(pp.methodSig) ) continue;
			
			Set<FieldID> fieldReads = pp2FutureFieldReads.get(pp);
			Set<FieldID> fieldWrites = pp2FutureFieldWrites.get(pp);			

			Set<FieldID> fieldAccesses = new HashSet<FieldID>();
			fieldAccesses.addAll(fieldReads);
			fieldAccesses.addAll(fieldWrites);
			
			System.out.println(pp.methodSig + ":" + pp.insnPos);
			
			for (FieldID fa : fieldAccesses)
			{
				if ( ! Debug.isWatchedEntity(fa.tgtObjectID.toString()) ) continue;
				
				if (fieldReads.contains(fa) && fieldWrites.contains(fa)) System.out.println("\t both: " + fa.toString());
				else if (fieldReads.contains(fa)) System.out.println("\t read: " + fa.toString());
				else if (fieldWrites.contains(fa)) System.out.println("\t write: " + fa.toString());
			}
		}
		
		System.out.println("");
	}
	
	protected static void createFieldsNumbering(Graph<BasicBlockInContext<IExplodedBasicBlock>> icfg)
	{
		fieldsNumbering = new MutableMapping<FieldID>(new FieldID[1]);
		
		for (BasicBlockInContext<IExplodedBasicBlock> bb : icfg)
		{
			CGNode mthNode = bb.getNode();
				
			IR methodIR = mthNode.getIR();
				
			if (methodIR == null) continue;

			int firstInsnIdx = bb.getFirstInstructionIndex();
			int lastInsnIdx = bb.getLastInstructionIndex();
   
			// basic block without instructions
			if ((firstInsnIdx < 0) || (lastInsnIdx < 0)) continue;

			SSAInstruction[] instructions = methodIR.getInstructions();

			for (int insnIndex = firstInsnIdx; insnIndex <= lastInsnIdx; insnIndex++) 
			{
				SSAInstruction insn = instructions[insnIndex];

				if (insn instanceof SSAFieldAccessInstruction) 
				{
					SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) insn;
						
					try
					{
						Set<FieldID> fieldIDs = FieldAccessCodeInfo.getFieldsForInsn(mthNode, faInsn, insnIndex);
						for (FieldID fid : fieldIDs) fieldsNumbering.add(fid);
					}
					catch (Exception ex) { ex.printStackTrace(); }						
				}
			}
		}
	}
	
	protected static FieldID getFieldForNum(int fieldNum)
	{
		return fieldsNumbering.getMappedObject(fieldNum);
	}
	
	
	static class FieldAccessCollector
	{
		protected Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG;
		
		protected boolean considerReads;
		protected boolean considerWrites;
		
		protected boolean excludeChildThreads;
			
		protected FieldAccessCollector(Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG, boolean reads, boolean writes, boolean ect)
		{
			this.bwICFG = bwICFG;
			
			this.considerReads = reads;
			this.considerWrites = writes;
			
			this.excludeChildThreads = ect;
		}
	}
	

	// collecting field accesses before return ("BR") from the method
	static class FieldAccessesBR extends FieldAccessCollector
	{		
		public FieldAccessesBR(Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG, boolean reads, boolean writes, boolean ect)
		{
			super(bwICFG, reads, writes, ect);
		}
		
		class TransferFunctionsFABR implements ITransferFunctionProvider<BasicBlockInContext<IExplodedBasicBlock>, BitVectorVariable> 
		{
			public AbstractMeetOperator<BitVectorVariable> getMeetOperator() 
			{
				return BitVectorUnion.instance();
			}

			public UnaryOperator<BitVectorVariable> getEdgeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> src, BasicBlockInContext<IExplodedBasicBlock> dst) 
			{
				// we do not propagate through return-to-exit edges				
				if (dst.getDelegate().isExitBlock()) 
				{
					return BitVectorKillAll.getInstance();
				}
				
				if (excludeChildThreads)
				{
					// in this configuration we do not propagate any data flow facts over the calls of java.lang.Thread.start()
					
					SSAInstruction dstInsn = dst.getDelegate().getInstruction();
					
					if (WALAUtils.isThreadStartCall(dstInsn) && src.isEntryBlock()) return BitVectorKillAll.getInstance();
				}

				// default
				return BitVectorIdentity.instance();
			}
	
			public UnaryOperator<BitVectorVariable> getNodeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> bb) 
			{
				IExplodedBasicBlock ebb = bb.getDelegate();
				
				SSAInstruction insn = ebb.getInstruction();

				if (insn instanceof SSAFieldAccessInstruction) 
				{
					SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) insn;
					
					if ((considerReads && (faInsn instanceof SSAGetInstruction)) || (considerWrites && (faInsn instanceof SSAPutInstruction)))
					{
						// this must be empty -> we do not need to kill anything
						BitVector kill = new BitVector();

						BitVector gen = new BitVector();

						Set<FieldID> fieldIDs = FieldAccessCodeInfo.getFieldsForInsn(bb.getNode(), faInsn, ebb.getFirstInstructionIndex());							

						try
						{
							for (FieldID fid : fieldIDs)
							{
								int fieldNum = fieldsNumbering.getMappedIndex(fid);
								
								gen.set(fieldNum);
							}							
						}
						catch (Exception ex) { ex.printStackTrace(); }
						
						return new BitVectorKillGen(kill, gen);
					}
				}
				
				// identity function for all other cases and instructions
				return BitVectorIdentity.instance();
			}
			
			public boolean hasEdgeTransferFunctions() 
			{
				return true;
			}
			
			public boolean hasNodeTransferFunctions() 
			{
				return true;
			}
		}
	
		public BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> analyze() 
		{
			BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, FieldID> framework = new BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, FieldID>(bwICFG, new TransferFunctionsFABR(), fieldsNumbering);
			
			BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solver = new BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>>(framework);

			try
			{
				solver.solve(null);
			}
			catch (Exception ex) { ex.printStackTrace(); }
			
			return solver;
		}
	}


	// collecting field accesses in the rest of thread lifetime ("RT")
	// input: field accesses occuring inside each method before return
	static class FieldAccessesRT extends FieldAccessCollector
	{
		// map from method signature to field accesses before the method return
		private Map<String, Set<FieldID>> mthSig2FABR;
		
		public FieldAccessesRT(Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG, boolean reads, boolean writes, boolean ect, Map<String, Set<FieldID>> mthSig2FABR)
		{
			super(bwICFG, reads, writes, ect);
			
			this.mthSig2FABR = mthSig2FABR;
		}
		
		class TransferFunctionsFART implements ITransferFunctionProvider<BasicBlockInContext<IExplodedBasicBlock>, BitVectorVariable> 
		{
			public AbstractMeetOperator<BitVectorVariable> getMeetOperator() 
			{
				return BitVectorUnion.instance();
			}

			public UnaryOperator<BitVectorVariable> getEdgeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> src, BasicBlockInContext<IExplodedBasicBlock> dst) 
			{
				if (dst.getDelegate().isExitBlock()) 
				{
					// return-to-exit edges propagate identical set

					return BitVectorIdentity.instance();
				}
				
				if (src.getDelegate().isEntryBlock())
				{
					// call-to-entry edges propagate only field accesses that can happen before return from the callee method
					// we need to kill everything else

					BitVector kill = new BitVector(fieldsNumbering.getSize()+1);
					kill.setAll();
					kill.clear(0);

					BitVector gen = new BitVector();

					try
					{
						String fullMethodSig = WALAUtils.getMethodSignature(src.getNode().getMethod());
						
						Set<FieldID> mthAccessesBR = mthSig2FABR.get(fullMethodSig);					
						if (mthAccessesBR == null) mthAccessesBR = new HashSet<FieldID>();

						for (FieldID fid : mthAccessesBR)
						{
							int fieldNum = fieldsNumbering.getMappedIndex(fid);
							
							gen.set(fieldNum);
							kill.clear(fieldNum);
						}
					}
					catch (Exception ex) { ex.printStackTrace(); }
					
					return new BitVectorKillGen(kill, gen);
				}
				
				if (excludeChildThreads)
				{
					// in this configuration we do not propagate any data flow facts over the calls of java.lang.Thread.start()
					
					SSAInstruction dstInsn = dst.getDelegate().getInstruction();
					
					if (WALAUtils.isThreadStartCall(dstInsn) && src.isEntryBlock()) return BitVectorKillAll.getInstance();
				}
				
				// default
				return BitVectorIdentity.instance();
			}
			
			public UnaryOperator<BitVectorVariable> getNodeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> bb) 
			{
				IExplodedBasicBlock ebb = bb.getDelegate();
				
				SSAInstruction insn = ebb.getInstruction();

				if (insn instanceof SSAFieldAccessInstruction) 
				{
					SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) insn;
					
					if ((considerReads && (faInsn instanceof SSAGetInstruction)) || (considerWrites && (faInsn instanceof SSAPutInstruction)))
					{
						// this must be empty -> we do not need to kill anything
						BitVector kill = new BitVector();

						BitVector gen = new BitVector();

						try
						{
							Set<FieldID> fieldIDs = FieldAccessCodeInfo.getFieldsForInsn(bb.getNode(), faInsn, ebb.getFirstInstructionIndex());
							
							for (FieldID fid : fieldIDs)
							{
								int fieldNum = fieldsNumbering.getMappedIndex(fid);
								
								gen.set(fieldNum);
							}							
						}
						catch (Exception ex) { ex.printStackTrace(); }
						
						return new BitVectorKillGen(kill, gen);
					}
				}
	
				// identity function for all other cases and instructions
				return BitVectorIdentity.instance();
			}
			
			public boolean hasEdgeTransferFunctions() 
			{
				return true;
			}
			
			public boolean hasNodeTransferFunctions() 
			{
				return true;
			}
		}
	
		public BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> analyze() 
		{
			BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, FieldID> framework = new BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, FieldID>(bwICFG, new TransferFunctionsFART(), fieldsNumbering);
			
			BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solver = new BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>>(framework);

			try
			{
				solver.solve(null);
			}
			catch (Exception ex) { ex.printStackTrace(); }
			
			return solver;
		}
	}
	
	
	// collecting field accesses in one phase ("OP") 
	static class FieldAccessesOP extends FieldAccessCollector 
	{
		public FieldAccessesOP(Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG, boolean reads, boolean writes, boolean ect)
		{
			super(bwICFG, reads, writes, ect);
		}
		
		class TransferFunctionsFAOP implements ITransferFunctionProvider<BasicBlockInContext<IExplodedBasicBlock>, BitVectorVariable> 
		{
			public AbstractMeetOperator<BitVectorVariable> getMeetOperator() 
			{
				return BitVectorUnion.instance();
			}

			public UnaryOperator<BitVectorVariable> getEdgeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> src, BasicBlockInContext<IExplodedBasicBlock> dst) 
			{
				if (excludeChildThreads)
				{
					// in this configuration we do not propagate any data flow facts over the calls of java.lang.Thread.start()
					
					SSAInstruction dstInsn = dst.getDelegate().getInstruction();
					
					if (WALAUtils.isThreadStartCall(dstInsn) && src.isEntryBlock()) return BitVectorKillAll.getInstance();
				}

				// default				
				return BitVectorIdentity.instance();
			}			
			
			public UnaryOperator<BitVectorVariable> getNodeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> bb) 
			{
				IExplodedBasicBlock ebb = bb.getDelegate();
				
				SSAInstruction insn = ebb.getInstruction();
				
				if (insn instanceof SSAFieldAccessInstruction) 
				{
					SSAFieldAccessInstruction faInsn = (SSAFieldAccessInstruction) insn;
					
					if ((considerReads && (faInsn instanceof SSAGetInstruction)) || (considerWrites && (faInsn instanceof SSAPutInstruction)))
					{
						// this must be empty -> we do not need to kill anything
						BitVector kill = new BitVector();

						BitVector gen = new BitVector();

						Set<FieldID> fieldIDs = FieldAccessCodeInfo.getFieldsForInsn(bb.getNode(), faInsn, ebb.getFirstInstructionIndex());							

						try
						{
							for (FieldID fid : fieldIDs)
							{
								int fieldNum = fieldsNumbering.getMappedIndex(fid);
								
								gen.set(fieldNum);
							}							
						}
						catch (Exception ex) { ex.printStackTrace(); }
						
						return new BitVectorKillGen(kill, gen);
					}
				}
				
				// identity function for all other cases and instructions
				return BitVectorIdentity.instance();
			}
			
			public boolean hasEdgeTransferFunctions() 
			{
				return true;
			}
			
			public boolean hasNodeTransferFunctions() 
			{
				return true;
			}
		}
	
		public BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> analyze() 
		{
			BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, FieldID> framework = new BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, FieldID>(bwICFG, new TransferFunctionsFAOP(), fieldsNumbering);
			
			BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solver = new BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>>(framework);

			try
			{
				solver.solve(null);
			}
			catch (Exception ex) { ex.printStackTrace(); }
			
			return solver;
		}
	}

}

