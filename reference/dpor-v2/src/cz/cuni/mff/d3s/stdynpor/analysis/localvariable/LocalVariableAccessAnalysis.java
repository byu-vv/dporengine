/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.analysis.localvariable;

import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.List;

import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.impl.Everywhere;
import com.ibm.wala.ipa.cfg.ExplodedInterproceduralCFG;
import com.ibm.wala.ipa.cfg.BasicBlockInContext;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.analysis.ExplodedControlFlowGraph;
import com.ibm.wala.ssa.analysis.IExplodedBasicBlock;
import com.ibm.wala.util.intset.BitVector;
import com.ibm.wala.util.intset.OrdinalSetMapping;
import com.ibm.wala.util.intset.MutableMapping;
import com.ibm.wala.util.intset.IntSet; 
import com.ibm.wala.util.intset.IntIterator;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.util.graph.impl.GraphInverter;
import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.UnaryOperator;
import com.ibm.wala.dataflow.graph.BitVectorSolver;
import com.ibm.wala.dataflow.graph.BitVectorFramework;
import com.ibm.wala.dataflow.graph.AbstractMeetOperator;
import com.ibm.wala.dataflow.graph.BitVectorKillGen;
import com.ibm.wala.dataflow.graph.BitVectorUnion;
import com.ibm.wala.dataflow.graph.BitVectorIdentity;
import com.ibm.wala.dataflow.graph.ITransferFunctionProvider;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.Debug;
import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.LocalVarID;
import cz.cuni.mff.d3s.stdynpor.data.LocalVarName;
import cz.cuni.mff.d3s.stdynpor.bytecode.ByteCodeUtils;
import cz.cuni.mff.d3s.stdynpor.wala.WALAUtils;
import cz.cuni.mff.d3s.stdynpor.wala.LocalVarAccessCodeInfo;
import cz.cuni.mff.d3s.stdynpor.wala.dataflow.BitVectorKillAll;


public class LocalVariableAccessAnalysis
{
	// for each method (signature), a map from program points to sets of local variables possibly read in the rest of method execution (future)
	private static Map<String, Map<ProgramPoint, Set<LocalVarName>>> mth2FutureVarReads;

	// for each method (signature), a map from program points to sets of local variables possibly written in the rest of method execution (future)
	private static Map<String, Map<ProgramPoint, Set<LocalVarName>>> mth2FutureVarWrites;


	static
	{
		mth2FutureVarReads = new HashMap<String, Map<ProgramPoint, Set<LocalVarName>>>();
		mth2FutureVarWrites = new HashMap<String, Map<ProgramPoint, Set<LocalVarName>>>();
	}


	public static void analyzeProgram(CallGraph clGraph) throws Exception
	{
		// for each program point P find all future accesses (reads, writes) to local variables performed in the future during execution of the method M that contains P

		// method signatures
		Set<String> processedMethods = new HashSet<String>();
		
		// analyze methods reachable in the call graph
		for (Iterator<CGNode> cgnIt = clGraph.iterator(); cgnIt.hasNext(); )
		{
			IMethod mth = cgnIt.next().getMethod();
		
			// we skip native methods and abstract methods
			if (mth.isNative() || mth.isAbstract()) continue;

			// fake root method
			if ( ! (mth instanceof IBytecodeMethod) ) continue;
			
			String mthSig = WALAUtils.getMethodSignature(mth);
			
			if (processedMethods.contains(mthSig)) continue;
			processedMethods.add(mthSig);

			IR ir = WALAUtils.cache.getIRFactory().makeIR(mth, Everywhere.EVERYWHERE, SSAOptions.defaultOptions());
			
			// create the control-flow graph of the given method
			ExplodedControlFlowGraph cfg = ExplodedControlFlowGraph.make(ir);

			// create the backwards-oriented control-flow graph of the method
			Graph<IExplodedBasicBlock> bwCFG = GraphInverter.invert(cfg);

			// perform the analysis of local variable reads
		
			MethodVariableAccesses mvaReads = new MethodVariableAccesses(mth, bwCFG, ir, true, false);
			BitVectorSolver<IExplodedBasicBlock> solverReads = mvaReads.analyze();

			Map<ProgramPoint, Set<LocalVarName>> pp2FutureVarReads = new HashMap<ProgramPoint, Set<LocalVarName>>();

			// collect analysis results (sets of local variables for all program points)
			for (IExplodedBasicBlock ebb : bwCFG)
			{
				int insnPos = WALAUtils.getInsnBytecodePos(mth, ebb.getFirstInstructionIndex());
			
				ProgramPoint pp = new ProgramPoint(mthSig, insnPos);
				
				Set<LocalVarName> futVarReads = new HashSet<LocalVarName>();
			
				IntSet out = solverReads.getOut(ebb).getValue();
				if (out != null)
				{
					for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
					{
						int lvNum = outIt.next();
					
						LocalVarName lv = mvaReads.getLocalVarForNum(lvNum);
						
						futVarReads.add(lv);
					}
				}
				
				pp2FutureVarReads.put(pp, futVarReads);
			}
			
			mth2FutureVarReads.put(mthSig, pp2FutureVarReads);
			
			// perform the analysis of local variable writes
		
			MethodVariableAccesses mvaWrites = new MethodVariableAccesses(mth, bwCFG, ir, false, true);
			BitVectorSolver<IExplodedBasicBlock> solverWrites = mvaWrites.analyze();

			Map<ProgramPoint, Set<LocalVarName>> pp2FutureVarWrites = new HashMap<ProgramPoint, Set<LocalVarName>>();

			// collect analysis results (sets of local variables for all program points)
			for (IExplodedBasicBlock ebb : bwCFG)
			{
				int insnPos = WALAUtils.getInsnBytecodePos(mth, ebb.getFirstInstructionIndex());
			
				ProgramPoint pp = new ProgramPoint(mthSig, insnPos);
				
				Set<LocalVarName> futVarWrites = new HashSet<LocalVarName>();
			
				IntSet out = solverWrites.getOut(ebb).getValue();
				if (out != null)
				{
					for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
					{
						int lvNum = outIt.next();
					
						LocalVarName lv = mvaWrites.getLocalVarForNum(lvNum);
						
						futVarWrites.add(lv);
					}
				}
				
				pp2FutureVarWrites.put(pp, futVarWrites);
			}
			
			mth2FutureVarWrites.put(mthSig, pp2FutureVarWrites);
		}
	}

	public static Set<LocalVarName> getLocalVarReadsForProgramPoint(ProgramPoint pp)
	{
		if (pp == ProgramPoint.INVALID) return null;
			
		Map<ProgramPoint, Set<LocalVarName>> pp2VarReads = mth2FutureVarReads.get(pp.methodSig);
		
		Set<LocalVarName> futVarReads = pp2VarReads.get(pp);
		
		return futVarReads;
	}

	public static Set<LocalVarName> getLocalVarWritesForProgramPoint(ProgramPoint pp)
	{
		if (pp == ProgramPoint.INVALID) return null;
			
		Map<ProgramPoint, Set<LocalVarName>> pp2VarWrites = mth2FutureVarWrites.get(pp.methodSig);
		
		Set<LocalVarName> futVarWrites = pp2VarWrites.get(pp);
		
		return futVarWrites;
	}

	public static void printLocalVariableAccesses()
	{
		System.out.println("LOCAL VARIABLE ACCESSES");
		System.out.println("=======================");
		
		// sort the set of method signatures
		// the set is the same for both reads and writes
		Set<String> methodSigs = new TreeSet<String>();
		methodSigs.addAll(mth2FutureVarReads.keySet());
		
		for (String mthSig : methodSigs)
		{
			if ( ! Debug.isWatchedEntity(mthSig) ) continue;
			
			Map<ProgramPoint, Set<LocalVarName>> pp2FutureVarReads = mth2FutureVarReads.get(mthSig);
			Map<ProgramPoint, Set<LocalVarName>> pp2FutureVarWrites = mth2FutureVarWrites.get(mthSig);

			// the set of program points is the same for both reads and writes
			Set<ProgramPoint> progPoints = new TreeSet<ProgramPoint>();
			progPoints.addAll(pp2FutureVarReads.keySet());
		
			for (ProgramPoint pp : progPoints)
			{
				Set<LocalVarName> varReads = pp2FutureVarReads.get(pp);
				Set<LocalVarName> varWrites = pp2FutureVarWrites.get(pp);

				Set<LocalVarName> varAccesses = new HashSet<LocalVarName>();
				varAccesses.addAll(varReads);
				varAccesses.addAll(varWrites);
			
				System.out.println(pp.methodSig + ":" + pp.insnPos);

				for (LocalVarName lv : varAccesses)
				{
					if (varReads.contains(lv) && varWrites.contains(lv)) System.out.println("\t both: " + lv.toString());
					else if (varReads.contains(lv)) System.out.println("\t read: " + lv.toString());
					else if (varWrites.contains(lv)) System.out.println("\t write: " + lv.toString());
				}
			}
		}
		
		System.out.println("");
	}

	
	static class MethodVariableAccesses
	{
		private IMethod mth;
		
		private Graph<IExplodedBasicBlock> mthBwCFG;
		
		private IR mthIR;

		private boolean considerReads;
		private boolean considerWrites;

		// mapping between local variable names and integer numbers (used in bitvectors)
		private OrdinalSetMapping<LocalVarName> mthLocalVarsNumbering;
		
		public MethodVariableAccesses(IMethod mth, Graph<IExplodedBasicBlock> mthBwCFG, IR mthIR, boolean reads, boolean writes)
		{
			this.mth = mth;
			this.mthBwCFG = mthBwCFG;
			this.mthIR = mthIR;			
			
			this.considerReads = reads;
			this.considerWrites = writes;
			
			createMethodLocalVariablesNumbering();
		}

		private void createMethodLocalVariablesNumbering()
		{
			mthLocalVarsNumbering = new MutableMapping<LocalVarName>(new LocalVarName[1]);

			try
			{
				SSAInstruction[] instructions = mthIR.getInstructions();
				
				for (int insnIndex = 0; insnIndex < instructions.length; insnIndex++) 
				{
					Set<LocalVarName> localVars = LocalVarAccessCodeInfo.getLocalVarNamesForInsn(mth, insnIndex, true, true);
					for (LocalVarName lv : localVars) mthLocalVarsNumbering.add(lv);
				}
			}
			catch (Exception ex) { ex.printStackTrace(); }
		}

		protected LocalVarName getLocalVarForNum(int lvNum)
		{
			return mthLocalVarsNumbering.getMappedObject(lvNum);
		}

		class TransferFunctionsMVA implements ITransferFunctionProvider<IExplodedBasicBlock, BitVectorVariable> 
		{
			public AbstractMeetOperator<BitVectorVariable> getMeetOperator() 
			{
				return BitVectorUnion.instance();
			}
	
			public UnaryOperator<BitVectorVariable> getEdgeTransferFunction(IExplodedBasicBlock src, IExplodedBasicBlock dst) 
			{
				return BitVectorIdentity.instance();
			}

			public UnaryOperator<BitVectorVariable> getNodeTransferFunction(IExplodedBasicBlock ebb) 
			{
				// no valid ssa instruction index
				if (ebb.isEntryBlock() || ebb.isExitBlock()) return BitVectorIdentity.instance();

				SSAInstruction insn = ebb.getInstruction();
				
				int insnIndex = ebb.getFirstInstructionIndex();
				
				BitVector gen = new BitVector();
				
				// this must be empty -> we do not need to kill anything
				BitVector kill = new BitVector();
				
				Set<LocalVarName> localVars = LocalVarAccessCodeInfo.getLocalVarNamesForInsn(mth, insnIndex, considerReads, considerWrites);

				try
				{
					for (LocalVarName lv : localVars)
					{
						int lvNum = mthLocalVarsNumbering.getMappedIndex(lv);
						
						gen.set(lvNum);
					}							
				}
				catch (Exception ex) { ex.printStackTrace(); }

				return new BitVectorKillGen(kill, gen);
			}
			
			public boolean hasEdgeTransferFunctions() 
			{
				return true;
			}
			
			public boolean hasNodeTransferFunctions() 
			{
				return true;
			}
		}

		public BitVectorSolver<IExplodedBasicBlock> analyze() 
		{
			BitVectorFramework<IExplodedBasicBlock, LocalVarName> framework = new BitVectorFramework<IExplodedBasicBlock, LocalVarName>(mthBwCFG, new TransferFunctionsMVA(), mthLocalVarsNumbering);
			
			BitVectorSolver<IExplodedBasicBlock> solver = new BitVectorSolver<IExplodedBasicBlock>(framework);
			
			try
			{
				solver.solve(null);
			}
			catch (Exception e) {}
			
			return solver;
		}
	}
}
