/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.jpf;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.SystemState;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.PathSharednessPolicy;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.choice.ThreadChoiceFromSet;
import gov.nasa.jpf.vm.bytecode.ArrayElementInstruction;
import gov.nasa.jpf.jvm.bytecode.JVMArrayElementInstruction;
import gov.nasa.jpf.util.StringSetMatcher;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.AnalysisResultProcessor;


public class ArraySharednessPolicy extends PathSharednessPolicy
{
	private static StringSetMatcher neverBreakMethodPatterns;

	private static boolean enabledBreakArrays;

	
	public ArraySharednessPolicy(Config config)
	{
		super(config);
		
		String[] patterns = config.getStringArray("vm.shared.never_break_arrays");
		if (patterns != null) neverBreakMethodPatterns = new StringSetMatcher(patterns);

		enabledBreakArrays = config.getBoolean("cg.threads.break_arrays", false);
	}
	
	public boolean canHaveSharedArrayCG(ThreadInfo curTh, Instruction insn, ElementInfo eiArray, int idx)
	{
		if ( ! (insn instanceof ArrayElementInstruction) ) return false;

		Boolean ret = canHaveSharednessCG(curTh, insn, eiArray, null);
		if (ret != null) 
		{
			if (ret.booleanValue() == false) return false;
		}

		if ( ! enabledBreakArrays ) return false;

		if (isNeverBreakArrayElementAccess(curTh.getPC())) return false;

		MethodInfo mi = insn.getMethodInfo();
		String curMethodSig = JPFUtils.getMethodSignature(mi);
			
		String arrayObjClassName = JPFUtils.getArrayClassName(eiArray);
		
		int arrayDimensions = JPFUtils.getArrayDimensionsCount(eiArray);
		
		int elementIndex = curTh.getTopFrame().peek();

		if ( Configuration.usingArrayAccess )
		{
			// check whether some other thread may access this array object later during program execution (from the current state) in a conflicting way

			JVMArrayElementInstruction arrayInsn = (JVMArrayElementInstruction) insn;

			if (arrayInsn.isRead())
			{
				if ( ! AnalysisResultProcessor.existsConflictingArrayWrite(curTh, curMethodSig, insn.getPosition(), arrayObjClassName, eiArray.getObjectRef(), arrayDimensions, elementIndex) )
				{
					// we do not have to make a thread choice
					return false;
				}
			}
			else // isWrite
			{
				if ( ! AnalysisResultProcessor.existsConflictingArrayRead(curTh, curMethodSig, insn.getPosition(), arrayObjClassName, eiArray.getObjectRef(), arrayDimensions, elementIndex) )
				{
					// we do not have to make a thread choice
					return false;
				}
			}
		}
		
		// the default option is to make a new thread choice
		return true;
	} 
	
	/**
	 * Filtering thread choices in library methods (java.*, javax.*).
	 */
	public static boolean isNeverBreakArrayElementAccess(Instruction insn)
	{
		if ( ! (insn instanceof ArrayElementInstruction) ) return false;

		if ( ! enabledBreakArrays ) return true;

		if (neverBreakMethodPatterns != null)
		{
			MethodInfo mth = insn.getMethodInfo();
			if (mth != null)
			{
				String mthName = mth.getFullName();
				
				if (neverBreakMethodPatterns.matchesAny(mthName)) return true;
			}
		}
		
		return false;
	}
}
