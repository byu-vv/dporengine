/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.jpf.dpor;

import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

import gov.nasa.jpf.vm.MethodInfo;


public class VisibleAction
{
	// index on the current execution trace
	public int index;
	
	public Type type;
	
	// heap address of target object
	public int targetObjRef;
	
	// field access: index within the given class
	// array access: element index
	// thread start: new thread index
	// other instructions: nothing	
	public int targetVarId;
	
	// thread which performed this action
	public int threadId;
	
	// map from thread ID to the nearest previous action in the given thread that must happen before this action
	public VisibleAction[] th2PrevHB;
	
	private int hc = 0;
	
	
	static enum Type
	{
		FREAD,
		FWRITE,
		AREAD,
		AWRITE,
		LOCK,
		UNLOCK,
		WAIT,
		NOTIFY,
		TSTART,
		TJOIN
	}
	
	
	public VisibleAction(Type t, int objRef, int varId, int thId)
	{
		type = t;
		targetObjRef = objRef;
		targetVarId = varId;
		threadId = thId;
		
		th2PrevHB = new VisibleAction[2];
	}
	
	public boolean isConflictingWith(VisibleAction prevAct)
	{
		if (this.threadId == prevAct.threadId) return false;
		
		if (this.type == Type.FREAD)
		{
			if ((prevAct.type == Type.FWRITE) && (this.targetObjRef == prevAct.targetObjRef) && (this.targetVarId == prevAct.targetVarId)) return true;
			else return false;
		}
		
		if (this.type == Type.FWRITE)
		{
			if ((prevAct.type == Type.FREAD) && (this.targetObjRef == prevAct.targetObjRef) && (this.targetVarId == prevAct.targetVarId)) return true;
			else return false;
		}
		
		if (this.type == Type.AREAD)
		{
			if ((prevAct.type == Type.AWRITE) && (this.targetObjRef == prevAct.targetObjRef) && (this.targetVarId == prevAct.targetVarId)) return true;
			else return false;
		}
		
		if (this.type == Type.AWRITE)
		{
			if ((prevAct.type == Type.AREAD) && (this.targetObjRef == prevAct.targetObjRef) && (this.targetVarId == prevAct.targetVarId)) return true;
			else return false;
		}
		
		if (this.isSynchronizationEvent())
		{
			if (prevAct.isSynchronizationEvent() && (this.targetObjRef == prevAct.targetObjRef)) return true;
			else return false;
		}

		if (prevAct.type == Type.TSTART)
		{
			if (this.threadId == prevAct.targetVarId) return true;
			else return false;
		}

		return false;
	}
	
	public boolean isFieldAccess()
	{
		return ((type == Type.FREAD) || (type == Type.FWRITE));
	}
	
	public boolean isArrayAccess()
	{
		return ((type == Type.AREAD) || (type == Type.AWRITE));
	}
	
	public boolean isSynchronizationEvent()
	{
		return ((type == Type.LOCK) || (type == Type.UNLOCK) || (type == Type.WAIT) || (type == Type.NOTIFY));
	}
	
	public boolean mustHappenAfter(VisibleAction act)
	{
		if (act.threadId >= th2PrevHB.length) return false;
		
		VisibleAction thPrev = th2PrevHB[act.threadId];
		
		if (thPrev == null) return false;
		
		if (thPrev.index >= act.index) return true;
		
		return false;
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof VisibleAction) ) return false;
		
		VisibleAction other = (VisibleAction) obj;
		
		if (this.index != other.index) return false;
		if (this.type != other.type) return false;
		if (this.targetObjRef != other.targetObjRef) return false;
		if (this.targetVarId != other.targetVarId) return false;
		if (this.threadId != other.threadId) return false;
		
		if ( ! Arrays.equals(this.th2PrevHB, other.th2PrevHB) ) return false;
		
		return true;
	}
	
	public int hashCode()
	{
		if (hc != 0) return hc;
		
		hc = hc * 31 + this.index;
		hc = hc * 31 + this.type.hashCode();		
		hc = hc * 31 + this.targetObjRef;
		hc = hc * 31 + this.targetVarId;
		hc = hc * 31 + this.threadId;
		hc = hc * 31 + Arrays.hashCode(this.th2PrevHB);
		
		return hc;
	} 
	
	public String toString()
	{
		return printToString(true);
	}
	
	public String printToString(boolean includeHB)
	{
		StringBuffer strbuf = new StringBuffer();
		
		if (type == Type.FREAD) strbuf.append("read field: ");
		if (type == Type.FWRITE) strbuf.append("write field: ");
		if (type == Type.AREAD) strbuf.append("read array: ");
		if (type == Type.AWRITE) strbuf.append("write array: ");
		if (type == Type.LOCK) strbuf.append("lock: ");
		if (type == Type.UNLOCK) strbuf.append("unlock: ");
		if (type == Type.WAIT) strbuf.append("wait: ");
		if (type == Type.NOTIFY) strbuf.append("notify: ");
		if (type == Type.TSTART) strbuf.append("start thread: ");
		if (type == Type.TJOIN) strbuf.append("join thread: ");
					
		strbuf.append("target object ref = ");
		strbuf.append(targetObjRef);
		if (isFieldAccess()) strbuf.append(", field index = " + targetVarId);
		if (isArrayAccess()) strbuf.append(", element index = " + targetVarId);
		
		strbuf.append(", thread = " + threadId);
		
		if (includeHB)
		{
			strbuf.append(", happens-before = [");
			for (VisibleAction va : th2PrevHB)
			{
				strbuf.append("    ");
				if (va != null) strbuf.append(va.printToString(false));
				else strbuf.append("null");
			}
			strbuf.append("    ]");
		}
		
		return strbuf.toString();
	}
}
