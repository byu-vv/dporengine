/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.analysis.methodcall;

import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.List;

import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.cfg.ExplodedInterproceduralCFG;
import com.ibm.wala.ipa.cfg.BasicBlockInContext;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.analysis.ExplodedControlFlowGraph;
import com.ibm.wala.ssa.analysis.IExplodedBasicBlock;
import com.ibm.wala.util.intset.BitVector;
import com.ibm.wala.util.intset.OrdinalSetMapping;
import com.ibm.wala.util.intset.MutableMapping;
import com.ibm.wala.util.intset.IntSet; 
import com.ibm.wala.util.intset.IntIterator;
import com.ibm.wala.util.graph.impl.GraphInverter;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.UnaryOperator;
import com.ibm.wala.dataflow.graph.BitVectorSolver;
import com.ibm.wala.dataflow.graph.BitVectorFramework;
import com.ibm.wala.dataflow.graph.AbstractMeetOperator;
import com.ibm.wala.dataflow.graph.BitVectorKillGen;
import com.ibm.wala.dataflow.graph.BitVectorUnion;
import com.ibm.wala.dataflow.graph.BitVectorIdentity;
import com.ibm.wala.dataflow.graph.ITransferFunctionProvider;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.Debug;
import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.bytecode.ByteCodeUtils;
import cz.cuni.mff.d3s.stdynpor.wala.WALAUtils;
import cz.cuni.mff.d3s.stdynpor.wala.dataflow.BitVectorKillAll;


public class MethodCallsAnalysis
{
	// map from program point to the set of methods possibly called in the future
	protected static Map<ProgramPoint, Set<String>> pp2FutureMethodCalls; 
	
	// mapping between method signatures and integer numbers (used in bitvectors)
	private static OrdinalSetMapping<String> methodsNumbering;

	static
	{
		pp2FutureMethodCalls = new LinkedHashMap<ProgramPoint, Set<String>>();
		
		methodsNumbering = null;
	}
	
	
	public static void analyzeProgram(CallGraph clGraph, ExplodedInterproceduralCFG icfg) throws Exception
	{
		// create the backwards-oriented control-flow graph of the whole program
		Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG = GraphInverter.invert(icfg);
		
		// prepare common data structures		
		createMethodSignaturesNumbering(clGraph, bwICFG);

		// perform the analysis of method calls
		
		FutureMethodCalls fmc = new FutureMethodCalls(clGraph, bwICFG);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverFMC = fmc.analyze();

		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();

			int ppInsnPos = WALAUtils.getInsnBytecodePos(bb.getNode(), ebb.getFirstInstructionIndex());
		
			String ppFullMthSig = WALAUtils.getMethodSignature(bb.getNode().getMethod());
		
			ProgramPoint pp = new ProgramPoint(ppFullMthSig, ppInsnPos);

			Set<String> methodCalls = new HashSet<String>();
		
			IntSet out = solverFMC.getOut(bb).getValue();
			if (out != null)
			{
				for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
				{
					int methodNum = outIt.next();
				
					String methodSig = methodsNumbering.getMappedObject(methodNum);
				
					methodCalls.add(methodSig);
				}
			}
			
			pp2FutureMethodCalls.put(pp, methodCalls);
		}		
	}
	
	public static Set<String> getMethodCallsForProgramPoint(ProgramPoint pp)
	{
		Set<String> methodCalls = pp2FutureMethodCalls.get(pp);
					
		return methodCalls; 
	}

	public static void printMethodCalls()
	{
		System.out.println("METHOD CALLS");
		System.out.println("============");
		
		Set<ProgramPoint> progPoints = new TreeSet<ProgramPoint>();
		progPoints.addAll(pp2FutureMethodCalls.keySet());
		
		for (ProgramPoint pp : progPoints)
		{
			if ( ! Debug.isWatchedEntity(pp.methodSig) ) continue;
			
			Set<String> methodCalls = pp2FutureMethodCalls.get(pp);

			System.out.println(pp.methodSig + ":" + pp.insnPos);
			
			for (String mthSig : methodCalls)
			{
				if ( ! Debug.isWatchedEntity(mthSig) ) continue;
				
				System.out.println("\t " + mthSig);
			}
		}
		
		System.out.println("");
	}
	
	private static void createMethodSignaturesNumbering(CallGraph clGraph, Graph<BasicBlockInContext<IExplodedBasicBlock>> icfg)
	{
		methodsNumbering = new MutableMapping<String>(new String[1]);
		
		for (BasicBlockInContext<IExplodedBasicBlock> bb : icfg)
		{
			CGNode mthNode = bb.getNode();
				
			IR methodIR = mthNode.getIR();
				
			if (methodIR == null) continue;

			int firstInsnIdx = bb.getFirstInstructionIndex();
			int lastInsnIdx = bb.getLastInstructionIndex();
   
			// basic block without instructions
			if ((firstInsnIdx < 0) || (lastInsnIdx < 0)) continue;

			SSAInstruction[] instructions = methodIR.getInstructions();

			for (int i = firstInsnIdx; i <= lastInsnIdx; i++) 
			{
				SSAInstruction insn = instructions[i];
				
				if (insn instanceof SSAInvokeInstruction) 
				{
					SSAInvokeInstruction invokeInsn = (SSAInvokeInstruction) insn;
									
					// process all the possible callees for this invoke
					for ( CGNode tgtNode : clGraph.getPossibleTargets(mthNode, invokeInsn.getCallSite()) )
					{
						String tgtMthSig = WALAUtils.getMethodSignature(tgtNode.getMethod());
						
						methodsNumbering.add(tgtMthSig);
					}
				}
			}
		}
	}


	static class FutureMethodCalls
	{
		private CallGraph clGraph;
		
		private Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG;
		
		public FutureMethodCalls(CallGraph clGraph, Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG)
		{
			this.clGraph = clGraph;
			this.bwICFG = bwICFG;
		}

		
		class TransferFunctionsMC implements ITransferFunctionProvider<BasicBlockInContext<IExplodedBasicBlock>, BitVectorVariable> 
		{
			public AbstractMeetOperator<BitVectorVariable> getMeetOperator() 
			{
				return BitVectorUnion.instance();
			}

			public UnaryOperator<BitVectorVariable> getEdgeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> src, BasicBlockInContext<IExplodedBasicBlock> dst) 
			{
				// we do not propagate through return-to-exit edges 
				
				if (dst.getDelegate().isExitBlock()) 
				{
					return BitVectorKillAll.getInstance();
				}
				else
				{
					return BitVectorIdentity.instance();
				}
			}
	
			public UnaryOperator<BitVectorVariable> getNodeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> bb) 
			{
				IExplodedBasicBlock ebb = bb.getDelegate();
				
				SSAInstruction insn = ebb.getInstruction();
				
				if (insn instanceof SSAInvokeInstruction) 
				{
					SSAInvokeInstruction invokeInsn = (SSAInvokeInstruction) insn;
					
					// this must be empty -> we do not need to kill anything
					BitVector kill = new BitVector();

					BitVector gen = new BitVector();
					
					// process all the possible callees for this invoke
					for ( CGNode tgtNode : clGraph.getPossibleTargets(bb.getNode(), invokeInsn.getCallSite()) )
					{
						String tgtMthSig = WALAUtils.getMethodSignature(tgtNode.getMethod());
						
						int tgtMethodNum = methodsNumbering.getMappedIndex(tgtMthSig);
						
						gen.set(tgtMethodNum);
					}
					
					return new BitVectorKillGen(kill, gen);
				}
				else
				{
					// identity function for all other instructions
					return BitVectorIdentity.instance();
				}
			}
			
			public boolean hasEdgeTransferFunctions() 
			{
				return true;
			}
			
			public boolean hasNodeTransferFunctions() 
			{
				return true;
			}
		}
	
		public BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> analyze() 
		{
			BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, String> framework = new BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, String>(bwICFG, new TransferFunctionsMC(), methodsNumbering);
			
			BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solver = new BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>>(framework);

			try
			{
				solver.solve(null);
			}
			catch (Exception ex) { ex.printStackTrace(); }
			
			return solver;
		}
	}
}
