/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.data;

import java.util.List;
import java.util.ArrayList;

// for object-sensitive pointer analysis
public class ObjectStringContext extends Context
{
	private List<AllocationSite> allocSites;
	
	
	public ObjectStringContext()
	{
		super();
		
		this.allocSites = new ArrayList<AllocationSite>();
	}
	
	public void addFirstElement(AllocationSite as)
	{
		allocSites.add(0, as);
	}
	
	public AllocationSite getElement(int index)
	{
		return allocSites.get(index);
	}
	
	public void keepOnlyLastElement()
	{
		AllocationSite lastAS = allocSites.get(allocSites.size()-1);
		
		allocSites.clear();
		
		allocSites.add(lastAS);
	}
	
	public void dropFirstElement()
	{
		allocSites.remove(0);
	}

	public void dropAllElements()
	{
		allocSites.clear();
	}
	
	public int length()
	{
		return this.allocSites.size();
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		
		if ( ! (obj instanceof ObjectStringContext) ) return false;
		
		ObjectStringContext other = (ObjectStringContext) obj;
		
		if ( ! this.allocSites.equals(other.allocSites) ) return false;
		
		return true;
	}
	
	public int hashCode()
	{
		return this.allocSites.hashCode();
	}

	protected String createStringRepr()
	{
		StringBuffer strbuf = new StringBuffer();

		for (AllocationSite as : this.allocSites)
		{
			// not the first item
			if (strbuf.length() > 0) strbuf.append(", ");

			strbuf.append(as.toString());
		}

		return strbuf.toString();
	}
}

