/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.analysis.fieldaccess;

import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;

import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.impl.Everywhere;
import com.ibm.wala.ipa.cfg.ExplodedInterproceduralCFG;
import com.ibm.wala.ipa.cfg.BasicBlockInContext;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.IField;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.util.graph.Graph;

import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.FieldName;
import cz.cuni.mff.d3s.stdynpor.data.LocalVarID;
import cz.cuni.mff.d3s.stdynpor.wala.WALAUtils;


public class ImmutableFieldsDetector
{
	private static Set<FieldName> mutableFields;
	
	static
	{
		mutableFields = new HashSet<FieldName>();
	}
		
	
	/**
	 * This method performs stage 3 of the immutable fields analysis.
	 */
	public static void analyzeProgram(CallGraph clGraph, Map<ProgramPoint, Set<LocalVarID>> pp2EscapedVars) throws Exception
	{
		// analyze methods reachable in the call graph
		for (Iterator<CGNode> cgnIt = clGraph.iterator(); cgnIt.hasNext(); )
		{
			IMethod mth = cgnIt.next().getMethod();
			
			// we skip native methods and abstract methods
			if (mth.isNative() || mth.isAbstract()) continue;
			
			IClass cls = mth.getDeclaringClass();
			
			String className = WALAUtils.getClassName(cls);
			
			// we ignore library classes (JPF does not make thread choices at field accesses in their methods)
			if (className.startsWith("java.") || className.startsWith("javax.")) continue;
			
			String methodSig = WALAUtils.getMethodSignature(mth);
	
			IR mthIR = WALAUtils.cache.getIRFactory().makeIR(mth, Everywhere.EVERYWHERE, SSAOptions.defaultOptions());

			// for each field write look into results of escape analysis (stage 2) to see whether the target object already escaped to the heap (at the previous code location)

			SSAInstruction[] instructions = mthIR.getInstructions();
			for (int idx = 0; idx < instructions.length; idx++)
			{
				SSAInstruction insn = instructions[idx];
				
				if (insn == null) continue;
				
				if (insn instanceof SSAPutInstruction)
				{
					SSAPutInstruction putInsn = (SSAPutInstruction) insn;
	
					String fieldNameStr = putInsn.getDeclaredField().getName().toUnicodeString();
					
					String fieldDeclClsNameStr = WALAUtils.getDeclaringClassNameForField(putInsn.getDeclaredField());

					if (putInsn.isStatic())
					{
						// static fields are never considered immutable because they are always reachable from multiple threads
						
						FieldName tgtField = FieldName.createFromString(fieldDeclClsNameStr, fieldNameStr);

						mutableFields.add(tgtField);
					}
					else
					{
						int putInsnTgtRef = putInsn.getRef();
					
						// get local variables that may be already escaped at the given code location (after the previous bytecode instruction)
					
						int prevInsnPos = WALAUtils.getInsnBytecodePos(mth, idx-1);
					
						ProgramPoint prevPP = new ProgramPoint(methodSig, prevInsnPos);
					
						Set<LocalVarID> escapedVars = pp2EscapedVars.get(prevPP);
					
						// check for the target variable of the field write access whether the variable itself escaped already
						// we do not have to consider aliasing here
					
						// important cases:
							// not escaped and field not present in the set -> immutable field
							// not escaped and present in the set -> already known mutable field
							// escaped -> possibly new mutable field
					
						if (escapedVars != null)
						{
							for (LocalVarID ev : escapedVars)
							{
								// we do not have to check the method signature here because the set "escapedVars" contains only local variables of the same method
							
								if (ev.varNumber == putInsnTgtRef)
								{
									FieldName tgtField = FieldName.createFromString(fieldDeclClsNameStr, fieldNameStr);
								
									mutableFields.add(tgtField);
							
									break;
								}
							}
						}
					}
				}		
			}
		}
	}
	
	public static boolean checkFieldImmutability(FieldName fn)
	{
		return ( ! mutableFields.contains(fn) );
	}
	
	public static void printImmutableFields()
	{		
		System.out.println("IMMUTABLE FIELDS");
		System.out.println("================");
		
		// check all fields in each class
		for (Iterator<IClass> clsIt = WALAUtils.classHierarchy.iterator(); clsIt.hasNext(); )
		{
			IClass cls = clsIt.next();
			
			try
			{
				String classNameStr = WALAUtils.getClassName(cls);
				
				if (classNameStr.startsWith("java.")) continue;
				
				Collection<IField> fields = cls.getAllFields();
				
				for (IField fld : fields)
				{
					String fieldNameStr = fld.getName().toUnicodeString();
					
					FieldName fn = FieldName.createFromString(classNameStr, fieldNameStr);
						
					if ( ! mutableFields.contains(fn) ) System.out.println(fn.toString());
				}
			}
			catch (Exception ex) { ex.printStackTrace(); }
		}
		
		System.out.println("");
	}
}
