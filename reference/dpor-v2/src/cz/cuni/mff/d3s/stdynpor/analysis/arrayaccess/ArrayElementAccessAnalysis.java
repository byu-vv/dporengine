/*
 * Copyright (C) 2015, Charles University in Prague.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cz.cuni.mff.d3s.stdynpor.analysis.arrayaccess;

import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.List;

import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.cfg.ExplodedInterproceduralCFG;
import com.ibm.wala.ipa.cfg.BasicBlockInContext;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAArrayReferenceInstruction;
import com.ibm.wala.ssa.SSAArrayLoadInstruction;
import com.ibm.wala.ssa.SSAArrayStoreInstruction;
import com.ibm.wala.ssa.analysis.ExplodedControlFlowGraph;
import com.ibm.wala.ssa.analysis.IExplodedBasicBlock;
import com.ibm.wala.util.intset.BitVector;
import com.ibm.wala.util.intset.OrdinalSetMapping;
import com.ibm.wala.util.intset.MutableMapping;
import com.ibm.wala.util.intset.IntSet; 
import com.ibm.wala.util.intset.IntIterator;
import com.ibm.wala.util.graph.impl.GraphInverter;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.fixpoint.BitVectorVariable;
import com.ibm.wala.fixpoint.UnaryOperator;
import com.ibm.wala.dataflow.graph.BitVectorSolver;
import com.ibm.wala.dataflow.graph.BitVectorFramework;
import com.ibm.wala.dataflow.graph.AbstractMeetOperator;
import com.ibm.wala.dataflow.graph.BitVectorKillGen;
import com.ibm.wala.dataflow.graph.BitVectorUnion;
import com.ibm.wala.dataflow.graph.BitVectorIdentity;
import com.ibm.wala.dataflow.graph.ITransferFunctionProvider;

import cz.cuni.mff.d3s.stdynpor.Configuration;
import cz.cuni.mff.d3s.stdynpor.Debug;
import cz.cuni.mff.d3s.stdynpor.data.ProgramPoint;
import cz.cuni.mff.d3s.stdynpor.data.ArrayElementAccess;
import cz.cuni.mff.d3s.stdynpor.bytecode.ByteCodeUtils;
import cz.cuni.mff.d3s.stdynpor.wala.WALAUtils;
import cz.cuni.mff.d3s.stdynpor.wala.ArrayAccessCodeInfo;
import cz.cuni.mff.d3s.stdynpor.wala.dataflow.BitVectorKillAll;


public class ArrayElementAccessAnalysis
{
	// map from program point (method signature and bytecode position) to the set of array element read accesses (xALOAD)
	protected static Map<ProgramPoint, Set<ArrayElementAccess>> pp2FutureArrayElementReads; 

	// map from program point (method signature and bytecode position) to the set of array element write accesses (xASTORE)
	protected static Map<ProgramPoint, Set<ArrayElementAccess>> pp2FutureArrayElementWrites;

	// mapping between array element accesses and integer numbers (used in bitvectors)
	private static OrdinalSetMapping<ArrayElementAccess> accessNumbering;

	static
	{
		pp2FutureArrayElementReads = new LinkedHashMap<ProgramPoint, Set<ArrayElementAccess>>();
		pp2FutureArrayElementWrites = new LinkedHashMap<ProgramPoint, Set<ArrayElementAccess>>();
		
		accessNumbering = null;
	}
	
	
	public static void analyzeProgram(CallGraph clGraph, ExplodedInterproceduralCFG icfg) throws Exception
	{
		// create the backwards-oriented control-flow graph of the whole program
		Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG = GraphInverter.invert(icfg);
		
		// prepare common data structures
		
		createArrayElementAccessNumbering(bwICFG);

		// perform the analysis of array element reads
		
		ArrayElementAccessCollector aeReads = new ArrayElementAccessCollector(bwICFG, true, false);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverReads = aeReads.analyze();

		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();

			int insnPos = WALAUtils.getInsnBytecodePos(bb.getNode(), ebb.getFirstInstructionIndex());
		
			String fullMethodSig = WALAUtils.getMethodSignature(bb.getNode().getMethod());
		
			ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);

			Set<ArrayElementAccess> elementReads = new HashSet<ArrayElementAccess>();
		
			IntSet out = solverReads.getOut(bb).getValue();
			if (out != null)
			{
				for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
				{
					int aeaNum = outIt.next();
				
					ArrayElementAccess aea = accessNumbering.getMappedObject(aeaNum);
				
					elementReads.add(aea);
				}
			}
			
			pp2FutureArrayElementReads.put(pp, elementReads);
		}
		
		// perform the analysis of array element writes
		
		ArrayElementAccessCollector aeWrites = new ArrayElementAccessCollector(bwICFG, false, true);
		BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solverWrites = aeWrites.analyze();

		for (BasicBlockInContext<IExplodedBasicBlock> bb : bwICFG) 
		{
			IExplodedBasicBlock ebb = bb.getDelegate();

			int insnPos = WALAUtils.getInsnBytecodePos(bb.getNode(), ebb.getFirstInstructionIndex());
		
			String fullMethodSig = WALAUtils.getMethodSignature(bb.getNode().getMethod());
		
			ProgramPoint pp = new ProgramPoint(fullMethodSig, insnPos);

			Set<ArrayElementAccess> elementWrites = new HashSet<ArrayElementAccess>();
		
			IntSet out = solverWrites.getOut(bb).getValue();
			if (out != null)
			{
				for (IntIterator outIt = out.intIterator(); outIt.hasNext(); )
				{
					int aeaNum = outIt.next();
				
					ArrayElementAccess aea = accessNumbering.getMappedObject(aeaNum);
				
					elementWrites.add(aea);
				}	
			}
			
			pp2FutureArrayElementWrites.put(pp, elementWrites);
		}
	}
	
	public static Set<ArrayElementAccess> getArrayElementReadsForProgramPoint(ProgramPoint pp)
	{
		Set<ArrayElementAccess> elementReads = pp2FutureArrayElementReads.get(pp);
					
		if (elementReads != null) return elementReads;
		
		if (pp != ProgramPoint.INVALID)
		{
			// there are some analysis results for the program point, but no read accesses to array elements
			if (pp2FutureArrayElementWrites.containsKey(pp)) return new HashSet<ArrayElementAccess>();
		}

		// invalid program point or no data available
		return null; 
	}

	public static Set<ArrayElementAccess> getArrayElementWritesForProgramPoint(ProgramPoint pp)
	{
		Set<ArrayElementAccess> elementWrites = pp2FutureArrayElementWrites.get(pp);
					
		if (elementWrites != null) return elementWrites;
		
		if (pp != ProgramPoint.INVALID)
		{
			// there are some analysis results for the program point, but no write accesses to array elements
			if (pp2FutureArrayElementReads.containsKey(pp)) return new HashSet<ArrayElementAccess>();
		}
		
		// invalid program point or no data available
		return null; 
	}
	
	public static void printArrayElementAccesses()
	{
		System.out.println("ARRAY ELEMENT ACCESSES");
		System.out.println("======================");
		
		// the key set is the same for both reads and writes
		Set<ProgramPoint> progPoints = new TreeSet<ProgramPoint>();
		progPoints.addAll(pp2FutureArrayElementReads.keySet());
		
		for (ProgramPoint pp : progPoints)
		{
			if ( ! Debug.isWatchedEntity(pp.methodSig) ) continue;
			
			Set<ArrayElementAccess> elementReads = pp2FutureArrayElementReads.get(pp);
			Set<ArrayElementAccess> elementWrites = pp2FutureArrayElementWrites.get(pp);			

			Set<ArrayElementAccess> elementAccesses = new HashSet<ArrayElementAccess>();
			elementAccesses.addAll(elementReads);
			elementAccesses.addAll(elementWrites);
			
			System.out.println(pp.methodSig + ":" + pp.insnPos);
			
			for (ArrayElementAccess aea : elementAccesses)
			{
				if ( ! Debug.isWatchedEntity(aea.progPoint.methodSig) ) continue;
				
				if (elementReads.contains(aea) && elementWrites.contains(aea)) System.out.println("\t both: " + aea.toString());
				else if (elementReads.contains(aea)) System.out.println("\t read: " + aea.toString());
				else if (elementWrites.contains(aea)) System.out.println("\t write: " + aea.toString());
			}
		}
		
		System.out.println("");
	}
	
	private static void createArrayElementAccessNumbering(Graph<BasicBlockInContext<IExplodedBasicBlock>> icfg)
	{
		accessNumbering = new MutableMapping<ArrayElementAccess>(new ArrayElementAccess[1]);
		
		for (BasicBlockInContext<IExplodedBasicBlock> bb : icfg)
		{
			CGNode mthNode = bb.getNode();
				
			IR methodIR = mthNode.getIR();
				
			if (methodIR == null) continue;

			int firstInsnIdx = bb.getFirstInstructionIndex();
			int lastInsnIdx = bb.getLastInstructionIndex();
   
			// basic block without instructions
			if ((firstInsnIdx < 0) || (lastInsnIdx < 0)) continue;

			SSAInstruction[] instructions = methodIR.getInstructions();

			for (int idx = firstInsnIdx; idx <= lastInsnIdx; idx++) 
			{
				SSAInstruction insn = instructions[idx];

				if (insn instanceof SSAArrayReferenceInstruction) 
				{
					SSAArrayReferenceInstruction arrInsn = (SSAArrayReferenceInstruction) insn;
						
					try
					{
						Set<ArrayElementAccess> accesses = ArrayAccessCodeInfo.getArrayElementAccessesForInsn(mthNode, arrInsn, idx);
						for (ArrayElementAccess aea : accesses) accessNumbering.add(aea);
					}
					catch (Exception ex) { ex.printStackTrace(); }						
				}
			}
		}
	}


	static class ArrayElementAccessCollector
	{
		private Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG;
		
		private boolean considerReads;
		private boolean considerWrites;
		
		public ArrayElementAccessCollector(Graph<BasicBlockInContext<IExplodedBasicBlock>> bwICFG, boolean reads, boolean writes)
		{
			this.bwICFG = bwICFG;
			
			this.considerReads = reads;
			this.considerWrites = writes;			
		}

		
		class TransferFunctionsAEA implements ITransferFunctionProvider<BasicBlockInContext<IExplodedBasicBlock>, BitVectorVariable> 
		{
			public AbstractMeetOperator<BitVectorVariable> getMeetOperator() 
			{
				return BitVectorUnion.instance();
			}

			public UnaryOperator<BitVectorVariable> getEdgeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> src, BasicBlockInContext<IExplodedBasicBlock> dst) 
			{
				// we do not propagate through return-to-exit edges 
				
				if (dst.getDelegate().isExitBlock()) 
				{
					return BitVectorKillAll.getInstance();
				}
				else
				{
					return BitVectorIdentity.instance();
				}
			}
	
			public UnaryOperator<BitVectorVariable> getNodeTransferFunction(BasicBlockInContext<IExplodedBasicBlock> bb) 
			{
				IExplodedBasicBlock ebb = bb.getDelegate();
				
				SSAInstruction insn = ebb.getInstruction();

				if (insn instanceof SSAArrayReferenceInstruction) 
				{
					SSAArrayReferenceInstruction arrInsn = (SSAArrayReferenceInstruction) insn;
					
					if ((considerReads && (arrInsn instanceof SSAArrayLoadInstruction)) || (considerWrites && (arrInsn instanceof SSAArrayStoreInstruction)))
					{
						// this must be empty -> we do not need to kill anything
						BitVector kill = new BitVector();

						BitVector gen = new BitVector();

						Set<ArrayElementAccess> accesses = ArrayAccessCodeInfo.getArrayElementAccessesForInsn(bb.getNode(), arrInsn, ebb.getFirstInstructionIndex());
						
						try
						{
							for (ArrayElementAccess aea : accesses)
							{
								int aeaNum = accessNumbering.getMappedIndex(aea);
								
								gen.set(aeaNum);
							}
						}
						catch (Exception ex) { ex.printStackTrace(); }
						
						return new BitVectorKillGen(kill, gen);
					}
					else 
					{
						// identity function for all other cases
						return BitVectorIdentity.instance();
					}
				}
				else
				{
					// identity function for all other instructions
					return BitVectorIdentity.instance();
				}
			}
			
			public boolean hasEdgeTransferFunctions() 
			{
				return true;
			}
			
			public boolean hasNodeTransferFunctions() 
			{
				return true;
			}
		}
	
		public BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> analyze() 
		{
			BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, ArrayElementAccess> framework = new BitVectorFramework<BasicBlockInContext<IExplodedBasicBlock>, ArrayElementAccess>(bwICFG, new TransferFunctionsAEA(), accessNumbering);
			
			BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>> solver = new BitVectorSolver<BasicBlockInContext<IExplodedBasicBlock>>(framework);

			try
			{
				solver.solve(null);
			}
			catch (Exception ex) { ex.printStackTrace(); }
			
			return solver;
		}
	}
}
