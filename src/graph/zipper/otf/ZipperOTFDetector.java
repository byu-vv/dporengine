package graph.zipper.otf;

import graph.zipper.otf.node.IsolatedNode;
import util.Detector;

import java.util.HashMap;
import java.util.Map;
import dporengine.graph.DporNode;

public class ZipperOTFDetector extends Detector {
  private ZipperOTF state = new ZipperOTF(0);

  @Override
  public void setUseDC(boolean useDC) {
    state.useDC = useDC;
  }

  @Override
  public void handleAcquire(int tid) {
    if (!this.state.hasRace())
      state.isolateStart(tid);
  }

  @Override
  public void handleRelease(int tid) {
    if (!this.state.hasRace()) {
      if (currentDporNode == null) throw new RuntimeException("handleRelease called before onCreateNode");
      currentDporNode.putDependenceInfo(state.isolateEnd(tid));
      //cg.putObject(state.isolateEnd(tid));
    }
  }

  @Override
  public void handleRead(int tid, String uniqueLabel) {
    if (!this.state.hasRace())
      state.read(tid, uniqueLabel);
  }

  @Override
  public void handleWrite(int tid, String uniqueLabel) {
    if (!this.state.hasRace())
      state.write(tid, uniqueLabel);
  }

  @Override
  public void handleFork(int parent, int child, boolean future) {
    if (!this.state.hasRace())
      state.fork(parent, child);
  }

  @Override
  public void handleJoin(int parent, int child, boolean finish) {
    if (!this.state.hasRace())
      state.join(parent, child);
  }

  @Override
  public void handleHalt(String programName) {
    if(debug) {
      state.halt(programName);
    }
  }

  @Override
  public boolean dependent(DporNode node1, DporNode node2) {
    IsolatedNode iso1 = (IsolatedNode) node1.getDependenceInfo();
    IsolatedNode iso2 = (IsolatedNode) node2.getDependenceInfo();
    return iso1.incoming.contains(iso2) || iso2.incoming.contains(iso1);
  }

  @Override
  public void resetState(Object state) {
    this.state = ZipperOTF.resetTo((ZipperOTF) state);
  }

  @Override
  public Object getImmutableState() {
    return state.deepCopy();
  }

  @Override
  public boolean hasRace() {
    return this.state.hasRace();
  }
}
