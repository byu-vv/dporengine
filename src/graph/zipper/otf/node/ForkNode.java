package graph.zipper.otf.node;

import gov.nasa.jpf.tool.Run;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ForkNode extends ZipperNode {
  public RegularNode forked;

  public ForkNode(int tid, int newTid, Map<Integer, ZipperNode> sync) {
    super(tid, sync);
    child = new RegularNode(tid, sync);
    Map<Integer, ZipperNode> forkedSync = new HashMap<>(sync);
    forked = new RegularNode(newTid, forkedSync);
    sync.put(newTid, forked);
    forkedSync.put(tid, child);
  }
}
