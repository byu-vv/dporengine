package util;

import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.ThreadInfo;
import graph.util.Node;
import dporengine.examples.ScheduleOverIsolate;
import dporengine.graph.DporNode;

public abstract class Detector extends ScheduleOverIsolate {
  public int haltCount = 0;
  public Search owner;
  private boolean DPORWarningPrinted = false;
  protected DporNode currentDporNode = null;
  // For rewinding
  public abstract void resetState(Object state);
  public abstract Object getImmutableState();
  // Event handling
  public void handleRead(int tid, String uniqueLabel) {}
  public void handleWrite(int tid, String uniqueLabel) {}
  public void handleAcquire(int tid) {}
  public void handleRelease(int tid) {}
  public void handleFork(int parent, int child, boolean future) {}
  public void handleJoin(int parent, int child, boolean finish) {}
  public void handleHalt(String programName) {
      //System.out.println("HALT COUNT: " + ++haltCount);
  }
  public Node getIsolatedNode() {return null;}
  public boolean useDC = true;
  public boolean debug = true;
  // Properties
  public boolean hasRace() {
    return false;
  }

  public Detector() {

  }

  public Detector(boolean useDC) {
    this.useDC = useDC;
  }

  public String error() {
    return "Data Race Detected";
  }

 @Override
  public void onCreateNode(DporNode parent, DporNode child) {
    currentDporNode = child;
  }

 @Override
  public boolean dependent(DporNode first, DporNode second) {
    if(!DPORWarningPrinted) {
      System.err.println("DPOR not implemented in detector.");
      DPORWarningPrinted = true;
    }
    return true;
  };

  public void setUseDC(boolean useDC) {
    this.useDC = useDC;
  }
}
