package dporengine;

import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Instruction;

/** Interface for instruction marker. Must determine if a given instruction must be rescheduled
*/
public interface DporInstructionMarker {
  public DporInstructionMark mark(VM vm, ThreadInfo currentTread, Instruction instruction);
}
