package dporengine;

import dporengine.graph.DporNode;

/** An interface for a dependence relation
*/
public interface DporDependenceRelation {
  //Given two nodes the interface must maintain data structures to determine if they are dependent
  public boolean dependent(DporNode first, DporNode second);
  //When a new node is created this method is called. It is suggested that the Dependence relation calls putDependenceInfo on the parent. The child node will likely need info from future events;
  public void onCreateNode(DporNode parent, DporNode child);
}
