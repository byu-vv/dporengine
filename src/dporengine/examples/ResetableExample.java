package dporengine.examples;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.Config;
import dporengine.DporEngine;
import dporengine.DporResetable;


/**
 * Example of how to register an object to be reset with JPF search
 */
public class ResetableExample implements DporResetable {


  //you need to have access to the scheduler so you can register yourself with the DporEngine.
  //There are lots of ways to do this but if you instantiate using config.getEssentialInstance("key");
  //JPF will call this constructor with a JPF object and you can extract the scheduler from it.
  public ResetableExample(Config conf, JPF jpf) {
    if (jpf != null) {
      DporEngine.registerWithJPF(jpf, this);
    }
    else throw new RuntimeException("Resetable constructor must have JPF object");
  }


  public Object getImmutableState() {
    return "A string";
  }

  public void resetState(Object stateObject) {
    String state = (String) stateObject;
    System.out.println("Reseting state and the state was: " + state);
  }

}
