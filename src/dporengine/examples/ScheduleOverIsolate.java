package dporengine.examples;

import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.bytecode.FieldInstruction;
import gov.nasa.jpf.vm.bytecode.ReadOrWriteInstruction;
import dporengine.DporInstructionMark;
import dporengine.DporInstructionMarker;
import dporengine.DporDependenceRelation;
import dporengine.graph.DporNode;

import gov.nasa.jpf.jvm.bytecode.INVOKESTATIC;

/** An example of a InstructionMarker and DependenceRelation in one
*   This this interleaves the possible schedules of isolated sections assuming all isolated sections are dependent
*/
public class ScheduleOverIsolate implements DporInstructionMarker, DporDependenceRelation {
  public ScheduleOverIsolate() {
  }
  public DporInstructionMark mark(VM vm, ThreadInfo currentThread, Instruction instruction) {
    if (isValidIsolateInstruction(vm, instruction)) return DporInstructionMark.YES; //its an isolate instruction enumerate over it
    return DporInstructionMark.NO; //otherwise do nothing for it
  }

  public boolean dependent(DporNode first, DporNode second) {
    return true; //assume all isolated sections are dependent
  }

  public void onCreateNode(DporNode parent, DporNode child) {
    //we don't need this method because we return true for dependent
  }

  private boolean isValidIsolateInstruction(VM vm, Instruction inst) {
    if (!validMethod(vm, inst.getMethodInfo())) return false;
    if (inst instanceof INVOKESTATIC) {
      INVOKESTATIC inStat = (INVOKESTATIC) inst;
      if (inStat.getInvokedMethod().getName().equals("isolated")) {
        return true;
      }
    }
    return false;
  }

  private boolean validMethod(VM vm, MethodInfo mi) {
    String methodName = mi.getBaseName();
    String fileName = vm.getSUTName();
    return methodName.contains(fileName); //check if the instruction came from the file we are analyzing
  }
  
}
