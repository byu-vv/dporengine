package dporengine.examples;

import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.bytecode.FieldInstruction;
import gov.nasa.jpf.vm.bytecode.ReadOrWriteInstruction;
import dporengine.DporInstructionMark;
import dporengine.DporInstructionMarker;
import dporengine.DporDependenceRelation;
import dporengine.DporResetable;
import dporengine.graph.DporNode;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

/** An example of a InstructionMarker and DependenceRelation in one
*   This attempts to enumerate all behaviors possible due to data race
*/
public class DataRaceBehavior implements DporInstructionMarker, DporDependenceRelation, DporResetable {
  private Access lastAccess; //keep track of the last access used as DependenceInfo to determine dependence
  private Set<String> sharedVariables; //a set of variables we know are shared and must schedule over accesses
  private Map<String, ThreadInfo> lastAccessingThread; //a map of variable name to thread that is used to detect sharedness
  public DataRaceBehavior() {
    sharedVariables = new HashSet();
    lastAccess = new Access();
    lastAccessingThread = new HashMap();
  }
  //a custom class for recording information that is necessary to determine if a access is dependent
  private class Access {
    String name;
    boolean read;
    ThreadInfo thread;

    public String toString() {
      return name + "[" + thread + "]" + (read ? "R" : "W");
    }
  }

  public DporInstructionMark mark(VM vm, ThreadInfo currentThread, Instruction instruction) {
    //check if the instruction comes from the class we are checking or from a library
    if (!validInstruction(vm, instruction)) return DporInstructionMark.NO; //if from a library we will not enumerate behavior
    //System.out.println("Exec line " + instruction.getLineNumber());
    if(instruction instanceof FieldInstruction) { 
      Access currentAccess = new Access(); //must make new object for each potential new node
      currentAccess.name = ((FieldInstruction)instruction).getFieldName();
      currentAccess.read = ((FieldInstruction)instruction).isRead();
      currentAccess.thread = currentThread;
      if(!currentAccess.thread.equals(lastAccessingThread.get(currentAccess.name))) { //if this access is on a different thread then the last access to this variable
        sharedVariables.add(lastAccess.name); //add this variable to the shared set
      }
      lastAccessingThread.put(currentAccess.name, currentAccess.thread);
      lastAccess = currentAccess; //store this for DependenceInfo in onCreateNode
      if(sharedVariables.contains(lastAccess.name)) { //if it is shared
        return DporInstructionMark.YES; //we need to enumerate schedules with this point
      }
      return DporInstructionMark.MAYBE; //if it is not shared we may discover later that it is shared
    }
    return DporInstructionMark.NO; //if it is not a field access we do not care to enumerate schedules
  }

  public boolean dependent(DporNode first, DporNode second) {
    Access firstAccess = (Access) first.getDependenceInfo();
    Access secondAccess = (Access) second.getDependenceInfo();
    if (firstAccess == null || secondAccess == null || firstAccess.thread == null || firstAccess.name == null) return false;
    if (firstAccess.thread.getId() == secondAccess.thread.getId()) return false; //on different threads
    if (!firstAccess.name.equals(secondAccess.name)) return false; //on the same variable
    if (firstAccess.read && secondAccess.read) return false;//at least one is a write
    //System.out.println("First:  " + firstAccess);
    //System.out.println("Second: " + secondAccess);
    return true;
  }

  public Object getImmutableState() { return null; }
  public void resetState(Object state) {}

  public void onCreateNode(DporNode parent, DporNode child) {
    if (lastAccess == null || parent == null) return;
    child.putDependenceInfo(lastAccess); //TODO re-evaluate logic for why adding to parent
  }

  private boolean validInstruction(VM vm, Instruction inst) {
    if (!validMethod(vm, inst.getMethodInfo())) return false;
    if (inst.toString().contains("this$")) return false;
    if (inst.toString().contains("java.lang")) return false;
    return true;
  }
  private boolean validMethod(VM vm, MethodInfo mi) {
    String methodName = mi.getBaseName();
    String fileName = vm.getSUTName();
    return methodName.contains(fileName); //check if the instruction came from the file we are analyzing
    /*
    String[] invalidPackages = {
      "java.util",
      "hj.lang",
      "hj.runtime",
      "hj.util",
      "java.lang",
      "edu.rice.hj",
      "permission",
      "gov.nasa.jpf",
      "sun.misc",
      "java.io"
    };
    for (String name : invalidPackages) {
      if (methodName.contains(name)) {
        return false;
      }
    }
    return true;
    */
  }

}
