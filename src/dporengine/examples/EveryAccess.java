package dporengine.examples;

import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.bytecode.FieldInstruction;
import gov.nasa.jpf.vm.bytecode.ReadOrWriteInstruction;
import dporengine.DporInstructionMark;
import dporengine.DporInstructionMarker;
import dporengine.DporDependenceRelation;
import dporengine.DporResetable;
import dporengine.graph.DporNode;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

/** An example of a InstructionMarker and DependenceRelation in one
*   This attempts to enumerate all behaviors possible due to data race
*/
public class EveryAccess implements DporInstructionMarker, DporDependenceRelation, DporResetable {
  private String lastAccessName;
  public EveryAccess() {
  }

  public DporInstructionMark mark(VM vm, ThreadInfo currentThread, Instruction instruction) {
    //check if the instruction comes from the class we are checking or from a library
    if (!validInstruction(vm, instruction)) return DporInstructionMark.NO; //if from a library we will not enumerate behavior
    if(instruction instanceof FieldInstruction) { 
      lastAccessName = ((FieldInstruction) instruction).getVariableId();
      return DporInstructionMark.YES; //Insert Choice Generator at every access
    }
    return DporInstructionMark.NO; //if it is not a field access we do not care to enumerate schedules
  }

  public boolean dependent(DporNode first, DporNode second) {
    if (first.getDependenceInfo() == null || second.getDependenceInfo() == null) return false;
    return first.getDependenceInfo().equals(second.getDependenceInfo());
  }

  public Object getImmutableState() { return null; }
  public void resetState(Object state) {}

  public void onCreateNode(DporNode parent, DporNode child) {
    if (lastAccessName != null) child.putDependenceInfo(new String(lastAccessName));
  }

  private boolean validInstruction(VM vm, Instruction inst) {
    if (!validMethod(vm, inst.getMethodInfo())) return false;
    if (inst.toString().contains("this$")) return false;
    if (inst.toString().contains("java.lang")) return false;
    return true;
  }
  private boolean validMethod(VM vm, MethodInfo mi) {
    String methodName = mi.getBaseName();
    String fileName = vm.getSUTName();
    return methodName.contains(fileName); //check if the instruction came from the file we are analyzing
  }

}
