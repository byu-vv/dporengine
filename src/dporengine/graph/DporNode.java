package dporengine.graph;

import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.HashSet;
import java.lang.StringBuilder;
import dporengine.DporInstructionMark;

import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Instruction;
import dporengine.DporEngine;

public class DporNode {
  private DporNode parent; //the current node when this node was created
  private ThreadInfo thread; //the thread of the instruction that caused this node to be created
  private Object dependeceInfo; //used by user to keep information needed to determine dependence
  private DporInstructionMark type; //YES, MAYBE, or REPLAY. Types determine DporEngine Behavior
  private List<ThreadInfo> runnables; //all the threads that can be run from this point
  private List<ThreadInfo> choices; //all the threads in the backtrack set (that will be run from this point)
  private Set<DporNode> children; //the children of this node. Nodes that were created due to different choices here
  private Instruction inst; //the instruction that caused this node to be created
  public int choiceIndex; //where we are in our list of choices to be made
  public boolean mustReplay; //used only for REPLAY type nodes
  private int id; //a unique id for each node
  private static int count; //used to generate the unique id

  public DporNode(ThreadInfo currentThread, List<ThreadInfo> runnables) {
    this.parent = null;
    this.thread = currentThread;
    this.type = DporInstructionMark.YES;
    this.choices = new ArrayList<ThreadInfo>();
    this.choiceIndex = -1; //start at -1 because advance is called first before choices are requested
    this.mustReplay = false;
    this.runnables = runnables;
    this.id = count++;
    this.children = new HashSet();
    this.inst = null;
  }

  public DporNode(DporNode parent, ThreadInfo currentThread, DporInstructionMark type, List<ThreadInfo> runnables, Instruction inst) {
    setParent(parent);
    this.type = type;
    this.thread = currentThread;
    this.choices = new ArrayList<ThreadInfo>();
    this.choiceIndex = -1;
    this.mustReplay = false;
    this.runnables = runnables;
    this.id = count++;
    this.children = new HashSet();
    this.inst = inst;
  }

  public DporNode getParent() { return parent; }
  public void setParent(DporNode parent) {
    if(this.parent != null) { //if the parrent is being changed (I don't think this happens but just in case)
      this.parent.children.remove(this); //remove myself from old parents children
    }
    this.parent = parent; // set the parent
    if (parent != null) {
      this.parent.children.add(this); //add myself to the parents children
    }
  }

  //used by DependenceRelation to help determine dependence
  public Object getDependenceInfo() { return dependeceInfo; }
  public void putDependenceInfo(Object dependeceInfo) {
    this.dependeceInfo = dependeceInfo;
  }

  //used by DporEngine to treat different types of nodes differently
  public DporInstructionMark getType() { return type; }
  public void setType(DporInstructionMark type) {
    this.type = type;
  }

  public ThreadInfo getThread() {
    return thread;
  }

  public List<ThreadInfo> getRunnables() {
    return runnables;
  }

  public boolean hasMoreChoices() {
    //if we were to advance would our index still be in our choices?
    return choiceIndex + 1 < choices.size();
  }
  public ThreadInfo getNextChoice() {
    return getChoice(choiceIndex); //get the current choice
  }
  public ThreadInfo getChoice(int index) {
    if(index < 0 || index >= choices.size()) return null;
    return choices.get(index); //get the choice at the given valid index
  }
  public boolean addChoice(ThreadInfo choice) {  
    if (!choices.contains(choice)) { //if we don't already have the choice
      choices.add(choice); //add it
      return true;
    }
    return false; //otherwise return we did not add it
  }
  public boolean addChoices(List<ThreadInfo> choices) {
    boolean returny = false;
    for (ThreadInfo choice : choices) {
      if (!choices.contains(choice)) {
        choices.add(choice);
        returny = true;
      }
    }
    return returny;
  }
  public List<ThreadInfo> getChoices() {
    return choices;
  }
  public void setChoices(List<ThreadInfo> choices) {
    this.choices = choices;
  }

  public Set<DporNode> getChildren() {
    return children;
  }

  public Instruction getInstruction() {
    return inst;
  }
  
  @Override
  /** This string is what is used for the names of nodes in the emitted dot files
  */
  public String toString() {
    String line = "";
    if (inst != null) {
      line = "]Line[" + inst.getLineNumber() + "]";
    }
    return "Type[" + type + "]ID[" + id + "]Thread[" + thread.getId() + line; 
/*
    StringBuilder sb = new StringBuilder();
    sb.append("Type[" + type + "]ID(" + id + ")ThreadChoices{");
    boolean comma = false;
    for (ThreadInfo choice : choices) {
      if(!comma) comma = true;
      else sb.append(",");
      sb.append(choice.getId());
    }
    sb.append("}");
    sb.append("ThreadRunnables{");
    comma = false;
    for (ThreadInfo choice : runnables) {
      if(!comma) comma = true;
      else sb.append(",");
      sb.append(choice.getId());
    }
    sb.append("}");
    sb.append("CurrentThread{" + thread.getId() + "}");
    if(inst != null) {
      sb.append(" line " + inst.getLineNumber());
    }
    return sb.toString();
*/
  }
}
