package dporengine.graph;

import java.util.Map;
import java.util.HashMap;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Instruction;
import dporengine.DporInstructionMark;
import dporengine.DporEngine;
import java.util.Arrays;
import java.util.List;

import java.lang.StringBuilder;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;


public class DporGraph {
  private DporNode root;
  private DporNode currentNode;
  private DporEngine owner;
  
  public DporGraph(DporEngine owner) {
    this.owner = owner;
  }
  public DporNode start(VM vm, ThreadInfo startThread) {
    this.root = createNode(vm, null, startThread, DporInstructionMark.REPLAY, null);
    this.currentNode = root;
    return root;
  }
  public void setCurrentNode(DporNode node) {
    this.currentNode = node;
  }
  public DporNode getCurrentNode() {
    return currentNode;
  }

  /** if mark is not NO this creates a new node of the appropriate type
  */
  public void update(VM vm, ThreadInfo currentThread, DporInstructionMark mark, Instruction instruction) {
    if (mark == DporInstructionMark.YES || mark == DporInstructionMark.MAYBE || mark == DporInstructionMark.REPLAY) {
      DporNode child = createNode(vm, currentNode, currentThread, mark, instruction); //use current node as parent
      currentNode = child; //update current node to be the newly created node
    }
  }

  /** helper function to create new nodes
  */
  private DporNode createNode(VM vm, DporNode parent, ThreadInfo currentThread, DporInstructionMark type, Instruction inst) {
      List<ThreadInfo> runnables = Arrays.asList(vm.getThreadList().getTimeoutRunnables());
      DporNode child = new DporNode(parent, currentThread, type, runnables, inst);
      child.addChoice(currentThread); //add the current thread as it's first choice
      owner.onCreateNode(parent, child); //call back so dependence info can be added
      return child;
  }

  public void emmitDot(String filename) {
    try {
      BufferedWriter writer = new BufferedWriter(new FileWriter("dots/" + filename + ".dot"));
      writer.write("digraph { \n");
      appendToDot(root, writer); //recursively add nodes to dot file
      writer.append("}");
      writer.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void appendToDot(DporNode node, BufferedWriter writer) throws IOException {
    if (node == null) return;
    writer.append(String.format("\"%s\"\n", node)); //add the node to the file
    for (DporNode child : node.getChildren()) {
      appendToDot(child, writer); //add the child and it's children to the file
      writer.append(String.format("\"%s\" -> \"%s\";\n", node, child)); //connect this node with it's child
    }
  }
  
}
