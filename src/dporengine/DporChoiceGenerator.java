package dporengine;

import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.ChoiceGeneratorBase;
import gov.nasa.jpf.vm.ThreadChoiceGenerator;
import gov.nasa.jpf.vm.ThreadInfo;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;

import dporengine.graph.DporNode;

/** A choice generator that delegates decisions on which thread should run next and if there are more choices
*   to an "oracle" DporEngine
*/
public class DporChoiceGenerator extends ChoiceGeneratorBase<ThreadInfo> implements ThreadChoiceGenerator {

  private final DporEngine engine; //the object that we delegate decisions to
  private final DporNode node; //the node in the DporGraph that was created for this choice generator

  protected DporChoiceGenerator(String id, DporEngine engine, DporNode node) {
    super(id);
    this.engine = engine; //save the owner DporEngine
    this.node = node; //save the node that was created for us
  }

  @Override
    /** Asks the DporEngine to get the choice at the given index
        It seems most of the time this is never called
    */
    public ThreadInfo getChoice(int idx) {
      return engine.getChoice(node, idx);
    }

  @Override
    /** Gives a simple toString that identifies it as a DporCG
    */
    public String toString() {
      return "DporCG" + this.id;
    }

  @Override
    /** This is currently not implemented
    *   It seems most of the time this is never called.
    */
    public void reset () {
      isDone = false;
      throw new UnsupportedOperationException("Not Implemented");
    }

  @Override
    /** Asks the DporEngine to that the next choice
    *   This is the most common method for retrieving choices from CGs
    */
    public ThreadInfo getNextChoice () {
      return engine.getNextChoice(node);
    }

  @Override
    /** Asks the DporEngine to check if there are more choices
        Called each time before advance and getNextChoice;
    */
    public boolean hasMoreChoices () {
      return engine.hasMoreChoices(node);
    }


  /**
   * this has to handle timeouts, which we do with temporary thread status
   * changes (i.e. the TIMEOUT_WAITING threads are in our list of choices, but
   * only change their status to TIMEDOUT when they are picked as the next choice)
   *
   * <2do> this should be in SystemState.nextSuccessor - there might be
   * other ThreadChoiceGenerators, and we should handle this consistently
   */
  @Override
    public void advance () {    
      engine.advance(node); //this handles state reset? Not currently but perhaps
    }

  @Override
    public int getTotalNumberOfChoices () {
      throw new UnsupportedOperationException("Not Implemented");
    }

  @Override
    public int getProcessedNumberOfChoices () {
      return node.choiceIndex + 1;
    }

  public Object getNextChoiceObject () {
    return getNextChoice();
  }

  @Override
    public boolean supportsReordering() {
      return false;
    }

  @Override
    public ThreadChoiceGenerator reorder (Comparator<ThreadInfo> comparator) {
      throw new UnsupportedOperationException("Not Implemented");
    }

  @Override
    public void printOn (PrintWriter pw) {
      pw.print(getClass().getName());
      pw.append("[id=\"");
      pw.append(id);
      pw.append('"');

      pw.append(",isCascaded:");
      pw.append(Boolean.toString(isCascaded));
    }

  @Override
    public DporChoiceGenerator randomize () {
      throw new UnsupportedOperationException("Not Implemented");
    }

  @Override
    public boolean contains (ThreadInfo ti) {
      throw new UnsupportedOperationException("Not Implemented");
    }

  @Override
    public Class<ThreadInfo> getChoiceType() {
      return ThreadInfo.class;
    }

  @Override
    /* Not sure what this boolean does. I think it makes the choice generator work to context switch 
    *  threads rather then change local and global variables
    */
    public boolean isSchedulingPoint() {
      return true;
    }
}

