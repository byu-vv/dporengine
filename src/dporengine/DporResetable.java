package dporengine;

import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Instruction;

/** Interface for objects that we would like to register with DporEngine to reset to stay syncronized with search
*/
public interface DporResetable {
  public Object getImmutableState();
  public void resetState(Object state);
}
