package dporengine;

//enum for marking whether instruction need to be considered for rescheduling
public enum DporInstructionMark { YES, NO, MAYBE, REPLAY};
