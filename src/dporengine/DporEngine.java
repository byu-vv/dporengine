package dporengine;

import gov.nasa.jpf.vm.DelegatingScheduler;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.jvm.ClassFile;
import gov.nasa.jpf.report.Publisher;
import gov.nasa.jpf.report.PublisherExtension;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.search.SearchListener;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.VMListener;
import gov.nasa.jpf.vm.ApplicationContext;
import gov.nasa.jpf.vm.DelegatingScheduler;
import gov.nasa.jpf.jvm.bytecode.InstanceInvocation;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.search.Search;

import dporengine.graph.DporGraph;
import dporengine.graph.DporNode;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

/** This is the main class for the DporEngine, It is an oracle that determines which threads need to be run for each
*   choice generator. It uses algorithms by Flanagan and Godefroid as well as Yang et al, to do this
*/
public class DporEngine extends DporScheduler implements VMListener {
  //used to determine if a given instruction should be considered for rescheduling
  DporInstructionMarker instructionMarker;
  //used to determine if two nodes in the DporGraph are dependent and must be reordered to observe required behavior
  DporDependenceRelation dependenceRelation;
  //The DporGrpah will have several "Replay" nodes. 
  //These are dummy choice generators to create points from which execution can be replayed in order to insert choice generators in the correct places
  // This is the node that need to replay from if we detect this case
  DporNode currentReplay;
  // A graph that is very similar to the jpf-state-space graph.
  // However this graph will contain extra "maybe" nodes.
  // This is where the instruction marker has indicated that rescheduling may be necessary but might not be.
  // To reduce the needed state space we do not insert a ChoiceGenerator unless it is confirmed to be needed. 
  // In that case we use the replay point to re-execute a portion of the program in order to insert the needed choice generators. 
  DporGraph graph;
  //needed for various info such as file names and search progress
  VM vm;
  //used to assign new emitted dot files a unique id.
  int count = 0;

  //It is convinent for the user to not need to worry about the process of model checking
  // this can be accomplished by implementing a method of saving and reseting to states at model checking points
  // this removes the burden of tracking the model checking. To the user it as if there is only 1 execution.
  // this will keep track any number of objects for a given state to reset their states.
  private Map<DporNode, Map<DporResetable, Object>> userStateMap;
  private Set<DporResetable> resetables;


  public DporEngine(Config config) {
    syncPolicy = new DporSyncPolicy(config); //set the sync policy from the super class DporScheduler
    sharednessPolicy = new DporSharednessPolicy(config); //set the sharednessPolicy
    instructionMarker = null;
    dependenceRelation = null;
    if (config.getString("dporengine.InstructionMarker.class") != null) {
      //lookup from config the class that will be used as the instruction marker
      instructionMarker = config.getEssentialInstance("dporengine.InstructionMarker.class", DporInstructionMarker.class);
      if (config.getString("dporengine.InstructionMarker.class").equals(config.getString("dporengine.DependenceRelation.class"))) { //if using same class for both
        dependenceRelation = (DporDependenceRelation) instructionMarker;
      }
      else if (config.getString("dporengine.DependenceRelation.class") != null) {
        //otherwise load the dependence relation class
        dependenceRelation = config.getEssentialInstance("dporengine.DependenceRelation.class", DporDependenceRelation.class);
      }
    }
    else if (config.getString("dporengine.DependenceRelation.class") != null) {
        dependenceRelation = config.getEssentialInstance("dporengine.DependenceRelation.class", DporDependenceRelation.class);
    }
    //if the instruction marker and the dependence relation will use the same class keep the first instance
    //start a new graph
    this.graph = new DporGraph(this);
    this.resetables = new HashSet();
    this.userStateMap = new HashMap();
  }

  //Delegating Scheduler Overrides

  @Override
    public void initialize(VM vm, ApplicationContext appCtx) {
      super.initialize(vm, appCtx);
      this.vm = vm;
      //add this as a vm listener to get access to the executeInstruction callback
      vm.addListener(this);
    }
  @Override
    public void setRootCG() {
      ThreadInfo startThread = vm.getCurrentThread();
      DporNode root = graph.start(vm, startThread); //start the graph. This creates a first replay node
      currentReplay = root; //set the root as the current replay
      DporChoiceGenerator cg = new DporChoiceGenerator("DPOR_ENGINE", this, root); //create a new choice generator for the first node
      colectStates(root);
      vm.getSystemState().setNextChoiceGenerator(cg); //set the first choice generator as requested
    }

  //the VMListener Interface
  @Override
    public void executeInstruction(VM vm, ThreadInfo currentThread, Instruction instructionToExecute) {
    }

    public void instructionExecuted(VM vm, ThreadInfo currentThread, Instruction nextInst, Instruction lastInst) {
      if (currentThread.isFirstStepInsn()) {
        //System.out.println("Ignoring second execution of " + nextInst);
        return;
      }
      if (nextInst == null) return;
      //ask marker to determine if this instruction may need to be reordered
      DporInstructionMark iMark = instructionMarker.mark(vm, currentThread, nextInst); 
      DporNode currentNode = graph.getCurrentNode(); //the node that will have new info if not NO
      graph.update(vm, currentThread, iMark, nextInst); //creates new blank node if not NO
      DporNode newNode = graph.getCurrentNode(); //the node that will have new info if not NO
      //if (iMark != DporInstructionMark.NO) System.out.println("Exec" + nextInst.getLineNumber());
      if (iMark == DporInstructionMark.YES) { //if this for sure needs to be rescheduled
        addChoiceGenerator(vm, newNode); //add a new choice generator for it
        addChoicesToDependent(currentNode); //and execute the Dpor Algorithm in Flanagan and Godefroid
      }
      if (iMark == DporInstructionMark.MAYBE) { //this instruction may need to be rescheduled but it cannot be determined at this time
        addChoicesToDependent(currentNode); //should we execute Dpor Algorithm in this case? Or wait until confirmed yes?
      }
      else if (iMark == DporInstructionMark.REPLAY) { //we should create a replay point now to speed up the maybe promotion process
        addChoiceGenerator(vm, newNode);
        currentReplay = newNode;
      }
    }

  @Override
    public void threadTerminated(VM vm, ThreadInfo terminatedThread) {
      addChoicesToDependent(graph.getCurrentNode());
      graph.emmitDot(vm.getSUTName() + count++); //when a thread terminates emit a graph
    }
  /* These methods are required for the VM Listener interface but we do not use them
  */
  @Override
    public void vmInitialized(VM vm) {}
  @Override
    public void threadStarted(VM vm, ThreadInfo startedThread) {}
  @Override
    public void threadWaiting (VM vm, ThreadInfo waitingThread) {}
  @Override
    public void threadNotified (VM vm, ThreadInfo notifiedThread) {}
  @Override
    public void threadInterrupted (VM vm, ThreadInfo interruptedThread) {}
  @Override
    public void threadScheduled (VM vm, ThreadInfo scheduledThread) {}
  @Override
    public void threadBlocked (VM vm, ThreadInfo blockedThread, ElementInfo lock) {}
  @Override
    public void loadClass (VM vm, ClassFile cf) {}
  @Override
    public void classLoaded(VM vm, ClassInfo loadedClass) {}
  @Override
    public void objectCreated(VM vm, ThreadInfo currentThread, ElementInfo newObject) {}
  @Override
    public void objectReleased(VM vm, ThreadInfo currentThread, ElementInfo releasedObject) {}
  @Override
    public void objectLocked (VM vm, ThreadInfo currentThread, ElementInfo lockedObject) {}
  @Override
    public void objectUnlocked (VM vm, ThreadInfo currentThread, ElementInfo unlockedObject) {}
  @Override
    public void objectWait (VM vm, ThreadInfo currentThread, ElementInfo waitingObject) {}
  @Override
    public void objectNotify (VM vm, ThreadInfo currentThread, ElementInfo notifyingObject) {}
  @Override
    public void objectNotifyAll (VM vm, ThreadInfo currentThread, ElementInfo notifyingObject) {}
  @Override
    public void objectExposed (VM vm, ThreadInfo currentThread, ElementInfo fieldOwnerObject, ElementInfo exposedObject) {}
  @Override
    public void objectShared (VM vm, ThreadInfo currentThread, ElementInfo sharedObject) {}
  @Override
    public void gcBegin(VM vm) {}
  @Override
    public void gcEnd(VM vm) {}
  @Override
    public void exceptionThrown(VM vm, ThreadInfo currentThread, ElementInfo thrownException) {}
  @Override
    public void exceptionBailout(VM vm, ThreadInfo currentThread) {}
  @Override
    public void exceptionHandled(VM vm, ThreadInfo currentThread) {}
  @Override
    public void choiceGeneratorRegistered (VM vm, ChoiceGenerator<?> nextCG, ThreadInfo currentThread, Instruction executedInstruction) {}
  @Override
    public void choiceGeneratorSet (VM vm, ChoiceGenerator<?> newCG) {}
  @Override
    public void choiceGeneratorAdvanced (VM vm, ChoiceGenerator<?> currentCG) {}
  @Override
    public void choiceGeneratorProcessed (VM vm, ChoiceGenerator<?> processedCG) {}
  @Override
    public void methodEntered (VM vm, ThreadInfo currentThread, MethodInfo enteredMethod) {}
  @Override
    public void methodExited (VM vm, ThreadInfo currentThread, MethodInfo exitedMethod) {}

  // DporEngine Graph Control

  public boolean hasMoreChoices(DporNode node) {
    if(node.getType() == DporInstructionMark.REPLAY) {
      return checkMustReplay(node) || node.choiceIndex == -1;
    }
    return node.hasMoreChoices(); //otherwise check to see if the node has more choices in it's backtrack set
  }

  public ThreadInfo getChoice(DporNode node, int index) {
    if(node.getType() == DporInstructionMark.REPLAY) { //if it's a replay throw an exception
      throw new IllegalArgumentException("Not Implemented for REPLAY");
    }
    return node.getChoice(index); //request next choice from node
  }
  public ThreadInfo getNextChoice(DporNode node) {
    if(graph.getCurrentNode() != node) {
      //System.out.println("CurrentNode changed to " + node);
      graph.setCurrentNode(node); //when we get a choice from a node it becomes current;
    }
    if(node.getType() == DporInstructionMark.REPLAY) { //if we are replaying a section
      if (node.choiceIndex == 0) { //if this is the first choice for the replay it's not a replay its first execution
        if(node.mustReplay == true) throw new RuntimeException("First run of Replay node had mustReplay flag set");
      }
      else {
        //this is only called after hasMoreChoices returns true
        node.mustReplay = false; //unset the flag
        System.out.println("Replay called"); //print a message for debug
      }
      return node.getThread(); //return the same thread again
    }
    return node.getNextChoice(); //otherwise get the next choice from the node
  }

  public void advance(DporNode node) {
    node.choiceIndex++; //advances the node to the next choice
    resetStates(node);
  }

  public void registerResetable(DporResetable resetable) {
    resetables.add(resetable);
  }
  private void resetStates(DporNode node) {
    Map<DporResetable, Object> resetMap = userStateMap.get(node);
    for (DporResetable resetable : resetMap.keySet()) {
      resetable.resetState(resetMap.get(resetable));
    }
  }
  private void colectStates(DporNode node) {
    Map<DporResetable, Object> resetMap = new HashMap();
    for (DporResetable resetable : resetables) {
      resetMap.put(resetable, resetable.getImmutableState()); 
    }
    userStateMap.put(node, resetMap);
  }

  private boolean checkMustReplay(DporNode node) {
    if (node.mustReplay == false) return false;
    DporNode parent = node;
    while (parent.getParent() != null) {
      parent = parent.getParent();
      if (parent.getType() == DporInstructionMark.REPLAY && parent.mustReplay == true) {
        return false;
      }
    }
    return true;
  }

  //convenience method to help keep CGs consistent
  private void addChoiceGenerator(VM vm, DporNode node) {
    DporChoiceGenerator cg = new DporChoiceGenerator("DPOR_ENGINE", this, node);
    colectStates(node);
    vm.getSystemState().setNextChoiceGenerator(cg);
  }

  //adds a choice to a node if possible returns true if it was added
  private boolean addChoice(DporNode node, ThreadInfo choice) {
    if(getRunnables(node).contains(choice)) { //if the choice is runnable from this point
      boolean choiceAdded = node.addChoice(choice); //add the choice. (If it was already in the backtrack set returns false;
      if(choiceAdded && node.getType() == DporInstructionMark.MAYBE) { //if we added a choice to a maybe it must be promoted to YES
        setReplayFlag(node);
        //NOTE: on the replay we assume that the Instruction marker will return YES for the re-executed instruction
      }
      return choiceAdded;
    }
    else return false; //if we cant run the choice we can't add it
  }

  private void setReplayFlag(DporNode node) {
    DporNode parent = node.getParent();
    while (parent.getType() != DporInstructionMark.REPLAY) {
      parent = parent.getParent();
    }
    parent.mustReplay = true; //set the flag to replay this section
  }

  // adds all runnable threads. This is a conservative decision when which thread must run next cannot be determined
  private void addAllChoices(DporNode node) {
    List<ThreadInfo> runnables = getRunnables(node); 
    //System.out.println("All Runnables: " + runnables);
    node.addChoices(runnables);
  }

  private List<ThreadInfo> getRunnables(DporNode node) {
    return node.getRunnables();
  }

  private void addChoicesToDependent(DporNode currentNode) {
    DporNode oldNode = currentNode.getParent();
    while (oldNode != null) { //for every node before this one, working backwards
      if (oldNode.getType() != DporInstructionMark.REPLAY && dependenceRelation.dependent(oldNode, currentNode)) { //if we need to enumerate both orders between the new and old scheduling points
        addScheduleFor(oldNode, currentNode);
        //break; //we only need to find the most recent dependent scheduling point
      }
      oldNode = oldNode.getParent(); //continue up the graph
    }
  }

  private void addScheduleFor(DporNode oldNode, DporNode currentNode) {
        if (!addChoice(oldNode, currentNode.getThread())) { //try to enable running the new point dirrectly just before executing this point
          addAllChoices(oldNode); //if we can't we need to add all the threads to be sure we get what we need
        }
  }

  public void onCreateNode(DporNode parent, DporNode child) {
    dependenceRelation.onCreateNode(parent, child); //call back to the dependence relation to give it a chance to update its data structures and insert DependenceInfo
  }

  public static void registerWithJPF(JPF jpf, Object object) {
    VM vm = jpf.getVM();
    Search search = jpf.getSearch();
    DporEngine dporEngine = (DporEngine) vm.getScheduler();
    if (object instanceof DporResetable) dporEngine.registerResetable((DporResetable) object);
    if (object instanceof DporDependenceRelation) dporEngine.dependenceRelation = (DporDependenceRelation) object;
    if (object instanceof DporInstructionMarker) dporEngine.instructionMarker = (DporInstructionMarker) object;
    //if (object instanceof VMListener && !vm.hasListenerOfType(object.getClass())) vm.addListener((VMListener) object);
    //if (object instanceof SearchListener) search.addListener((SearchListener) object);
  }

}
