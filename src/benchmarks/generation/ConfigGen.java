package benchmarks.generation;

import benchmarks.misc.configurable.Block;
import benchmarks.misc.configurable.Access;

// A Configurable Generator this generates a benchmark with the provided properties
public class ConfigGen {
    private boolean racey = true;
    private boolean readWriteRace = false; //default write-write Race
    private int raceDepthFirst = 10; //how many accesses before inserting the first racey write
    private int raceDepthSecond = 30; //how many accesses before inserting the racey read/write
    private int numThreads = 5;
    private int accPerNode = 0;
    private int commonFactor = 5;
    private boolean isolated = false;
    private int isolationFactor = 1;
    private int dependentFactor = 2;
    private int subgraphDepth = 1;

    private Access raceWrite = null;
    private Access isoWrite = null;
    private int isoCount = 0;

    private int filesGenerated = 0;

    public static void main(String[] args) {
        ConfigGen cg = new ConfigGen();
        cg.racey = false;
        while (true) { //try racey true and false
            cg.readWriteRace = false;
            while (true) { //try readWrite true and false
                for (cg.numThreads = 1; cg.numThreads <= 15625; cg.numThreads *= 5) { // try 1, 5, 25, 125, and 625 threads 
                    for (cg.accPerNode = 1; cg.accPerNode <= 125; cg.accPerNode *= 5) { // try 1, 5, 25, 125, and 625 threads 
                        for (cg.commonFactor = 1; cg.commonFactor <= cg.accPerNode; cg.commonFactor *= 5) { // try different amounts of accesses to common variables
                            cg.run();
                            /*
                            cg.isolated = false;
                            while (true) { //try isolated true and false
                                for (cg.dependentFactor = 1; cg.dependentFactor <= cg.numThreads; cg.dependentFactor *= 5) { //for variang amounts of dependence
                                    cg.run();
                                    if(!cg.isolated) break; //if not isolated don't try all dependent factors
                                }
                                if(cg.isolated) break; 
                                cg.isolated = true;
                            }
                            */
                        }
                    }
                }
                if(cg.readWriteRace || !cg.racey) break; //if there is no race don't try both ways
                cg.readWriteRace = true;
            }
            if(cg.racey) break; 
            cg.racey = true;
        }
    }

    public ConfigGen() {

    }

    public ConfigGen(boolean racey, int numThreads, int accPerNode, int commonFactor, boolean isolated, int dependentFactor, int subgraphDepth) {
        this.racey = racey;
        this.numThreads = numThreads;
        this.accPerNode = accPerNode;
        this.commonFactor = commonFactor;
        this.isolated = isolated;
        this.dependentFactor = dependentFactor;
        this.subgraphDepth = subgraphDepth;
    }

    public void run() {
        //reset state
        raceWrite = null;
        isoWrite = null;
        isoCount = 0;

        Block main = Block.create();
        for (int t = 0; t < numThreads; t++) { //create each thread
            Block next = Block.create();
            main.async(next);
            addAccesses(next);//node in spawned thread
            if(racey) addRace(next);
            if(subgraphDepth > 1) {
                makeSubGraph(next);
            }
            if(isolated) addIsolated(next);
            addAccesses(next);
        }
        if(racey) addRace(main);
        Access.resetVariableId();
        generate(main);
    }


    private void generate(Block toGen) {
        String name = "Gen" + "Race" + racey + "_RaceWrite" + readWriteRace + "_Numthreads" + numThreads + "_Accpernode" + accPerNode + "_CommonFactor" + commonFactor + "_Isolated" + isolated + "_DependentFactor" + dependentFactor;
        //String name = "Gen" + "." + racey + "." + readWriteRace + "." + numThreads + "." + accPerNode + "." + commonFactor + "." + isolated + "." + dependentFactor;
        //String name = "Gen" + "RACE" + racey + filesGenerated++;
        toGen.emmit(name);
        System.out.println("Generated " + name + ".java");
    }

    private void addAccesses(Block thread) {
        for(int a = 0; a < accPerNode; a += commonFactor) {
            if(a + commonFactor > accPerNode) {
                thread.multiRead(accPerNode - a);
                thread.multiWrite(accPerNode - a);
            }
            thread.multiRead(commonFactor);
            thread.multiWrite(commonFactor);
        }
    }


    private void addRace(Block thread) {
        if(raceWrite == null) {
            raceWrite = thread.write().get();
        }
        else {
            if(readWriteRace) {
                thread.read(raceWrite);
            }
            else {
                thread.write(raceWrite);
            }
        }
    }

    private void addIsolated(Block thread) {
        Block isoNode = Block.create();
        thread.isolated(isoNode);
        if(isoCount++ % dependentFactor == 0) { //if this one should be depenent
            if (isoWrite == null) {
                isoWrite = isoNode.write().get(); //write a new common variable
            }
            else {
                isoNode.write(isoWrite); //write to the common variable
            }
        }
        else {
            for(int a = 0; a < accPerNode; a++) {
                isoNode.read();
                isoNode.write();
            }
        }
    }

    private void makeSubGraph(Block thread) {
        for(int d = 0; d < subgraphDepth; d++) {
            //TODO flesh this
        }
    }
}
