package benchmarks;

import static permission.PermissionChecks.*;
/*
   A translation of: doall1-orig-no.c
   Originally produced at the Lawrence Livermore National Laboratory
   Written by Chunhua Liao, Pei-Hung Lin, Joshua Asplund,
   Markus Schordan, and Ian Karlin

   Translated at Brigham Young University by Kyle Storey

 */

import static edu.rice.hj.Module2.launchHabaneroApp;
import static edu.rice.hj.Module2.forAll;
import static edu.rice.hj.Module2.isolated;

import edu.rice.hj.api.*;

public class DisjointIsolated {
  static final int loops = 6;
  static final int items = 6;
  static int[] a = new int[items];
  static int[] iso = new int[loops];
  static int count = 0;
  public static void main(String[] args) throws SuspendableException {
    launchHabaneroApp(new HjSuspendable() {

      @Override
      public void run() throws SuspendableException {

        forAll(0, loops - 1, new HjSuspendingProcedure<Integer>() {
          public void apply(Integer i) throws SuspendableException {
            final int index = i;
            isolated(new HjRunnable() {
              public void run() {
                System.out.println("Isolated " + index + " accesses " + index % items);
                acquireW(a, index);
                a[index % items] = a[index % items] + 1;
                releaseW(a, index);
                iso[count++] = index;
                System.out.print("[");
                if (count == loops) {
                    for (int j = 0; j < count; j++) {
                        System.out.print(iso[j] + ", ");
                    }
                    System.out.println("\b\b]");
                }
              }
            });
//            isolated(new HjRunnable() {
//              public void run() {
//                System.out.println("Isolated skip " + index);
//                int throwAway = a[(index) % items];
//              }
//            });
//            if (index == 0) {
//              int throwAway = a[(index) % items];
//              System.out.println("Throwaway is:" + throwAway);
//            }
          }
        });
      }

    });
  }
}
