package benchmarks.nonisolated;

import benchmarks.misc.configurable.Block;

public class ConfigurableManyThreadsNo {
  public static void main(String[] args) {
    Block main = Block.create();
    Block previous = main;
    for(int i = 0; i < 5000; i++) {
      Block next = Block.create();
      previous.async(next);
      for(int j = 0; j < 100; j++) {
        next.write().read();
      }
      previous = next;
    }
    main.run();
  }
}
