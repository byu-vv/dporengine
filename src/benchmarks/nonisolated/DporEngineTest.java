package benchmarks.nonisolated;

import static edu.rice.hj.Module2.launchHabaneroApp;
import static edu.rice.hj.Module2.async;
import static edu.rice.hj.Module2.finish;
import edu.rice.hj.api.*;

public class DporEngineTest {
  public static void main(String[] args) throws SuspendableException {
    launchHabaneroApp(new HjSuspendable() {

        Integer a = 1;
        Integer b = 1;
        Integer c = 1;
        Integer d = 1;
        Integer e = 1;
        Integer f = 1;
        Integer g = 1;
        Integer h = 1;

        @Override
        public void run() throws SuspendableException {
          finish ( new HjSuspendable() {
            public void run() {
              async(new HjRunnable() {
                public void run() {
                  a = 2;
                }
              });
              async(new HjRunnable() {
                public void run() {
                  b = a;
                }
              });
              async(new HjRunnable() {
                public void run() {
                  c =  b;
                }
              });
              a = 1;
              b = 1;
              c = 1;
              d = 1;
              e = 1;
              f = 1;
              g = 1;
              h = 1;
            }
          });
          System.out.println("a is " + a);
          System.out.println("b is " + b);
          System.out.println("c is " + c);
          System.out.println("d is " + d);
          System.out.println("e is " + e);
          System.out.println("f is " + f);
          System.out.println("g is " + g);
          System.out.println("h is " + h);
        }
    });
  }
}
