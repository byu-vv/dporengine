package benchmarks.nonisolated;

import static edu.rice.hj.Module2.launchHabaneroApp;
import static edu.rice.hj.Module2.async;
import static edu.rice.hj.Module2.finish;
import edu.rice.hj.api.*;

public class LessSimple {
  public static void main(String[] args) throws SuspendableException {
    launchHabaneroApp(new HjSuspendable() {

        Integer i = 1;

        @Override
        public void run() throws SuspendableException {
          finish ( new HjSuspendable() {
            public void run() {
              async(new HjRunnable() {
                public void run() {
                  int temp = i + 1;
                  i = temp;
                }
              });
              int temp = i * 2;
              i = temp;
            }
          });
          System.out.println("i is " + i);
        }
    });
  }
}
