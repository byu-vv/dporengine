package benchmarks.nonisolated;

import static edu.rice.hj.Module2.launchHabaneroApp;
import static edu.rice.hj.Module2.async;
import static edu.rice.hj.Module2.finish;
import edu.rice.hj.api.*;

public class MaybeExample {
  public static void main(String[] args) throws SuspendableException {
    launchHabaneroApp(new HjSuspendable() {

        @Override
        public void run() throws SuspendableException {
          finish ( new HjSuspendable() {
            Integer i = 1;
            public void run() {
              async(new HjRunnable() {
                public void run() {
                  i = 1;
                }
              });
              i = 2;
            }
          });
          //System.out.println("i is " + i);
        }
    });
  }
}
