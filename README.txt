This repository will hold the code for Kyle Storey's GSoC project "Dynamic Partial Order Reduction Engine connected with Symbolic Data Race Detection for Habanero Java."

For examples try running one of these commands:

./gradlew clean build run -Pbenchmark=dataraceisolatesimple -Pconfig=testDpor

./gradlew clean build run -Pbenchmark=dataraceisolatesimple -Pconfig=testIsoDpor

./gradlew clean build run -Pbenchmark=dataraceisolatesimple -Pdetector=hb

./gradlew clean build run -Pbenchmark=dataraceisolatesimple -Pdetector=zipper


